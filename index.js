import {AppRegistry} from 'react-native';
import 'react-native-gesture-handler'
import { gestureHandlerRootHOC } from 'react-native-gesture-handler';

// import App from './src/App';
import App from './App';

import {name as appName} from './app.json';
import {YellowBox} from 'react-native';

// 
AppRegistry.registerComponent(appName, () => gestureHandlerRootHOC(App));

YellowBox.ignoreWarnings([
          '-[RCTRootView cancelTouches]` is deprecated and will be deleted soon.',
          'Unrecognized WebSocket connection option(s) `agent`, `perMessageDeflate`, `pfx`, `key`, `passphrase`, `cert`, `ca`, `ciphers`, `rejectUnauthorized`. Did you mean to put these under `headers`?',
          'Unable to find module for UIManager'
        ]);