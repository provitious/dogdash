import React, { Component, Fragment } from 'react';
import { StyleSheet, Dimensions, YellowBox, Text, View, Button, TextInput, Image, ScrollView, TouchableOpacity, Alert, SafeAreaView, PixelRatio } from 'react-native';
import {KeyboardAvoidingView, Keyboard, TouchableWithoutFeedback} from 'react-native';
import userDefaults from 'react-native-user-defaults'

import Constant from '../CommonClasses/Constant';
import CommonUtilsObj from '../CommonClasses/CommonUtils';
import CustomStyles from '../CommonClasses/CustomStyles';
import Message from '../CommonClasses/Message';
import APIManagerObj from '../CommonClasses/APIManager';

import NetInfo from "@react-native-community/netinfo";
import Toast from 'react-native-simple-toast';
import firebase from 'react-native-firebase';
import ProgressLoader from 'rn-progress-loader';
import App from '../../App';


const txtInputHeight = (((Dimensions.get('window').width) * 9.8) / 100);

class OTPVerification extends Component {
 
    constructor(props) {
        super(props);

        this.onFocus = this.onFocus.bind(this);
        this.confirmCode = this.confirmCode.bind(this);
        this.onSubmitTxt1 = this.onSubmitTxt1.bind(this);
        this.onSubmitTxt2 = this.onSubmitTxt2.bind(this);
        this.onSubmitTxt3 = this.onSubmitTxt3.bind(this);
        this.onSubmitTxt4 = this.onSubmitTxt4.bind(this);
        this.onSubmitTxt5 = this.onSubmitTxt5.bind(this);
        this.onSubmitTxt6 = this.onSubmitTxt6.bind(this);

        this.txt1Ref = this.updateRef.bind(this, 'txt1');
        this.txt2Ref = this.updateRef.bind(this, 'txt2');
        this.txt3Ref = this.updateRef.bind(this, 'txt3');
        this.txt4Ref = this.updateRef.bind(this, 'txt4');
        this.txt5Ref = this.updateRef.bind(this, 'txt5');
        this.txt6Ref = this.updateRef.bind(this, 'txt6');

        this.state = {
            txt1: '',
            txt2: '',
            txt3: '',
            txt4: '',
            txt5: '',
            txt6: '',
            connection_Status : "",
            user: null,
            message: '',
            codeInput: '',
            phoneNumber: '',
            confirmResult: null,
            visible: false,
            registerParams: '',
            resendCnt: 0,
        };

        YellowBox.ignoreWarnings([
          'Warning: componentWillMount is deprecated',
          'Warning: componentWillReceiveProps is deprecated',
          'Warning: componentWillUpdate is deprecated',
          'Warning: componentWillUpdate has been renamed, and is not recommended for use',
          'Warning: componentWillReceiveProps has been renamed, and is not recommended for use',
        ]);
    }

    componentDidMount() {

      const { navigation } = this.props;  
      this.setState ({
        phoneNumber: navigation.getParam('mobile', ''),
        registerParams: navigation.getParam('registerParams', ''),
      });

      // setTimeout(() => {
      //   const { phoneNumber } = this.state;
      //   this.setState({ message: 'Sending code ...' });

      //   firebase.auth().signInWithPhoneNumber(navigation.getParam('mobile', ''))
      //   .then(confirmResult => {
      //     this.setState({ confirmResult, message: 'Code has been sent!' });
      //     setTimeout(() => {
      //       Toast.showWithGravity("Code has been sent!", Toast.LONG, Toast.TOP)
      //     }, 200);
      //   })
      //   .catch(error => {
      //     this.setState({ message: `Sign In With Phone Number Error: ${error.message}` });
      //     setTimeout(() => {
      //       Toast.showWithGravity(`Sign In With Phone Number Error: ${error.message}`, Toast.LONG, Toast.TOP)
      //     }, 200);
      //   });
      // }, 200);

      NetInfo.isConnected.addEventListener(
          'connectionChange',
          this._handleConnectivityChange
      );
     
      NetInfo.isConnected.fetch().done((isConnected) => {

        if(isConnected == true)
        {
          this.setState({connection_Status : "Online"})
        }
        else
        {
          this.setState({connection_Status : "Offline"})
        }

      });

      this.unsubscribe = firebase.auth().onAuthStateChanged((user) => {
        if (user) {
          this.setState({ user: user.toJSON() });
        } else {
          // User has been signed out, reset the state
          // this.setState({
          //   user: null,
          //   message: '',
          //   codeInput: '',
          //   phoneNumber: '+44',
          //   confirmResult: null,
          // });
        }
      });
    }

    componentWillUnmount() {
      NetInfo.isConnected.removeEventListener(
          'connectionChange',
          this._handleConnectivityChange
      );
    }

    _handleConnectivityChange = (isConnected) => {

      if(isConnected == true)
        {
          this.setState({connection_Status : "Online"})
        }
        else
        {
          this.setState({connection_Status : "Offline"})
        }
    };

    managePasswordVisibility = () =>
    {
      this.setState({ hidePassword: !this.state.hidePassword });
    }

    onFocus() {
      let { errors = {} } = this.state;

      for (let name in errors) {
        let ref = this[name];

        if (ref && ref.isFocused()) {
          delete errors[name];
        }
      }

      this.setState({ errors });
    }

    updateRef(name, ref) {
      this[name] = ref;
    }

    onSubmitTxt1(text) {
      this.txt2.focus();
    }

    onSubmitTxt2(text) {
      this.txt3.focus();
    }

    onSubmitTxt3(text) {
      this.txt4.focus();
    }

    onSubmitTxt4(text) {
      this.txt5.focus();
    }

    onSubmitTxt5(text) {
      this.txt6.focus();
    }

    onSubmitTxt6(text) {
      this.txt6.blur();
    }

    confirmCode = () => {
      this.registerDriver();
      // let otp = this.state.txt1 + this.state.txt2 + this.state.txt3 + this.state.txt4 + this.state.txt5 + this.state.txt6;

      // if (otp == '') {
      //   Toast.showWithGravity('OTP should not be empty', Toast.LONG, Toast.TOP)
      // }
      // else {
      //   this.setState({
      //       visible: true
      //   });

      //   this.txt1.blur();
      //   this.txt2.blur();
      //   this.txt3.blur();
      //   this.txt4.blur();
      //   this.txt5.blur();
      //   this.txt6.blur();

      //   this.state.codeInput = this.state.txt1 + this.state.txt2 + this.state.txt3 + this.state.txt4 + this.state.txt5 + this.state.txt6;
      //   const { codeInput, confirmResult } = this.state;

      //   if (confirmResult && codeInput.length) {
      //     confirmResult.confirm(codeInput)
      //       .then((user) => {
      //         this.setState({
      //             visible: false,
      //             message: 'Code Confirmed!', 
      //         });
      //         console.log("Code Confirmed! " + this.state.registerParams);

      //         setTimeout(() => {
      //             Toast.showWithGravity('Code Confirmed!', Toast.LONG, Toast.TOP)
      //             this.registerDriver();
      //         }, 200);

      //       })
      //       .catch(error => {
      //         this.setState({
      //             visible: false
      //         });
      //         console.log('Code Confirm Error: ${error.message}');
      //         setTimeout(() => {
      //           // Toast.showWithGravity(`${error.message}`, Toast.LONG, Toast.TOP)

      //           Toast.showWithGravity('The SMS verification code is invalid', Toast.LONG, Toast.TOP)
      //         }, 200);
      //       });
      //   }
      //   else {
      //     this.setState({
      //         visible: false
      //     });

      //     setTimeout(() => {
      //       Toast.showWithGravity('The SMS verification code is invalid', Toast.LONG, Toast.TOP)
      //     }, 200);
      //   }
      // }
    };

    resendOTP() {

      console.log('here');
      console.log('this.state.phoneNumber ' + this.state.phoneNumber);

      if (this.state.resendCnt == 1) {
          Toast.showWithGravity('Your number will be block in next attempt', Toast.LONG, Toast.TOP)
      }

      this.setState({
          resendCnt: this.state.resendCnt + 1,
      });

      firebase.auth().signInWithPhoneNumber(this.state.phoneNumber)
        .then(confirmResult => {
          this.setState({ confirmResult, message: 'Code has been sent!' });
          console.log('Code has been sent!');
          setTimeout(() => {
            Toast.showWithGravity("Code has been sent!", Toast.LONG, Toast.TOP)
          }, 200);
        })
        .catch(error => {
          this.setState({ message: `Sign In With Phone Number Error: ${error.message}` });
          console.log(`Sign In With Phone Number Error: ${error.message}`);
          setTimeout(() => {
            Toast.showWithGravity(`Sign In With Phone Number Error: ${error.message}`, Toast.LONG, Toast.TOP)
          }, 200);
        });
    }

    fetchProfileData() {
      CommonUtilsObj.getLoggedUserDetails();
    }

    async registerDriver() {
      this.txt1.blur();
      this.txt2.blur();
      this.txt3.blur();
      this.txt4.blur();
      this.txt5.blur();
      this.txt6.blur();

      if (this.state.connection_Status == "Online") {
      
        this.setState({
            visible: true
        });

        let responseData = await APIManagerObj.callApiWithData(Constant.KSignUpURL, this.state.registerParams);
        console.log('responseText json.....' + JSON.stringify(responseData));

        if (responseData.status == Constant.KSuccess) {

          // if (Platform.OS==='android') {
          //     CommonUtilsObj.setLoggedUserDetails(JSON.stringify(responseData));
          // }
          // else {
          //     CommonUtilsObj.setLoggedUserDetails(responseData);
          // }
          this.setState({
              visible: false
          });
          
          // this.fetchProfileData();

          setTimeout(() => {
            Toast.showWithGravity(responseData.message, Toast.LONG, Toast.TOP)
            // CommonUtilsObj.setLoginState(Constant.KLogin);
            this.props.navigation.navigate('AddAccountScreen',{responseData: responseData})
          }, 200);
        }
        else {
          this.setState({
            visible: false,
          }); 
          setTimeout(() => {
            if(typeof(responseData.message) === 'string')
            {
                Toast.showWithGravity(responseData.message, Toast.LONG, Toast.TOP)
            }
            else if(typeof(responseData.error) === 'string')
            {
                Toast.showWithGravity(responseData.error, Toast.LONG, Toast.TOP)
            }
            else {
                if (responseData.error) {
                  Toast.showWithGravity(responseData.error.toString(), Toast.LONG, Toast.TOP)
                }
                else {
                  Toast.showWithGravity(Message.KAPIError, Toast.LONG, Toast.TOP)
                }
            }
          }, 200);
        }
      }
      else {
        Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
      }
    }
    
    render() {
        let { errors = {}, secureTextEntry, ...data } = this.state;
        return(
            <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>

            <View style={CustomStyles.container}>

                <View
                style={{backgroundColor: "#fff", justifyContent: 'center', alignItems: 'center'}}>

                <ProgressLoader
                  visible={this.state.visible}
                  isModal={true} isHUD={true}
                  hudColor={"#fff"}
                  height={200}
                  width={200}
                  color={"#8edf01"} />
                </View>

                <View style={CustomStyles.centerContainer}>
                <KeyboardAvoidingView style={CustomStyles.flexColunm} behavior="padding" enabled>

                  <View style={CustomStyles.flexColunm}> 

                    <Text style={[CustomStyles.titleText, {marginHorizontal: 20}]}>Verify your mobile</Text>
                    <Text style={[CustomStyles.descGrayText, {marginHorizontal: 20, marginTop: 40}]}>Enter your OTP code here</Text>
                    
                    <View style={{height: txtInputHeight, marginTop: 15, borderRadius: 5, flexDirection: 'row', justifyContent: 'center' }}>

                    <TextInput
                      style={CustomStyles.otpTextInput}
                      ref={this.txt1Ref}
                      keyboardType='phone-pad'
                      autoCapitalize='none'
                      autoCorrect={true}
                      enablesReturnKeyAutomatically={true}
                      onFocus={this.onFocus}
                      onChangeText={(txt1) => this.setState({txt1})}
                      onChangeText={(txt1) => {
                        console.log(txt1);

                        this.setState({txt1});
                        if (txt1 != '') {
                          this.txt2.focus();
                        }
                      }}
                      onKeyPress={({ nativeEvent }) => {
                          nativeEvent.key === 'Backspace' ? this.txt1.focus() : this.txt2.focus()
                        }}
                      onSubmitEditing={this.onSubmitEmail}
                      returnKeyType='next'
                      maxLength={1}
                    />

                    <TextInput
                      style={CustomStyles.otpTextInput}
                      ref={this.txt2Ref}
                      keyboardType='phone-pad'
                      autoCapitalize='none'
                      autoCorrect={true}
                      enablesReturnKeyAutomatically={true}
                      onFocus={this.onFocus}
                      onChangeText={(txt2) => {
                        this.setState({txt2});
                        if (txt2 != '') {
                          this.txt3.focus();
                        }
                      }
                      }
                      onKeyPress={({ nativeEvent }) => {
                          nativeEvent.key === 'Backspace' ? this.txt1.focus() : this.txt3.focus()
                        }}
                      onSubmitEditing={this.onSubmitEmail}
                      returnKeyType='next'
                      maxLength={1}
                    />

                    <TextInput
                      style={CustomStyles.otpTextInput}
                      ref={this.txt3Ref}
                      keyboardType='phone-pad'
                      autoCapitalize='none'
                      autoCorrect={true}
                      enablesReturnKeyAutomatically={true}
                      onFocus={this.onFocus}
                      onChangeText={(txt3) => {
                        this.setState({txt3});
                        if (txt3 != '') {
                          this.txt4.focus();
                        }
                      }}
                      onKeyPress={({ nativeEvent }) => {
                          nativeEvent.key === 'Backspace' ? this.txt2.focus() : this.txt4.focus()
                        }}
                      onSubmitEditing={this.onSubmitEmail}
                      returnKeyType='next'
                      maxLength={1}
                    />

                    <TextInput
                      style={CustomStyles.otpTextInput}
                      ref={this.txt4Ref}
                      keyboardType='phone-pad'
                      autoCapitalize='none'
                      autoCorrect={true}
                      enablesReturnKeyAutomatically={true}
                      onFocus={this.onFocus}
                      onChangeText={(txt4) => {
                        this.setState({txt4});
                        if (txt4 != '') {
                          this.txt5.focus();
                        }
                      }
                      }
                      onKeyPress={({ nativeEvent }) => {
                          nativeEvent.key === 'Backspace' ? this.txt3.focus() : this.txt5.focus()
                        }}
                      onSubmitEditing={this.onSubmitEmail}
                      returnKeyType='next'
                      maxLength={1}
                    />

                    <TextInput
                      style={CustomStyles.otpTextInput}
                      ref={this.txt5Ref}
                      keyboardType='phone-pad'
                      autoCapitalize='none'
                      autoCorrect={true}
                      enablesReturnKeyAutomatically={true}
                      onFocus={this.onFocus}
                      onChangeText={(txt5) => {
                        this.setState({txt5});
                        if (txt5 != '') {
                          this.txt6.focus();
                        }
                      }}
                      onSubmitEditing={this.onSubmitEmail}
                      returnKeyType='next'
                      maxLength={1}
                      onKeyPress={({ nativeEvent }) => {
                          nativeEvent.key === 'Backspace' ? this.txt4.focus() : this.txt6.focus()
                        }}
                    />

                    <TextInput
                      style={CustomStyles.otpTextInput}
                      ref={this.txt6Ref}
                      keyboardType='phone-pad'
                      autoCapitalize='none'
                      autoCorrect={true}
                      enablesReturnKeyAutomatically={true}
                      onChangeText={(txt6) => {
                        this.setState({txt6});
                        if (txt6 != '') {
                          this.txt6.blur();
                        }
                      }}
                      onKeyPress={({ nativeEvent }) => {
                          nativeEvent.key === 'Backspace' ? this.txt5.focus() : this.txt6.blur()
                        }}
                      onFocus={this.onFocus}
                      onSubmitEditing={this.onSubmitEmail}
                      returnKeyType='next'
                      maxLength={1}
                    />
                    
                    </View>

                    <TouchableOpacity
                      style={[CustomStyles.themeButton, {marginTop: 60}]}
                      onPress={this.confirmCode.bind(this)}
                   >
                      <Text style={CustomStyles.themeButtonText}> Verify </Text>
                  </TouchableOpacity>

                  <TouchableOpacity
                      onPress={this.resendOTP.bind(this)}
                   >
                      <Text style={CustomStyles.resendBtn}> Resend </Text>
                  </TouchableOpacity>

                  </View>

                </KeyboardAvoidingView>
                
                
            </View>
        </View>
        </TouchableWithoutFeedback>
        )
    }
}


export default OTPVerification;
