import React, { Component, Fragment } from 'react';
import { StyleSheet, Dimensions, YellowBox, Text, View, Button, TextInput, Image, ScrollView, TouchableOpacity, Alert, SafeAreaView } from 'react-native';
import {KeyboardAvoidingView, Keyboard, TouchableWithoutFeedback} from 'react-native';
import userDefaults from 'react-native-user-defaults'
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'

import Constant from '../CommonClasses/Constant';
import CommonUtilsObj from '../CommonClasses/CommonUtils';
import CustomStyles from '../CommonClasses/CustomStyles';
import Message from '../CommonClasses/Message';
import APIManagerObj from '../CommonClasses/APIManager';

import NetInfo from "@react-native-community/netinfo";
import Toast from 'react-native-simple-toast';
import ProgressLoader from 'rn-progress-loader';

class ForgotPassword extends Component {
 
    constructor(props) {
        super(props);

        this.onFocus = this.onFocus.bind(this);
        this.onSubmitDetails = this.onSubmitDetails.bind(this);
        this.onChangeText = this.onChangeText.bind(this);
        this.onSubmitEmail = this.onSubmitEmail.bind(this);

        this.emailRef = this.updateRef.bind(this, 'email');

        this.state = {
            email: '',
            connection_Status : "",
            visible: false,
            btnBottom: 20,
        };

        YellowBox.ignoreWarnings([
          'Warning: componentWillMount is deprecated',
          'Warning: componentWillReceiveProps is deprecated',
          'Warning: componentWillUpdate is deprecated',
          'Warning: componentWillUpdate has been renamed, and is not recommended for use',
          'Warning: componentWillReceiveProps has been renamed, and is not recommended for use',
        ]);
    }

    componentDidMount() {

      NetInfo.isConnected.addEventListener(
          'connectionChange',
          this._handleConnectivityChange

      );
     
      NetInfo.isConnected.fetch().done((isConnected) => {

        if(isConnected == true)
        {
          this.setState({connection_Status : "Online"})
        }
        else
        {
          this.setState({connection_Status : "Offline"})
        }

      });

      this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
      this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
    }

    componentWillUnmount() {
 
      NetInfo.isConnected.removeEventListener(
          'connectionChange',
          this._handleConnectivityChange
   
      );
   
    }

    _keyboardDidShow = () => {
      this.setState({btnBottom : -100})
    }

    _keyboardDidHide = () => {
        this.setState({btnBottom : 30})
    }

    _handleConnectivityChange = (isConnected) => {

      if(isConnected == true)
        {
          this.setState({connection_Status : "Online"})
        }
        else
        {
          this.setState({connection_Status : "Offline"})
        }
    };

    onFocus() {
      let { errors = {} } = this.state;

      for (let name in errors) {
        let ref = this[name];

        if (ref && ref.isFocused()) {
          delete errors[name];
        }
      }

      this.setState({ errors });
    }

    onChangeText(text) {
      ['email']
        .map((name) => ({ name, ref: this[name] }))
        .forEach(({ name, ref }) => {
          if (ref.isFocused()) {
            this.setState({ [name]: text });
          }
        });
    }

    
    onSubmitEmail(text) {
      
      let emailVal = text.nativeEvent.text
      let emailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ;

      if (!emailVal) {
        Toast.showWithGravity(Message.KEmailEmpty, Toast.LONG, Toast.TOP)
      } 
      else {
        
        if (emailRegex.test(emailVal) == false) {
          Toast.showWithGravity(Message.KEmailInvalid, Toast.LONG, Toast.TOP)
        }
      }
    }

    onSubmitDetails() {


      this.email.blur();
      var flag = true;
      var errorMsg = [];

      let emailVal = this.state.email
      let emailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ;

      if (!emailVal) {
          errorMsg.push(Message.KEmailEmpty)
          flag = false;
      } 
      else {
        
        if (emailRegex.test(emailVal) == false) {
            errorMsg.push(Message.KEmailInvalid)
            flag = false;
        }
      }

      if (flag) {
        if (this.state.connection_Status == "Online") {
          this.onSubmitForgotPassword();
        }
        else {
          Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
        }
      }
      else {
        Toast.showWithGravity(errorMsg.toString(), Toast.LONG, Toast.TOP)
      }
    }

    updateRef(name, ref) {
      this[name] = ref;
    }

    async onSubmitForgotPassword() {
      if (this.state.connection_Status == "Online") {
        this.email.blur();
      
        this.setState({
            visible: true
        });

        var data = new FormData();
        data.append('email', this.state.email);

        let responseData = await APIManagerObj.callApiWithData(Constant.KForgotPasswordURL, data);
        console.log('responseText json.....' + JSON.stringify(responseData));

        if (responseData.status == Constant.KSuccess) {
          this.setState({
              visible: false
          });
          this.props.navigation.pop()
          setTimeout(() => {
            Toast.showWithGravity(responseData.message, Toast.LONG, Toast.TOP)
          }, 200);
        }
        else {
          this.setState({
          visible: false,
        }); 
        setTimeout(() => {
          if(typeof(responseData.message) === 'string')
          {
              Toast.showWithGravity(responseData.message, Toast.LONG, Toast.TOP)
          }
          else if(typeof(responseData.error) === 'string')
          {
              Toast.showWithGravity(responseData.error, Toast.LONG, Toast.TOP)
          }
          else {
              if (responseData.error) {
                Toast.showWithGravity(responseData.error.toString(), Toast.LONG, Toast.TOP)
              }
              else {
                Toast.showWithGravity(Message.KAPIError, Toast.LONG, Toast.TOP)
              }
          }
        }, 200);
        }
      }
      else {
        Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
      }
    }

    render() {
        let { errors = {}, secureTextEntry, ...data } = this.state;
        return(

            <View style={[{flex:1, backgroundColor: 'black'}]}>

                <View
                style={{backgroundColor: "#fff", justifyContent: 'center', alignItems: 'center'}}>

                <ProgressLoader
                  visible={this.state.visible}
                  isModal={true} isHUD={true}
                  hudColor={"#fff"}
                  height={200}
                  width={200}
                  color={"#8edf01"} />
                </View>

                <KeyboardAwareScrollView contentContainerStyle={{justifyContent: 'center', width: '100%',}}>
                   <View style={{marginHorizontal: 20, marginTop: 70}}>


                  <View style={[CustomStyles.flexColunm]}>
                    <Text style={[CustomStyles.titleText]}> Forgot Password? </Text>
                    <Text style={CustomStyles.descGrayText}> Retrive your password </Text>
                    { (this.state.email != '') &&
                      <Text style={CustomStyles.labelText}>Email</Text>
                    }
                    <TextInput
                      style={CustomStyles.textInput}
                      ref={this.emailRef}
                      placeholder = "Enter email" 
                      keyboardType='email-address'
                      autoCapitalize='none'
                      autoCorrect={true}
                      enablesReturnKeyAutomatically={true}
                      onFocus={this.onFocus}
                      onChangeText={this.onChangeText}
                      onSubmitEditing={this.onSubmitEmail}
                      returnKeyType='done'
                    />
                </View>
              </View>

                </KeyboardAwareScrollView>
              
            <TouchableOpacity
              style={[CustomStyles.themeButtonAbsolute, {bottom: this.state.btnBottom,}]}
              onPress={this.onSubmitDetails.bind(this)}
             >
                <Text style={CustomStyles.themeButtonText}> Submit </Text>
            </TouchableOpacity>
        </View>
        )
    }
}

export default ForgotPassword;
