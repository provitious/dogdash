import React, { Component, Fragment } from 'react';
import { StyleSheet, Dimensions, YellowBox, Text, View, Button, TextInput, Image, ScrollView, TouchableOpacity, Alert, SafeAreaView } from 'react-native';
import {KeyboardAvoidingView, Keyboard, TouchableWithoutFeedback} from 'react-native';
import userDefaults from 'react-native-user-defaults'
import DefaultPreference from 'react-native-default-preference';
import NetInfo from "@react-native-community/netinfo";
import Toast from 'react-native-simple-toast';
import ProgressLoader from 'rn-progress-loader';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'

import MaterialIcon from 'react-native-vector-icons/MaterialIcons';

import Constant from '../CommonClasses/Constant';
import CommonUtilsObj from '../CommonClasses/CommonUtils';
import CustomStyles from '../CommonClasses/CustomStyles';
import Message from '../CommonClasses/Message';
import APIManagerObj from '../CommonClasses/APIManager';

import App from '../../App';

class Login extends Component {
 
    constructor(props) {
        super(props);

        this.onFocus = this.onFocus.bind(this);
        this.onChangeText = this.onChangeText.bind(this);
        this.onSubmitEmail = this.onSubmitEmail.bind(this);
        this.onSubmitPassword = this.onSubmitPassword.bind(this);

        this.emailRef = this.updateRef.bind(this, 'email');
        this.passwordRef = this.updateRef.bind(this, 'password');
        
        this.onRegister = this.onRegister.bind(this);
        this.onSubmitLogin = this.onSubmitLogin.bind(this);

        this.state = {
            email: '',
            password: '',
            hidePassword: true,
            connection_Status : "",
            visible: false,
            checkRememberMe: false,
            visiblePasswordView: false,
        };

        YellowBox.ignoreWarnings([
          'Warning: componentWillMount is deprecated',
          'Warning: componentWillReceiveProps is deprecated',
          'Warning: componentWillUpdate is deprecated',
          'Warning: componentWillUpdate has been renamed, and is not recommended for use',
          'Warning: componentWillReceiveProps has been renamed, and is not recommended for use',
        ]);
    }

    componentDidMount() {

      NetInfo.isConnected.addEventListener(
          'connectionChange',
          this._handleConnectivityChange

      );
     
      NetInfo.isConnected.fetch().done((isConnected) => {

        if(isConnected == true)
        {
          this.setState({connection_Status : "Online"})
        }
        else
        {
          this.setState({connection_Status : "Offline"})
        }

      });

    }

    componentWillUnmount() {
 
      NetInfo.isConnected.removeEventListener(
          'connectionChange',
          this._handleConnectivityChange
      );
   
    }

    _handleConnectivityChange = (isConnected) => {

      if(isConnected == true)
        {
          this.setState({connection_Status : "Online"})
        }
        else
        {
          this.setState({connection_Status : "Offline"})
        }
    };

    managePasswordVisibility = () =>
    {
      this.setState({ hidePassword: !this.state.hidePassword });
    }

    updateRef(name, ref) {
      this[name] = ref;
    }

    onFocus() {
      let { errors = {} } = this.state;

      for (let name in errors) {
        let ref = this[name];

        if (ref && ref.isFocused()) {
          delete errors[name];
        }
      }

      this.setState({ errors });
    }

    onChangeText(text) {
      ['email', 'password']
        .map((name) => ({ name, ref: this[name] }))
        .forEach(({ name, ref }) => {
          // if (ref.isFocused()) {
          //   this.setState({ [name]: text });
          // }

          if (ref.isFocused()) {
            this.setState({ [name]: text });
            if (name == 'email'){
                // console.log('found event' + text);
                if (CommonUtilsObj.rememberMeList) {

                  CommonUtilsObj.rememberMeList.map(item => { 
                if (item.email == text) {
                  console.log('show password');
                  this.setState({
                    visiblePasswordView : true,
                    rememberMePassword : item.password,
                  })
                }
                else {
                  this.setState({
                    visiblePasswordView : false,
                    rememberMePassword : '',
                  })
                } 
              })
                }
                
            }
          }

        });
    }

    onSubmitEmail(text) {
      
      let emailVal = text.nativeEvent.text
      let emailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ;

      if (!emailVal) {
        Toast.showWithGravity(Message.KEmailEmpty, Toast.LONG, Toast.TOP)
      } 
      else {
        
        if (emailRegex.test(emailVal) == false) {
          Toast.showWithGravity(Message.KEmailInvalid, Toast.LONG, Toast.TOP)
        }
        else {
          this.password.focus();
        }
      }
    }

    onSubmitPassword(text) {
      this.password.blur();
      let passwordRegex  = /^(?=.*\d)(?=.*[aA-zZ]).{6,20}$/;
      let passwordVal = text.nativeEvent.text

      if (!passwordVal) {
        Toast.showWithGravity(Message.KPasswordEmpty, Toast.LONG, Toast.TOP)
      } 
      
    }

    onForgotPassword() {
      this.props.navigation.navigate('ForgotPassword');
    }

    onRegister() {
      this.props.navigation.navigate('Register');
    }

    onSubmitLogin() {

      this.email.blur();
      this.password.blur();

      var flag = true;
      var errorMsg = [];

      let emailVal = this.state.email
      let emailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ;

      if (!emailVal) {
          errorMsg.push(Message.KEmailEmpty)
          flag = false;
      } 
      else {
        
        if (emailRegex.test(emailVal) == false) {
            errorMsg.push(Message.KEmailInvalid)
            flag = false;
        }
        else {
          this.password.focus();
        }
      }

      let passwordRegex  = /^(?=.*\d)(?=.*[aA-zZ]).{6,20}$/;
      let passwordVal = this.state.password

      if (!passwordVal) {
        errorMsg.push(Message.KPasswordEmpty)
        flag = false;
      } 

      if (flag) {
        if (this.state.connection_Status == "Online") {
          this.onFetchLoginDetails();
        }
        else {
          Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
        }
      }
      else {
        let styles={backgroundColor:'red'}
        Toast.showWithGravity(errorMsg.toString(), Toast.LONG, Toast.TOP, styles)
      }
    }

    changeImage() {
        this.setState({
          checkTC:!this.state.checkTC
        });
    }

    fetchProfileData() {
      CommonUtilsObj.getLoggedUserDetails();
    }

    async onFetchLoginDetails() {
      if (this.state.connection_Status == "Online") {
        this.email.blur();
        this.password.blur();
      
        this.setState({
            visible: true
        });

        var data = new FormData();
        data.append('username', this.state.email);
        data.append('password', this.state.password);
        data.append('device_type', 'ios');
        data.append('device_token', CommonUtilsObj.deviceToken);

        let responseData = await APIManagerObj.callApiWithData(Constant.KLoginURL, data);
        console.log('responseText json.....' + JSON.stringify(responseData));

        if (responseData.status == Constant.KSuccess) {
          
          if (Platform.OS==='android') {
              CommonUtilsObj.setLoggedUserDetails(JSON.stringify(responseData));
          }
          else {
              CommonUtilsObj.setLoggedUserDetails(responseData);
          }
          
          this.fetchProfileData();

          if (this.state.checkRememberMe) {
            this.setRememberMeData();
          }

          // setInterval( () => {
          //   <App />
          // },1000)

          this.setState({
              visible: false
          });

          setTimeout(() => {
            CommonUtilsObj.setLoginState(Constant.KLogin);
            Toast.showWithGravity(responseData.message, Toast.LONG, Toast.TOP)
          }, 1000);
        }
        else {
          this.setState({
            visible: false,
          }); 
          setTimeout(() => {
            if(typeof(responseData.message) === 'string')
            {
                Toast.showWithGravity(responseData.message, Toast.LONG, Toast.TOP)
            }
            else if(typeof(responseData.error) === 'string')
            {
                Toast.showWithGravity(responseData.error, Toast.LONG, Toast.TOP)
            }
            else {
                if (responseData.error) {
                  Toast.showWithGravity(responseData.error.toString(), Toast.LONG, Toast.TOP)
                }
                else {
                  Toast.showWithGravity(Message.KAPIError, Toast.LONG, Toast.TOP)
                }
            }
          }, 200);
        }
      }
      else {
        Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
      }
    }

    onClickRememberMe() {
        this.setState({
          checkRememberMe:!this.state.checkRememberMe
        });
    }

    setRememberMeData(data) {
      if (CommonUtilsObj.rememberMeList) {

        if (CommonUtilsObj.rememberMeList.length != 0) {
          CommonUtilsObj.rememberMeList.map(item => { 
          if (item.email == this.state.email) {
            item.email = this.state.email;
            item.password = this.state.password;
          }
          else {
            var data = {
              email: this.state.email,
              password: this.state.password,
            };

            CommonUtilsObj.rememberMeList.push(data);
          } 
        })
        }
        else {
            CommonUtilsObj.rememberMeList = [];

        var data = {
          email: this.state.email,
          password: this.state.password,
        };

        CommonUtilsObj.rememberMeList.push(data);
        } 
        
      }
      else {

        CommonUtilsObj.rememberMeList = [];

        var data = {
          email: this.state.email,
          password: this.state.password,
        };

        CommonUtilsObj.rememberMeList.push(data);
      }
        
      if (Platform.OS === 'android')
      {
          DefaultPreference.set(Constant.KRememberMeKey, JSON.stringify(CommonUtilsObj.rememberMeList))
          .then(function() {console.log('setRememberMeData')})
          .catch(function (err) {
            console.log(err);
          });
      }
      else {
          userDefaults.set(Constant.KRememberMeKey, CommonUtilsObj.rememberMeList)
          .then(data => console.log('setRememberMeData'))
      }
    }

    hidePasswordView() {
     
        this.setState({
          visiblePasswordView : false,
          password : this.state.rememberMePassword,
        })
    }

    measureView(event) {
      // this.setState({
      //         initialHeight:event.nativeEvent.layout.height,
      //     });
      // emptyComponentHeight = event.nativeEvent.layout.height
      CommonUtilsObj.btnHeight = event.nativeEvent.layout.height;
    }

    render() {
        let { errors = {}, secureTextEntry, ...data } = this.state;
        return(
            

            <View style={CustomStyles.container}>
                <View
                style={{backgroundColor: "#fff", justifyContent: 'center', alignItems: 'center'}}>

                <ProgressLoader
                visible={this.state.visible}
                isModal={true} isHUD={true}
                hudColor={"#fff"}
                height={200}
                width={200}
                color={"#8edf01"} />
                </View>

                <KeyboardAwareScrollView contentContainerStyle={{justifyContent: 'center', flexGrow: 1}}>

                  <View style={CustomStyles.flexColunm}>
                    <Image
                        style={CustomStyles.logoImage}
                        source={require('../../images/logo.png')}
                        resizeMode="contain"
                    />

                    { (this.state.email != '') &&
                      <Text style={CustomStyles.labelText}>Email</Text>
                    }
                    <TextInput
                      style={CustomStyles.textInput}
                      ref={this.emailRef}
                      placeholder = "Enter email" 
                      keyboardType='email-address'
                      autoCapitalize='none'
                      autoCorrect={true}
                      enablesReturnKeyAutomatically={true}
                      onFocus={this.onFocus}
                      onChangeText={this.onChangeText}
                      onSubmitEditing={this.onSubmitEmail}
                      returnKeyType='next'
                      onLayout={(event) => this.measureView(event)}
                    />

                    { (this.state.password != '') &&
                      <Text style={CustomStyles.labelText}>Password</Text>
                    }
                    <View style={[CustomStyles.flexColunm,{margin: 0}]}>
                    <View style = { CustomStyles.textBoxBtnHolder }>
                    
                    <TextInput 
                      secureTextEntry = { this.state.hidePassword } 
                      style = { CustomStyles.textInput }
                      ref={this.passwordRef}
                      placeholder = "Enter password" 
                      autoCapitalize='none'
                      autoCorrect={false}
                      enablesReturnKeyAutomatically={true}
                      onFocus={this.onFocus}
                      onChangeText={this.onChangeText}
                      onSubmitEditing={this.onSubmitPassword}
                      returnKeyType='done'
                      maxLength={20}
                      characterRestriction={20}
                      value={this.state.password}
                    />
                    <TouchableOpacity 
                      activeOpacity = { 0.8 } 
                      style = { CustomStyles.visibilityBtn } 
                      onPress = { this.managePasswordVisibility }
                      >
                      <Image 
                      source = { ( this.state.hidePassword ) ? require('../../images/eyeHide.png') : require('../../images/eye.png') } 
                      style = { CustomStyles.btnImage } 
                      />
                    </TouchableOpacity>
                  </View>
                  { this.state.visiblePasswordView &&
                      <View 
                          style={{backgroundColor: 'gray', height: 40, position: 'absolute', top:(this.state.password == '' ? 0 : -30), width: (Dimensions.get('window').width)-40}}
                          >
                          <TextInput 
                              secureTextEntry = { true } 
                              style = { {marginTop: 7, padding: 5} }
                              value = {this.state.rememberMePassword}
                              // value={'ggggg'}
                              editable= {false}
                            />
                        <TouchableOpacity 
                          onPress={this.hidePasswordView.bind(this)}
                          style = {{height: 40, width: (Dimensions.get('window').width)-40, position: 'absolute'}}
                          >
                          
                        </TouchableOpacity>
                      </View>
                    }
                    </View>


                  <View style={CustomStyles.topContainer}>
                          <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-start' }}>
                              <TouchableOpacity
                                  style={CustomStyles.btnCheck}
                                  onPress={this.onClickRememberMe.bind(this)}
                               >
                              <Image
                                    ref="imgChecked"
                                    style={{height: 25, width: 25}}
                                    source={ this.state.checkRememberMe === true ?                  
                                    require('../../images/check.png') : 
                                    require('../../images/checkOff.png')}
                                  />
                              </TouchableOpacity>
                              <Text style={CustomStyles.rememberMeText}> Remember me </Text>
                          </View>
                          

                          <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-end'}}>
                              <TouchableOpacity
                                style={CustomStyles.forgotPWBtn}
                                onPress={this.onForgotPassword.bind(this)}
                                 >
                                <Text style={CustomStyles.ForgotPasswordText}> Forgot Password? </Text>
                              </TouchableOpacity>
                          </View>
                  </View>

                  <TouchableOpacity
                      style={[CustomStyles.themeButton, {marginTop: 20}]}
                      onPress={this.onSubmitLogin.bind(this)}
                   >
                      <Text style={CustomStyles.themeButtonText}> Login </Text>
                  </TouchableOpacity>

                  <View style={CustomStyles.rigsterNowContainer}>
                      <Text style={CustomStyles.otherText}> Don't have an account? </Text>
                      <TouchableOpacity                     
                        onPress={this.onRegister.bind(this)}
                       >
                          <Text style={CustomStyles.rememberMeText}> Register Now </Text>
                      </TouchableOpacity>
                  </View>
                </View>
                </KeyboardAwareScrollView>
        </View>
        )
    }
}


export default Login;
