import React from 'react';
import { StyleSheet, Dimensions, YellowBox, Text, View, Button, TextInput, Image, ScrollView, TouchableOpacity, Alert, SafeAreaView, PixelRatio, ListView } from 'react-native';
import {KeyboardAvoidingView, Keyboard, TouchableWithoutFeedback} from 'react-native';
import { NavigationBar } from 'navigationbar-react-native';
import userDefaults from 'react-native-user-defaults'
import { Modal, FlatList, SectionList, ActivityIndicator } from 'react-native';
import NetInfo from "@react-native-community/netinfo";
import ProgressLoader from 'rn-progress-loader';
import Toast from 'react-native-simple-toast';
import { Rating, AirbnbRating } from 'react-native-ratings';
import { SwipeListView, SwipeRow } from 'react-native-swipe-list-view';
import Icon from 'react-native-vector-icons/FontAwesome';

import CommonUtilsObj from '../CommonClasses/CommonUtils';
import Constant from '../CommonClasses/Constant';
import CustomStyles from '../CommonClasses/CustomStyles';
import Message from '../CommonClasses/Message';
import APIManagerObj from '../CommonClasses/APIManager';

const btnHeight = (((Dimensions.get('window').width) * 10) / 100);
var emptyComponentHeight = 0;

export default class CardList extends React.Component {

  constructor(props) {
 
    super(props);
 
    YellowBox.ignoreWarnings([
      'Warning: componentWillMount is deprecated',
      'Warning: componentWillReceiveProps is deprecated',
    ]);
    this.state = {
      visible: false,
      connection_Status : "",
      cardList: [],
      selectedIndex: 0,
      oldSelectedIndex: 0,
      initialHeight:0,
      deletedIndex: 0,
      isUpdated: false,
      selectedCardDetails: '',
    };

    this.onClickAddCard=this.onClickAddCard.bind(this);
    CommonUtilsObj.navigationProps = this.props;
    
   }

  returnData(isUpdated, cardDetails) {
    this.setState({isUpdated: isUpdated});
  }

  componentDidMount() {

    NetInfo.isConnected.addEventListener(
        'connectionChange',
        this._handleConnectivityChange

    );
   
    NetInfo.isConnected.fetch().done((isConnected) => {

      if(isConnected == true)
      {
        this.setState({connection_Status : "Online"})
      }
      else
      {
        this.setState({connection_Status : "Offline"})
      }

      if (this.state.connection_Status == "Online") {
        this.onFetchCardList();
      }
      else {
        Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
      }

    });

    const {navigation} = this.props;
    navigation.addListener ('willFocus', () =>{

      if (this.state.isUpdated) {
          this.setState({
            isUpdated: false,
          });

          if (this.state.connection_Status == "Online") {
            this.onFetchCardList();
          }
          else {
            Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
          }
      }
      
    });
  }

  componentWillUnmount() {

    NetInfo.isConnected.removeEventListener(
        'connectionChange',
        this._handleConnectivityChange
 
    );
 
  }

  componentWillUnmount() {

    NetInfo.isConnected.removeEventListener(
        'connectionChange',
        this._handleConnectivityChange
 
    );
 
  }

  _handleConnectivityChange = (isConnected) => {

    if(isConnected == true)
      {
        this.setState({connection_Status : "Online"})
      }
      else
      {
        this.setState({connection_Status : "Offline"})
      }
  };

  async onFetchCardList() {
    if (this.state.connection_Status == "Online") {

      this.setState({ visible: true });
      var data = new FormData();
      data.append('driver_id', CommonUtilsObj.userDetails.id);

      let responseData = await APIManagerObj.callApiWithData(Constant.KGetCardListURL, data);
      console.log('json response ....' + JSON.stringify(responseData));

      if (responseData.status == "success") {
        var cnt = 0;
        var selectedCnt = 0;
        const items = responseData.cardlist.map(function(item, index){
            if (item.is_primary == '1') {  
                selectedCnt=cnt
            }
            cnt += 1;
            item.key = String(index)
        });

        setTimeout(() => {
          this.setState({
            visible: false,
            cardList: responseData.cardlist,
            selectedIndex: selectedCnt,
            oldSelectedIndex: selectedCnt,
          });
        }, 200);         
      }
      else {
        this.setState({
          visible: false,
        }); 
        setTimeout(() => {
          if(typeof(responseData.message) === 'string')
          {
              Toast.showWithGravity(responseData.message, Toast.LONG, Toast.TOP)
          }
          else if(typeof(responseData.error) === 'string')
          {
              Toast.showWithGravity(responseData.error, Toast.LONG, Toast.TOP)
          }
          else {
              if (responseData.error) {
                Toast.showWithGravity(responseData.error.toString(), Toast.LONG, Toast.TOP)
              }
              else {
                Toast.showWithGravity(Message.KAPIError, Toast.LONG, Toast.TOP)
              }
          }
        }, 200); 
      }
    }
    else {
      this.setState({lastApiTag : 1})
      Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
    }
  }

  async onSelectPrimaryCard() {
    if (this.state.connection_Status == "Online") {

      this.setState({ visible: true });
      var data = new FormData();
      data.append('driver_id', CommonUtilsObj.userDetails.id);
      data.append('card_id', this.state.cardList[this.state.selectedIndex].card_id);
      data.append('is_primary', '1');

      let responseData = await APIManagerObj.callApiWithData(Constant.KSetPrimaryCardURL, data);
      console.log('json response ....' + JSON.stringify(responseData));

      if (responseData.status == "success") {
        this.setState({
            visible: false
        });
        setTimeout(() => {
          this.onFetchCardList();
          Toast.showWithGravity(responseData.message, Toast.LONG, Toast.TOP)  
        }, 200);        
      }
      else {
        this.setState({
          visible: false,
        }); 
        setTimeout(() => {
          if(typeof(responseData.error) === 'string')
          {
              Toast.showWithGravity(responseData.error, Toast.LONG, Toast.TOP)
          }
          else {
              if (responseData.error) {
                Toast.showWithGravity(responseData.error.toString(), Toast.LONG, Toast.TOP)
              }
              else {
                Toast.showWithGravity(Message.KAPIError, Toast.LONG, Toast.TOP)
              }
          }
        }, 200); 
      }
    }
    else {
      this.setState({lastApiTag : 1})
      Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
    }
  }

  async onDeleteCard() {
    if (this.state.connection_Status == "Online") {

      this.setState({ visible: true });
      var data = new FormData();
      data.append('driver_id', CommonUtilsObj.userDetails.id);
      data.append('card_id', this.state.cardList[this.state.deletedIndex].card_id);

      let responseData = await APIManagerObj.callApiWithData(Constant.KDeleteCardURL, data);
      console.log('json response ....' + JSON.stringify(responseData));

      if (responseData.status == "success") {
        this.setState({
            visible: false
        });
        
        setTimeout(() => {
          this.onFetchCardList();
          Toast.showWithGravity(responseData.message, Toast.LONG, Toast.TOP)  
        }, 200);          
      }
      else {
        this.setState({
          visible: false,
        }); 
        setTimeout(() => {
          if(typeof(responseData.error) === 'string')
          {
              Toast.showWithGravity(responseData.error, Toast.LONG, Toast.TOP)
          }
          else {
              if (responseData.error) {
                Toast.showWithGravity(responseData.error.toString(), Toast.LONG, Toast.TOP)
              }
              else {
                Toast.showWithGravity(Message.KAPIError, Toast.LONG, Toast.TOP)
              }
          }
        }, 200); 
      }
    }
    else {
      this.setState({lastApiTag : 1})
      Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
    }
  }

  onSelectCell = (item, index) => {

      this.setState({
        selectedIndex: index,
        refresh:true,
      });

      if (this.state.cardList[index].is_primary == '1') {
          Toast.showWithGravity(Message.KAlreadyPrimaryCard, Toast.SMALL, Toast.TOP)
      }
      else {
          Alert.alert(
            Constant.KAppTitle,
            'Are you sure, you want to choose this card as your primary card?',
            [
              {
                text: 'No',
                onPress: () => { 
                  this.setState({
                    selectedIndex:this.state.oldSelectedIndex,
                  });
                },
                style: 'cancel',
              },
              {text: 'Yes', onPress: () => {
                this.onSelectPrimaryCard()
              }},
            ],
            {cancelable: false},
        );
      }
      
  };

  onSelectDeleteCard = (item, index) => {

      this.setState({
        refresh:true,
      });

      if (this.state.cardList[index].is_primary == '1') {
          Toast.showWithGravity(Message.KDeletePrimaryCard, Toast.SMALL, Toast.TOP)
      }
      else {
          Alert.alert(
            Constant.KAppTitle,
            'Are you sure, you want to delete this card?',
            [
              {
                text: 'No',
                onPress: () => { 
                  this.setState({
                  });
                },
                style: 'cancel',
              },
              {text: 'Yes', onPress: () => {
               
                this.setState({
                  deletedIndex: index
                  });

                setTimeout(() => {
                  this.onDeleteCard();
                }, 300);
                
              }},
            ],
            {cancelable: false},
        );
      }
      
  };

  onSelectEditCard = (item, index) => {
      this.setState({
          selectedCardDetails: item
      });
      this.props.navigation.navigate('AddEditCardScreen', {returnData: this.returnData.bind(this), selectedCardDetails: item, isEdit: true});
  };

  renderCardListItem = ({ item, index }) => (
    <CardListCell
      item={item}
      index={index}
      onSelectCell={this.onSelectCell}
      selectedIndex={this.state.selectedIndex}
      cardList={this.state.cardList}
      fromSelection={this.fromSelection}
      onSelectDeleteCard={this.onSelectDeleteCard}
      onSelectEditCard={this.onSelectEditCard}
    />
  );

  renderHiddenItem = (data, rowMap) => (
    <View style={CustomStyles.standaloneRowBack}>
      <TouchableOpacity
          style={{backgroundColor: 'green', height: '100%', width: 65, justifyContent:'center'}}
          onPress={this.onSelectEditCard.bind(this, data.item, data.item.key)}
      >
        <Icon style={{ marginLeft: 20 }} name="pencil-square-o" size={25} color="#fff" />
      </TouchableOpacity>

      <TouchableOpacity
          style={{backgroundColor: 'red', height: '100%', width: 65, justifyContent:'center'}}
          onPress={this.onSelectDeleteCard.bind(this, data.item, data.item.key)}
      >
        <Icon style={{ marginLeft: 20 }} name="trash" size={25} color="#fff" />
      </TouchableOpacity>
    </View>
  );


  measureView(event) {
  this.setState({
          initialHeight:event.nativeEvent.layout.height,
      });
  emptyComponentHeight = event.nativeEvent.layout.height
  }

  onClickAddCard() {
    // this.props.navigation.navigate('AddEditCardScreen');
    this.props.navigation.navigate('AddEditCardScreen', {returnData: this.returnData.bind(this), selectedCardDetails: this.state.selectedCardDetails, isEdit: false});
  }

  emptyComponent = () => {
    return (
        <View style={CustomStyles.emptyContainer}>
          <Text style={CustomStyles.emptyText}>No records found</Text>
        </View>
    );
  };

  render() {

    return (
      <View style={[CustomStyles.container]}>

          <View
              style={{backgroundColor: "#fff", justifyContent: 'center', alignItems: 'center'}}>

              <ProgressLoader
              visible={this.state.visible}
              isModal={true} isHUD={true}
              hudColor={"#fff"}
              height={200}
              width={200}
              color={"#8edf01"} />
          </View>

          <View style={{height: (Dimensions.get('window').height) - (100 + btnHeight + this.state.initialHeight)}}>

            <SwipeListView
              data={this.state.cardList}
              renderItem={this.renderCardListItem}
              renderHiddenItem={this.renderHiddenItem}
              // leftOpenValue={65}
              rightOpenValue={-130}
              previewRowKey={'0'}
              previewOpenValue={-130}
              previewOpenDelay={1000}
              // onRowDidOpen={onRowDidOpen}
              ListEmptyComponent={this.emptyComponent.bind(this)}
            /> 
          </View>

          <Text style={CustomStyles.noteFont} onLayout={(event) => this.measureView(event)}>Please select the card you want to use for the booking. You can always change your card from manage payment section of the menu.</Text>
          
          <TouchableOpacity
            style={[CustomStyles.themeButtonAbsolute]}
            onPress={this.onClickAddCard.bind(this)}
           >
              <Text style={CustomStyles.plusBtnText}> + </Text>
          </TouchableOpacity>  
      </View>
      
    );
  }
}

class CardListCell extends React.Component {
  render() {
    const { item, index, selectedIndex, fromSelection} = this.props;

    return (
      <View style={CustomStyles.standalone}>

            <View style={CustomStyles.standaloneRowFront}>
              
                  <View 
                    style={
                      [
                        CustomStyles.countryStyle, 
                        {
                          flexDirection: 'row', 
                          justifyContet: 'center',
                          alignItems: 'center',
                          paddingTop: 5,
                          paddingBottom:5,
                        }
                      ]
                    }>

                    <TouchableWithoutFeedback 
                      onPress={this.props.onSelectCell.bind(this, item, index)}>
                        <Image
                          ref="imgChecked"
                          style={{height: 25, width: 25, marginRight: 12}}
                          source={ 
                            selectedIndex === index ? require('../../images/checkSelected.png') : require('../../images/checkUnselected.png')
                          }
                        />
                    </TouchableWithoutFeedback>

                    <View style={[CustomStyles.flexColunm, {justifyContent: 'flex-start', width: (Dimensions.get('window').width) - 80}]}>
                        <Text style={[CustomStyles.mediumFont, {alignSelf:'flex-start'}]}>Card Ending ...{item.card_number.substring((item.card_number.length-4),item.card_number.length)}</Text>
                        <Text style={[CustomStyles.smallFont, {alignSelf: 'flex-start'}]}>Added on {(new Date(item.created_at.replace(" ", "T"))).toDateString()}</Text>
                    </View>

                  </View>
              
            </View>
        </View>
      
    );
  }
}
  