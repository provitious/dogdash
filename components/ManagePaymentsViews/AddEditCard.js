import React from 'react';
import { StyleSheet, Dimensions, YellowBox, Text, View, Button, TextInput, Image, ScrollView, TouchableOpacity, Alert, SafeAreaView, PixelRatio } from 'react-native';
import {KeyboardAvoidingView, Keyboard, TouchableWithoutFeedback} from 'react-native';
import { NavigationBar } from 'navigationbar-react-native';
import userDefaults from 'react-native-user-defaults'
import NetInfo from "@react-native-community/netinfo";
import ProgressLoader from 'rn-progress-loader';
import Toast from 'react-native-simple-toast';
import { Dropdown } from 'react-native-material-dropdown';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'

import CommonUtilsObj from '../CommonClasses/CommonUtils';
import Constant from '../CommonClasses/Constant';
import CustomStyles from '../CommonClasses/CustomStyles';
import Message from '../CommonClasses/Message';
import APIManagerObj from '../CommonClasses/APIManager';

import Moment from 'moment';

const txtInputHeight = (((Dimensions.get('window').width) * 9.8) / 100);

var noOfMonths = [{value: '01'}, {value: '02'}, {value: '03'}, {value: '04'}, {value: '05'}, {value: '06'}, {value: '07'}, {value: '08'}, {value: '09'}, {value: '10'}, {value: '11'}, {value: '12'}];
var noOfYears = [];

export default class AddEditCard extends React.Component {

  constructor(props) {
 
    super(props);
 
    YellowBox.ignoreWarnings([
      'Warning: componentWillMount is deprecated',
      'Warning: componentWillReceiveProps is deprecated',
    ]);

    const { navigation } = this.props;  
    const isEdit = navigation.getParam('isEdit', false);

    if (isEdit) {
        const selectedCardDetails = navigation.getParam('selectedCardDetails', 'NO-Value');  
        
        this.state = {
          visible: false,
          connection_Status : "",
          cardHolderName: selectedCardDetails.card_holder_name,
          cardNumber: selectedCardDetails.card_number,
          cardId: selectedCardDetails.card_id,
          month: selectedCardDetails.card_exp_month,
          year: selectedCardDetails.card_exp_year,
          isEdit: true,
          btnBottom: 30,
        };
    }
    else {
        this.state = {
          visible: false,
          connection_Status : "",
          cardHolderName: '',
          cardNumber: '',
          month: '',
          year:'',
          isEdit: false,
          btnBottom: 30,
        };
    }

    this.onFocus = this.onFocus.bind(this);
    this.onValidate = this.onValidate.bind(this);
    this.onChangeText = this.onChangeText.bind(this);
    this.onSubmitCardHolderName = this.onSubmitCardHolderName.bind(this);
    this.onSubmitCardNumber = this.onSubmitCardNumber.bind(this);

    this.cardHolderNameRef = this.updateRef.bind(this, 'cardHolderName');
    this.cardNumberRef = this.updateRef.bind(this, 'cardNumber');

    let currentYear = new Date().getFullYear();
    for(let i = 0; i < 50; i++){
      noOfYears.push({value: currentYear + i});
    }
    
    CommonUtilsObj.navigationProps = this.props;
  }

  componentDidMount() {
    Moment.locale('en');

    NetInfo.isConnected.addEventListener(
        'connectionChange',
        this._handleConnectivityChange

    );
   
    NetInfo.isConnected.fetch().done((isConnected) => {

      if(isConnected == true)
      {
        this.setState({connection_Status : "Online"})
      }
      else
      {
        this.setState({connection_Status : "Offline"})
      }
    });

    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
  }

  _keyboardDidShow = () => {
      this.setState({btnBottom : -100})
  }

  _keyboardDidHide = () => {
      this.setState({btnBottom : 30})
  }

  componentWillUnmount() {

    NetInfo.isConnected.removeEventListener(
        'connectionChange',
        this._handleConnectivityChange
 
    );
 
  }

  _handleConnectivityChange = (isConnected) => {

    if(isConnected == true)
      {
        this.setState({connection_Status : "Online"})
      }
      else
      {
        this.setState({connection_Status : "Offline"})
      }
  };

  updateRef(name, ref) {
    this[name] = ref;
  }

  onFocus() {
    let { errors = {} } = this.state;

    for (let name in errors) {
      let ref = this[name];

      if (ref && ref.isFocused()) {
        delete errors[name];
      }
    }

    this.setState({ errors });
  }

  onChangeText(text) {
    ['cardHolderName', 'cardNumber']
      .map((name) => ({ name, ref: this[name] }))
      .forEach(({ name, ref }) => {
        if (ref.isFocused()) {
          this.setState({ [name]: text });
          if (name == 'cardNumber') {
            this.setState({ [name]: text.replace(/[^0-9]/g, '') });
          }

          if (name == 'cardHolderName') {
              if (!text.replace(/\s/g, '').length) {
                this.setState({ [name]: text.trim() });
              }
            }
        }
      });
  }

  onSubmitCardHolderName(text) {
      
    this.cardNumber.focus();
  }

  onSubmitCardNumber(text) {
    
    this.cardNumber.blur();

  }

  onValidate() {
    this.cardHolderName.blur();
    this.cardNumber.blur();

    var flag = true;
    var cFlag = true;
    var errorMsg = [];

    let nameVal = this.state.cardHolderName
    if (!nameVal) {
        errorMsg.push('Card holder name should not be empty')
        flag = false;
    }

    let numberVal = this.state.cardNumber
    if (!numberVal) {
        errorMsg.push('Card number should not be empty')
        flag = false;
    }
    else if (numberVal.length < 16) {
        errorMsg.push('Invalid card number')
        flag = false;
    }

    var curMon = Moment().format('MM');
    var curYr = Moment().format('YYYY');
    
    if (this.state.month == '') {
        errorMsg.push('Select card expiry month')
        flag = false;
    }
    else if (this.state.year == curYr) {
      if (Number(this.state.month) < Number(curMon)) {
        errorMsg.push('Select valid card expiry date')
        flag = false;
        cFlag = false;
      }
    }

    if (this.state.year == '') {
        errorMsg.push('Select card expiry year')
        flag = false;
    }
    else if (Number(this.state.year) == curYr) {
      if (Number(this.state.month) < Number(curMon)) {
        if (cFlag) {
          errorMsg.push('Select valid card expiry year')
          flag = false;
        }
        
      }
    }

    if (flag) {
      if (this.state.connection_Status == "Online") {

          if (this.state.isEdit) {
              this.onEditCard();
          }
          else {
              this.onAddCard();
          }
          
      }
      else {
        Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
      }
    }
    else {
      Toast.showWithGravity(errorMsg.toString(), Toast.LONG, Toast.TOP)
    }
  }

  async onAddCard() {
    if (this.state.connection_Status == "Online") {

      this.setState({ visible: true });
      var data = new FormData();
      data.append('driver_id', CommonUtilsObj.userDetails.id);
      data.append('card_number', this.state.cardNumber);
      data.append('card_exp_month', this.state.month);
      data.append('card_exp_year', this.state.year);
      data.append('card_holder_name', this.state.cardHolderName);

      let responseData = await APIManagerObj.callApiWithData(Constant.KAddCardDetailsURL, data);
      console.log('json response ....' + JSON.stringify(responseData));

      if (responseData.status == "success") {
        this.setState({
            visible: false
        });

        setTimeout(() => {
          Toast.showWithGravity(responseData.message, Toast.LONG, Toast.TOP)
          this.props.navigation.state.params.returnData(true);
          this.props.navigation.goBack();
        }, 200);        
      }
      else {
        this.setState({
          visible: false,
        }); 
        setTimeout(() => {
          if(typeof(responseData.message) === 'string')
          {
              Toast.showWithGravity(responseData.message, Toast.LONG, Toast.TOP)
          }
          else if(typeof(responseData.error) === 'string')
          {
              Toast.showWithGravity(responseData.error, Toast.LONG, Toast.TOP)
          }
          else {
              if (responseData.error) {
                Toast.showWithGravity(responseData.error.toString(), Toast.LONG, Toast.TOP)
              }
              else {
                Toast.showWithGravity(Message.KAPIError, Toast.LONG, Toast.TOP)
              }
          }
        }, 200); 
      }
    }
    else {
      this.setState({lastApiTag : 1})
      Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
    }
  }

  async onEditCard() {
    if (this.state.connection_Status == "Online") {

      this.setState({ visible: true });
      var data = new FormData();
      data.append('driver_id', CommonUtilsObj.userDetails.id);
      data.append('card_number', this.state.cardNumber);
      data.append('card_exp_month', this.state.month);
      data.append('card_exp_year', this.state.year);
      data.append('card_holder_name', this.state.cardHolderName);
      data.append('card_id', this.state.cardId);

      let responseData = await APIManagerObj.callApiWithData(Constant.KEditCardDetailsURL, data);
      console.log('json response ....' + JSON.stringify(responseData));

      if (responseData.status == "success") {
        this.setState({
            visible: false
        });
        
        setTimeout(() => {
          Toast.showWithGravity(responseData.message, Toast.LONG, Toast.TOP)
          this.props.navigation.state.params.returnData(true);
          this.props.navigation.goBack();
        }, 200);        
      }
      else {
        this.setState({
          visible: false,
        }); 
        setTimeout(() => {
          if(typeof(responseData.error) === 'string')
          {
              Toast.showWithGravity(responseData.error, Toast.LONG, Toast.TOP)
          }
          else {
              if (responseData.error) {
                Toast.showWithGravity(responseData.error.toString(), Toast.LONG, Toast.TOP)
              }
              else {
                Toast.showWithGravity(Message.KAPIError, Toast.LONG, Toast.TOP)
              }
          }
        }, 200); 
      }
    }
    else {
      this.setState({lastApiTag : 1})
      Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
    }
  }

  render() {

    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>

            <View style={CustomStyles.container}>
                <View
                style={{backgroundColor: "#fff", justifyContent: 'center', alignItems: 'center'}}>

                <ProgressLoader
                  visible={this.state.visible}
                  isModal={true} isHUD={true}
                  hudColor={"#fff"}
                  height={200}
                  width={200}
                  color={"#8edf01"} />
                </View>

                <View style={CustomStyles.centerContainer}>
                <KeyboardAwareScrollView style={[CustomStyles.flexColunm, {marginHorizontal: 20}]} behavior="padding" enabled>

                    { (this.state.cardHolderName != '') &&
                      <Text style={CustomStyles.labelText}>Card holder name</Text>
                    }
                    <TextInput
                      style={[CustomStyles.textInput,{marginBottom: 30, marginTop: 20}]}
                      ref={this.cardHolderNameRef}
                      placeholder = "Card holder name" 
                      keyboardType='default'
                      autoCapitalize='sentences'
                      autoCorrect={true}
                      enablesReturnKeyAutomatically={true}
                      onFocus={this.onFocus}
                      onChangeText={this.onChangeText}
                      onSubmitEditing={this.onSubmitCardHolderName}
                      returnKeyType='next'
                      value= {this.state.cardHolderName}
                    />

                    { (this.state.cardNumber != '') &&
                      <Text style={CustomStyles.labelText}>Card number</Text>
                    }
                    <TextInput
                      style={CustomStyles.textInput}
                      ref={this.cardNumberRef}
                      placeholder = "Card number" 
                      keyboardType='phone-pad'
                      autoCapitalize='sentences'
                      autoCorrect={true}
                      enablesReturnKeyAutomatically={true}
                      onFocus={this.onFocus}
                      onChangeText={this.onChangeText}
                      onSubmitEditing={this.onSubmitCardNumber}
                      returnKeyType='next'
                      maxLength={16}
                      value={this.state.cardNumber}
                    />

                    <View style={{flexDirection: 'row', flex:1, marginTop: 40}}>
                    <View style={{flexDirection: 'column', }}>
                      <Text style={[CustomStyles.labelText, {marginTop: 0, marginBottom: 5, width:80}]}>Month</Text>
                      <Dropdown
                        data={noOfMonths}
                        containerStyle = {{width:80, borderBottomColor: 'transparent', backgroundColor: 'white', borderRadius: 5, borderWidth: 0.5, height: CommonUtilsObj.btnHeight, paddingLeft : 10, paddingRight : 10}}
                        dropdownOffset = {{top: 7, left: 0}}
                        value = {this.state.month}
                        onFocus={()=>{this.cardHolderName.blur(); this.cardNumber.blur();}}
                        dropdownPosition={0}
                        onChangeText={(value)=>{
                          var curMon = Moment().format('MM');
                          var curYr = Moment().format('YYYY');

                          if (this.state.year == curYr) {
                            if (Number(value) < Number(curMon)) {
                              setTimeout(() => {
                                Toast.showWithGravity('Select valid expiration date', Toast.LONG, Toast.TOP)
                                this.setState({ month: value });
                              }, 1000);
                              
                            }
                            else {
                              this.setState({ month: value });
                              console.log('valid');
                            }
                          }
                          else {
                            this.setState({ month: value });
                            console.log('valid');
                          }
                        }}
                      />
                    </View>

                    <View style={{flexDirection: 'column', }}>
                      <Text style={[CustomStyles.labelText, {marginTop: 0, marginBottom: 5, width:100, marginLeft: 20,}]}>Year</Text>
                    <Dropdown
                      data={noOfYears}
                      containerStyle = {{width:100, marginLeft: 20, marginRight: 20, borderBottomColor: 'transparent', backgroundColor: 'white', borderRadius: 5, borderWidth: 0.5, height: CommonUtilsObj.btnHeight, paddingLeft : 10, paddingRight : 10}}
                      dropdownOffset = {{top: 7, left: 0}}
                      value = {this.state.year}
                      onFocus={()=>{this.cardHolderName.blur(); this.cardNumber.blur();}}
                      dropdownPosition={0}
                      onChangeText={(value)=>{

                        var curMon = Moment().format('MM');
                        var curYr = Moment().format('YYYY');
                       
                        if (Number(value) == curYr) {
                          if (Number(this.state.month) < Number(curMon)) {
                            this.setState({ year: value });
                            console.log('not valid');
                            setTimeout(() => {
                              Toast.showWithGravity('Select valid expiration date', Toast.LONG, Toast.TOP)
                            }, 1000);

                            // setTimeout(() => {
                            //   this.setState({ year: 'YYYY' });
                            // }, 2000);
                            
                          }
                          else {
                            this.setState({ year: value });
                            console.log('valid');
                          }
                        }
                        else {
                          this.setState({ year: value });
                          console.log('valid');
                        }

                        // this.setState({ year: value });
                      }}
                    />
                    </View>
                    </View>
                </KeyboardAwareScrollView>
                
                
            </View>

                    <TouchableOpacity
                      style={[CustomStyles.themeButtonAbsolute, {bottom: this.state.btnBottom}]}
                      onPress={this.onValidate.bind(this)}
                     >
                        <Text style={CustomStyles.themeButtonText}> Save </Text>
                    </TouchableOpacity>
      </View>
        </TouchableWithoutFeedback>
      
    );
  }
}

