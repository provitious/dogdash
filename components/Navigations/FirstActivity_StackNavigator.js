import React, { Component } from 'react';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { StackActions, NavigationActions } from 'react-navigation';
import { Text, View, Button, TextInput, Image, ScrollView, TouchableHighlight, TouchableOpacity, Alert, PixelRatio, FlatList, Linking, ImageBackground, Share, Modal, Dimensions } from 'react-native';

import HamburgerIcon from '../SideMenu/HamburgerIcon';
import Icon from 'react-native-vector-icons/FontAwesome';

import Home from '../Home/Home';
import CommonUtilsObj from '../CommonClasses/CommonUtils';


// import BookRequest from '../Home/BookRequest';
// import LocationPicker1 from '../Home/LocationPicker';
// import AddMedia from '../Home/AddMedia';
// import RequestConfirmation from '../Home/RequestConfirmation';

import PrivacyPolicy from '../PrivacyPolicyViews/PrivacyPolicy';
import Support from '../SupportViews/Support';
import AboutUs from '../AboutUs/AboutUs';
import TermsConditions from '../TermsConditions/TermsConditions';
import Settings from '../SettingsViews/Settings';
import ChangePassword from '../SettingsViews/ChangePassword';
import EditProfile from '../SettingsViews/EditProfile';
import Review from '../ReviewView/Review';

import CardList from '../ManagePaymentsViews/CardList';
import AddEditCard from '../ManagePaymentsViews/AddEditCard';

import AccountList from '../ManageAccount/AccountList';
import AddEditAccount from '../ManageAccount/AddEditAccount';

import Wallet from '../WalletViews/Wallet';
import RequestPayout from '../WalletViews/RequestPayout';

import Request from '../RequestViews/Request';
import UpcomingRequests from '../RequestViews/UpcomingRequests';
// import AssignRequests from '../RequestViews/AssignRequests';
import HistoryRequests from '../RequestViews/HistoryRequests';
import HistoryRequestDetails from '../RequestViews/HistoryRequestDetails';
import RequestDetails from '../RequestViews/RequestDetails';
// import SearchRequest from '../RequestViews/SearchRequest';
import PaymentReceipt from '../RequestViews/PaymentReceipt';
import MediaDetails from '../RequestViews/MediaDetails';
import ReviewUser from '../RequestViews/ReviewUser';

import TruckList from '../ManageTrucks/TruckList';
import AddEditTruck from '../ManageTrucks/AddEditTruck';
import ColorPickerView from '../ManageTrucks/ColorPickerView';

import NewRequest from '../CommonClasses/NewRequest';

const FirstActivityStackNavigator = createStackNavigator({
  
  First: {
    screen: Home,
    navigationOptions: ({ navigation }) => ({
      title: 'Home',
      headerLeft: <HamburgerIcon navigationProps={navigation} />,

      headerStyle: {
        backgroundColor: '#000'
      },
      headerTintColor: '#fff',
    })
  },
});

const RequestScreenActivityStackNavigator = createStackNavigator({
  
  RequestScreen: {
      screen: Request,
      // key: 'RequestHome',
      navigationOptions: ({ navigation }) => ({
        title: 'Requests',
        headerLeft: <HamburgerIcon navigationProps={navigation} />,

        headerStyle: {
          backgroundColor: '#000'
        },
        headerTintColor: '#fff',
      })
  },
  UpcomingRequestsScreen: {
      screen: UpcomingRequests,
      key: 'UpcomingRequests',
      navigationOptions: ({ navigation }) => ({
        title: 'Upcoming Requests',
        headerLeft: <HamburgerIcon navigationProps={navigation} />,

        headerStyle: {
          backgroundColor: '#000'
        },
        headerTintColor: '#fff',
      })
  },
  RequestDetailsScreen: {
      screen: RequestDetails,
      navigationOptions: ({ navigation }) => ({
        title: 'Request Details',
        headerLeft: <TouchableOpacity 
        activeOpacity = { 0.8 } 
        style = {{justifyContent: 'center', alignItems: 'center', height: 50, width: 60}} 
        onPress={() => navigation.pop()}
        >
        <Icon name="arrow-left" size={25} color="#fff"/>
      </TouchableOpacity>,

        headerStyle: {
          backgroundColor: '#000'
        },
        headerTintColor: '#fff',
      })
  },
  RequestAccountListScreen: {
      screen: AccountList,
      navigationOptions: ({ navigation }) => ({
        title: 'Account List',
        headerLeft: <TouchableOpacity 
        activeOpacity = { 0.8 } 
        style = {{justifyContent: 'center', alignItems: 'center', height: 50, width: 60}} 
        onPress={() => navigation.pop()}
        >
        <Icon name="arrow-left" size={25} color="#fff"/>
      </TouchableOpacity>,

        headerStyle: {
          backgroundColor: '#000'
        },
        headerTintColor: '#fff',
      })
  },

  // AssignRequestsScreen: {
  //     screen: AssignRequests,
  //     key: 'AssignRequests',
  //     navigationOptions: ({ navigation }) => ({
  //       title: 'Assign Requests',
  //       headerLeft: <HamburgerIcon navigationProps={navigation} />,

  //       headerStyle: {
  //         backgroundColor: '#000'
  //       },
  //       headerTintColor: '#fff',
  //     })
  // },
  HistoryRequestsScreen: {
      screen: HistoryRequests,
      key: 'HistoryRequests',
      navigationOptions: ({ navigation }) => ({
        title: 'History Requests',
        headerLeft: <HamburgerIcon navigationProps={navigation} />,

        headerStyle: {
          backgroundColor: '#000'
        },
        headerTintColor: '#fff',
      })
  },
  HistoryRequestDetailsScreen: {
      screen: HistoryRequestDetails,
      navigationOptions: ({ navigation }) => ({
        title: 'Request Details',
        headerLeft: <TouchableOpacity 
        activeOpacity = { 0.8 } 
        style = {{justifyContent: 'center', alignItems: 'center', height: 50, width: 60}} 
        onPress={() => navigation.pop()}
        >
        <Icon name="arrow-left" size={25} color="#fff"/>
      </TouchableOpacity>,

        headerStyle: {
          backgroundColor: '#000'
        },
        headerTintColor: '#fff',
      })
  },
  ReviewUserScreen: {
      screen: ReviewUser,
      navigationOptions: ({ navigation }) => ({
        title: 'Review User',
        headerLeft: <TouchableOpacity 
        activeOpacity = { 0.8 } 
        style = {{justifyContent: 'center', alignItems: 'center', height: 50, width: 60}} 
        onPress={() => navigation.pop()}
        >
        <Icon name="arrow-left" size={25} color="#fff"/>
      </TouchableOpacity>,

        headerStyle: {
          backgroundColor: '#000'
        },
        headerTintColor: '#fff',
      })
  },
  PaymentReceiptScreen: {
      screen: PaymentReceipt,
      navigationOptions: ({ navigation }) => ({
        title: 'Payment Details',
        headerLeft: <TouchableOpacity 
        activeOpacity = { 0.8 } 
        style = {{justifyContent: 'center', alignItems: 'center', height: 50, width: 60}} 
        onPress={() => navigation.pop()}
        >
        <Icon name="arrow-left" size={25} color="#fff"/>
      </TouchableOpacity>,

        headerStyle: {
          backgroundColor: '#000'
        },
        headerTintColor: '#fff',
      })
  },
  MediaDetailsScreen: {
      screen: MediaDetails,
      navigationOptions: ({ navigation }) => ({
        title: 'Media Details',
        headerLeft: <TouchableOpacity 
        activeOpacity = { 0.8 } 
        style = {{justifyContent: 'center', alignItems: 'center', height: 50, width: 60}} 
        onPress={() => navigation.pop()}
        >
        <Icon name="arrow-left" size={25} color="#fff"/>
      </TouchableOpacity>,

        headerStyle: {
          backgroundColor: '#000'
        },
        headerTintColor: '#fff',
      })
  },
   NotificationRequestDetailsScreen: {
      screen: RequestDetails,
      navigationOptions: ({ navigation }) => ({
        title: 'Request Details',
        headerLeft: <TouchableOpacity 
        activeOpacity = { 0.8 } 
        style = {{justifyContent: 'center', alignItems: 'center', height: 50, width: 60}} 
        onPress={() => navigation.pop()}
        >
        <Icon name="arrow-left" size={25} color="#fff"/>
      </TouchableOpacity>,

        headerStyle: {
          backgroundColor: '#000'
        },
        headerTintColor: '#fff',
      })
  },
  RequestAccountListScreen: {
      screen: AccountList,
      navigationOptions: ({ navigation }) => ({
        title: 'Account List',
        headerLeft: <TouchableOpacity 
        activeOpacity = { 0.8 } 
        style = {{justifyContent: 'center', alignItems: 'center', height: 50, width: 60}} 
        onPress={() => navigation.pop()}
        >
        <Icon name="arrow-left" size={25} color="#fff"/>
      </TouchableOpacity>,

        headerStyle: {
          backgroundColor: '#000'
        },
        headerTintColor: '#fff',
      })
  },

 
});


const ReviewScreenActivityStackNavigator = createStackNavigator({
  ReviewScreen: {
      screen: Review,
      navigationOptions: ({ navigation }) => ({
        title: 'Review',
        headerLeft: <HamburgerIcon navigationProps={navigation} />,

        headerStyle: {
          backgroundColor: '#000'
        },
        headerTintColor: '#fff',
      })
  },
});

const WalletScreenActivityStackNavigator = createStackNavigator({
  WalletScreen: {
      screen: Wallet,
      key: 'WalletHome',
      navigationOptions: ({ navigation }) => ({
        title: 'Wallet',
        headerLeft: <HamburgerIcon navigationProps={navigation} />,

        headerStyle: {
          backgroundColor: '#000'
        },
        headerTintColor: '#fff',
      })
  },
  RequestPayoutScreen: {
      screen: RequestPayout,
      navigationOptions: ({ navigation }) => ({
        title: 'Request Payout',
        headerLeft: <TouchableOpacity 
        activeOpacity = { 0.8 } 
        style = {{justifyContent: 'center', alignItems: 'center', height: 50, width: 60}} 
        onPress={() => navigation.pop()}
        >
        <Icon name="arrow-left" size={25} color="#fff"/>
      </TouchableOpacity>,

        headerStyle: {
          backgroundColor: '#000'
        },
        headerTintColor: '#fff',
      })
  },
});

const TruckScreenActivityStackNavigator = createStackNavigator({
  TruckListScreen: {
      screen: TruckList,
      navigationOptions: ({ navigation }) => ({
        title: 'Truck List',
        headerLeft: <HamburgerIcon navigationProps={navigation} />,

        headerStyle: {
          backgroundColor: '#000'
        },
        headerTintColor: '#fff',
      })
  },
  AddEditTruckScreen: {
      screen: AddEditTruck,
      navigationOptions: ({ navigation }) => ({
        title: 'Add Truck',
        headerLeft: <TouchableOpacity 
        activeOpacity = { 0.8 } 
        style = {{justifyContent: 'center', alignItems: 'center', height: 50, width: 60}} 
        onPress={() => navigation.pop()}
        >
        <Icon name="arrow-left" size={25} color="#fff"/>
      </TouchableOpacity>,

        headerStyle: {
          backgroundColor: '#000'
        },
        headerTintColor: '#fff',
      })
  },
  ColorPickerViewScreen: {
      screen: ColorPickerView,
      navigationOptions: ({ navigation }) => ({
        title: 'Pick Color',
        headerLeft: <TouchableOpacity 
        activeOpacity = { 0.8 } 
        style = {{justifyContent: 'center', alignItems: 'center', height: 50, width: 60}} 
        onPress={() => navigation.pop()}
        >
        <Icon name="arrow-left" size={25} color="#fff"/>
      </TouchableOpacity>,

        headerStyle: {
          backgroundColor: '#000'
        },
        headerTintColor: '#fff',
      })
  },
});

const CardScreenActivityStackNavigator = createStackNavigator({
  CardListScreen: {
      screen: CardList,
      navigationOptions: ({ navigation }) => ({
        title: 'Card List',
        headerLeft: <HamburgerIcon navigationProps={navigation} />,

        headerStyle: {
          backgroundColor: '#000'
        },
        headerTintColor: '#fff',
      })
  },
  AddEditCardScreen: {
      screen: AddEditCard,
      navigationOptions: ({ navigation }) => ({
        title: 'Add Card',
        headerLeft: <TouchableOpacity 
        activeOpacity = { 0.8 } 
        style = {{justifyContent: 'center', alignItems: 'center', height: 50, width: 60}} 
        onPress={() => navigation.pop()}
        >
        <Icon name="arrow-left" size={25} color="#fff"/>
      </TouchableOpacity>,

        headerStyle: {
          backgroundColor: '#000'
        },
        headerTintColor: '#fff',
      })
  },
});

const AccountScreenActivityStackNavigator = createStackNavigator({
  AccountListScreen: {
      screen: AccountList,
      navigationOptions: ({ navigation }) => ({
        title: 'Account List',
        headerLeft: <HamburgerIcon navigationProps={navigation} />,

        headerStyle: {
          backgroundColor: '#000'
        },
        headerTintColor: '#fff',
      })
  },
  AddEditAccountScreen: {
      screen: AddEditAccount,
      navigationOptions: ({ navigation }) => ({
        title: 'Add Account',
        headerLeft: <TouchableOpacity 
        activeOpacity = { 0.8 } 
        style = {{justifyContent: 'center', alignItems: 'center', height: 50, width: 60}} 
        onPress={() => navigation.pop()}
        >
        <Icon name="arrow-left" size={25} color="#fff"/>
      </TouchableOpacity>,

        headerStyle: {
          backgroundColor: '#000'
        },
        headerTintColor: '#fff',
      })
  },
});

const SettingsScreenActivityStackNavigator = createStackNavigator({
  SettingsScreen: {
      screen: Settings,
      navigationOptions: ({ navigation }) => ({
        title: 'Account Settings',
        headerLeft: <HamburgerIcon navigationProps={navigation} />,

        headerStyle: {
          backgroundColor: '#000'
        },
        headerTintColor: '#fff',
      })
  },
  ChangePasswordScreen: {
      screen: ChangePassword,
      navigationOptions: ({ navigation }) => ({
        title: 'Change Password',
        headerLeft: <TouchableOpacity 
        activeOpacity = { 0.8 } 
        style = {{justifyContent: 'center', alignItems: 'center', height: 50, width: 60}} 
        onPress={() => navigation.pop()}
        >
        <Icon name="arrow-left" size={25} color="#fff"/>
      </TouchableOpacity>,

        headerStyle: {
          backgroundColor: '#000'
        },
        headerTintColor: '#fff',
      })
  },
  EditProfileScreen: {
      screen: EditProfile,
      navigationOptions: ({ navigation }) => ({
        title: 'Edit Profile',
        headerLeft: <TouchableOpacity 
        activeOpacity = { 0.8 } 
        style = {{justifyContent: 'center', alignItems: 'center', height: 50, width: 60}} 
        onPress={() => navigation.pop()}
        >
        <Icon name="arrow-left" size={25} color="#fff"/>
      </TouchableOpacity>,

        headerStyle: {
          backgroundColor: '#000'
        },
        headerTintColor: '#fff',
      })
  },
});

const PrivacyPolicyScreenActivityStackNavigator = createStackNavigator({
  PrivacyPolicyScreen: {
    screen: PrivacyPolicy,
    navigationOptions: ({ navigation }) => ({
      title: 'Privacy Policy',
      headerLeft: <HamburgerIcon navigationProps={navigation} />,

      headerStyle: {
        backgroundColor: '#000'
      },
      headerTintColor: '#fff',
    })
  },
});

const SupportScreenActivityStackNavigator = createStackNavigator({
  SupportScreen: {
    screen: Support,
    navigationOptions: ({ navigation }) => ({
      title: 'Support',
      headerLeft: <HamburgerIcon navigationProps={navigation} />,

      headerStyle: {
        backgroundColor: '#000'
      },
      headerTintColor: '#fff',
    })
  },
});

const AboutUsScreenActivityStackNavigator = createStackNavigator({
  AboutUsScreen: {
    screen: AboutUs,
    navigationOptions: ({ navigation }) => ({
      title: 'About Us',
      headerLeft: <HamburgerIcon navigationProps={navigation} />,

      headerStyle: {
        backgroundColor: '#000'
      },
      headerTintColor: '#fff',
    })
  },
});

const TermsConditionsScreenActivityStackNavigator = createStackNavigator({
  TermsConditionsScreen: {
    screen: TermsConditions,
    navigationOptions: ({ navigation }) => ({
      title: 'Terms & Conditions',
      headerLeft: <HamburgerIcon navigationProps={navigation} />,

      headerStyle: {
        backgroundColor: '#000'
      },
      headerTintColor: '#fff',
    })
  },
});

const NotificationRequestScreenActivityStackNavigator = createStackNavigator({
  
   NotificationRequestDetailsScreen: {
      screen: RequestDetails,
      navigationOptions: ({ navigation }) => ({
        title: 'Request Details',
        headerLeft: 
        <TouchableOpacity 
        activeOpacity = { 0.8 } 
        style = {{justifyContent: 'center', alignItems: 'center', height: 50, width: 60}} 
        onPress={() =>
        { 
          const navigateAction = StackActions.reset({
                  index: 0,
                  actions: [NavigationActions.navigate({ routeName: "RequestScreen"})],
                });
                CommonUtilsObj.navigationProps.navigation.dispatch(navigateAction);
        }}
        >
        <Icon name="arrow-left" size={25} color="#fff"/>
      </TouchableOpacity>,

        headerStyle: {
          backgroundColor: '#000'
        },
        headerTintColor: '#fff',
      })
  },
});

const ModalScreenActivityStackNavigator = createStackNavigator({
  MyModal: {
    screen: NewRequest,
    navigationOptions: {
      header: null,
      headerStyle: {
        backgroundColor: 'transparent'
      },
    },
  },

},
{
    mode: 'modal',
    transparentCard: true,
    cardStyle: {
      backgroundColor: 'transparent',
      // opacity: 1,
    },
    transitionConfig : () => ({
      containerStyle: {
        backgroundColor: 'transparent',
      }
    }),
    headerMode: 'none',
  }
);

const RootStack = createStackNavigator(
  {
    First: {
      screen: FirstActivityStackNavigator,
    },
    RequestScreen: {
      screen: RequestScreenActivityStackNavigator,
    },
    ReviewScreen: {
      screen: ReviewScreenActivityStackNavigator,
    },
    WalletScreen: {
      screen: WalletScreenActivityStackNavigator,
    },
    TruckListScreen: {
      screen: TruckScreenActivityStackNavigator,
    },
    SettingsScreen: {
      screen: SettingsScreenActivityStackNavigator,
    },
    CardListScreen: {
      screen: CardScreenActivityStackNavigator,
    },
    AccountListScreen: {
      screen: AccountScreenActivityStackNavigator,
    },
    PrivacyPolicyScreen: {
      screen: PrivacyPolicyScreenActivityStackNavigator,
    },
    AboutUsScreen: {
      screen: AboutUsScreenActivityStackNavigator,
    },
    SupportScreen: {
      screen: SupportScreenActivityStackNavigator,
    },
    TermsConditionsScreen: {
      screen: TermsConditionsScreenActivityStackNavigator,
    },
    NotificationRequestDetailsScreen: {
      screen: NotificationRequestScreenActivityStackNavigator,
    },    
    MyModal: {
      screen: ModalScreenActivityStackNavigator,
    },
  },
  {
    mode: 'modal',
    transparentCard: true,
    cardStyle: {
      backgroundColor: 'transparent',
      // opacity: 1,
  },
  transitionConfig : () => ({
    containerStyle: {
      backgroundColor: 'transparent',
    }
  }),
    headerMode: 'none',
  }
);

const FirstActivity_StackNavigator = createAppContainer(RootStack);
export default FirstActivity_StackNavigator;

