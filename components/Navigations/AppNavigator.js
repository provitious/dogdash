import React, { Component } from 'react';
import { StyleSheet, Alert } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { Text, View, Button, TextInput, Image, ScrollView, TouchableHighlight, TouchableOpacity, PixelRatio, FlatList, Linking, ImageBackground, Share, Modal, Dimensions } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';

import Constant from '../CommonClasses/Constant';
import Login from '../Login/Login';
import ForgotPassword from '../Login/ForgotPassword';
import Register from '../Login/Register';
import OTPVerification from '../Login/OTPVerification';
import AddAccount from '../Login/AddAccount';
import TermsConditions from '../TermsConditions/TermsConditions';

const NavigationStack = createStackNavigator({
  Login: { 
  	screen: Login,
  	navigationOptions: {
      header: null,
      headerStyle: {
        backgroundColor: '#000'
      },
    },
  },
  Register: { 
    screen: Register,
    navigationOptions: ({ navigation }) => ({
      title: 'Register',
      headerLeft: <TouchableOpacity 
        activeOpacity = { 0.8 } 
        style = {{justifyContent: 'center', alignItems: 'center', height: 50, width: 60}} 
        onPress={() => navigation.pop()}
        >
        <Icon name="arrow-left" size={25} color="#fff"/>
      </TouchableOpacity>,
      headerStyle: {
        backgroundColor: '#000'
      },
      headerTintColor: '#fff',
    })  
  },
  OTPVerification: { 
    screen: OTPVerification,
    navigationOptions: ({ navigation }) => ({
      title: 'OTP Verification',
      headerLeft: <TouchableOpacity 
        activeOpacity = { 0.8 } 
        style = {{justifyContent: 'center', alignItems: 'center', height: 50, width: 60}} 
        onPress={() => navigation.pop()}
        >
        <Icon name="arrow-left" size={25} color="#fff"/>
      </TouchableOpacity>,
      headerStyle: {
        backgroundColor: '#000'
      },
      headerTintColor: '#fff',
    })  
  },
  ForgotPassword: { 
    screen: ForgotPassword,
    navigationOptions: ({ navigation }) => ({
      title: 'Forgot Password?',
      headerLeft: <TouchableOpacity 
        activeOpacity = { 0.8 } 
        style = {{justifyContent: 'center', alignItems: 'center', height: 50, width: 60}} 
        onPress={() => navigation.pop()}
        >
        <Icon name="arrow-left" size={25} color="#fff"/>
      </TouchableOpacity>,
      headerStyle: {
        backgroundColor: '#000'
      },
      headerTintColor: '#fff',
    })  
  },
  TermsConditionsScreen: {
    screen: TermsConditions,
    navigationOptions: ({ navigation }) => ({
      title: 'Terms & Conditions',
      headerLeft: <TouchableOpacity 
        activeOpacity = { 0.8 } 
        style = {{justifyContent: 'center', alignItems: 'center', height: 50, width: 60}} 
        onPress={() => navigation.pop()}
        >
        <Icon name="arrow-left" size={25} color="#fff"/>
      </TouchableOpacity>,

      headerStyle: {
        backgroundColor: '#000'
      },
      headerTintColor: '#fff',
    })
  },
  AddAccountScreen: {
      screen: AddAccount,
      navigationOptions: ({ navigation }) => ({
        title: 'Add Account',
        headerLeft: <TouchableOpacity 
        activeOpacity = { 0.8 } 
        style = {{justifyContent: 'center', alignItems: 'center', height: 50, width: 60}} 
        onPress={() => navigation.pop()}
        >
        <Icon name="arrow-left" size={25} color="#fff"/>
      </TouchableOpacity>,
        headerStyle: {
          backgroundColor: '#000'
        },
        headerTintColor: '#fff',
      })
  },
});

const Container = createAppContainer(NavigationStack);
export default Container;