import React from 'react';
import Constant from './Constant';
import { Platform } from 'react-native';
import CommonUtilsObj from './CommonUtils';
// import Geolocation from 'react-native-geolocation-service'; //'@react-native-community/geolocation';
// import Geocoder from 'react-native-geocoding';

class APIManager {

  static sharedManager = null;
  
  static getInstance() {
      if (APIManager.sharedManager == null) {
          APIManager.sharedManager = new APIManager();
      }

      return this.sharedManager;
  }

  async callApi(url) {

    let response = await this.onFetchDataResponse(url).then(
        function (responseData) {
          
          console.log('ddddddddContents: ' + responseData);
          return responseData;
        }.bind(this),
        function (reason) {
          
          setTimeout(() => {
            // Toast.showWithGravity(`sss${reason.message}`, Toast.LONG, Toast.TOP)
          }, 200);
          console.log('error ' + reason.message);
          return reason.message;
          
        }.bind(this));

    return response
  }

  async onFetchDataResponse (url) {

      console.log(url);

      return new Promise(
        function (resolve, reject) {
          fetch(
            url,
            {
              method: "POST",
              headers: CommonUtilsObj.customHeaders,
           }
          )
          .then(
            function(response) {
              console.log('responseText json.....' + JSON.stringify(response));
              if (response.ok) {    
                response.json().then((responseData) => {
                  resolve(responseData);
                });
              }
              else {
                console.log('responseText json.....' + JSON.stringify(response));
                response.json().then((responseData) => {
                  // console.log('new ......responseText error' + JSON.stringify(responseData));
                  // reject(responseData);
                  resolve(responseData);
                  // resolve(responseData);
                });
                
                // reject(new Error(`Unable to retrieve events.\nInvalid response received - (${response.status}).`));
              }
            }.bind(this)
          )
          .catch(
            function(error) {
              reject(new Error(`Network Unable to retrieve events.\n${error.message}`));
            }
          );
        }.bind(this)
      );
  }

  async callApiWithData(url, params) {

    let response = await this.onFetchDataResponseWithParmas(url, params).then(
        function (responseData) {
          
          // console.log('ddddddddContents: ' + responseData);
          return responseData;
        }.bind(this),
        function (reason) {
          
          setTimeout(() => {
            // Toast.showWithGravity(`sss${reason.message}`, Toast.LONG, Toast.TOP)
          }, 200);
          console.log('error ' + reason.message);
          return reason.message;
          
        }.bind(this));

    return response
  }

  async onFetchDataResponseWithParmas (url, params) {

      console.log(url);

      return new Promise(
        function (resolve, reject) {
          fetch(
            url,
            {
              method: "POST",
              headers: CommonUtilsObj.customHeaders,
              body: params,
           }
          )
          .then(
            function(response) {
              
              if (response.ok) {    
                response.json().then((responseData) => {
                  console.log('responseText json.....' + JSON.stringify(response));
                  resolve(responseData);
                });
              }
              else {
                response.json().then((responseData) => {
                  // console.log('new ......responseText error' + JSON.stringify(responseData));
                  // reject(responseData);
                  resolve(responseData);
                  // resolve(responseData);
                });
                console.log('responseText error' + JSON.stringify(response));
                // reject(new Error(`Unable to retrieve events.\nInvalid response received - (${response.status}).`));
              }
            }.bind(this)
          )
          .catch(
            function(error) {
              console.log('Network Unable to retrieve events');
              reject(new Error(`Network Unable to retrieve events.\n${error.message}`));
            }
          );
        }.bind(this)
      );
    
  }
}

let APIManagerObj = APIManager.getInstance();
export default APIManagerObj;

