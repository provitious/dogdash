class Constant {
    static KBaseURL  = "http://103.50.212.135/dogdash/index.php/web_service/"
    static KBaseDownloadURL = "http://103.50.212.135/dogdash/"
    static KNodeBaseURL = "http://103.50.212.135:1337/api/"
    static KSocketURL = "http://103.50.212.135:3000/"

    // static KBaseURL  = "http://mydogdash.com/dogdash/web_service/"
    // static KBaseDownloadURL = "http://mydogdash.com/dogdash/"
    // static KNodeBaseURL = "http://132.148.246.249:1337/api/"
    // static KSocketURL = "http://132.148.246.249:3000/"

    static KCarImagePathURL = Constant.KBaseDownloadURL + "car_image/"
    static KUserImagePathURL = Constant.KBaseDownloadURL + "user_image/"
    static KDriverImagePathURL = Constant.KBaseDownloadURL + "driverimages/"
    static KMediaPathURL = Constant.KBaseDownloadURL + "book_media/"

    // Login/Register

    static KLoginURL = Constant.KBaseURL  + "driver_login"
    static KSignUpURL = Constant.KBaseURL  + "driver_sign_up"
    static KGetCityListURL = Constant.KBaseURL  + "getcity"
    static KGetStateListURL = Constant.KBaseURL  + "getstates"
    // static KCheckBeforeSignUp = Constant.KBaseURL  + "chk_before_signup"
    static KLogoutURL = Constant.KBaseURL  + "logout_driver"
    static KForgotPasswordURL = Constant.KBaseURL  + "driver_forgot_password"
    static KChangePasswordURL = Constant.KBaseURL  + "driver_change_password"
    static KProfileEditURL = Constant.KBaseURL  + "driver_profile_edit"
    static KManageDriverStatusURL = Constant.KNodeBaseURL + "manage-driver"
    static KCheckCurrentBookingURL = Constant.KBaseURL + "getrunningtrips"

    // All Request

    static KGetUpcomingRequestListURL = Constant.KBaseURL  + "driver_upcoming_bookings"
    static KFilterRequestListURL = Constant.KBaseURL  + ""
    static KGetAssignJobListURL = Constant.KBaseURL  + "driver_assigned_jobs"
    static KGetRequestHistoryListURL = Constant.KBaseURL  + "driver_history_bookings"
    static KRejectRequestURL = Constant.KBaseURL  + "driver_reject_trip"
    static KAcceptRequestURL = Constant.KBaseURL  + "driver_accept_trip"
    static KArrivedAtPickUpURL = Constant.KBaseURL  + "driver_arrived_trip"
    static KOnTripURL = Constant.KBaseURL  + "driver_on_trip"
    static KCompleteTripURL = Constant.KBaseURL  + "driver_completed_trip"
    static KRequestDetailURL = Constant.KBaseURL  + "single_trip"

    // Request History

    static KHistoryJobRequests = Constant.KBaseURL  + "driver_history_bookings"

    // Privacy Policy, Support, About Us, Terms & Conditions

    static KPrivacyPolicyURL = Constant.KBaseURL  + "driver_privacy_policy"
    static KsupportURL = Constant.KBaseURL  + "driver_support"
    static KAboutUsURL = Constant.KBaseURL  + "driver_aboutus"
    static KTermsConditionURL = Constant.KBaseURL  + "driver_terms_conditions"

    // Manage Payment

    static KAddCardDetailsURL = Constant.KBaseURL  + "driver_add_card_details"
    static KGetCardListURL = Constant.KBaseURL  + "drivergetcardslist"
    static KEditCardDetailsURL = Constant.KBaseURL  + "driver_edit_card_details"
    static KDeleteCardURL = Constant.KBaseURL  + "delete_driver_card"
    static KSetPrimaryCardURL = Constant.KBaseURL  + "driver_update_primary_card"
    static KAddMoneyToWalletURL = Constant.KBaseURL  + "driver_add_to_wallet"
    static KRequestPayoutURL = Constant.KBaseURL  + "request_payout"

    // Manage Bank Account

    static KGetAccountListURL = Constant.KBaseURL  + "drivergetbanklist"
    static KAddEditAccountURL = Constant.KBaseURL  + "driver_add_bank_details"
    static KDeleteAccountURL = Constant.KBaseURL  + "delete_driver_bank"
    static KSetPrimaryAccountURL = Constant.KBaseURL  + "driver_update_primary_bank"

    // Wallet

    static KGetWalletBalanceURL = Constant.KBaseURL  + "getdrivercurrentbalance"
    static KGetAllTransactionListURL = Constant.KBaseURL  + "driver_transactions"
    static KGetDebitTransactionListURL = Constant.KBaseURL  + "driver_debit_transactions"
    static KGetCreditTransactionListURL = Constant.KBaseURL  + "driver_credit_transactions"

    // Review & rating

    static KGetReviewRatingURL = Constant.KBaseURL  + "get_driver_rate"
    static KReviewUserURL = Constant.KBaseURL  + "user_rate"

    // Manage Truck

    static KGetCabListURL = Constant.KBaseURL  + "car_type"
    static KGetTruckListURL = Constant.KBaseURL  + "get_driver_cars"
    static KAddTruckDetailsURL = Constant.KBaseURL  + "driver_cars"
    static KEditTruckDetailsURL = Constant.KBaseURL  + "update_driver_car"
    static KDeleteTruckURL = Constant.KBaseURL  + "delete_driver_car"
    static KSetCurrentTruckURL = Constant.KBaseURL  + "set_current_car"

    // Find Job

    static KGetJobListURL = Constant.KBaseURL  + "find_jobs"
    static KSetJobReceivedTimeURL = Constant.KBaseURL  + "set_job"

    // Constant Declaration

    static KRememberPassword = "storedPassword"
    static KRememberEmail = "storedEmail"

    static KAppTitle = "DogDash"

    static KAppColor = "#8edf01"
    static KAppColor1 = "#1b1c1c"

    static KThemeBGColor = "#000"
    static KThemeBtnColor = "#8edf01"
    static KThemeFontColor = "#000"
    static KThemeFontColor1 = "gray"
    static KThemeFontColor2 = "#fff"

    static KThemeNavigationBGColor = "#8edf01"
    static KThemeNavigationFontColor = "#000"

    static KOffline = "Not connected"
    static KWifi = "Connected via WiFi"
    static KOnline = "Connected via WWAN"
    static KCheckInternetConnection = "Please check internet connection."

    static KError = "Something went wrong..."
    static KLogin = "Login"
    static KLogout = "Logout"

    static KDriverId = "id"
    static KFaceBookLogin = "FaceBook"
    static KGmailLogin = "Gmail"
    static KAppLogin = "Login"
    static KUserDetailsKey = "LoggedUserDetails"
    static KLoginStatusKey = "LoginStatus"
    static KRememberMeKey = "RememberMeData"

    static KGoogleMapAPIKey = "AIzaSyBN5r-VhVvzoJp8H_O8M7pG_CVhKRbdjdY"

    //  Request Status

    static KRequestAccepted = "Accepted"
    static KRequestRejected = "Rejected"
    static KRequestArrived = "Arrived"
    static KRequestOnWay = "OnTheWay"
    static KRequestBegin = "Begin"
    static KRequestEnd = "End"

    static KSuccess = "success"
    static KFail = "failed"
    static KTrue = "true"
    static KFalse = "false"

    static KThemeFontBlack = "Montserrat-Black"
    static KThemeFontBold = "Montserrat-Bold"
    static KThemeFontExtraBold = "Montserrat-ExtraBold"
    static KThemeFontHairline = "Montserrat-Hairline"
    static KThemeFontLight = "Montserrat-Light"
    static KThemeFontRegular = "Montserrat-Regular"
    static KThemeFontSemiBold = "Montserrat-SemiBold"
    static KThemeFontThin = "Montserrat-Thin"
    static KThemeFontUltraLight = "Montserrat-UltraLight"
    static KThemeColor = "#29ACE4"

}

export default Constant;