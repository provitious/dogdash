import React from 'react';
import { StyleSheet, Text, View, Modal, TouchableOpacity,Dimensions } from 'react-native';
import userDefaults from 'react-native-user-defaults'
import Constant from './Constant';
import DefaultPreference from 'react-native-default-preference';
import { Platform } from 'react-native';

class CommonUtils {

    static sharedManager = null;

    rememberMeList = [];
    deviceToken = '123456789';
    loggedUserData = [];
    _loginStatus = '';

    userLoginStatus = "";
    username = "";
    password = "";

    status = "";
    message = "";
    errorCode = "";
    userDetails = [];
    cabDetails = [];
    timeDetails = [];
    favouritePlaceList = [];

    driverLatitude = 0;
    driverLongitude = 0;
    driverDirection = 0;
    goToTripDetails = false;
    notificationBookingDetailsObj = {};
    notificationRequestDetails = {};
    requestStatus = Constant.KRequestEnd;
    rootScreenName = 'Home';
    navigationProps = '';
    customHeaders = [];
    btnHeight = (((Dimensions.get('window').width) * 10.5) / 100);
    newRequestDetails = '';

    static getInstance() {
        if (CommonUtils.sharedManager == null) {
            CommonUtils.sharedManager = new CommonUtils();
        }

        return this.sharedManager;
    }

    setLoginState(data) {
        CommonUtilsObj.userLoginStatus =data;
        
        if (Platform.OS === 'android')
        {
            DefaultPreference.set(Constant.KLoginStatusKey, data)
            .then(function() {console.log('done saved')})
            .catch(function (err) {
              console.log(err);
            });
        }
        else {
            userDefaults.set(Constant.KLoginStatusKey, data)
            .then(data => console.log('login status saved successfully!'))
        }
    }

    setLoggedUserDetails(data) {
        CommonUtilsObj.userLoginStatus = Constant.KLogin;
        
        if (Platform.OS === 'android')
        {
            DefaultPreference.set(Constant.KUserDetailsKey, data)
            .then(data => this.getLoggedUserDetails()) 
            .catch(function (err) {
              console.log(err);
            });
        }
        else {
            userDefaults.set(Constant.KUserDetailsKey, data)
            .then(data => this.getLoggedUserDetails()) 
        }
    }

    getLoggedUserDetails() {
      if (Platform.OS === 'android')
      {
          DefaultPreference.get(Constant.KUserDetailsKey)
          .then(function(data) {
            let obj = JSON.parse(data);
            let userData = JSON.stringify(obj.Driver_detail[0]);
            let countryData = JSON.stringify(obj.country_detail[0]);
            let cabData = JSON.stringify(obj.current_car[0]);
            console.log(obj);

            CommonUtilsObj.loggedUserData = obj;
            CommonUtilsObj.status = obj.status;
            CommonUtilsObj.message = obj.message;
            CommonUtilsObj.errorCode = "";
            CommonUtilsObj.userDetails = JSON.parse(userData);
            CommonUtilsObj.cabDetails = JSON.parse(cabData);
            CommonUtilsObj.countryDetails = JSON.parse(countryData);
            CommonUtilsObj.customHeaders = {
              "Accept": "application/json",
              "Content-Type": "multipart/form-data",
              // "Authorization" : 'Bearer ' + CommonUtilsObj.authenticationToken,
            };
            console.log("driver data set is....................." + JSON.stringify(CommonUtilsObj.userDetails));

            EventBus.getInstance().fireEvent("ProfileChangeEvent", {
                    
                  });
            return this.loggedUserData;
          })
          .catch(function (err) {
            console.log(err);
          });
      }
      else {

         userDefaults.get(Constant.KUserDetailsKey)
        .then(data => {
            let obj = JSON.parse(data);
            let userData = JSON.stringify(obj.Driver_detail[0]);
            let countryData = JSON.stringify(obj.country_detail[0]);
            let cabData = JSON.stringify(obj.current_car[0]);

            CommonUtilsObj.loggedUserData = obj;
            CommonUtilsObj.status = obj.status;
            CommonUtilsObj.message = obj.message;
            CommonUtilsObj.errorCode = "";
            CommonUtilsObj.userDetails = JSON.parse(userData);
            CommonUtilsObj.cabDetails = JSON.parse(cabData);
            CommonUtilsObj.countryDetails = JSON.parse(countryData);
            CommonUtilsObj.customHeaders = {
              "Accept": "application/json",
              "Content-Type": "multipart/form-data",
              // "Authorization" : 'Bearer ' + CommonUtilsObj.authenticationToken,
            };
            console.log("driver data set is....................." + JSON.stringify(CommonUtilsObj.userDetails));
            // console.log(obj);
            return this.loggedUserData;
        })
        .catch(function (err) {
          return err;
        });
        
      }
      
    }

    getLoginStatus() {

      if (Platform.OS === 'android')
      {
          DefaultPreference.get(Constant.KLoginStatusKey)
          .then(function(value) {
            console.log('getting login...................' + value);
            CommonUtilsObj.userLoginStatus = value;
          })
          .catch(function (err) {
            console.log(err);
          });
      }
      else {
          userDefaults.get(Constant.KLoginStatusKey)
          .then(data => {
              CommonUtilsObj.userLoginStatus = data;
              return this.userLoginStatus;
          })
          .catch(function (err) {
            return err;
          });
      }

    }

  Capitalize(str){
    return str.charAt(0).toUpperCase() + str.slice(1);
  }

  getFormatedDate(date){

    var day, month, hour, year, minutes, seconds;
    day = date.getDate(); 
    month = date.getMonth() + 1;
    year = date.getFullYear();

    // return year.toString() + '-' + month.toString() + '-' + day.toString();
    return month.toString() + '-' + day.toString() + '-' + year.toString(); 
  }

  renderNewRequestModal() {
      <Modal
            animationType="slide" 
            transparent={false}
            visible={true}>
            <View style={{ flex: 1, backgroundColor: Constant.KAppColor1 }}>
              
                <Text style={{fontSize: 18, color: 'white', fontWeight: 'bold', marginTop: 40, marginBottom: 20, alignSelf: 'center', textAlign: 'center'}}>
                  Select country code
                </Text>
              <View style={{ flex: 10}}>
                
              </View>
              <TouchableOpacity
                style={styles.closeButtonStyle}>
                <Text style={{backgroundColor: Constant.KAppColor1, fontSize: 20, color: 'white', fontWeight: 'bold', paddingLeft: 40, paddingRight:40, paddingTop: 8, paddingBottom:8, borderRadius:5}}>
                  Close
                </Text>
              </TouchableOpacity>
            </View>
      </Modal>
  }
    
}
const styles = StyleSheet.create({
  textStyle: {
    fontSize: 20,
    color: 'black',
    fontWeight: 'bold'
  },
  countryStyle: {
    flex: 1,
    borderTopColor: 'gray',
    borderTopWidth: 0.5,
    padding: 3,
    backgroundColor: 'white'
  },
  closeButtonStyle: {
    flex: 1,
    padding: 2,
    alignItems: 'center', 
    justifyContent: 'center',
    backgroundColor: 'white'
  }
})
let CommonUtilsObj = CommonUtils.getInstance();
export default CommonUtilsObj;

