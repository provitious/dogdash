
import {StyleSheet, Dimensions, PixelRatio} from 'react-native'

import Constant from './Constant';
import CommonUtilsObj from './CommonUtils';

const txtInputFontSize = (((Dimensions.get('window').width) * 4.1) / 100);
const txtInputHeight = (((Dimensions.get('window').width) * 9.8) / 100);
const btnHeight = (((Dimensions.get('window').width) * 10.5) / 100);
const btnFontSize = (((Dimensions.get('window').width) * 4.2) / 100);
const imageMargin = (((Dimensions.get('window').width) * 36.5) / 100);
const imageSize = (((Dimensions.get('window').width) * 22) / 100);

export default StyleSheet.create({
  container: {
    flex:1, 
    backgroundColor: Constant.KThemeBGColor,
    alignItems: 'center',
  },
  container1: {
    flex:1, 
    backgroundColor: Constant.KThemeBGColor,
  },
  logoImage: {
      width: '50%', 
      height: '40%', 
      alignSelf: 'center', 
  },
  centerContainer: {
    margin: 20,
    width: '100%',
  },
  flexColunm:{
      flexDirection: 'column',
      margin: 10,
  },
  flexRow:{
      flexDirection: 'row'
  },
  textInput: {
      // height: txtInputHeight,
      paddingVertical: 7,
      fontSize: txtInputFontSize,
      fontFamily: Constant.KThemeFontRegular,
      backgroundColor: 'white',
      paddingLeft: 5,
      borderRadius :5,
      marginTop:15,
  },
  themeButton:{
      marginTop:35,
      borderRadius:5,
      height: CommonUtilsObj.btnHeight,
      backgroundColor: Constant.KThemeBtnColor,
      justifyContent: 'center',
      alignItems: 'center',
      width: Dimensions.get('window').width-40,
      alignSelf: 'center',
      // paddingVertical: 15,
  },
  themeButton1:{
      borderRadius:5,
      height: CommonUtilsObj.btnHeight,
      backgroundColor: Constant.KThemeBtnColor,
      justifyContent: 'center',
      alignItems: 'center',
      width: Dimensions.get('window').width-40,
      // paddingVertical: 15,
  },
  themeButtonText:{
      color: Constant.KThemeFontColor,
      textAlign: 'center',
      // margin: 5,
      fontSize: btnFontSize,
      fontFamily: Constant.KThemeFontSemiBold,
      // paddingVertical: 10,
  },
  otherText: {
      height: 50,
      fontSize: txtInputFontSize,
      fontFamily: Constant.KThemeFontRegular,
      color: Constant.KThemeFontColor2,
  },
  topContainer: {
      // flex: 1,
      flexDirection: 'row',
      marginTop: 20,
  },
  btnCheck:{
      height:25,
      width:25,
      borderRadius:10,
  },
  rememberMeText: {
      marginTop: 0,
      fontSize: txtInputFontSize,
      fontFamily: Constant.KThemeFontRegular,
      color: Constant.KThemeFontColor1,
      height: 50,
   },
  forgotPWBtn:{
      height: 50,
      fontSize: txtInputFontSize,
      fontFamily: Constant.KThemeFontRegular,
      color: Constant.KThemeFontColor1,
  },
  ForgotPasswordText: {
      fontSize: txtInputFontSize,
      fontFamily: Constant.KThemeFontRegular,
      color: Constant.KThemeFontColor1,
   },
  rememberMeContainer: {
      marginTop:20,
      flex: 1,
      flexDirection: "row",
      display: "flex",
  },
  rigsterNowContainer: {
      marginTop : 20,
      flexDirection: "row",
      alignItems: "center",
      justifyContent: "center",
  },
  textBoxBtnHolder:
  {
      position: 'relative',
      alignSelf: 'stretch',
  },
  visibilityBtn:
  {
      position: 'absolute',
      right: 3,
      height: 30,
      width: 30,
      marginTop: 20
  },
  btnImage:
  {
      resizeMode: 'contain',
      height: '100%',
      width: '100%'
  },
  titleText:{
      marginTop: 30,
      color:'white',
      textAlign: 'center',
      fontSize: txtInputFontSize,
      fontFamily: Constant.KThemeFontLight,
  },
  descGrayText: {
      textAlign: 'center',
      fontSize: txtInputFontSize,
      fontFamily: Constant.KThemeFontLight,
      color: 'gray',
  },
  avatarContainer: {
    borderColor: '#9B9B9B',
    borderWidth: 1 / PixelRatio.get(),
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: imageMargin,
    marginLeft: imageMargin,
    marginTop: 15
  },
  avatar: {
    borderRadius: imageSize/2,
    width: imageSize,
    height: imageSize,
  },
  birhDateBtn: {
    height: CommonUtilsObj.btnHeight,
    backgroundColor: '#fff',
    paddingLeft: 5,
    borderRadius :5,
    marginTop:15,
    justifyContent: 'center',
  },
  legalDocContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 5,
    alignSelf: 'center',
  },
  legalDoc: {
    width: '90%',
    height: (((Dimensions.get('window').width) * 35) / 100),
  },
  otpTextInput: {
    height: txtInputHeight,
    width : txtInputHeight,
    fontSize: txtInputFontSize,
    fontFamily: Constant.KThemeFontRegular,
    backgroundColor: 'white',
    padding: 5,
    borderRadius :txtInputHeight/2,
    margin: 4,
    textAlign: 'center',
  },
  resendBtn: {
    fontSize: (((Dimensions.get('window').width) * 5) / 100),
    fontFamily: Constant.KThemeFontRegular,
    color:'#fff', 
    // textDecorationLine: 'underline',
    width: 80,
    marginLeft: (Dimensions.get('window').width) - 120,
    marginTop: 12 
  },
  flexRowStyle1:{
      flexDirection: 'row',
      height: 0,
  },
  flexRowStyle2:{
      flexDirection: 'row',
      height: 45,
      backgroundColor: '#000'
  },
  sectionStyle1: {
    fontWeight: 'bold', 
    height: 0, 
    backgroundColor: 'gray'
  },
  sectionStyle2: {
    fontWeight: 'bold', 
    height: 50, 
    backgroundColor: 'gray'
  },
  mediumFont: {
    fontSize: txtInputFontSize+1,
    fontFamily: Constant.KThemeFontRegular,
    color: '#fff',
    alignSelf: 'center',
 },
 smallFont: {
    fontSize: txtInputFontSize-1,
    fontFamily: Constant.KThemeFontRegular,
    color: 'gray',
    alignSelf: 'center',
},
margin10: {
  marginTop:10,
},
headerFont: {
  fontSize: txtInputFontSize+3,
  fontFamily: Constant.KThemeFontSemiBold,
  color: '#000',
},
headerSubFont: {
  fontSize: txtInputFontSize+2,
  fontFamily: Constant.KThemeFontRegular,
  color: '#000',
},
height0: {
  height:0,
},
avatarReview: {
  borderRadius: imageSize/2,
  width: imageSize,
  height: imageSize,
  marginTop: 25,
  alignSelf: 'center',
},
noteFont: {
  fontSize: txtInputFontSize-2,
  fontFamily: Constant.KThemeFontRegular,
  color: 'gray',
  justifyContent: 'center',
  position: 'absolute',
  bottom: 40 + btnHeight,
  padding: 20,
  textAlign: 'center',
},
themeButtonAbsolute:{
  marginTop:35,
  borderRadius:5,
  height: CommonUtilsObj.btnHeight,
  backgroundColor: '#8edf01',
  justifyContent: 'center',
  alignItems: 'center',
  bottom: 20,
  position: 'absolute',
  width: '90%',
  margin: 20,
  // paddingVertical: 10,
},
plusBtnText:{
  color:'#222',
  textAlign: 'center',
  margin: 5,
  alignSelf: 'center',
  fontSize: txtInputFontSize+17,
  marginTop: 0,
  fontFamily: Constant.KThemeFontSemiBold,
  // height: btnHeight-3,
  // paddingVertical: -2,
},
emptyContainer: {
  alignItems: 'center',
  justifyContent: 'center',
  height: (Dimensions.get('window').height) - (120 + btnHeight + 50),
},
emptyText: {
  fontSize: (((Dimensions.get('window').width) * 5.5) / 100),
  fontFamily: Constant.KThemeFontSemiBold,
  color: '#fff',
},
standalone: {
  // marginTop: 5,
  // marginBottom: 5,
  // marginHorizontal: 15,
},
standaloneRowFront: {
  backgroundColor: '#000',
  // height: 50,
  marginVertical: 5,
},
standaloneRowBack: {
  alignItems: 'center',
  backgroundColor: '#000',
  flex: 1,
  flexDirection: 'row',
  justifyContent: 'flex-end',
  marginVertical: 5,
},
btnFont: {
  fontSize: txtInputFontSize+1,
  fontFamily: Constant.KThemeFontLight,
  color: '#fff',
  marginLeft: 15,
  alignSelf: 'center'
},
walletEmptyContainer: {
  alignItems: 'center',
  justifyContent: 'center',
  height: (((Dimensions.get('window').height) * 75 ) / 100) - 110,
},
walletEmptyText: {
  fontSize: (((Dimensions.get('window').width) * 5.5) / 100),
  fontFamily: Constant.KThemeFontSemiBold,
  color: '#fff',
},
cellContainer : {
  margin: 10,
  marginBottom: 5
},
creditFontColor : {
  color: 'red',
},
debitFontColor : {
  color: 'green',
},
cellDateFont: {
  fontSize: txtInputFontSize,
  fontFamily: Constant.KThemeFontSemiBold,
  color: '#fff',
  marginBottom: 15,
},
cellHeaderFont: {
  fontSize: txtInputFontSize,
  fontFamily: Constant.KThemeFontRegular,
  color: '#fff',
},
cellOtherFont: {
  fontSize: txtInputFontSize-1,
  fontFamily: Constant.KThemeFontRegular,
  color: 'gray',
},
requestCellContainer: {
    flex: 1,
    margin:15,
    borderRadius: 5,
    backgroundColor: 'black',
    borderColor: 'gray',
    borderWidth: 0.5,
    marginBottom: 0,
},
requestCellTopContainer: {
    flex: 1,
    backgroundColor: '#fff',
    height: 80,
    flexDirection:"row",
    marginBottom: 15,
    borderTopRightRadius: 5,
    borderTopLeftRadius: 5,
},
whiteFont: {
  color: '#fff',
},
blackFont: {
  color: '#000',
},
appColorFont: {
  color: Constant.KAppColor,
},
garyFont: {
  color: 'gray',
},
title: {
    fontSize: 16,
},
container_text: {
    flex: 1,
    flexDirection: 'column',
    marginLeft: 12,
    justifyContent: 'center',
},
description: {
    fontSize: 11,
    fontStyle: 'italic',
},
photo: {
    height: 50,
    width: 50,        
},
pickUpImage: {
    flexDirection:"column", 
    flex: 0.1, 
    alignItems: 'center',
},
margin5: {
  marginTop: 8,
},
requestCellFont: {
  fontSize: txtInputFontSize+1,
  fontFamily: Constant.KThemeFontSemiBold,
  color: '#000',
},
requestMediumCellFont: {
  fontSize: txtInputFontSize,
  fontFamily: Constant.KThemeFontRegular,
  color: '#fff',
},
requestSmallFont: {
  fontSize: txtInputFontSize-2,
  fontFamily: Constant.KThemeFontRegular,
  color: '#000',
},
locationContainer: {
  margin:5,
  backgroundColor: Constant.KAppColor1,
  marginTop: 10,
  flexDirection: 'column',
},
pickUpImage: {
  flexDirection:"column", 
  flex: 0.1, 
  alignItems: 'center',
},
requestThemeButton:{
  borderRadius:5,
  height: btnHeight,
  backgroundColor: '#8edf01',
  justifyContent: 'center',
  alignItems: 'center',
  height: CommonUtilsObj.btnHeight,
  // paddingVertical: 10,
},
requestTextInput: {
  height: txtInputHeight,
  width : txtInputHeight,
  fontSize: txtInputFontSize,
  fontFamily: Constant.KThemeFontRegular,
  backgroundColor: 'white',
  padding: 5,
  borderRadius :txtInputHeight/2,
  margin: 4,
  textAlign: 'center',
},
labelText: {
  fontSize: txtInputFontSize,
  fontFamily: Constant.KThemeFontRegular,
  color: Constant.KThemeFontColor2,
  marginTop: 20,
  marginBottom: -5
  },
  newRequestPopUpContainer: {
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.5,
    shadowRadius: 2,  
    elevation: 3,
    backgroundColor: 'black', 
    height: 290, 
    width: (Dimensions.get('window').width)-40,
    borderRadius: 8,
  },
  notificationPopupText:{
      color: Constant.KThemeFontColor,
      textAlign: 'center',
      // margin: 5,
      fontSize: btnFontSize-1,
      fontFamily: Constant.KThemeFontRegular,
      // paddingVertical: 10,
  },
});