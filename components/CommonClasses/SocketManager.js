import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import userDefaults from 'react-native-user-defaults'
import Constant from './Constant';
import DefaultPreference from 'react-native-default-preference';
import { Platform } from 'react-native';
import SocketIOClient from 'socket.io-client';
import CommonUtilsObj from './CommonUtils';
import Geolocation from 'react-native-geolocation-service'; //'@react-native-community/geolocation';
// import Geolocation from '@react-native-community/geolocation';
import RNDeviceHeading from 'react-native-device-heading';
import EventBus from 'react-native-event-bus'

const degreesUpdateRate = 1; 

class SocketManager {

    static sharedManager = null;

    socket = '';

    static getInstance() {
        if (SocketManager.sharedManager == null) {
            SocketManager.sharedManager = new SocketManager();
        }

        return this.sharedManager;
    }

    initializeSocket() {
        SocketManager.socket = SocketIOClient(Constant.KSocketURL); 
    }

    establishConnection() {
        console.log('establishConnection connected....');
        SocketManager.socket.on('connect', () => {
          
          RNDeviceHeading.start(degreesUpdateRate, degree => {
            CommonUtilsObj.heading = degree + 90;
            // console.log('connected....' + CommonUtilsObj.heading);
            EventBus.getInstance().fireEvent("HeadingChangeEvent", {
              // CommonUtilsObj.heading,
            });
          });

          SocketManager.socket.emit("New-Room-Delete-Driver", CommonUtilsObj.userDetails.id);
          
          SocketManager.socket.emit("New-Room-Creation-Driver", CommonUtilsObj.userDetails.id);
        });
    }

    closeConnection() {
        SocketManager.socket.emit("New-Room-Delete-Driver", CommonUtilsObj.userDetails.id);
        SocketManager.socket.on('disconnect', () => {
          console.log('disconnected....');
          
        });
    }

    setDriverOnlineToSocket() {
        SocketManager.socket.emit("New-Room-Creation-Driver", CommonUtilsObj.userDetails.id);
    }
    
    setDriverOfflineToSocket() {
        SocketManager.socket.emit("New-Room-Delete-Driver", CommonUtilsObj.userDetails.id);
    }

    getCoordinates() {
      SocketManager.socket.on('UpdateDriverLocationNotificaiton', (dataArray) => {
          console.log('UpdateDriverLocationNotificaiton' + JSON.stringify(dataArray))
            // CommonUtilsObj.driverLatitude = dataArray.latitude;
            // CommonUtilsObj.driverLongitude = dataArray.longitude;
            // CommonUtilsObj.driverDirection = dataArray.direction;

            EventBus.getInstance().fireEvent("LocationChangeEvent1", {
                    latitude: dataArray.latitude,
                    longitude: dataArray.longitude,
                    direction: dataArray.direction,
            });
        // }
      });

      
    }

    sendCoordinates() {
        // socket.on("UpdateDriverLocationNotificaiton") { ( dataArray, ack) -> Void in
        //     print("UpdateDriverLocationNotificaiton : \(dataArray)")
        //     EventBus.getInstance().fireEvent("LocationChangeEvent1", {
        //       latitude: dataArray.latitude,
        //       longitude: dataArray.longitude,
        //       direction: dataArray.direction,
        //     });
        // }

        console.log('sendCoordinates')

        Geolocation.getCurrentPosition(
              (position) => {
                  console.log('log test -> ' + position);
                   // console.log('here' + position.coords.latitude + position.coords.longitude + position.coords.heading + CommonUtilsObj.userDetails.id);
                 SocketManager.socket.emit("update-driver-location", CommonUtilsObj.userDetails.id, position.coords.latitude, position.coords.longitude, CommonUtilsObj.heading);
                CommonUtilsObj.driverLatitude = position.coords.latitude;
                CommonUtilsObj.driverLongitude = position.coords.longitude;
                EventBus.getInstance().fireEvent("LocationChangeEvent", {
                  // CommonUtilsObj.heading,
                });

                // EventBus.getInstance().fireEvent("LocationChangeEvent1", {
                //   // CommonUtilsObj.heading,
                // });
              },
              (error) => {
                  // See error code charts below.
                  console.log(error.code, error.message);
              },
              { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
          );
    }

    updateDriverLocationToUser() {
        // console.log('updateDriverLocationToUser' + CommonUtilsObj.heading);
        Geolocation.getCurrentPosition(
              (position) => {
                 SocketManager.socket.emit("update-user-side-location", position.coords.latitude, position.coords.longitude, CommonUtilsObj.heading, CommonUtilsObj.userDetails.id, '2');
              },
              (error) => {
                  // See error code charts below.
                  console.log(error.code, error.message);
              },
              { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
          );
    }
}

let SocketManagerObj = SocketManager.getInstance();
export default SocketManagerObj;