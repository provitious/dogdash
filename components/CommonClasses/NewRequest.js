import React, { Component, Fragment } from 'react';
import { StyleSheet, Dimensions, YellowBox, Text, View, Button, TextInput, Image, ScrollView, TouchableOpacity, Alert, SafeAreaView, PixelRatio } from 'react-native';
import {KeyboardAvoidingView, Keyboard, TouchableWithoutFeedback} from 'react-native';
import userDefaults from 'react-native-user-defaults'
import { Modal } from 'react-native';
import ProgressLoader from 'rn-progress-loader';
import { StackActions, NavigationActions } from 'react-navigation';
import { AnimatedCircularProgress } from 'react-native-circular-progress';

import Constant from './Constant';
import CommonUtilsObj from './CommonUtils';
import Message from './Message';
import APIManagerObj from './APIManager';
import CustomStyles from '../CommonClasses/CustomStyles';

import NetInfo from "@react-native-community/netinfo";
import Toast from 'react-native-simple-toast';

const txtInputHeight = (((Dimensions.get('window').width) * 9.8) / 100);
const btnHeight = (((Dimensions.get('window').width) * 10) / 100);
const fontHeight = (((Dimensions.get('window').width) * 4) / 100);
const txtInputFontSize = (((Dimensions.get('window').width) * 3.6) / 100);
var Sound = require('react-native-sound');
var currentSound;

class NewRequest extends Component {
 
    constructor(props) {
        super(props);

        const { navigation } = this.props;  
        const details = navigation.getParam('requestDetails', '');
        // details.id = 254;
        
        this.state = {
            connection_Status : "",
            visible: false,
            requestDetails: details,
            detailsObj: '',
            fill: 0,
            timer: 0,
        };

        YellowBox.ignoreWarnings([
          'Warning: componentWillMount is deprecated',
          'Warning: componentWillReceiveProps is deprecated',
          'Warning: componentWillUpdate is deprecated',
          'Warning: componentWillUpdate has been renamed, and is not recommended for use',
          'Warning: componentWillReceiveProps has been renamed, and is not recommended for use',
        ]);
    }

    componentDidMount() {

      NetInfo.isConnected.addEventListener(
          'connectionChange',
          this._handleConnectivityChange
      );
     
      NetInfo.isConnected.fetch().done((isConnected) => {

        if(isConnected == true)
        {
          this.setState({connection_Status : "Online"})
          // this.onFetchRequestDetails();
        }
        else
        {
          this.setState({connection_Status : "Offline"})
        }

      });

      this._interval = setInterval(() => {
        if (this.state.timer >= 30) {
          clearInterval(this._interval);
          currentSound.release();
          this.onRejectRequest()
          // this.props.navigation.pop();
        }
        else {
          this.setState({
            fill: this.state.fill + 3.33,
            timer: this.state.timer + 1,
          });
        }
      }, 1000);

      this.onPlayBeep();
    }

    componentWillUnmount() {
 
      NetInfo.isConnected.removeEventListener(
          'connectionChange',
          this._handleConnectivityChange
   
      );
      clearInterval(this._interval);
      currentSound.release();
    }

    _handleConnectivityChange = (isConnected) => {

      if(isConnected == true)
        {
          this.setState({connection_Status : "Online"})
        }
        else
        {
          this.setState({connection_Status : "Offline"})
        }
    };

    onPlayBeep = () => {
      // Sound.setCategory('Playback');
      currentSound = new Sound('sound.mp3',
      Sound.MAIN_BUNDLE,
      error => {
        if (error) {          
        } else {
          currentSound.setNumberOfLoops(-1);
          currentSound.play(() => {
            // currentSound.release();

          });
        }
      });
      // currentSound.setNumberOfLoops(10);
    };
   
    async onFetchRequestDetails() {
      if (this.state.connection_Status == "Online") {

        // this.setState({
        //   visible: true,
        //   loaded: false,
        // });

        var data = new FormData();
        data.append('driver_id', CommonUtilsObj.userDetails.id);
        data.append('off', 0);
        data.append('booking_id', this.state.requestDetails.id);

        let responseData = await APIManagerObj.callApiWithData(Constant.KRequestDetailURL, data);
        console.log('json response ....' + JSON.stringify(responseData));

        if (responseData.status == "success") {
          var showAllDetails = true;
          let details = responseData.all_trip[0];
          this.setState({
            visible: false,
            detailsObj: details,
          });
        }
        else {
          this.setState({
            visible: false,
          }); 
          setTimeout(() => {
            if(typeof(responseData.message) === 'string')
            {
                Toast.showWithGravity(responseData.message, Toast.LONG, Toast.TOP)
            }
            else if(typeof(responseData.error) === 'string')
            {
                Toast.showWithGravity(responseData.error, Toast.LONG, Toast.TOP)
            }
            else {
                if (responseData.error) {
                  Toast.showWithGravity(responseData.error.toString(), Toast.LONG, Toast.TOP)
                }
                else {
                  Toast.showWithGravity(Message.KAPIError, Toast.LONG, Toast.TOP)
                }
            }
          }, 200); 
        }
      }
      else {
        this.setState({lastApiTag : 1})
        Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
      }
    }

  onClickView() {
    CommonUtilsObj.newRequestDetails = this.state.requestDetails;
    this.props.navigation.pop();

    const navigateAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: "NotificationRequestDetailsScreen" , params: { fromNotification: true, requestDetails: CommonUtilsObj.newRequestDetails }})],
    });
    CommonUtilsObj.navigationProps.navigation.dispatch(navigateAction);
  }

  onClickReject() {
      Alert.alert(
        Constant.KAppTitle,
        'Are you sure, you want to reject this request?',
        [
          {text: 'Yes', onPress: () => this.onRejectRequest()},
          {
            text: 'No',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
          },
        ],
        {cancelable: false},
      );
  }

  async onRejectRequest() {
    if (this.state.connection_Status == "Online") {

      this.setState({
        visible: true,
      });

      var data = new FormData();
      data.append('driver_id', CommonUtilsObj.userDetails.id);
      data.append('reject_type', 0);
      data.append('booking_id', this.state.requestDetails.id);

      let responseData = await APIManagerObj.callApiWithData(Constant.KRejectRequestURL, data);
      console.log('json response ....' + JSON.stringify(responseData));

      if (responseData.status == "success") {
        this.setState({
          visible: false,
        });

        setTimeout(() => {
          Toast.showWithGravity(responseData.message, Toast.LONG, Toast.TOP);
          this.props.navigation.pop();
        }, 100);       
      }
      else {
        this.setState({
          visible: false,
        }); 
        setTimeout(() => {
          if(typeof(responseData.message) === 'string')
          {
              Toast.showWithGravity(responseData.message, Toast.LONG, Toast.TOP)
          }
          else if(typeof(responseData.error) === 'string')
          {
              Toast.showWithGravity(responseData.error, Toast.LONG, Toast.TOP)
          }
          else {
              if (responseData.error) {
                Toast.showWithGravity(responseData.error.toString(), Toast.LONG, Toast.TOP)
              }
              else {
                Toast.showWithGravity(Message.KAPIError, Toast.LONG, Toast.TOP)
              }
          }
        }, 200); 
      }
    }
    else {
      Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
    }
  }

  onclickClose() {
    this.props.navigation.pop();
  }

  render() {
    return(

        <Modal animationType="slide" transparent={true} visible={true} presentationStyle = "overFullScreen">
          <View style={{flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}>
            <View style={CustomStyles.newRequestPopUpContainer}>
              <View style={{ backgroundColor: '#8edf01', flexDirection: 'row', justifyContent: 'center', height:45, marginBottom:8, borderTopRightRadius: 8, borderTopLeftRadius: 8}}>
                <Text style={[CustomStyles.notificationPopupText, {alignSelf: 'center'}]}>New Booking Request</Text>      
              </View>
              
              <View style={{ position: 'absolute', right: 5, height: '100%', width: 35, alignSelf: 'center'}}>
                <TouchableOpacity
                  onPress={this.onclickClose.bind(this)}
                  style={{justifyContent: 'center', alignItems: 'center', height: 45}}
                >
                  <Image
                    ref="imgChecked"
                    style={{height: 20, width: 20}}
                    source={require('../../images/cancel.png')}
                  />
                </TouchableOpacity>
              </View>
              <View style={{justifyContent:'center', alignItems: 'center', marginVertical: 15}}>
              <Text style={[CustomStyles.notificationPopupText, {color: '#fff', marginBottom: 15}]}>Id : {this.state.requestDetails.id}</Text>
              <AnimatedCircularProgress
                size={100}
                width={10}
                fill={this.state.fill}
                tintColor="red"
                backgroundColor="gray">
                {
                  (fill) => (
                    <Text style={{color: '#fff'}}>
                      { this.state.timer }
                    </Text>
                  )
                }
              </AnimatedCircularProgress>
              </View>

              <View style={{flexDirection: 'row', justifyContent: 'flex-start', alignItems:'center', marginHorizontal: 10}}>
                <View style={[CustomStyles.flexColunm, {justifyContent: 'center'}]}>
                  <View style={[CustomStyles.flexRow, {justifyContent: 'space-between', alignItems: 'center'}]}>
                    <TouchableOpacity
                      style={[CustomStyles.requestThemeButton, {backgroundColor: 'white', marginRight: 20, width: ((Dimensions.get('window').width)-100)/2}]}
                      onPress={this.onClickReject.bind(this)}
                    >
                      <Text style={CustomStyles.themeButtonText}>Decline</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                      style={[CustomStyles.requestThemeButton, {backgroundColor: Constant.KAppColor, width: ((Dimensions.get('window').width)-100)/2}]}
                      onPress={this.onClickView.bind(this)}
                    >
                      <Text style={CustomStyles.themeButtonText}>View</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
          </View>
          </View>
        </Modal>
    )
  }
}


const customStyles = StyleSheet.create({
    container: {
        flex:0, 
        // justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent',
    },
    centerContainer: {
      width: '100%',
    },
    flexColunm:{
        flexDirection: 'column',
    },
    flexRow:{
        flexDirection: 'row'
    },
    themeButton:{
        marginTop:10,
        borderRadius:5,
        height: btnHeight,
        backgroundColor: '#8edf01',
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonTitleText:{
        color:'black',
        textAlign: 'center',
        fontSize: fontHeight,
        fontFamily: Constant.KThemeFontSemiBold,
    },
});

const styles = StyleSheet.create({
  textStyle: {
    fontSize: 20,
    color: 'transparent',
    fontWeight: 'bold'
  },
  countryStyle: {
    flex: 1,
    borderTopColor: 'gray',
    borderTopWidth: 0.5,
    padding: 3,
    backgroundColor: 'white'
  },
  closeButtonStyle: {
    flex: 1,
    padding: 2,
    alignItems: 'center', 
    justifyContent: 'center',
    backgroundColor: 'white'
  }
})
export default NewRequest;
