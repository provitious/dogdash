import React from 'react';
import { StyleSheet, Text, View, Button, YellowBox, Dimensions, } from 'react-native';
import { NavigationBar } from 'navigationbar-react-native';
import { WebView } from 'react-native-webview';
import Toast from 'react-native-simple-toast';
import Constant from '../CommonClasses/Constant';
import CommonUtilsObj from '../CommonClasses/CommonUtils';
import NetInfo from "@react-native-community/netinfo";
import CustomStyles from '../CommonClasses/CustomStyles';
import Message from '../CommonClasses/Message';
import APIManagerObj from '../CommonClasses/APIManager';

export default class PrivacyPolicy extends React.Component {

  constructor(props) {
      super(props);
      this.state = {
        aboutUsContent: null,
        loaded: false,
        connection_Status : "",
      }

      CommonUtilsObj.navigationProps = this.props;
  }

  componentDidMount() {
      

      NetInfo.isConnected.addEventListener(
          'connectionChange',
          this._handleConnectivityChange

      );
     
      NetInfo.isConnected.fetch().done((isConnected) => {

        if(isConnected == true)
        {
          this.setState({connection_Status : "Online"})
          this.fetchData();
        }
        else
        {
          this.setState({connection_Status : "Offline"})
          Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
        }

      });
  }

  componentWillUnmount() {
 
    NetInfo.isConnected.removeEventListener(
        'connectionChange',
        this._handleConnectivityChange
    );
 
  }

  _handleConnectivityChange = (isConnected) => {

    if(isConnected == true)
      {
        this.setState({connection_Status : "Online"})
      }
      else
      {
        this.setState({connection_Status : "Offline"})
      }
  };

  fetchData() {
    if (this.state.connection_Status == "Online") {
      // fetch(Constant.KPrivacyPolicyURL)
      // .then((resp)=>{ return resp.text() })
      // .then((text)=>{ 
      //     console.log(text) 
      //     this.setState({
      //         aboutUsContent: text,
      //         loaded: true,
      //      });
      // })
      // .done();

      return new Promise(
        function (resolve, reject) {
          fetch(
            Constant.KPrivacyPolicyURL,
          )
          .then(
            function(response) {
              if (response.ok) {    
                response.text().then((responseData) => {
                  this.setState({
                    aboutUsContent: responseData,
                    loaded: true,
                 });
                });
              }
              else {
                Toast.showWithGravity(Constant.KAPIError, Toast.LONG, Toast.TOP)
                
                // reject(new Error(`Unable to retrieve events.\nInvalid response received - (${response.status}).`));
              }
            }.bind(this)
          )
          .catch(
            function(error) {
              //reject(new Error(`Network Unable to retrieve events.\n${error.message}`));
              Toast.showWithGravity(Constant.KAPIError, Toast.LONG, Toast.TOP)
            }
          );
        }.bind(this)
      );
    }
    else {
      Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
    }
    
  }

  render() {
    return (
      <WebView
        style = {{margin:5,}}
        source={{html: '<body text="black"><font size=7>' + this.state.aboutUsContent + '</font></body>'}}
      />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FCFCFC',
    marginTop: 20,
  }
});







