import React from 'react';
import { StyleSheet, Dimensions, YellowBox, Text, View, Button, TextInput, Image, ScrollView, TouchableOpacity, Alert, SafeAreaView, PixelRatio, ListView } from 'react-native';
import {KeyboardAvoidingView, Keyboard, TouchableWithoutFeedback} from 'react-native';
import { NavigationBar } from 'navigationbar-react-native';
import userDefaults from 'react-native-user-defaults'
import { Modal, FlatList, SectionList, ActivityIndicator } from 'react-native';
import NetInfo from "@react-native-community/netinfo";
import ProgressLoader from 'rn-progress-loader';
import Toast from 'react-native-simple-toast';
import { Rating, AirbnbRating } from 'react-native-ratings';
import { SwipeListView, SwipeRow } from 'react-native-swipe-list-view';
import Icon from 'react-native-vector-icons/FontAwesome';
import EventBus from 'react-native-event-bus'

import CommonUtilsObj from '../CommonClasses/CommonUtils';
import Constant from '../CommonClasses/Constant';
import CustomStyles from '../CommonClasses/CustomStyles';
import Message from '../CommonClasses/Message';
import APIManagerObj from '../CommonClasses/APIManager';

const btnHeight = (((Dimensions.get('window').width) * 10) / 100);
var emptyComponentHeight = 0;

export default class AccountList extends React.Component {

  constructor(props) {
 
    super(props);
 
    YellowBox.ignoreWarnings([
      'Warning: componentWillMount is deprecated',
      'Warning: componentWillReceiveProps is deprecated',
    ]);
    this.state = {
      visible: false,
      connection_Status : "",
      accountList: [],
      selectedIndex: 0,
      oldSelectedIndex: 0,
      initialHeight:0,
      deletedIndex: 0,
      isUpdated: false,
      selectedAccountDetails: '',
    };

    this.onClickAddAccount=this.onClickAddAccount.bind(this);
    CommonUtilsObj.navigationProps = this.props;
    
   }

  returnData(isUpdated, accountDetails) {
    this.setState({isUpdated: isUpdated});
  }

  componentDidMount() {

    NetInfo.isConnected.addEventListener(
        'connectionChange',
        this._handleConnectivityChange

    );
   
    NetInfo.isConnected.fetch().done((isConnected) => {

      if(isConnected == true)
      {
        this.setState({connection_Status : "Online"})
      }
      else
      {
        this.setState({connection_Status : "Offline"})
      }

      if (this.state.connection_Status == "Online") {
        this.onFetchAccountList();
      }
      else {
        Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
      }

    });

    // const {navigation} = this.props;
    // navigation.addListener ('willFocus', () =>{

    //   if (this.state.isUpdated) {
    //       this.setState({
    //         isUpdated: false,
    //       });

    //       if (this.state.connection_Status == "Online") {
    //         this.onFetchAccountList();
    //       }
    //       else {
    //         Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
    //       }
    //   }
      
    // });

    EventBus.getInstance().addListener("AccountEvent", this.listener = data => {
        // handle the event
        this.onFetchAccountList();
    })
  }

  componentWillUnmount() {

    NetInfo.isConnected.removeEventListener(
        'connectionChange',
        this._handleConnectivityChange
 
    );
 
  }

  componentWillUnmount() {

    NetInfo.isConnected.removeEventListener(
        'connectionChange',
        this._handleConnectivityChange
 
    );
 
  }

  _handleConnectivityChange = (isConnected) => {

    if(isConnected == true)
      {
        this.setState({connection_Status : "Online"})
      }
      else
      {
        this.setState({connection_Status : "Offline"})
      }
  };

  async onFetchAccountList() {
    if (this.state.connection_Status == "Online") {

      this.setState({ visible: true });
      var data = new FormData();
      data.append('driver_id', CommonUtilsObj.userDetails.id);

      let responseData = await APIManagerObj.callApiWithData(Constant.KGetAccountListURL, data);
      console.log('json response ....' + JSON.stringify(responseData));

      if (responseData.status == "success") {
        var cnt = 0;
        var selectedCnt = 0;
        const items = responseData.banklist.map(function(item, index){
            if (item.is_primary == '1') {  
                selectedCnt=cnt
            }
            cnt += 1;
            item.key = String(index)
        });

        setTimeout(() => {
          this.setState({
            visible: false,
            accountList: responseData.banklist,
            selectedIndex: selectedCnt,
            oldSelectedIndex: selectedCnt,
          });
        }, 200);         
      }
      else {
        this.setState({
          visible: false,
        }); 
        setTimeout(() => {
          if(typeof(responseData.message) === 'string')
          {
              Toast.showWithGravity(responseData.message, Toast.LONG, Toast.TOP)
          }
          else if(typeof(responseData.error) === 'string')
          {
              Toast.showWithGravity(responseData.error, Toast.LONG, Toast.TOP)
          }
          else {
              if (responseData.error) {
                Toast.showWithGravity(responseData.error.toString(), Toast.LONG, Toast.TOP)
              }
              else {
                Toast.showWithGravity(Message.KAPIError, Toast.LONG, Toast.TOP)
              }
          }
        }, 200); 
      }
    }
    else {
      this.setState({lastApiTag : 1})
      Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
    }
  }

  async onSelectPrimaryAccount() {
    if (this.state.connection_Status == "Online") {

      this.setState({ visible: true });
      var data = new FormData();
      data.append('driver_id', CommonUtilsObj.userDetails.id);
      data.append('bank_id', this.state.accountList[this.state.selectedIndex].id);
      data.append('is_primary', '1');

      let responseData = await APIManagerObj.callApiWithData(Constant.KSetPrimaryAccountURL, data);
      console.log('json response ....' + JSON.stringify(responseData));

      if (responseData.status == "success") {
        this.setState({
            visible: false
        });
        setTimeout(() => {
          this.onFetchAccountList();
          Toast.showWithGravity(responseData.message, Toast.LONG, Toast.TOP)  
        }, 200);        
      }
      else {
        this.setState({
          visible: false,
        }); 
        setTimeout(() => {
          if(typeof(responseData.message) === 'string')
            {
                Toast.showWithGravity(responseData.message, Toast.LONG, Toast.TOP)
            }
          else if(typeof(responseData.error) === 'string')
          {
              Toast.showWithGravity(responseData.error, Toast.LONG, Toast.TOP)
          }
          else {
              if (responseData.error) {
                Toast.showWithGravity(responseData.error.toString(), Toast.LONG, Toast.TOP)
              }
              else {
                Toast.showWithGravity(Message.KAPIError, Toast.LONG, Toast.TOP)
              }
          }
        }, 200); 
      }
    }
    else {
      this.setState({lastApiTag : 1})
      Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
    }
  }

  async onDeleteAccount() {
    if (this.state.connection_Status == "Online") {

      this.setState({ visible: true });
      var data = new FormData();
      data.append('driver_id', CommonUtilsObj.userDetails.id);
      data.append('bank_id', this.state.accountList[this.state.deletedIndex].id);

      let responseData = await APIManagerObj.callApiWithData(Constant.KDeleteAccountURL, data);
      console.log('json response ....' + JSON.stringify(responseData));

      if (responseData.status == "success") {
        this.setState({
            visible: false
        });
        
        setTimeout(() => {
          this.onFetchAccountList();
          Toast.showWithGravity(responseData.message, Toast.LONG, Toast.TOP)  
        }, 200);          
      }
      else {
        this.setState({
          visible: false,
        }); 
        setTimeout(() => {
          if(typeof(responseData.message) === 'string')
            {
                Toast.showWithGravity(responseData.message, Toast.LONG, Toast.TOP)
            }
          else if(typeof(responseData.error) === 'string')
          {
              Toast.showWithGravity(responseData.error, Toast.LONG, Toast.TOP)
          }
          else {
              if (responseData.error) {
                Toast.showWithGravity(responseData.error.toString(), Toast.LONG, Toast.TOP)
              }
              else {
                Toast.showWithGravity(Message.KAPIError, Toast.LONG, Toast.TOP)
              }
          }
        }, 200); 
      }
    }
    else {
      this.setState({lastApiTag : 1})
      Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
    }
  }

  onSelectCell = (item, index) => {

      this.setState({
        selectedIndex: index,
        refresh:true,
      });

      if (this.state.accountList[index].is_primary == '1') {
          Toast.showWithGravity(Message.KAlreadyPrimaryAccount, Toast.SMALL, Toast.TOP)
      }
      else {
          Alert.alert(
            Constant.KAppTitle,
            'Are you sure, you want to choose this account as your primary account?',
            [
              {
                text: 'No',
                onPress: () => { 
                  this.setState({
                    selectedIndex:this.state.oldSelectedIndex,
                  });
                },
                style: 'cancel',
              },
              {text: 'Yes', onPress: () => {
                this.onSelectPrimaryAccount()
              }},
            ],
            {cancelable: false},
        );
      }
      
  };

  onSelectDeleteAccount = (item, index) => {

      this.setState({
        refresh:true,
      });

      if (this.state.accountList[index].is_primary == '1') {
          Toast.showWithGravity(Message.KDeletePrimaryAccount, Toast.SMALL, Toast.TOP)
      }
      else {
          Alert.alert(
            Constant.KAppTitle,
            'Are you sure, you want to delete this account?',
            [
              {
                text: 'No',
                onPress: () => { 
                  this.setState({
                  });
                },
                style: 'cancel',
              },
              {text: 'Yes', onPress: () => {
               
                this.setState({
                  deletedIndex: index
                  });

                setTimeout(() => {
                  this.onDeleteAccount();
                }, 300);
                
              }},
            ],
            {cancelable: false},
        );
      }
      
  };

  onSelectEditAccount = (item, index) => {
      this.setState({
          selectedAccountDetails: item
      });
      this.props.navigation.navigate('AddEditAccountScreen', {returnData: this.returnData.bind(this), selectedAccountDetails: item, isEdit: true});
  };

  renderAccountListItem = ({ item, index }) => (
    <AccountListCell
      item={item}
      index={index}
      onSelectCell={this.onSelectCell}
      selectedIndex={this.state.selectedIndex}
      accountList={this.state.accountList}
      fromSelection={this.fromSelection}
      onSelectDeleteAccount={this.onSelectDeleteAccount}
      onSelectEditAccount={this.onSelectEditAccount}
    />
  );

  renderHiddenItem = (data, rowMap) => (
    <View style={CustomStyles.standaloneRowBack}>
      <TouchableOpacity
          style={{backgroundColor: 'green', height: '100%', width: 65, justifyContent:'center'}}
          onPress={this.onSelectEditAccount.bind(this, data.item, data.item.key)}
      >
        <Icon style={{ marginLeft: 20 }} name="pencil-square-o" size={25} color="#fff" />
      </TouchableOpacity>

      <TouchableOpacity
          style={{backgroundColor: 'red', height: '100%', width: 65, justifyContent:'center'}}
          onPress={this.onSelectDeleteAccount.bind(this, data.item, data.item.key)}
      >
        <Icon style={{ marginLeft: 20 }} name="trash" size={25} color="#fff" />
      </TouchableOpacity>
    </View>
  );


  measureView(event) {
  this.setState({
          initialHeight:event.nativeEvent.layout.height,
      });
  emptyComponentHeight = event.nativeEvent.layout.height
  }

  onClickAddAccount() {
    this.props.navigation.navigate('AddEditAccountScreen', {returnData: this.returnData.bind(this), selectedAccountDetails: this.state.selectedAccountDetails, isEdit: false});
  }

  emptyComponent = () => {
    return (
        <View style={CustomStyles.emptyContainer}>
          <Text style={CustomStyles.emptyText}>No records found</Text>
        </View>
    );
  };

  render() {

    return (
      <View style={[CustomStyles.container]}>

          <View
              style={{backgroundColor: "#fff", justifyContent: 'center', alignItems: 'center'}}>

              <ProgressLoader
              visible={this.state.visible}
              isModal={true} isHUD={true}
              hudColor={"#fff"}
              height={200}
              width={200}
              color={"#8edf01"} />
          </View>

          <View style={{height: (Dimensions.get('window').height) - (100 + btnHeight + this.state.initialHeight)}}>

            <SwipeListView
              data={this.state.accountList}
              renderItem={this.renderAccountListItem}
              renderHiddenItem={this.renderHiddenItem}
              // leftOpenValue={65}
              rightOpenValue={-130}
              previewRowKey={'0'}
              previewOpenValue={-130}
              previewOpenDelay={1000}
              // onRowDidOpen={onRowDidOpen}
              ListEmptyComponent={this.emptyComponent.bind(this)}
            /> 
          </View>

          <Text style={CustomStyles.noteFont} onLayout={(event) => this.measureView(event)}>Please select the account you want to use for the request payout. You can always change your account from manage account section of the menu.</Text>
          
          <TouchableOpacity
            style={[CustomStyles.themeButtonAbsolute]}
            onPress={this.onClickAddAccount.bind(this)}
           >
              <Text style={CustomStyles.plusBtnText}> + </Text>
          </TouchableOpacity>  
      </View>
      
    );
  }
}

class AccountListCell extends React.Component {
  render() {
    const { item, index, selectedIndex, fromSelection} = this.props;

    return (
      <View style={CustomStyles.standalone}>

            <View style={CustomStyles.standaloneRowFront}>
              
                  <View style={[CustomStyles.countryStyle,{flexDirection: 'row', justifyContet: 'center', alignItems: 'center', paddingTop: 5, paddingBottom:5, }]}>
                    <TouchableWithoutFeedback 
                      onPress={this.props.onSelectCell.bind(this, item, index)}>
                        <Image
                          ref="imgChecked"
                          style={{height: 25, width: 25, marginRight: 12}}
                          source={ 
                            selectedIndex === index ? require('../../images/checkSelected.png') : require('../../images/checkUnselected.png')
                          }
                        />
                    </TouchableWithoutFeedback>

                    <View style={[CustomStyles.flexColunm, {justifyContent: 'flex-start', width: (Dimensions.get('window').width) - 80}]}>
                        <Text style={[CustomStyles.mediumFont, {alignSelf:'flex-start'}]}>Account Ending ...{item.account_number.substring((item.account_number.length-4),item.account_number.length)}</Text>
                        <Text style={[CustomStyles.smallFont, {alignSelf: 'flex-start'}]}>{item.bank_name}</Text>
                    </View>

                  </View>
              
            </View>
        </View>
      
    );
  }
}
  