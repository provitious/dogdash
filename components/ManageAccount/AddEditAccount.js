import React from 'react';
import { StyleSheet, Dimensions, YellowBox, Text, View, Button, TextInput, Image, ScrollView, TouchableOpacity, Alert, SafeAreaView, PixelRatio } from 'react-native';
import {KeyboardAvoidingView, Keyboard, TouchableWithoutFeedback} from 'react-native';
import { NavigationBar } from 'navigationbar-react-native';
import userDefaults from 'react-native-user-defaults'
import NetInfo from "@react-native-community/netinfo";
import ProgressLoader from 'rn-progress-loader';
import Toast from 'react-native-simple-toast';
import { Dropdown } from 'react-native-material-dropdown';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'
import EventBus from 'react-native-event-bus'

import CommonUtilsObj from '../CommonClasses/CommonUtils';
import Constant from '../CommonClasses/Constant';
import CustomStyles from '../CommonClasses/CustomStyles';
import Message from '../CommonClasses/Message';
import APIManagerObj from '../CommonClasses/APIManager';

import Moment from 'moment';

const txtInputHeight = (((Dimensions.get('window').width) * 9.8) / 100);

export default class AddEditAccount extends React.Component {

  constructor(props) {
 
    super(props);
    YellowBox.ignoreWarnings([
      'Warning: componentWillMount is deprecated',
      'Warning: componentWillReceiveProps is deprecated',
    ]);

    const { navigation } = this.props;  
    const isEdit = navigation.getParam('isEdit', false);
    const selectedAccountDetails = navigation.getParam('selectedAccountDetails', '');  
        
    this.state = {
      visible: false,
      connection_Status : "",
      accountHolderName: (isEdit) ? selectedAccountDetails.name : '',
      bankName: (isEdit) ? selectedAccountDetails.bank_name : '',
      accountNumber: (isEdit) ? selectedAccountDetails.account_number : '',
      routingNumber: (isEdit) ? selectedAccountDetails.routing_number : '',
      isEdit: isEdit,
      btnBottom: 30,
      selectedAccountDetails: selectedAccountDetails,
    };

    this.onFocus = this.onFocus.bind(this);
    this.onValidate = this.onValidate.bind(this);
    this.onChangeText = this.onChangeText.bind(this);
    this.onSubmitAccountHolderName = this.onSubmitAccountHolderName.bind(this);
    this.onSubmitBankName = this.onSubmitBankName.bind(this);
    this.onSubmitAccountNumber = this.onSubmitAccountNumber.bind(this);
    this.onSubmitRoutingNumber = this.onSubmitRoutingNumber.bind(this);

    this.accountHolderNameRef = this.updateRef.bind(this, 'accountHolderName');
    this.bankNameRef = this.updateRef.bind(this, 'bankName');
    this.accountNumberRef = this.updateRef.bind(this, 'accountNumber');
    this.routingNumberRef = this.updateRef.bind(this, 'routingNumber');

  }

  componentDidMount() {
    Moment.locale('en');

    NetInfo.isConnected.addEventListener(
        'connectionChange',
        this._handleConnectivityChange
    );
   
    NetInfo.isConnected.fetch().done((isConnected) => {
      if(isConnected == true)
      {
        this.setState({connection_Status : "Online"})
      }
      else
      {
        this.setState({connection_Status : "Offline"})
      }
    });

    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
  }

  _keyboardDidShow = () => {
      this.setState({btnBottom : -100})
  }

  _keyboardDidHide = () => {
      this.setState({btnBottom : 30})
  }

  componentWillUnmount() {
    NetInfo.isConnected.removeEventListener(
        'connectionChange',
        this._handleConnectivityChange
    );
  }

  _handleConnectivityChange = (isConnected) => {
    if(isConnected == true)
      {
        this.setState({connection_Status : "Online"})
      }
      else
      {
        this.setState({connection_Status : "Offline"})
      }
  };

  updateRef(name, ref) {
    this[name] = ref;
  }

  onFocus() {
    let { errors = {} } = this.state;
    for (let name in errors) {
      let ref = this[name];

      if (ref && ref.isFocused()) {
        delete errors[name];
      }
    }
    this.setState({ errors });
  }

  onChangeText(text) {
    ['accountHolderName', 'bankName', 'accountNumber', 'routingNumber']
      .map((name) => ({ name, ref: this[name] }))
      .forEach(({ name, ref }) => {
        if (ref.isFocused()) {
          this.setState({ [name]: text });

          if (name == 'accountHolderName') {
            if (!text.replace(/\s/g, '').length) {
              this.setState({ [name]: text.trim() });
            }
          }

          if (name == 'bankName') {
            if (!text.replace(/\s/g, '').length) {
              this.setState({ [name]: text.trim() });
            }
          }

          if (name == 'accountNumber') {
            if (!text.replace(/\s/g, '').length) {
              this.setState({ [name]: text.trim() });
            }
          }

          if (name == 'routingNumber') {
            if (!text.replace(/\s/g, '').length) {
              this.setState({ [name]: text.trim() });
            }
          }

          // if (name == 'accountNumber') {
          //   this.setState({ [name]: text.replace(/[^0-9]/g, '') });
          // }


        }
      });
  }

  onSubmitAccountHolderName(text) {
    this.bankName.focus();
  }

  onSubmitBankName(text) {
    this.accountNumber.focus();
  }

  onSubmitAccountNumber(text) {
    this.routingNumber.focus();
  }

  onSubmitRoutingNumber(text) {
    this.routingNumber.blur();
  }

  onValidate() {
    this.accountHolderName.blur();
    this.bankName.blur();
    this.accountNumber.blur();
    this.routingNumber.blur();

    var flag = true;
    var cFlag = true;
    var errorMsg = [];

    let nameVal = this.state.accountHolderName
    if (!nameVal) {
        errorMsg.push(Message.KAccountNameEmpty)
        flag = false;
    }

    if (this.state.bankName == '') {
        errorMsg.push(Message.KBankNameEmpty)
        flag = false;
    }

    if (this.state.accountNumber == '') {
        errorMsg.push(Message.KBankAccountNumberEmpty)
        flag = false;
    }

    if (this.state.routingNumber == '') {
        errorMsg.push(Message.KBankRoutingNuberEmpty)
        flag = false;
    }

    if (flag) {
      this.onAddEditAccount();
    }
    else {
      Toast.showWithGravity(errorMsg.toString(), Toast.LONG, Toast.TOP)
    }
  }

  async onAddEditAccount() {
    if (this.state.connection_Status == "Online") {

      this.setState({ visible: true });
      var data = new FormData();
      data.append('driver_id', CommonUtilsObj.userDetails.id);
      data.append('name', this.state.accountHolderName);
      data.append('bank_name', this.state.bankName);
      data.append('account_number', this.state.accountNumber);
      data.append('routing_number', this.state.routingNumber);

      if (this.state.isEdit) {
        data.append('bank_id', this.state.selectedAccountDetails.id);
      }
      else {
        data.append('bank_id', 0);
      }

      let responseData = await APIManagerObj.callApiWithData(Constant.KAddEditAccountURL, data);
      console.log('json response ....' + JSON.stringify(responseData));

      if (responseData.status == "success") {
        this.setState({
            visible: false
        });

        setTimeout(() => {
          Toast.showWithGravity(responseData.message, Toast.LONG, Toast.TOP)
          // this.props.navigation.state.params.returnData(true);
          
          this.props.navigation.pop();
          EventBus.getInstance().fireEvent("AccountEvent", {
              
            });
        }, 200);        
      }
      else {
        this.setState({
          visible: false,
        }); 
        setTimeout(() => {
          if(typeof(responseData.message) === 'string')
          {
              Toast.showWithGravity(responseData.message, Toast.LONG, Toast.TOP)
          }
          else if(typeof(responseData.error) === 'string')
          {
              Toast.showWithGravity(responseData.error, Toast.LONG, Toast.TOP)
          }
          else {
              if (responseData.error) {
                Toast.showWithGravity(responseData.error.toString(), Toast.LONG, Toast.TOP)
              }
              else {
                Toast.showWithGravity(Message.KAPIError, Toast.LONG, Toast.TOP)
              }
          }
        }, 200); 
      }
    }
    else {
      this.setState({lastApiTag : 1})
      Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
    }
  }

  render() {

    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>

            <View style={CustomStyles.container}>
                <View
                style={{backgroundColor: "#fff", justifyContent: 'center', alignItems: 'center'}}>
                <ProgressLoader
                  visible={this.state.visible}
                  isModal={true} isHUD={true}
                  hudColor={"#fff"}
                  height={200}
                  width={200}
                  color={"#8edf01"} />
                </View>

                <KeyboardAwareScrollView style={[CustomStyles.flexColunm, {marginHorizontal: 20, width: '100%'}]} behavior="padding" enabled>
                  <View style={{marginHorizontal: 20}}>
                    { (this.state.accountHolderName != '') &&
                      <Text style={[CustomStyles.labelText, {marginTop: 5}]}>Account holder name</Text>
                    }
                    <TextInput
                      style={[CustomStyles.textInput,{marginBottom: 20, marginTop: 10}]}
                      ref={this.accountHolderNameRef}
                      placeholder = "Robin" 
                      keyboardType='default'
                      autoCapitalize='sentences'
                      autoCorrect={true}
                      enablesReturnKeyAutomatically={true}
                      onFocus={this.onFocus}
                      onChangeText={this.onChangeText}
                      onSubmitEditing={this.onSubmitAccountHolderName}
                      returnKeyType='next'
                      value= {this.state.accountHolderName}
                    />

                    { (this.state.bankName != '') &&
                      <Text style={[CustomStyles.labelText, {marginTop: 10}]}>Bank name</Text>
                    }
                    <TextInput
                      style={[CustomStyles.textInput,{marginBottom: 20, marginTop: 10}]}
                      ref={this.bankNameRef}
                      placeholder = "Customer Bank" 
                      keyboardType='default'
                      autoCapitalize='sentences'
                      autoCorrect={true}
                      enablesReturnKeyAutomatically={true}
                      onFocus={this.onFocus}
                      onChangeText={this.onChangeText}
                      onSubmitEditing={this.onSubmitBankName}
                      returnKeyType='next'
                      value= {this.state.bankName}
                    />

                    { (this.state.accountNumber != '') &&
                      <Text style={[CustomStyles.labelText, {marginTop: 10}]}>Account number</Text>
                    }
                    <TextInput
                      style={[CustomStyles.textInput,{marginBottom: 20, marginTop: 10}]}
                      ref={this.accountNumberRef}
                      placeholder = "000123456789" 
                      keyboardType='default'
                      autoCapitalize='sentences'
                      autoCorrect={true}
                      enablesReturnKeyAutomatically={true}
                      onFocus={this.onFocus}
                      onChangeText={this.onChangeText}
                      onSubmitEditing={this.onSubmitAccountNumber}
                      returnKeyType='next'
                      value= {this.state.accountNumber}
                    />

                    { (this.state.routingNumber != '') &&
                      <Text style={[CustomStyles.labelText, {marginTop: 10}]}>Routing number</Text>
                    }
                    <TextInput
                      style={[CustomStyles.textInput,{marginBottom: 20, marginTop: 10}]}
                      ref={this.routingNumberRef}
                      placeholder = "123456789" 
                      keyboardType='default'
                      autoCapitalize='sentences'
                      autoCorrect={true}
                      enablesReturnKeyAutomatically={true}
                      onFocus={this.onFocus}
                      onChangeText={this.onChangeText}
                      onSubmitEditing={this.onSubmitRoutingNumber}
                      returnKeyType='next'
                      value= {this.state.routingNumber}
                    />

                    {
                    // { (this.state.cardNumber != '') &&
                    //   <Text style={CustomStyles.labelText}>Card number</Text>
                    // }
                    // <TextInput
                    //   style={CustomStyles.textInput}
                    //   ref={this.cardNumberRef}
                    //   placeholder = "Card number" 
                    //   keyboardType='phone-pad'
                    //   autoCapitalize='sentences'
                    //   autoCorrect={true}
                    //   enablesReturnKeyAutomatically={true}
                    //   onFocus={this.onFocus}
                    //   onChangeText={this.onChangeText}
                    //   onSubmitEditing={this.onSubmitCardNumber}
                    //   returnKeyType='next'
                    //   maxLength={16}
                    //   value={this.state.cardNumber}
                    // />
                  }
                  </View>
                </KeyboardAwareScrollView>
                

                    <TouchableOpacity
                      style={[CustomStyles.themeButtonAbsolute, {bottom: this.state.btnBottom}]}
                      onPress={this.onValidate.bind(this)}
                     >
                        <Text style={CustomStyles.themeButtonText}> Save </Text>
                    </TouchableOpacity>
      </View>
        </TouchableWithoutFeedback>
      
    );
  }
}

