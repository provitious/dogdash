import React from 'react';
import { Text, View, Dimensions } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createBottomTabNavigator, createMaterialTopTabNavigator } from 'react-navigation-tabs';
import Icon from 'react-native-vector-icons/Ionicons';  
import Constant from '../CommonClasses/Constant';

import UpcomingRequests from './UpcomingRequests';
// import AssignRequests from './AssignRequests';
import HistoryRequests from './HistoryRequests';

const TabNavigator = createMaterialTopTabNavigator(  
    {  
        Upcoming: UpcomingRequests,  
        // Assign: AssignRequests, 
        History: HistoryRequests 
    },  
    {  
        tabBarOptions: {  
            activeTintColor: 'black',  
            inactiveTintColor: 'gray',
            showIcon: false,  
            showLabel:true,  
            style: {  
                backgroundColor:'white'  
            } ,
            labelStyle: {
              fontSize: (((Dimensions.get('window').width) * 3.8) / 100),
              fontFamily: Constant.KThemeFontSemiBold,
            },
            indicatorStyle: {
              backgroundColor: Constant.KAppColor,
              height: 5
            } 
        },  
    }  
)  

export default createAppContainer(TabNavigator);