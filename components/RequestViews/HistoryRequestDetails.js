import React from 'react';
import { StyleSheet, Text, View, Image, YellowBox, Dimensions, TouchableOpacity, Alert } from 'react-native';
import { NavigationBar } from 'navigationbar-react-native';
// import Icon from 'react-native-vector-icons/FontAwesome';
// import GoogleMap from 'react-native-maps-google';
import MapView, {PROVIDER_GOOGLE} from 'react-native-maps';
import { Marker } from 'react-native-maps';

import Polyline from '@mapbox/polyline';

import { Rating, AirbnbRating } from 'react-native-ratings';
import { Linking, Platform } from 'react-native';
import NetInfo from "@react-native-community/netinfo";
import ProgressLoader from 'rn-progress-loader';
import Toast from 'react-native-simple-toast';

import CommonUtilsObj from '../CommonClasses/CommonUtils';
import Constant from '../CommonClasses/Constant';
import CustomStyles from '../CommonClasses/CustomStyles';
import Message from '../CommonClasses/Message';
import APIManagerObj from '../CommonClasses/APIManager';

import userDefaults from 'react-native-user-defaults'

const headerFontSize = (((Dimensions.get('window').width) * 3.8) / 100);
const txtMediumSize = (((Dimensions.get('window').width) * 3.4) / 100);
const txtSmallSize = (((Dimensions.get('window').width) * 3.2) / 100);

export default class HistoryRequestDetails extends React.Component {

  constructor(props) {
 
    super(props);
 
    YellowBox.ignoreWarnings([
      'Warning: componentWillMount is deprecated',
      'Warning: componentWillReceiveProps is deprecated',
    ]);
    
    const { navigation } = this.props;  
    const details = navigation.getParam('requestDetails', '');
    
    this.state = {
        visible: false,
        connection_Status: '',
        requestDetails: details,
        lineHeight: 0,
        showAllDetails: true,
        coords: [],
        fromNotification: navigation.getParam('fromNotification', false),
        loaded: false,
    };

  }

  componentDidMount() {

    NetInfo.isConnected.addEventListener(
        'connectionChange',
        this._handleConnectivityChange

    );
   
    NetInfo.isConnected.fetch().done((isConnected) => {

      if(isConnected == true)
      {
        this.setState({connection_Status : "Online"})
      }
      else
      {
        this.setState({connection_Status : "Offline"})
      }


    if (this.state.connection_Status == "Online") {

      this.onFetchRequestDetails();

      // this.getDirections(this.state.requestDetails.pickup_lat, this.state.requestDetails.pickup_longs, this.state.requestDetails.drop_lat, this.state.requestDetails.drop_longs);
      // this.refs.map.fitToElements(true);

    }
    else {
      Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
    }

    });

  }

  componentWillUnmount() {

    NetInfo.isConnected.removeEventListener(
        'connectionChange',
        this._handleConnectivityChange
 
    );
 
  }

  

  _handleConnectivityChange = (isConnected) => {

    if(isConnected == true)
      {
        this.setState({connection_Status : "Online"})
      }
      else
      {
        this.setState({connection_Status : "Offline"})
      }
  };

  async onFetchRequestDetails() {
    if (this.state.connection_Status == "Online") {

      this.setState({
        visible: true,
        loaded: false,
      });

      var data = new FormData();
      data.append('driver_id', CommonUtilsObj.userDetails.id);
      data.append('off', 0);
      data.append('booking_id', this.state.requestDetails.id);

      let responseData = await APIManagerObj.callApiWithData(Constant.KRequestDetailURL, data);
      console.log('json response ....' + JSON.stringify(responseData));

      if (responseData.status == "success") {
        var showAllDetails = true;
        let details = responseData.all_trip[0];
        switch(details.status) {
          case '4':
            details.detailStatus = 'Your trip was cancelled';
            details.payment_type = 'Nil'
            details.base_fare = '0'
            details.actual_km = '0'
            details.trip_distance_price = '0'
            details.total_actual_time = '0'
            details.trip_time_price = '0'
            details.wait_time = '0'
            details.trip_waitTime_price = '0'
            details.final_amount_after_discount = '0'

            details.statusColor = 'red';
            showAllDetails = false;
            break;
     
          case '5':
            details.detailStatus = 'Your trip was cancelled';
            details.statusColor = 'red';
            details.payment_type = 'Nil'
            details.base_fare = '0'
            details.actual_km = '0'
            details.trip_distance_price = '0'
            details.total_actual_time = '0'
            details.trip_time_price = '0'
            details.wait_time = '0'
            details.trip_waitTime_price = '0'
            details.final_amount_after_discount = '0'
            showAllDetails = false;
            break;

          case '6':
            details.detailStatus = 'Your trip was cancelled';
            details.statusColor = 'red';
            details.payment_type = 'Nil'
            details.base_fare = '0'
            details.actual_km = '0'
            details.trip_distance_price = '0'
            details.total_actual_time = '0'
            details.trip_time_price = '0'
            details.wait_time = '0'
            details.trip_waitTime_price = '0'
            details.final_amount_after_discount = '0'
            showAllDetails = false;
            break;
     
          case '9':
            details.detailStatus = 'Your trip was successfully finished.';
            details.statusColor = 'green';
            break;
          
          case '18':
            details.detailStatus = 'Your trip was successfully finished.';
            detail.statusColor = 'green';
            break;

          default:
            console.log('');
        }
        this.setState({
          visible: false,
          requestDetails: details,
          showAllDetails: showAllDetails,
          loaded: true,
        });

        this.getDirections(this.state.requestDetails.pickup_lat, this.state.requestDetails.pickup_longs, this.state.requestDetails.drop_lat, this.state.requestDetails.drop_longs);
        // this.refs.map.fitToElements(true);

        if (this.state.requestDetails.status == "9" || this.state.requestDetails.status == "18") {

          if (this.state.requestDetails.isflag == "0") {
            this.props.navigation.navigate('ReviewUserScreen', {booking_id: this.state.requestDetails.id, requestDetails: responseData.all_trip[0]});
          }
        }        
      }
      else {
        this.setState({
          visible: false,
        }); 
        setTimeout(() => {
          if(typeof(responseData.message) === 'string')
          {
              Toast.showWithGravity(responseData.message, Toast.LONG, Toast.TOP)
          }
          else if(typeof(responseData.error) === 'string')
          {
              Toast.showWithGravity(responseData.error, Toast.LONG, Toast.TOP)
          }
          else {
              if (responseData.error) {
                Toast.showWithGravity(responseData.error.toString(), Toast.LONG, Toast.TOP)
              }
              else {
                Toast.showWithGravity(Message.KAPIError, Toast.LONG, Toast.TOP)
              }
          }
        }, 200); 
      }
    }
    else {
      this.setState({lastApiTag : 1})
      Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
    }
  }

  async getDirections(pickupLat, pickupLong, dropLat, dropLong) {

    if (this.state.connection_Status == "Online") {
      try {
        let response = await fetch(
          `https://maps.googleapis.com/maps/api/directions/json?origin=${pickupLat},${pickupLong}&destination=${dropLat},${dropLong}&sensor=false&mode=driving&key=${Constant.KGoogleMapAPIKey}`,
          {
            method: "POST",
            headers: {
             "Accept": "application/json",
             "Content-Type": "application/json"
            },
         }
        )
    .then((response) => response.json())
    .then((responseData) => {

       let points = Polyline.decode(responseData.routes[0].overview_polyline.points);
            let coords = points.map((point, index) => {
                return  {
                    latitude : point[0],
                    longitude : point[1]
                }
            })
            this.setState({coords: coords})

             setTimeout(() => {
                if (this.map != null) {
                   this.map.fitToSuppliedMarkers(['mk1','mk2'],{ edgePadding: 
                    {top: 50,
                      right: 50,
                      bottom: 50,
                      left: 50}
                  })
                }
            }, 1000);
    })
    .done();
 
    } catch (errors) {
      
      let err = "Error is" + errors
      Toast.showWithGravity(err, Toast.SHORT, Toast.TOP)

       
    }
    }
    else {
      Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
    }

     
  }

  render() {

    return (
      <View style={CustomStyles.container1}>
        
        { this.state.loaded && 
        <MapView 
          ref={map => {this.map = map}}
          provider = {PROVIDER_GOOGLE}
          style={{flex: 1}}        
          region={{          
            latitude: Number(this.state.requestDetails.pickup_lat),          
            longitude: Number(this.state.requestDetails.pickup_longs), 
            latitudeDelta: 1,
            longitudeDelta: 1
          }}        
          showsUserLocation={true}      
        >

        <Marker
          coordinate={{
              latitude: Number(this.state.requestDetails.pickup_lat),
              longitude: Number(this.state.requestDetails.pickup_longs)}}
          title={'Pick Up Location'}
          description={this.state.requestDetails.pickup_area}
          identifier={'mk1'}
        />

        <Marker
          coordinate={{
              latitude: Number(this.state.requestDetails.drop_lat),
              longitude: Number(this.state.requestDetails.drop_longs)}}
          title={'Destination Location'}
          description={this.state.requestDetails.drop_area}
          identifier={'mk2'}
        />

        <MapView.Polyline 
            coordinates={this.state.coords}
            strokeWidth={2}
            strokeColor="red"/>

        </MapView>
        }

        { this.state.loaded && 
        <View style={[CustomStyles.flexColunm, {margin: 20, marginTop: 10, marginBottom: 10}]}>

                  <Text style={[CustomStyles.whiteFont, CustomStyles.requestMediumCellFont, {textAlign: 'center'}]}>Job Details</Text>

                  <View style={[CustomStyles.flexRow, {justifyContent: 'space-between'}]}>
                      <Text style={[CustomStyles.whiteFont, CustomStyles.requestMediumCellFont, CustomStyles.margin5]}>Charges</Text>
                      <Text style={[CustomStyles.whiteFont, CustomStyles.requestMediumCellFont, CustomStyles.margin5]}>Payment Type : {this.state.requestDetails.payment_type}</Text>
                  </View>

                  <View style={[CustomStyles.flexRow, {justifyContent: 'space-between'}]}>
                      <Text style={[CustomStyles.whiteFont, CustomStyles.requestMediumCellFont, CustomStyles.margin5]}>Base Fare</Text>
                      <Text style={[CustomStyles.whiteFont, CustomStyles.requestMediumCellFont, CustomStyles.margin5]}>${this.state.requestDetails.base_fare}</Text>
                  </View>

                  <View style={[CustomStyles.flexRow, {justifyContent: 'space-between'}]}>
                      <Text style={[CustomStyles.whiteFont, CustomStyles.requestMediumCellFont, CustomStyles.margin5]}>Distance({this.state.requestDetails.km} km)</Text>
                      <Text style={[CustomStyles.whiteFont, CustomStyles.requestMediumCellFont, CustomStyles.margin5]}>${this.state.requestDetails.trip_distance_price}</Text>
                  </View>

                  <View style={[CustomStyles.flexRow, {justifyContent: 'space-between'}]}>
                      <Text style={[CustomStyles.whiteFont, CustomStyles.requestMediumCellFont, CustomStyles.margin5]}>Trip Time({this.state.requestDetails.total_actual_time} minutes)</Text>
                      <Text style={[CustomStyles.whiteFont, CustomStyles.requestMediumCellFont, CustomStyles.margin5]}>${this.state.requestDetails.trip_time_price}</Text>
                  </View>

                  <View style={[CustomStyles.flexRow, {justifyContent: 'space-between'}]}>
                      <Text style={[CustomStyles.whiteFont, CustomStyles.requestMediumCellFont, CustomStyles.margin5]}>Loading Time({this.state.requestDetails.wait_time} minutes)</Text>
                      <Text style={[CustomStyles.whiteFont, CustomStyles.requestMediumCellFont, CustomStyles.margin5]}>${this.state.requestDetails.trip_waitTime_price}</Text>
                  </View>

                  <View style={[CustomStyles.flexRow, {justifyContent: 'space-between'}]}>
                      <Text style={[CustomStyles.whiteFont, CustomStyles.requestMediumCellFont, CustomStyles.margin5]}>Sub Total</Text>
                      <Text style={[CustomStyles.whiteFont, CustomStyles.requestMediumCellFont, CustomStyles.margin5]}>${this.state.requestDetails.final_amount_after_discount}</Text>
                  </View>

        </View>
        }

        { this.state.loaded && 
        <View style={[CustomStyles.flexColunm, {marginTop: 10, backgroundColor: this.state.requestDetails.statusColor, height:30, justifyContent:'center'}]}>
            <Text style={[CustomStyles.whiteFont, CustomStyles.requestMediumCellFont, {textAlign: 'center'}]}>{this.state.requestDetails.detailStatus}</Text>
        </View>
        }
        
      </View>
    );
  }
}

const CustomStyles1 = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000',    
  },
  locationContainer: {
    margin:5,
    backgroundColor: Constant.KAppColor1,
    marginTop: 10,
    flexDirection: 'column',
  },
  flexColunm:{
      flexDirection: 'column',
  },
  flexRow:{
      flexDirection: 'row'
  },
  requestMediumCellFont: {
    fontSize: txtMediumSize,
    fontFamily: Constant.KThemeFontRegular,
 },
 requestSmallFont: {
    fontSize: txtSmallSize,
    fontFamily: Constant.KThemeFontRegular,
 },
 headerFont: {
  fontSize: headerFontSize,
  fontFamily: Constant.KThemeFontSemiBold,
},
whiteFont: {
  color: '#fff',
},
blackFont: {
  color: '#000',
},
appColorFont: {
  color: Constant.KAppColor,
},
garyFont: {
  color: 'gray',
},
margin5: {
  marginTop: 12,
},
pickUpImage: {
  flexDirection:"column", 
  flex: 0.1, 
  alignItems: 'center',
},
});







