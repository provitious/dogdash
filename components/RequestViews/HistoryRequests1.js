import React from 'react';
import { StyleSheet, Dimensions, YellowBox, Text, View, Button, TextInput, Image, ScrollView, TouchableOpacity, Alert, SafeAreaView, PixelRatio } from 'react-native';
import {KeyboardAvoidingView, Keyboard, TouchableWithoutFeedback, TouchableHighlight} from 'react-native';
import { NavigationBar } from 'navigationbar-react-native';
import userDefaults from 'react-native-user-defaults'
import { Modal, FlatList, SectionList, ActivityIndicator } from 'react-native';
import NetInfo from "@react-native-community/netinfo";
import ProgressLoader from 'rn-progress-loader';
import Toast from 'react-native-simple-toast';
import 'react-native-gesture-handler'

import CommonUtilsObj from '../CommonClasses/CommonUtils';
import Constant from '../CommonClasses/Constant';
import CustomStyles from '../CommonClasses/CustomStyles';
import Message from '../CommonClasses/Message';
import APIManagerObj from '../CommonClasses/APIManager';

export default class HistoryRequests extends React.Component {

  constructor(props) {
 
    super(props);
 
    YellowBox.ignoreWarnings([
      'Warning: componentWillMount is deprecated',
      'Warning: componentWillReceiveProps is deprecated',
    ]);
    this.state = {
      visible: false,
      historyLoading: false,
      connection_Status : "",
      historyLoadMoreCompleted: false,
      historyRequestList: [],
      historyOffset: 0,
      historyPullToRefresh: false,
    };
  }

  componentDidMount() {

    NetInfo.isConnected.addEventListener(
        'connectionChange',
        this._handleConnectivityChange

    );
   
    NetInfo.isConnected.fetch().done((isConnected) => {

      if(isConnected == true)
      {
        this.setState({connection_Status : "Online"})
        this.onFetchHistoryRequestList();
      }
      else
      {
        this.setState({connection_Status : "Offline"})
      }
    });
  }

  componentWillUnmount() {
    NetInfo.isConnected.removeEventListener(
        'connectionChange',
        this._handleConnectivityChange
 
    );
  }

  componentWillUnmount() {
    NetInfo.isConnected.removeEventListener(
        'connectionChange',
        this._handleConnectivityChange
    );
  }

  _handleConnectivityChange = (isConnected) => {
    if(isConnected == true)
      {
        this.setState({connection_Status : "Online"})
      }
      else
      {
        this.setState({connection_Status : "Offline"})
      }
  };

  async onFetchHistoryRequestList() {
    if (this.state.connection_Status == "Online") {

      this.setState({ 
        visible: true,
        historyLoading: true,
        historyPullToRefresh: false,
      });

      var data = new FormData();
      data.append('driver_id', CommonUtilsObj.userDetails.id);
      data.append('off', this.state.historyOffset);
      console.log(data)

      let responseData = await APIManagerObj.callApiWithData(Constant.KGetRequestHistoryListURL, data);
      console.log('json response ....' + JSON.stringify(responseData));

      if (responseData.status == "success") {
        var list1=this.state.historyRequestList
        var list=responseData.all_trip;
        list1.push(...list)
   
        const items = list1.map(function(item){
            if (item.status == '1' || item.status == '2' || item.status == '5') {
                item.statusText = 'Pending';
                item.statusImg = require('../../images/pending.png')
            }
            else if (item.status == '3') {
                item.statusText = 'Accepted';
                item.statusImg = require('../../images/acceptRequest.png')
            }
            else if (item.status == '4' || item.status == '6') {
                item.statusText = 'Cancelled';
                item.statusImg = require('../../images/cancelRequest.png')
            }
            else if (item.status == '7') {
                item.statusText = 'Arrived';
                item.statusImg = require('../../images/ArrivedRequest.png')
            }
            else if (item.status == '8') {
                item.statusText = 'On Trip';
                item.statusImg = require('../../images/onTripRequest.png')
            }
            else if (item.status == '9') {
                item.statusText = 'Completed';
                item.statusImg = require('../../images/completed.png')
            }
            else {
                item.statusText = '';
                item.statusImg = require('../../images/cancelRequest.png')
            }
        });

        this.setState({
          visible: false,
          historyOffset: responseData.offset,
          historyRequestList: list1,
          historyLoading: false,
          historyPullToRefresh: false,
        });        
      }
      else {
        this.setState({
          visible: false,
          historyLoading: false,
          historyLoadMoreCompleted: true,
          historyPullToRefresh: false,
        }); 
        setTimeout(() => {
          if (responseData.error_code != '16' && responseData.error_code != '15') {
            if(typeof(responseData.error) === 'string')
            {
                Toast.showWithGravity(responseData.error, Toast.LONG, Toast.TOP)
            }
            else {
                if (responseData.error) {
                  Toast.showWithGravity(responseData.error.toString(), Toast.LONG, Toast.TOP)
                }
                else {
                  Toast.showWithGravity(Message.KAPIError, Toast.LONG, Toast.TOP)
                }
            }
          }
        }, 200); 
      }
    }
    else {
      this.setState({lastApiTag : 1})
      Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
    }
  }
  
  renderHistoryRequestItem = ({ item, index }) => (
    <RequestListCell
      item={item}
      index={index}
      onSelectHistoryRequestCell={this.onSelectHistoryRequestCell}
      historyRequestList={this.state.historyRequestList}
    />
  );

  returnHistoryData(isUpdated) {
    this.setState({isUpdated: isUpdated});
  }

  onSelectHistoryRequestCell = (item, index) => {

    if (item.status == '1' || item.status == '2' || item.status == '5') {
        this.props.navigation.navigate('SearchRequestScreen', {returnHistoryData: this.returnHistoryData.bind(this), requestDetails: item});
    }
    else if (item.status == '3') {
        this.props.navigation.navigate({ routeName: 'RequestDetailsScreen', key: 'RequestHome', params: { requestDetails: item, returnHistoryData: this.returnHistoryData.bind(this) } });
        // this.props.navigation.navigate('RequestDetailsScreen', {returnHistoryData: this.returnHistoryData.bind(this), requestDetails: item});
    }
    else if (item.status == '4' || item.status == '6') {
        this.props.navigation.navigate({ routeName: 'HistoryRequestDetailsScreen', key: 'RequestHome', params: { requestDetails: item, returnHistoryData: this.returnHistoryData.bind(this) } });
    }
    else if (item.status == '7') {
        this.props.navigation.navigate({ routeName: 'RequestDetailsScreen', key: 'RequestHome', params: { requestDetails: item, returnHistoryData: this.returnHistoryData.bind(this) } });
    }
    else if (item.status == '8') {
        this.props.navigation.navigate({ routeName: 'RequestDetailsScreen', key: 'RequestHome', params: { requestDetails: item, returnHistoryData: this.returnHistoryData.bind(this) } });
    }
    else if (item.status == '9') {
        this.props.navigation.navigate({ routeName: 'HistoryRequestDetailsScreen', key: 'RequestHome', params: { requestDetails: item, returnHistoryData: this.returnHistoryData.bind(this) } });
    }
    
  };

  onHistoryRefresh() {

    this.setState({
        historyPullToRefresh:true,
        historyRequestList: [],
        historyOffset: 0,
        historyLoading: false,
        historyLoadMoreCompleted: false,
      });

    if (this.state.connection_Status == "Online") {
      
      setTimeout(() => {

          this.onFetchHistoryRequestList();
          
      }, 2000);
      
    }
    else {
      this.setState({ historyPullToRefresh: false });
      Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
    }
  }

  handleHistoryLoadMore = () => {
    if (!this.state.historyLoading && !this.state.historyLoadMoreCompleted){
      
      this.onFetchHistoryRequestList();
      
    }
  };

  emptyHistoryComponent = () => {
    return (
        <View style={CustomStyles.emptyContainer}>
          <Text style={CustomStyles.emptyText}>No records found</Text>
        </View>
    );
    
  };

  


  render() {

    if (this.state.historyLoading && this.page === 1) {
      return <View style={{
        width: '100%',
        height: '100%'
      }}><ActivityIndicator style={{ color: '#000' }} /></View>;
    }

    return (
      <View style={CustomStyles.container1}>
          <View style={{backgroundColor: "#fff", justifyContent: 'center', alignItems: 'center'}}>

          <ProgressLoader
            visible={this.state.visible}
            isModal={true} isHUD={true}
            hudColor={"#fff"}
            height={200}
            width={200}
            color={"#8edf01"} />
          </View>

          <FlatList
                data={this.state.historyRequestList}
                onHistoryRefresh={() => this.onRefresh()}
                refreshing={this.state.historyPullToRefresh}
                onEndReachedThreshold={0.4}
                onEndReached={this.handleHistoryLoadMore.bind(this)}
                renderItem={this.renderHistoryRequestItem}
                keyExtractor={(item, index) => index.toString()}
                ListemptyHistoryComponent={this.emptyComponent.bind(this)}
          />   
      </View>
      
    );
  }
}

class RequestListCell extends React.Component {
  render() {
    const { item, index, onSelectHistoryRequestCell, historyRequestList} = this.props;
    return (
      //RequestDetailsScreen
      <View style={CustomStyles.requestCellContainer}>
        
        <TouchableOpacity
                    onPress={this.props.onSelectHistoryRequestCell.bind(this, item, index)}
                    activeOpacity = {1.0}
              >
               
              
        <View style={CustomStyles.requestCellTopContainer}>
            <Image source={{uri: Constant.KCarImagePathURL + item.icon}} style={[CustomStyles.photo, {flex: 0.2, alignSelf: 'center', margin: 5}]} />
            <View style={{flexDirection:"column", flex: 0.8, justifyContent: 'center', marginLeft: 7}}>
                <Text style={[CustomStyles.requestCellFont]}>${item.final_amount_after_discount}</Text>
                <Text style={[CustomStyles.requestSmallFont]}>{item.pickup_date_time}</Text>
                <Text style={[CustomStyles.requestSmallFont]}>{item.car_type}</Text>
            </View>

            <View style={[CustomStyles.flexColunm, {justifyContent: 'center', flex: 0.2, alignItems: 'center', margin: 5}]}>
            <Image 
                source={ 
                  (item.user_detail != null) ? {uri: Constant.KUserImagePathURL + item.user_detail.image} : require('../../images/userPlaceholder.png')
                } 
                style={[CustomStyles.photo, {borderRadius: 25, marginBottom: 3}]} 
            />
            </View>
        </View>

        <View style={{flexDirection:"row", justifyContent: 'space-between', marginLeft: 10, marginRight: 10, marginBottom: 20}}>
            <Text style={[CustomStyles.requestCellFont, CustomStyles.whiteFont]}>Booking Id : {item.id}</Text>
            <View style={{alignSelf:'flex-end', flexDirection:"row", justifyContent: 'center'}}>
                <Image source={item.statusImg} style={{height: 15, width: 15, marginRight: 3}} />
                <Text style={[CustomStyles.requestSmallFont, CustomStyles.whiteFont, {marginTop: -2}]}>{item.statusText}</Text>
            </View>
        </View>
        

        <View style={{flexDirection:"row", flex: 1, marginBottom: 20, marginRight: 10}}>

            <View style={CustomStyles.pickUpImage}>
                <Image source={require('../../images/pickUp.png')} style={{height: 20, width: 20}} />
                <Image style={{height: "100%", width: 1, backgroundColor:'white',}} />
            </View> 

            <View style={{flexDirection:"column", flex: 0.9,}}>
                    <Text style={[CustomStyles.garyFont, CustomStyles.smallFont, {alignSelf: 'flex-start'}]}>Pick Up Location</Text>
                    <Text style={[CustomStyles.requestMediumCellFont, CustomStyles.margin5]}>{item.pickup_area}</Text>
            </View>
           
        </View>

        <View style={{flexDirection:"row", flex: 1, marginBottom: 20, marginRight: 10}}>

            <View style={{flexDirection:"column", flex: 0.1, alignItems: 'center'}}>
                <Image source={require('../../images/destination.png')} style={{height: 20, width: 20}} />
            </View>

            <View style={{flexDirection:"column", flex: 0.9,}}>
                <View style={{flexDirection:"column"}}>
                    <Text style={[CustomStyles.garyFont, CustomStyles.smallFont, {alignSelf: 'flex-start'}]}>Destination Location</Text>
                    <Text style={[CustomStyles.requestMediumCellFont, CustomStyles.margin5]}>{item.drop_area}</Text>
                </View>
            </View>
           
        </View>

        </TouchableOpacity>
    </View>
      
    );
  }
}
