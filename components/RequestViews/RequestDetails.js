import React from 'react';
import { StyleSheet, Text, View, Image, YellowBox, Dimensions, TouchableOpacity, Alert, Modal, KeyboardAvoidingView, TextInput, TouchableWithoutFeedback, Keyboard, FlatList } from 'react-native';
import { NavigationBar } from 'navigationbar-react-native';
// import Icon from 'react-native-vector-icons/FontAwesome';
// import GoogleMap from 'react-native-maps-google';
import MapView, {PROVIDER_GOOGLE} from 'react-native-maps';
import { Marker } from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';

import Polyline from '@mapbox/polyline';
import { StackActions, NavigationActions } from 'react-navigation';
import EventBus from 'react-native-event-bus'

import { Rating, AirbnbRating } from 'react-native-ratings';
import { Linking, Platform } from 'react-native';
import NetInfo from "@react-native-community/netinfo";
import ProgressLoader from 'rn-progress-loader';
import Toast from 'react-native-simple-toast';
import Geolocation from 'react-native-geolocation-service';

import CommonUtilsObj from '../CommonClasses/CommonUtils';
import Constant from '../CommonClasses/Constant';
import CustomStyles from '../CommonClasses/CustomStyles';
import Message from '../CommonClasses/Message';
import APIManagerObj from '../CommonClasses/APIManager';

import userDefaults from 'react-native-user-defaults'

const headerFontSize = (((Dimensions.get('window').width) * 3.8) / 100);
const txtMediumSize = (((Dimensions.get('window').width) * 3.4) / 100);
const txtSmallSize = (((Dimensions.get('window').width) * 3.2) / 100);
const btnHeight = (((Dimensions.get('window').width) * 10) / 100);
const btnFontSize = (((Dimensions.get('window').width) * 3.6) / 100);
const txtInputHeight = (((Dimensions.get('window').width) * 9.8) / 100);
const txtInputFontSize = (((Dimensions.get('window').width) * 3.6) / 100);

export default class RequestDetails extends React.Component {

  constructor(props) {
 
    super(props);
 
    YellowBox.ignoreWarnings([
      'Warning: componentWillMount is deprecated',
      'Warning: componentWillReceiveProps is deprecated',
    ]);
    
    const { navigation } = this.props;  
    const details = navigation.getParam('requestDetails', '');
    // alert(navigation.getParam('fromNotification', false))
    this.state = {

        visible: false,
        connection_Status: '',
        requestDetails: details,
        lineHeight: 0,
        showAllDetails: true,
        coords: [],
        fromNotification: navigation.getParam('fromNotification', false),
        loaded: false,
        isPending: false,
        isAccepted: false,
        isArrived: false,
        isOnTrip: false,
        isCompleted: false,
        visibleMapOption: false,
        visibleOTPModel: false,
        txt1: '',
        txt2: '',
        txt3: '',
        txt4: '',
        latDelta: 0.05,
        longDelta: 0.05,
        viewBottom: 10,
        runningList: [],
        heading: CommonUtilsObj.heading,
        driverLatitude: 0, //CommonUtilsObj.driverLatitude,
        driverLongitude: 0, //CommonUtilsObj.driverLongitude,
        firstTime: true,
        showTopList: false,
    };

    this.onFocus = this.onFocus.bind(this);

    this.txt1Ref = this.updateRef.bind(this, 'txt1');
    this.txt2Ref = this.updateRef.bind(this, 'txt2');
    this.txt3Ref = this.updateRef.bind(this, 'txt3');
    this.txt4Ref = this.updateRef.bind(this, 'txt4');

    this.handleViewableItemsChanged = this.handleViewableItemsChanged.bind(this)
        this.viewabilityConfig = {viewAreaCoveragePercentThreshold: 50}

    
  }

  getLocationUser = async () => {
    Geolocation.getCurrentPosition(
      position => {
        const { latitude, longitude } = position.coords;

        this.setState({
          driverLatitude: position.coords.latitude,
          driverLongitude: position.coords.longitude,
        });
      },
      error => {
        console.log(error);
      },
    );
  };

  componentDidMount() {
    NetInfo.isConnected.addEventListener(
        'connectionChange',
        this._handleConnectivityChange
    );
   
    NetInfo.isConnected.fetch().done((isConnected) => {
      if(isConnected == true)
      {
        this.setState({connection_Status : "Online"})
        // this.onFetchRequestDetails();
        this.onLoadFetchRequestDetais();
      }
      else
      {
        this.setState({connection_Status : "Offline"})
      }
    });

    setInterval( () => { 
     this.getLocationUser();
    },1000)

    // this.getLocationUser();

    // let watchID = Geolocation.watchPosition(
    //   (position) => {
    //     console.log('watchID ---> ' + position);
    //     this.setState({ 
    //       driverLatitude: Number(position.coords.latitude),
    //       driverLongitude: Number(position.coords.longitude),
    //     })
        
    //   },
    //   (error) => console.log('Watch Position error ' + error.message),
    //   { enableHighAccuracy: true, timeout: 20000, maximumAge: 0, distanceFilter: 0.00000001 });

    // this.setState({ watchID: watchID })

    EventBus.getInstance().addListener("HeadingChangeEvent", this.listener = data => {
       this.setState({
        heading : CommonUtilsObj.heading,
      })
    });

    // EventBus.getInstance().addListener("LocationChangeEvent1", this.listener = data => {
    //    this.setState({
    //     driverLatitude: data.latitude,
    //     driverLongitude: data.longitude,
    //   })
    // });

    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);

  }

  componentWillUnmount() {
    NetInfo.isConnected.removeEventListener(
        'connectionChange',
        this._handleConnectivityChange
    );

    Geolocation.clearWatch(this.state.watchID);
  }

  _handleConnectivityChange = (isConnected) => {
    if(isConnected == true)
      {
        this.setState({connection_Status : "Online"})
      }
      else
      {
        this.setState({connection_Status : "Offline"})
      }
  };

  _keyboardDidShow = () => {
      this.setState({viewBottom : -200})
  }

  _keyboardDidHide = () => {
      this.setState({viewBottom : 10})
  }

  async onLoadFetchRequestDetais() {
    if (this.state.connection_Status == "Online") {

      var data = new FormData();
      data.append('driver_id', CommonUtilsObj.userDetails.id);
      data.append('off', 0);
      data.append('booking_id', this.state.requestDetails.id);

      let responseData = await APIManagerObj.callApiWithData(Constant.KRequestDetailURL, data);
      console.log('json response ....' + JSON.stringify(responseData));

      if (responseData.status == "success") {

        let details = responseData.all_trip[0];
       

        console.log('---------------------' + JSON.stringify(responseData.running_trip));
        if (details.status != '1') {
                                        
          if (responseData.running_trip.length == 0) {
            this.onFetchRequestDetails();
            this.setState({
                showTopList: false,
              });
          }
          else {
            this.setState({
                showTopList: true,
              });
            // alert('1' + responseData.running_trip.length)
            if (this.state.runningList.length == 0) {
              this.setState({
                runningList: responseData.running_trip,
              });
            }
            else if (this.state.runningList.length == responseData.running_trip.length) {
              this.state.runningList.map(item => { 
                responseData.running_trip.map(itemObj => { 
                  if (item.booking_id == itemObj.booking_id) {
                    item.status = itemObj.status;
                  }
                });
              });
            }
            else {
              this.setState({
                runningList: responseData.running_trip,
              });
            }
          }
          // if (this.state.runningList.length == 0) {
            
          //   if (responseData.running_trip.length == 0) {
          //      let obj = {
          //         "booking_id": details.id,
          //         "pickup_lat": Number(details.pickup_lat),
          //         "pickup_longs": Number(details.pickup_longs),
          //         "drop_lat": Number(details.drop_lat),
          //         "drop_longs": Number(details.drop_longs),
          //         "pickup_area": details.pickup_area,
          //         "drop_area": details.drop_area,
          //         "status": details.status,
          //       }
          //       this.setState({
          //         runningList: [obj],
          //       });
          //   }
          //   else {
          //     this.setState({
          //       runningList: responseData.running_trip,
          //     });
          //   }
          // }
          // else {

          // }
        }
        else {
          this.setState({
                showTopList: false,
              });
          this.onFetchRequestDetails();
          // if (this.state.runningList.length != responseData.running_trip.length) {
          //   if (responseData.running_trip.length != 0) {
          //     this.setState({
          //       runningList: responseData.running_trip,
          //     });
          //   }
          // }

          // if (responseData.running_trip.length == 0) {
          //   this.setState({
          //     visible: false,
          //     requestDetails: details,
          //     // showAllDetails: showAllDetails,
          //     // loaded: true,
          //   });
          // }
        }

        // setTimeout(() => {

        //   this.setState({
        //     loaded: true,
        //   });

        //   if (this.map != null) {
        //   this.map.fitToSuppliedMarkers(['mk1','mk2'],{ edgePadding: 
        //     {top: 50,
        //       right: 50,
        //       bottom: 50,
        //       left: 50}
        //   })
        //   }
        // }, 1000); 
      
      }
      else {
        this.setState({
          visible: false,
        }); 
        setTimeout(() => {
          if(typeof(responseData.message) === 'string')
          {
              Toast.showWithGravity(responseData.message, Toast.LONG, Toast.TOP)
          }
          else if(typeof(responseData.error) === 'string')
          {
              Toast.showWithGravity(responseData.error, Toast.LONG, Toast.TOP)
          }
          else {
              if (responseData.error) {
                Toast.showWithGravity(responseData.error.toString(), Toast.LONG, Toast.TOP)
              }
              else {
                Toast.showWithGravity(Message.KAPIError, Toast.LONG, Toast.TOP)
              }
          }
        }, 200); 
      }
    }
    else {
      this.setState({lastApiTag : 1})
      Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
    }
  }

  async onFetchRequestDetails() {
    if (this.state.connection_Status == "Online") {

      this.setState({
        visible: true,
        loaded: false,
      });

      var data = new FormData();
      data.append('driver_id', CommonUtilsObj.userDetails.id);
      data.append('off', 0);
      data.append('booking_id', this.state.requestDetails.id);

      let responseData = await APIManagerObj.callApiWithData(Constant.KRequestDetailURL, data);
      console.log('json response ....' + JSON.stringify(responseData));

      if (responseData.status == "success") {
        var showAllDetails = true;
        let details = responseData.all_trip[0];
        switch(details.status) {

          case '1':
            details.detailStatus = 'Pending';
            details.statusColor = 'yellow';
            this.setState({
              isPending: true,
              isAccepted: false,
              isArrived: false,
              isOnTrip: false,
              isCompleted: false,
            });
            break;
          
          case '3':
            details.detailStatus = 'Accepted';
            details.statusColor = '#4b5724';
            this.setState({
              isPending: false,
              isAccepted: true,
              isArrived: false,
              isOnTrip: false,
              isCompleted: false,
            });
            break;

          case '4':
            details.detailStatus = 'Your trip was cancelled';
            details.payment_type = 'Nil'
            details.base_fare = '0'
            details.actual_km = '0'
            details.trip_distance_price = '0'
            details.total_actual_time = '0'
            details.trip_time_price = '0'
            details.wait_time = '0'
            details.trip_waitTime_price = '0'
            details.final_amount_after_discount = '0'
            details.statusColor = 'red';
            showAllDetails = false;
            break;
     
          case '5':
            details.detailStatus = 'Your trip was cancelled';
            details.statusColor = 'red';
            details.payment_type = 'Nil'
            details.base_fare = '0'
            details.actual_km = '0'
            details.trip_distance_price = '0'
            details.total_actual_time = '0'
            details.trip_time_price = '0'
            details.wait_time = '0'
            details.trip_waitTime_price = '0'
            details.final_amount_after_discount = '0'
            showAllDetails = false;
            break;

          case '6':
            details.detailStatus = 'Your trip was cancelled';
            details.statusColor = 'red';
            details.payment_type = 'Nil'
            details.base_fare = '0'
            details.actual_km = '0'
            details.trip_distance_price = '0'
            details.total_actual_time = '0'
            details.trip_time_price = '0'
            details.wait_time = '0'
            details.trip_waitTime_price = '0'
            details.final_amount_after_discount = '0'
            showAllDetails = false;
            break;

          case '7':
            details.detailStatus = 'Driver arrived';
            details.statusColor = 'purple';
            this.setState({
              isPending: false,
              isAccepted: false,
              isArrived: true,
              isOnTrip: false,
              isCompleted: false,
            });
            break;
     
          case '8':
            details.detailStatus = 'On the way';
            details.statusColor = '#5583c9';
            this.setState({
              isPending: false,
              isAccepted: false,
              isArrived: false,
              isOnTrip: true,
              isCompleted: false,
            });
            break;
     
          case '9':
            details.detailStatus = 'Your trip was successfully finished.';
            details.statusColor = 'green';
            break;
          
          case '18':
            details.detailStatus = 'Your trip was successfully finished.';
            detail.statusColor = 'green';
            break;

          default:
            console.log('');
        }

        this.setState({
          requestDetails: details,
          showAllDetails: showAllDetails,
        });

        setTimeout(() => {
          this.setState({
          visible: false,
          loaded: true,
          });
        }, 1000); 

        // if (details.status != '1') {

        // if (this.state.runningList.length == 0) {
        //   if (responseData.running_trip.length == 0) {
        //      let obj = {
        //         "booking_id": details.id,
        //         "pickup_lat": Number(details.pickup_lat),
        //         "pickup_longs": Number(details.pickup_longs),
        //         "drop_lat": Number(details.drop_lat),
        //         "drop_longs": Number(details.drop_longs),
        //         "pickup_area": details.pickup_area,
        //         "drop_area": details.drop_area,
        //       }
        //       this.setState({
        //         runningList: [obj],
        //       });
        //   }
        //   else {
        //     this.setState({
        //       runningList: responseData.running_trip,
        //     });
        //   }
        //   }
        // }
        // else {

        //   if (this.state.runningList.length != responseData.running_trip.length) {
        //     if (responseData.running_trip.length != 0) {
        //       this.setState({
        //         runningList: responseData.running_trip,
        //       });
        //     }
        //   }
        // }

        // this.getDirections(this.state.requestDetails.pickup_lat, this.state.requestDetails.pickup_longs, this.state.requestDetails.drop_lat, this.state.requestDetails.drop_longs);
        // this.refs.map.fitToElements(true);

        // if (this.map != null) {
        //   setTimeout(() => {
        //     let latitude = (this.state.requestDetails.driver_lat) ? Number(this.state.requestDetails.driver_lat) : Number(this.state.requestDetails.drop_lat);
        //     let longitude = (this.state.requestDetails.driver_long) ? Number(this.state.requestDetails.driver_long) : Number(this.state.requestDetails.drop_longs);

        //     let platitude = Number(this.state.requestDetails.pickup_lat);         
        //     let plongitude = Number(this.state.requestDetails.pickup_longs); 
        //     // if (latitude != 0 && longitude != 0 && platitude != 0 && plongitude != 0) {
        //       // if (this.map != null) {
        //       // this.map.fitToSuppliedMarkers(['mk1','mk2'],{ edgePadding: 
        //       //   {top: 50,
        //       //     right: 50,
        //       //     bottom: 50,
        //       //     left: 50}
        //       // })
        //       // }
        //     // }
        //   }, 2000);
        // }
            
        // setTimeout(() => {
        //    this.map.fitToSuppliedMarkers(['mk1','mk2'],{ edgePadding: 
        //     {top: 50,
        //       right: 50,
        //       bottom: 50,
        //       left: 50}
        //   })
        // }, 1000);  

        setTimeout(() => {
          this.setState({
            firstTime: false,
          });
        }, 300);   

        setTimeout(() => {
          if (this.map != null) {
            if (this.state.requestDetails.status == '1') {
          this.map.fitToSuppliedMarkers(['mk1','mk2'],{ edgePadding: 
            {top: 50,
              right: 50,
              bottom: 50,
              left: 50}
          })
        }
          }
        }, 2000);    

      }
      else {
        this.setState({
          visible: false,
        }); 
        setTimeout(() => {
          if(typeof(responseData.message) === 'string')
          {
              Toast.showWithGravity(responseData.message, Toast.LONG, Toast.TOP)
          }
          else if(typeof(responseData.error) === 'string')
          {
              Toast.showWithGravity(responseData.error, Toast.LONG, Toast.TOP)
          }
          else {
              if (responseData.error) {
                Toast.showWithGravity(responseData.error.toString(), Toast.LONG, Toast.TOP)
              }
              else {
                Toast.showWithGravity(Message.KAPIError, Toast.LONG, Toast.TOP)
              }
          }
        }, 200); 
      }
    }
    else {
      this.setState({lastApiTag : 1})
      Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
    }
  }

  async getDirections(pickupLat, pickupLong, dropLat, dropLong) {
    if (this.state.connection_Status == "Online") {
      try {
        let response = await fetch(
          `https://maps.googleapis.com/maps/api/directions/json?origin=${pickupLat},${pickupLong}&destination=${dropLat},${dropLong}&sensor=false&mode=driving&key=${Constant.KGoogleMapAPIKey}`,
          {
            method: "POST",
            headers: {
             "Accept": "application/json",
             "Content-Type": "application/json"
             // 'Content-Type': 'multipart/form-data',
            },
         }
        )
    .then((response) => response.json())
    .then((responseData) => {

        console.log('here......' + JSON.stringify(responseData));

        if (responseData.routes.length != 0) {
          let points = Polyline.decode(responseData.routes[0].overview_polyline.points);
            let coords = points.map((point, index) => {
                return  {
                    latitude : point[0],
                    longitude : point[1]
                }
            })
            this.setState({coords: coords})
        }
       
    })
    .done();
 
    } catch (errors) {
      let err = "Error is" + errors
      Toast.showWithGravity(err, Toast.LONG, Toast.TOP)
    }
    }
    else {
      Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
    }
     
  }

  onClickJobDetails() {
      this.props.navigation.navigate('MediaDetailsScreen', {requestDetails: this.state.requestDetails});
  }

  onClickReject() {
      Alert.alert(
        Constant.KAppTitle,
        'Are you sure, you want to reject this request?',
        [
          {text: 'Yes', onPress: () => this.onRejectRequest()},
          {
            text: 'No',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
          },
        ],
        {cancelable: false},
      );
  }

  async onRejectRequest() {
    if (this.state.connection_Status == "Online") {

      this.setState({
        visible: true,
        loaded: false,
      });

      var data = new FormData();
      data.append('driver_id', CommonUtilsObj.userDetails.id);
      data.append('reject_type', 0);
      data.append('booking_id', this.state.requestDetails.id);

      let responseData = await APIManagerObj.callApiWithData(Constant.KRejectRequestURL, data);
      console.log('json response ....' + JSON.stringify(responseData));

      if (responseData.status == "success") {
        this.setState({
          visible: false,
        });

        setTimeout(() => {
          Toast.showWithGravity(responseData.message, Toast.LONG, Toast.TOP);
            const navigateAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: "RequestScreen"})],
          });

          this.props.navigation.dispatch(navigateAction);
        }, 100);       
      }
      else {
        this.setState({
          visible: false,
        }); 
        setTimeout(() => {
          if(typeof(responseData.message) === 'string')
          {
              Toast.showWithGravity(responseData.message, Toast.LONG, Toast.TOP)
          }
          else if(typeof(responseData.error) === 'string')
          {
              Toast.showWithGravity(responseData.error, Toast.LONG, Toast.TOP)
          }
          else {
              if (responseData.error) {
                Toast.showWithGravity(responseData.error.toString(), Toast.LONG, Toast.TOP)
              }
              else {
                Toast.showWithGravity(Message.KAPIError, Toast.LONG, Toast.TOP)
              }
          }
        }, 200); 
      }
    }
    else {
      this.setState({lastApiTag : 1})
      Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
    }
  }

  async onAcceptRequest() {
    if (this.state.connection_Status == "Online") {

      this.setState({
        visible: true,
        loaded: false,
      });

      var data = new FormData();
      data.append('driver_id', CommonUtilsObj.userDetails.id);
      data.append('accept_type', 0);
      data.append('booking_id', this.state.requestDetails.id); //this.state.requestDetails.id
      data.append('user_id', CommonUtilsObj.userDetails.id);

      let responseData = await APIManagerObj.callApiWithData(Constant.KAcceptRequestURL, data);
      console.log('json response ....' + JSON.stringify(responseData));

      if (responseData.status == "success") {
        this.setState({
          visible: false,
          isPending: false,
          isAccepted: true,
          requestDetails: responseData.trip_detail[0],
          loaded: false
        });
        this.onLoadFetchRequestDetais();
        this.onFetchRequestDetails();

        setTimeout(() => {
            Toast.showWithGravity(responseData.message, Toast.LONG, Toast.TOP)
        }, 200);       
      }
      else {
        this.setState({
          visible: false,
        }); 
        this.onFetchRequestDetails();
        //RequestAccountListScreen
        setTimeout(() => {

          if(typeof(responseData.message) === 'string')
          {
              Toast.showWithGravity(responseData.message, Toast.LONG, Toast.TOP)
          }
          else if(typeof(responseData.error) === 'string')
          {
              Toast.showWithGravity(responseData.error, Toast.LONG, Toast.TOP)
          }
          else {
              if (responseData.error) {
                Toast.showWithGravity(responseData.error.toString(), Toast.LONG, Toast.TOP)
              }
              else {
                Toast.showWithGravity(Message.KAPIError, Toast.LONG, Toast.TOP)
              }
          }

          // alert(JSON.stringify(responseData))
          if (responseData.error_code == '79') {
            const navigateAction = StackActions.reset({
              index: 0,
              actions: [NavigationActions.navigate({ routeName: "First"})],
            });

            CommonUtilsObj.navigationProps.navigation.dispatch(navigateAction);
          }
          if (responseData.error_code == '545') {
            // const navigateAction = StackActions.reset({
            //   index: 0,
            //   actions: [NavigationActions.navigate({ routeName: "First"})],
            // });

            // CommonUtilsObj.navigationProps.navigation.dispatch(navigateAction);
            this.props.navigation.navigate('RequestAccountListScreen');
          }
        }, 200); 
      }
    }
    else {
      this.setState({lastApiTag : 1})
      Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
    }
    // this.props.navigation.navigate('RequestAccountListScreen');
  }

  onClickCallCustomer() {
    console.log('onClickCallDriver');

    let phoneNumber = this.state.requestDetails.user_detail.mobile;

    if (Platform.OS !== 'android') {
        phoneNumber = `telprompt:${this.state.requestDetails.user_detail.mobile}`;
    }
    else  {
        phoneNumber = `tel:${this.state.requestDetails.user_detail.mobile}`;
    }
    
    Linking.canOpenURL(phoneNumber)
    .then(supported => {
      if (!supported) {
        Toast.showWithGravity('Phone number is not available', Toast.LONG, Toast.TOP)
      } 
      else {
        return Linking.openURL(phoneNumber);
      }
    })
    .catch(err => console.log(err));
  }

  showModal() {
    this.setState({ visibleMapOption: true });
  }

  hideModal() {
    this.setState({ 
      visibleMapOption: false,
    });
  }

  openAppleMap() {
    Linking.canOpenURL(`http://maps.apple.com/maps?daddr=${this.state.requestDetails.drop_lat},${this.state.requestDetails.drop_longs}&dirflg=d`)
    .then(supported => {
      if (!supported) {
        Toast.showWithGravity('not available', Toast.LONG, Toast.TOP)
      } 
      else {
        return Linking.openURL(`http://maps.apple.com/maps?daddr=${this.state.requestDetails.drop_lat},${this.state.requestDetails.drop_longs}&dirflg=d`);
      }
    })
    .catch(err => console.log(err));
    this.setState({ visibleMapOption: false });
  }
  
  openGoogleMap() {
    Linking.canOpenURL(`http://maps.google.com/?daddr=${this.state.requestDetails.drop_lat},${this.state.requestDetails.drop_longs}&directionsmode=driving`)
    .then(supported => {
      if (!supported) {
        Toast.showWithGravity('not available', Toast.LONG, Toast.TOP)
      } 
      else {
        return Linking.openURL(`http://maps.google.com/?daddr=${this.state.requestDetails.drop_lat},${this.state.requestDetails.drop_longs}&directionsmode=driving`);
      }
    })
    .catch(err => console.log(err));
    this.setState({ visibleMapOption: false });
  }

  onClickShowAlert(msg) {
      Alert.alert(
        Constant.KAppTitle,
        msg,
        [
          {text: 'Yes', onPress: () => this.setState({ visibleOTPModel: true })},
          {
            text: 'No',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
          },
        ],
        {cancelable: false},
      );
  }

  hideOTPModal() {

    this.setState({ 
      visibleOTPModel: false,
      txt1: '',
      txt2: '',
      txt3: '',
      txt4: '',
    });
  }

  onFocus() {
    let { errors = {} } = this.state;

    for (let name in errors) {
      let ref = this[name];

      if (ref && ref.isFocused()) {
        delete errors[name];
      }
    }

    this.setState({ errors });
  }

  updateRef(name, ref) {
    this[name] = ref;
  }

  verifyCode () {

      this.txt1.blur();
      this.txt2.blur();
      this.txt3.blur();
      this.txt4.blur();

      let otp = this.state.txt1 + this.state.txt2 + this.state.txt3 + this.state.txt4 ;
      if (this.state.requestDetails.otp == otp) {
          this.hideOTPModal();

          if (this.state.isAccepted == true) {
              this.onArrivedAtPickUp();
          }
          else if (this.state.isArrived == true) {
              this.onItemReceived();
          } 
          else if (this.state.isOnTrip == true) {
              this.onEndTrip();
          } 
      }
      else {
          this.setState({
            txt1: '',
            txt2: '',
            txt3: '',
            txt4: '',
          });
          Toast.showWithGravity('OTP not matched.', Toast.LONG, Toast.TOP)
      }
  }

  async onArrivedAtPickUp() {
    if (this.state.connection_Status == "Online") {

      this.setState({
        visible: true,
      });

      var data = new FormData();
      data.append('driver_id', CommonUtilsObj.userDetails.id);
      data.append('booking_id', this.state.requestDetails.id);

      let responseData = await APIManagerObj.callApiWithData(Constant.KArrivedAtPickUpURL, data);
      console.log('json response ....' + JSON.stringify(responseData));

      if (responseData.status == "success") {
        this.setState({
          visible: false,
          isPending: false,
          isAccepted: true,
          requestDetails: responseData.trip_detail[0],
          loaded: false,
        });
        this.onLoadFetchRequestDetais();
        this.onFetchRequestDetails();
        setTimeout(() => {
            Toast.showWithGravity(responseData.message, Toast.LONG, Toast.TOP)
        }, 200);       
      }
      else {
        this.setState({
          visible: false,
        }); 
        setTimeout(() => {
          if(typeof(responseData.message) === 'string')
          {
              Toast.showWithGravity(responseData.message, Toast.LONG, Toast.TOP)
          }
          else if(typeof(responseData.error) === 'string')
          {
              Toast.showWithGravity(responseData.error, Toast.LONG, Toast.TOP)
          }
          else {
              if (responseData.error) {
                Toast.showWithGravity(responseData.error.toString(), Toast.LONG, Toast.TOP)
              }
              else {
                Toast.showWithGravity(Message.KAPIError, Toast.LONG, Toast.TOP)
              }
          }
        }, 200); 
      }
    }
    else {
      this.setState({lastApiTag : 1})
      Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
    }
  }

  async onItemReceived() {
    if (this.state.connection_Status == "Online") {

      this.setState({
        visible: true,
      });

      var data = new FormData();
      data.append('driver_id', CommonUtilsObj.userDetails.id);
      data.append('booking_id', this.state.requestDetails.id);

      let responseData = await APIManagerObj.callApiWithData(Constant.KOnTripURL, data);
      console.log('json response ....' + JSON.stringify(responseData));

      if (responseData.status == "success") {
        this.setState({
          visible: false,
          isPending: false,
          isAccepted: true,
          requestDetails: responseData.trip_detail[0],
          loaded: false,
        });
        this.onLoadFetchRequestDetais();
        this.onFetchRequestDetails();

        setTimeout(() => {
            Toast.showWithGravity(responseData.message, Toast.LONG, Toast.TOP)
        }, 200);       
      }
      else {
        this.setState({
          visible: false,
        }); 
        setTimeout(() => {
          if(typeof(responseData.message) === 'string')
          {
              Toast.showWithGravity(responseData.message, Toast.LONG, Toast.TOP)
          }
          else if(typeof(responseData.error) === 'string')
          {
              Toast.showWithGravity(responseData.error, Toast.LONG, Toast.TOP)
          }
          else {
              if (responseData.error) {
                Toast.showWithGravity(responseData.error.toString(), Toast.LONG, Toast.TOP)
              }
              else {
                Toast.showWithGravity(Message.KAPIError, Toast.LONG, Toast.TOP)
              }
          }
        }, 200); 
      }
    }
    else {
      this.setState({lastApiTag : 1})
      Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
    }
  }

  async onEndTrip() {
    if (this.state.connection_Status == "Online") {

      this.setState({
        visible: true,
      });

      var data = new FormData();
      data.append('driver_id', CommonUtilsObj.userDetails.id);
      data.append('booking_id', this.state.requestDetails.id);
      data.append('user_id', this.state.requestDetails.user_id);
      data.append('final_amount', this.state.requestDetails.final_amount);
      data.append('discount_amount', this.state.requestDetails.discount_amount);
      data.append('final_amount_after_discount', this.state.requestDetails.final_amount_after_discount);
      data.append('final_time', this.state.requestDetails.final_time);
      data.append('actual_km', this.state.requestDetails.actual_km);
      data.append('payment_type', this.state.requestDetails.payment_type);
      data.append('base_fare', this.state.requestDetails.base_fare);
      data.append('wait_time', this.state.requestDetails.wait_time);
      data.append('trip_distance_price', this.state.requestDetails.trip_distance_price);
      data.append('trip_time_price', this.state.requestDetails.trip_time_price);
      data.append('trip_waitTime_price', this.state.requestDetails.trip_waitTime_price);

      let responseData = await APIManagerObj.callApiWithData(Constant.KCompleteTripURL, data);
      console.log('json response ....' + JSON.stringify(responseData));

      if (responseData.status == "success") {
        this.setState({
          visible: false,
        });
        
        setTimeout(() => {
            Toast.showWithGravity(responseData.message, Toast.LONG, Toast.TOP)
            this.props.navigation.navigate('ReviewUserScreen', {booking_id: responseData.booking_id, requestDetails: this.state.requestDetails});
        }, 200);       
      }
      else {
        this.setState({
          visible: false,
        }); 
        setTimeout(() => {
          if(typeof(responseData.message) === 'string')
          {
              Toast.showWithGravity(responseData.message, Toast.LONG, Toast.TOP)
          }
          else if(typeof(responseData.error) === 'string')
          {
              Toast.showWithGravity(responseData.error, Toast.LONG, Toast.TOP)
          }
          else {
              if (responseData.error) {
                Toast.showWithGravity(responseData.error.toString(), Toast.LONG, Toast.TOP)
              }
              else {
                Toast.showWithGravity(Message.KAPIError, Toast.LONG, Toast.TOP)
              }
          }
        }, 200); 
      }
    }
    else {
      this.setState({lastApiTag : 1})
      Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
    }
  }

  onClickRequestID= ({ item, index }) => {
    alert(index)
  }

  renderBokingItem = ({ item, index }) => {
    return (
        <View style = {{ width: (this.state.runningList.length > 1) ? ((Dimensions.get('window').width) * 70) /100 : (Dimensions.get('window').width) , height: 50, 
          justifyContent:'center', backgroundColor: 'transparent', alignItems: 'center', flexDirection: 'row'}}>
          <TouchableOpacity
              style={{height: '100%', width: '100%', justifyContent: 'center', alignItems: 'center'}}
              onPress={() => {
                // alert('You tapped the button!' + index);
                this.flatListRef.scrollToIndex({animated: true, index: index});
              }}
           >
          <Text style={[CustomStyles.themeButtonText, CustomStyles.whiteFont]}>Booking ID : {item.booking_id}</Text>
          </TouchableOpacity>
          <View style={{backgroundColor: '#fff', height: 50, width: 2, right: 0, position: 'absolute'}}>
          </View>
        </View>
    )
  }

  handleViewableItemsChanged(info) {
    // alert(info.viewableItems[0].index)
    let obj = {}
    obj.id = this.state.runningList[info.viewableItems[0].index].booking_id;

    this.setState({requestDetails : obj})

    setTimeout(() => {
      // console.log(this.state.requestDetails.id)
      // if (!this.state.firstTime) {

        this.onFetchRequestDetails();
      // }
    }, 200); 
  }

  render() {
    let { errors = {}, secureTextEntry, ...data } = this.state;

    return (
      <View style={CustomStyles.container1}>

        <View style={{backgroundColor: "#000", justifyContent: 'center', alignItems: 'center'}}>

        <ProgressLoader
          visible={this.state.visible}
          isModal={true} isHUD={true}
          hudColor={"#fff"}
          height={200}
          width={200}
          color={"#8edf01"} />
        </View>
        <FlatList
          horizontal
          ref={(ref) => { this.flatListRef = ref; }}
          pagingEnabled={true}
          showsHorizontalScrollIndicator={false}
          data={this.state.runningList}
          renderItem={item => this.renderBokingItem(item)}
          // keyExtractor={photo => photo.id}
          snapToInterval={(this.state.runningList.length > 1) ? ((Dimensions.get('window').width) * 70) /100 : (Dimensions.get('window').width)}
          decelerationRate="fast"
          style={{width: Dimensions.get('window').width, height: (this.state.showTopList) ? 50 : 0, backgroundColor: 'green', marginHorizontal: 0, position: 'absolute'}}
          // removeClippedSubviews={false} 
          onViewableItemsChanged={this.handleViewableItemsChanged}
          viewabilityConfig={this.viewabilityConfig}
        />
        
        { this.state.loaded && 
        <MapView 
          ref={map => {this.map = map}}
          provider = {PROVIDER_GOOGLE}
          style={{flex: 1, marginTop: (this.state.requestDetails.status == '1' ? 0 : 50)}}        
          initialRegion={{         
            latitude: (this.state.requestDetails.status == '1') ? Number(this.state.requestDetails.pickup_lat) : Number(this.state.driverLatitude),
            longitude: (this.state.requestDetails.status == '1') ? Number(this.state.requestDetails.pickup_longs) : Number(this.state.driverLongitude), 
            // latitude: Number(this.state.requestDetails.pickup_lat),          
            // longitude: Number(this.state.requestDetails.pickup_longs), 
            latitudeDelta: this.state.latDelta,
            longitudeDelta: this.state.longDelta,
          }}        
          // showsUserLocation={true}
          onRegionChangeComplete={region => {
            if (this.state.latDelta != region.latitudeDelta || this.state.longDelta != region.longitudeDelta) {
              this.setState({
              latDelta: region.latitudeDelta,
              longDelta: region.longitudeDelta,
            });
            }
          }}      
        >
        {
        // <Marker
        //   coordinate={{
        //       latitude: Number(this.state.requestDetails.pickup_lat),
        //       longitude: Number(this.state.requestDetails.pickup_longs)}}
        //   title={'Pick Up Location'}
        //   description={this.state.requestDetails.pickup_area}
        //   identifier={'mk1'}
        // />

        // <Marker
        //   coordinate={{
        //       latitude: (this.state.requestDetails.driver_lat) ? Number(this.state.requestDetails.driver_lat) : Number(this.state.requestDetails.drop_lat),
        //       longitude: (this.state.requestDetails.driver_long) ? Number(this.state.requestDetails.driver_long) :Number(this.state.requestDetails.drop_longs)}}
        //   title={'Destination Location'}
        //   description={this.state.requestDetails.drop_area}
        //   identifier={'mk2'}
        // />

        
          // <MapView.Polyline 
          //           coordinates={this.state.coords}
          //           strokeWidth={2}
          //           strokeColor="red"/>

        //   <MapViewDirections
        //   origin={{latitude: this.state.driverLatitude, longitude: this.state.driverLongitude}}
        //   destination={{latitude: Number(this.state.requestDetails.drop_lat), longitude: Number(this.state.requestDetails.drop_longs)}}
        //   apikey={Constant.KGoogleMapAPIKey}
        //   strokeWidth={3}
        //   strokeColor="red"
        // />
                  }
        {
          this.state.runningList.map((item, index) => (
            console.log(',,,,,,,' + JSON.stringify(item))
            ))
        }

        { 

            (this.state.runningList.length != 0) &&
           this.state.runningList.map((item, index) => (
            <MapViewDirections
              origin={{latitude: this.state.driverLatitude, longitude: this.state.driverLongitude}}
              destination={{latitude: (item.status == '3') ? Number(item.pickup_lat) : Number(item.drop_lat), longitude: (item.status == '3') ? Number(item.pickup_longs) : Number(item.drop_longs)}}
              apikey={Constant.KGoogleMapAPIKey}
              strokeWidth={(this.state.requestDetails.drop_lat == item.drop_lat && this.state.requestDetails.drop_longs == item.drop_longs) ? 4 : 2}
              strokeColor={(this.state.requestDetails.drop_lat == item.drop_lat && this.state.requestDetails.drop_longs == item.drop_longs) ? "blue" : "red"}
              resetOnChange={false}
            />


            ))
        }

        { 
            (this.state.runningList.length != 0) &&
           this.state.runningList.map((item, index) => (
            <Marker 
              coordinate={{
                latitude: Number(item.pickup_lat),
                longitude: Number(item.pickup_longs),
              }}
              description={item.pickup_area}
            >
            <Image source={require('../../images/pickupPin.png')} style={{height: 35, width: 35 }} resizeMode={'contain'}/>
            </Marker>

          ))
        }

        { 
            (this.state.runningList.length != 0) &&
           this.state.runningList.map((item, index) => (
            <Marker 
              coordinate={{
                latitude: Number(item.drop_lat),
                longitude: Number(item.drop_longs),
              }}
              description={item.drop_area}
            >
            <Image source={require('../../images/dropPin.png')} style={{height: 35, width: 35 }} resizeMode={'contain'}/>
            </Marker>

          ))
        }

        { 
            (this.state.requestDetails.status == '1') &&
            <MapViewDirections
              origin={{latitude: this.state.requestDetails.pickup_lat, longitude: this.state.requestDetails.pickup_longs}}
              destination={{latitude: this.state.requestDetails.drop_lat, longitude: this.state.requestDetails.drop_longs}}
              apikey={Constant.KGoogleMapAPIKey}
              strokeWidth={2}
              strokeColor={"red"}
              resetOnChange={false}
            />
        }

        { 
            (this.state.requestDetails.status == '1') &&
            <Marker 
              coordinate={{
                latitude: Number(this.state.requestDetails.pickup_lat),
                longitude: Number(this.state.requestDetails.pickup_longs),
              }}
              description={this.state.requestDetails.pickup_area}
              identifier={'mk1'}
            >
            <Image source={require('../../images/pickupPin.png')} style={{height: 35, width: 35 }} resizeMode={'contain'}/>
            </Marker>
        }

        { 
            (this.state.requestDetails.status == '1') &&
            <Marker 
              coordinate={{
                latitude: Number(this.state.requestDetails.drop_lat),
                longitude: Number(this.state.requestDetails.drop_longs),
              }}
              description={this.state.requestDetails.drop_area}
              identifier={'mk2'}
            >
            <Image source={require('../../images/dropPin.png')} style={{height: 35, width: 35 }} resizeMode={'contain'}/>
            </Marker>
        }

        { (this.state.driverLatitude != 0) &&

        <Marker 
              coordinate={{
                latitude: this.state.driverLatitude,
                longitude: this.state.driverLongitude,
              }}
              rotation={this.state.heading}
              // description={this.state.requestDetails.drop_area}
            >
            <Image source={require('../../images/carIcon.png')} style={{height: 25, width: 35 }} resizeMode={'stretch'}/>
            </Marker>
          }

        </MapView>
        }

        { (this.state.isPending && this.state.loaded) &&
          <View style={[CustomStyles.flexColunm, {margin: 20, marginTop: 15, marginBottom: this.state.viewBottom}]}>

                    <TouchableOpacity
                        style={CustomStyles.requestThemeButton}
                        onPress={this.onClickJobDetails.bind(this)}
                     >
                        <Text style={CustomStyles.themeButtonText}> Job Details </Text>
                    </TouchableOpacity>

                    <View style={[CustomStyles.flexRow, {justifyContent: 'space-between', marginTop: 15}]}>
                        <TouchableOpacity
                            style={[CustomStyles.requestThemeButton, {backgroundColor: 'white', width: ((Dimensions.get('window').width)/2)-30}]}
                            onPress={this.onClickReject.bind(this)}
                         >
                            <Text style={CustomStyles.themeButtonText}> Reject </Text>
                        </TouchableOpacity>
                        
                        <TouchableOpacity
                            style={[CustomStyles.requestThemeButton, {backgroundColor: 'white', width: ((Dimensions.get('window').width)/2)-30}]}
                            onPress={this.onAcceptRequest.bind(this)}
                         >
                            <Text style={CustomStyles.themeButtonText}> Accept </Text>
                        </TouchableOpacity>

                    </View>
          </View>
        }

        { (this.state.isAccepted && this.state.loaded) &&
          <View style={[CustomStyles.flexColunm, {margin: 20, marginTop: 15, marginBottom: this.state.viewBottom}]}>

                    <View style={[CustomStyles.flexRow, {justifyContent: 'space-between', marginRight: 15}]}>
                          <View style={[CustomStyles.flexRow, {justifyContent: 'space-between', width:105}]}>
                              <TouchableOpacity
                                  style={[CustomStyles.requestThemeButton, {backgroundColor: 'transparent', }]}
                                  onPress={this.onClickCallCustomer.bind(this)}
                               >
                                  <Text numberOfLines={2} ellipsizeMode="tail" style={[CustomStyles.themeButtonText, {color: 'white', width: 100, textAlign: 'left'}]}> Call Customer </Text>
                              </TouchableOpacity>
                              
                              <TouchableOpacity
                                  style={[CustomStyles.requestThemeButton, {backgroundColor: 'transparent'}]}
                                  onPress={this.onClickCallCustomer.bind(this)}
                               >
                                  <Image source={require('../../images/phone.png')} style={{height: 25, width: 25, marginBottom:6}} />
                              </TouchableOpacity>
                          </View>

                          <View style={{backgroundColor: Constant.KAppColor, width: 2, height: 30}}>
                          </View>

                          <View style={[CustomStyles.flexRow, {justifyContent: 'space-between', width: 105}]}>
                              <TouchableOpacity
                                  style={[CustomStyles.requestThemeButton, {backgroundColor: 'transparent'}]}
                                  onPress={this.showModal.bind(this)}
                               >
                                  <Text numberOfLines={2} ellipsizeMode="tail" style={[CustomStyles.themeButtonText, {color: 'white', width: 100, textAlign: 'left'}]}> Tap for Direction </Text>
                              </TouchableOpacity>
                              
                              <TouchableOpacity
                                  style={[CustomStyles.requestThemeButton, {backgroundColor: 'transparent'}]}
                                  onPress={this.showModal.bind(this)}
                               >
                                  <Image source={require('../../images/navigation.png')} style={{height: 25, width: 25, marginBottom: 6}} />
                              </TouchableOpacity>
                          </View>

                    </View>
                    

                    <View style={[CustomStyles.flexRow, {justifyContent: 'space-between', marginTop: 15}]}>
                        <TouchableOpacity
                            style={[CustomStyles.requestThemeButton, {backgroundColor: 'white', width: ((Dimensions.get('window').width)/3.5)-15}]}
                            onPress={this.onClickJobDetails.bind(this)}
                         >
                            <Text style={CustomStyles.themeButtonText}> Job Details </Text>
                        </TouchableOpacity>
                        
                        <TouchableOpacity
                            style={[CustomStyles.requestThemeButton, {backgroundColor: 'white', width: ((Dimensions.get('window').width)/3.5)-15}]}
                            onPress={this.onClickReject.bind(this)}
                         >
                            <Text style={CustomStyles.themeButtonText}> Cancel Job </Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            style={[CustomStyles.requestThemeButton, {width: ((Dimensions.get('window').width)/2.5)-15}]}
                            onPress={this.onClickShowAlert.bind(this, 'Are you sure, you arrived at pick up location?')}
                         >
                            <Text style={CustomStyles.themeButtonText}> Arrived at Pick Up </Text>
                        </TouchableOpacity>

                    </View>
          </View>
        }

        { (this.state.isArrived && this.state.loaded) &&
          <View style={[CustomStyles.flexColunm, {margin: 20, marginTop: 15, marginBottom: this.state.viewBottom}]}>

                    <TouchableOpacity
                        style={CustomStyles.requestThemeButton}
                        onPress={this.onClickShowAlert.bind(this,'Are you sure, you received dogs?')}
                     >
                        <Text style={CustomStyles.themeButtonText}> Dogs Received </Text>
                    </TouchableOpacity>

                    <View style={[CustomStyles.flexRow, {justifyContent: 'space-between', marginTop: 15}]}>
                        <TouchableOpacity
                            style={[CustomStyles.requestThemeButton, {backgroundColor: 'white', width: ((Dimensions.get('window').width)/2)-30}]}
                            onPress={this.onClickJobDetails.bind(this)}
                         >
                            <Text style={CustomStyles.themeButtonText}> Job Details </Text>
                        </TouchableOpacity>
                        
                        <TouchableOpacity
                            style={[CustomStyles.requestThemeButton, {backgroundColor: 'white', width: ((Dimensions.get('window').width)/2)-30}]}
                            onPress={this.onClickReject.bind(this)}
                         >
                            <Text style={CustomStyles.themeButtonText}> Cancel Job </Text>
                        </TouchableOpacity>

                    </View>
          </View>
        }

        { (this.state.isOnTrip && this.state.loaded) &&

          <View style={[CustomStyles.flexColunm, {margin: 20, marginTop: 15, marginBottom: this.state.viewBottom}]}>

                    <View style={[CustomStyles.flexRow, {justifyContent: 'space-between', marginRight: 15}]}>
                          <View style={[CustomStyles.flexRow, {justifyContent: 'space-between', width:105}]}>
                              <TouchableOpacity
                                  style={[CustomStyles.requestThemeButton, {backgroundColor: 'transparent', }]}
                                  onPress={this.onClickCallCustomer.bind(this)}
                               >
                                  <Text numberOfLines={2} ellipsizeMode="middle" style={[CustomStyles.themeButtonText, {color: 'white', width: 100, textAlign: 'left'}]}> Call Customer </Text>
                              </TouchableOpacity>
                              
                              <TouchableOpacity
                                  style={[CustomStyles.requestThemeButton, {backgroundColor: 'transparent'}]}
                                  onPress={this.onClickCallCustomer.bind(this)}
                               >
                                  <Image source={require('../../images/phone.png')} style={{height: 25, width: 25, marginBottom:6}} />
                              </TouchableOpacity>
                          </View>

                          <View style={{backgroundColor: Constant.KAppColor, width: 2, height: 30}}>
                          </View>

                          <View style={[CustomStyles.flexRow, {justifyContent: 'space-between', width: 105}]}>
                              <TouchableOpacity
                                  style={[CustomStyles.requestThemeButton, {backgroundColor: 'transparent'}]}
                                  onPress={this.showModal.bind(this)}
                               >
                                  <Text numberOfLines={2} ellipsizeMode="middle" style={[CustomStyles.themeButtonText, {color: 'white', width: 100, textAlign: 'left'}]}> Tap for Direction </Text>
                              </TouchableOpacity>
                              
                              <TouchableOpacity
                                  style={[CustomStyles.requestThemeButton, {backgroundColor: 'transparent'}]}
                                  onPress={this.showModal.bind(this)}
                               >
                                  <Image source={require('../../images/navigation.png')} style={{height: 25, width: 25, marginBottom: 6}} />
                              </TouchableOpacity>
                          </View>

                    </View>
                    

                    <View style={[CustomStyles.flexRow, {justifyContent: 'space-between', marginTop: 15}]}>
                        <TouchableOpacity
                            style={[CustomStyles.requestThemeButton, {backgroundColor: 'white', width: ((Dimensions.get('window').width)/2)-30}]}
                            onPress={this.onClickJobDetails.bind(this)}
                         >
                            <Text style={CustomStyles.themeButtonText}> Job Details </Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            style={[CustomStyles.requestThemeButton, {backgroundColor: 'white', width: ((Dimensions.get('window').width)/2)-30}]}
                            onPress={this.onClickShowAlert.bind(this, 'Are you sure, you want to end this trip?')}
                         >
                            <Text style={CustomStyles.themeButtonText}> End Trip </Text>
                        </TouchableOpacity>

                    </View>
          </View>
          
        }

        <Modal
          animationType="slide" // fade
          transparent={true}
          visible={this.state.visibleMapOption}>
          <View style={{flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}>
              <View style={{backgroundColor: 'black', height: 210, width: (Dimensions.get('window').width)-80}}>
                  <View style={{ backgroundColor: '#8edf01', flexDirection: 'row', justifyContent: 'center', height:40, marginBottom:10}}>
                      <Text style={{color: 'black', fontWeight: 'bold',alignSelf: 'center', textAlign: 'center', padding: 15}}>Select Map</Text>
                      
                  </View>
                  <TouchableOpacity
                      style={[CustomStyles.requestThemeButton, {backgroundColor: 'transparent'}]}
                      onPress={this.openGoogleMap.bind(this)}
                  >
                      <Text style={[CustomStyles.themeButtonText, {color: 'white'}]}> Google Map</Text>
                  </TouchableOpacity>

                  <View style={{backgroundColor: 'gray', height: 1, margin: 8}}>
                  </View>

                  <TouchableOpacity
                      style={[CustomStyles.requestThemeButton, {backgroundColor: 'transparent'}]}
                      onPress={this.openAppleMap.bind(this)}
                  >
                      <Text style={[CustomStyles.themeButtonText, {color: 'white'}]}> Apple Map </Text>
                  </TouchableOpacity>

                  <View style={{backgroundColor: 'gray', height: 1, margin: 8}}>
                  </View>

                  <TouchableOpacity
                      style={[CustomStyles.requestThemeButton, {backgroundColor: 'transparent'}]}
                      onPress={this.hideModal.bind(this)}
                  >
                      <Text style={[CustomStyles.themeButtonText, {color: 'red', paddingBottom: 15}]}> Cancel </Text>
                  </TouchableOpacity>
              </View>
          </View>
        </Modal>

        <Modal
          animationType="slide" // fade
          transparent={true}
          visible={this.state.visibleOTPModel}>
          <View style={{flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}>
          <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false} style={{backgroundColor: 'gray'}}>
              <View style={{backgroundColor: 'black', height: 135, width: (Dimensions.get('window').width)-80}}>
              <TouchableOpacity
                        onPress={() => this.hideOTPModal()} 
                        style={{position: 'absolute', padding: 5, backgroundColor: Constant.KAppColor, marginLeft: (Dimensions.get('window').width)-98, marginTop:-12, height:32, width:32, borderRadius: 16, justifyContent: 'center', alignItems: 'center'}}
                        >
                        <Image
                              ref="imgChecked"
                              style={{height: 20, width: 20, position: 'absolute', }}
                              source={require('../../images/cancel.png')}
                            />
                        </TouchableOpacity>
                    <View style={{ flexDirection: 'column', justifyContent: 'center',marginBottom:3, marginTop: 15}}>
                        

                        <Text style={{color: 'black', fontWeight: 'bold',alignSelf: 'center', textAlign: 'center', color: 'white', fontSize: headerFontSize+3, marginBottom: 5}}>Please enter 4-digit code</Text>
                        <Text style={{color: 'black', fontSize: txtSmallSize, alignSelf: 'center', textAlign: 'center', color:'white'}}>The customer has 4-digit code</Text>
                    </View>

                    <View style={{height: txtInputHeight, marginTop: 10, borderRadius: 5, flexDirection: 'row', justifyContent: 'center' }}>
                        <TextInput
                          style={CustomStyles.requestTextInput}
                          ref={this.txt1Ref}
                          keyboardType='phone-pad'
                          autoCapitalize='none'
                          autoCorrect={true}
                          enablesReturnKeyAutomatically={true}
                          onFocus={this.onFocus}
                          onChangeText={(txt1) => {
                            console.log(txt1);

                            this.setState({txt1});
                            if (txt1 != '') {
                              this.txt2.focus();
                            }
                          }}
                          onKeyPress={({ nativeEvent }) => {
                              nativeEvent.key === 'Backspace' ? this.txt1.focus() : this.txt2.focus()
                            }}
                          returnKeyType='next'
                          maxLength={1}
                          value = {this.state.txt1}
                        />

                        <TextInput
                          style={CustomStyles.requestTextInput}
                          ref={this.txt2Ref}
                          keyboardType='phone-pad'
                          autoCapitalize='none'
                          autoCorrect={true}
                          enablesReturnKeyAutomatically={true}
                          onFocus={this.onFocus}
                          onChangeText={(txt2) => {
                            this.setState({txt2});
                            if (txt2 != '') {
                              this.txt3.focus();
                            }
                          }
                          }
                          onKeyPress={({ nativeEvent }) => {
                              nativeEvent.key === 'Backspace' ? this.txt1.focus() : this.txt3.focus()
                            }}
                          returnKeyType='next'
                          maxLength={1}
                          value = {this.state.txt2}
                        />

                        <TextInput
                          style={CustomStyles.requestTextInput}
                          ref={this.txt3Ref}
                          keyboardType='phone-pad'
                          autoCapitalize='none'
                          autoCorrect={true}
                          enablesReturnKeyAutomatically={true}
                          onFocus={this.onFocus}
                          onChangeText={(txt3) => {
                            this.setState({txt3});
                            if (txt3 != '') {
                              this.txt4.focus();
                            }
                          }}
                          onKeyPress={({ nativeEvent }) => {
                              nativeEvent.key === 'Backspace' ? this.txt2.focus() : this.txt4.focus()
                            }}
                          returnKeyType='next'
                          maxLength={1}
                          value = {this.state.txt3}
                        />

                        <TextInput
                          style={CustomStyles.requestTextInput}
                          ref={this.txt4Ref}
                          keyboardType='phone-pad'
                          autoCapitalize='none'
                          autoCorrect={true}
                          enablesReturnKeyAutomatically={true}
                          onFocus={this.onFocus}
                          onChangeText={(txt4) => {
                            this.setState({txt4});
                            if (txt4 != '') {
                              this.txt4.blur();
                            }

                            setTimeout(() => {
                                this.verifyCode();
                            }, 100);
                          }}
                          onKeyPress={({ nativeEvent }) => {
                              nativeEvent.key === 'Backspace' ? this.txt3.focus() : this.txt4.blur()
                            }}
                          returnKeyType='next'
                          maxLength={1}
                          value = {this.state.txt4}
                        />
                    </View>
              </View>
          </TouchableWithoutFeedback>
          </View>
        </Modal>

      </View>
      
    );
  }
}







