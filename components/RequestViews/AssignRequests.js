import React from 'react';
import { StyleSheet, Dimensions, YellowBox, Text, View, Button, TextInput, Image, ScrollView, TouchableOpacity, Alert, SafeAreaView, PixelRatio } from 'react-native';
import {KeyboardAvoidingView, Keyboard, TouchableWithoutFeedback, TouchableHighlight} from 'react-native';
import { NavigationBar } from 'navigationbar-react-native';
import userDefaults from 'react-native-user-defaults'
import { Modal, FlatList, SectionList, ActivityIndicator } from 'react-native';
import NetInfo from "@react-native-community/netinfo";
import ProgressLoader from 'rn-progress-loader';
import Toast from 'react-native-simple-toast';
import 'react-native-gesture-handler'

import CommonUtilsObj from '../CommonClasses/CommonUtils';
import Constant from '../CommonClasses/Constant';

const imageSize = (((Dimensions.get('window').width) * 30) / 100);

const headerFontSize = (((Dimensions.get('window').width) * 3.8) / 100);
const txtMediumSize = (((Dimensions.get('window').width) * 3.4) / 100);
const txtSmallSize = (((Dimensions.get('window').width) * 3.2) / 100);

export default class AssignRequests extends React.Component {

  constructor(props) {
 
    super(props);
 
    YellowBox.ignoreWarnings([
      'Warning: componentWillMount is deprecated',
      'Warning: componentWillReceiveProps is deprecated',
    ]);
    this.state = {
      visible: false,
      loading: false,
      connection_Status : "",
      loadMoreCompleted: false,
      requestList: [],
      offset: 0,
      pullToRefresh: false,
    };
  }

  componentDidMount() {

    NetInfo.isConnected.addEventListener(
        'connectionChange',
        this._handleConnectivityChange

    );
   
    NetInfo.isConnected.fetch().done((isConnected) => {

      if(isConnected == true)
      {
        this.setState({connection_Status : "Online"})
        this.onFetchRequestList();
      }
      else
      {
        this.setState({connection_Status : "Offline"})
      }
    });
  }

  componentWillUnmount() {
    NetInfo.isConnected.removeEventListener(
        'connectionChange',
        this._handleConnectivityChange
 
    );
  }

  componentWillUnmount() {
    NetInfo.isConnected.removeEventListener(
        'connectionChange',
        this._handleConnectivityChange
    );
  }

  _handleConnectivityChange = (isConnected) => {
    if(isConnected == true)
      {
        this.setState({connection_Status : "Online"})
      }
      else
      {
        this.setState({connection_Status : "Offline"})
      }
  };

  async onFetchRequestList() {

    if (this.state.connection_Status == "Online") {

      this.setState({
        visible: true,
        loading: true
    });

    // var data = {
    //     driver_id: CommonUtilsObj.userDetails.id,
    //     off: this.state.offset,
    // };

    var data = new FormData();
    data.append('driver_id', CommonUtilsObj.userDetails.id);
    data.append('off', this.state.offset);

    console.log(data);
    try {
        let response = await fetch(
          Constant.KGetAssignJobListURL,
          {
            method: "POST",
            headers: {
             "Accept": "application/json",
             // "Content-Type": "application/json"
             'Content-Type': 'multipart/form-data',
            },
            body: data //JSON.stringify(data)
         }
        )
    .then((response) => response.json())
    .then((responseData) => {
       console.log('json response ....' + JSON.stringify(responseData));
       this.setState({ pullToRefresh: false });
       if (responseData.status == "success") {

            var list1=this.state.requestList
            var list=responseData.all_trip;
            list1.push(...list)
       
            const items = list1.map(function(item){
                if (item.status == '1' || item.status == '2' || item.status == '5') {
                    item.statusText = 'Pending';
                    item.statusImg = require('../../images/pending.png')
                }
                else if (item.status == '3') {
                    item.statusText = 'Accepted';
                    item.statusImg = require('../../images/acceptRequest.png')
                }
                else if (item.status == '4' || item.status == '6') {
                    item.statusText = 'Cancelled';
                    item.statusImg = require('../../images/cancelRequest.png')
                }
                else if (item.status == '7') {
                    item.statusText = 'Arrived';
                    item.statusImg = require('../../images/ArrivedRequest.png')
                }
                else if (item.status == '8') {
                    item.statusText = 'On Trip';
                    item.statusImg = require('../../images/onTripRequest.png')
                }
                else if (item.status == '9') {
                    item.statusText = 'Completed';
                    item.statusImg = require('../../images/completed.png')
                }
                else {
                    item.statusText = '';
                    item.statusImg = require('../../images/cancelRequest.png')
                }
            });

            this.setState({
              visible: false,
              offset: responseData.offset,
              requestList: list1,
              loading: false,
            });

        }
        else {
          this.setState({
              visible: false,
              loading: false,
              loadMoreCompleted: true,
          });

        }
    })
    .done();
 
    } catch (errors) {
      this.setState({
          visible: false,
          loading: false,
          loadMoreCompleted: true,
      });
      let err = "Error is" + errors
      Toast.showWithGravity(err, Toast.SHORT, Toast.TOP)
    }
    }
    else {
      Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
    }
     
  }
  
  renderRequestItem = ({ item, index }) => (
    <RequestListCell
      item={item}
      index={index}
      onSelectRequestCell={this.onSelectRequestCell}
      requestList={this.state.requestList}
    />
  );

  returnData(isUpdated) {
    this.setState({isUpdated: isUpdated});
  }

  onSelectRequestCell = (item, index) => {

    if (item.status == '1' || item.status == '2' || item.status == '5') {
        this.props.navigation.navigate('SearchRequestScreen', {returnData: this.returnData.bind(this), requestDetails: item});
    }
    else if (item.status == '3') {
        this.props.navigation.navigate({ routeName: 'RequestDetailsScreen', key: 'RequestHome', params: { requestDetails: item, returnData: this.returnData.bind(this) } });
        // this.props.navigation.navigate('RequestDetailsScreen', {returnData: this.returnData.bind(this), requestDetails: item});
    }
    else if (item.status == '4' || item.status == '6') {
        this.props.navigation.navigate({ routeName: 'RequestDetailsScreen', key: 'RequestHome', params: { requestDetails: item, returnData: this.returnData.bind(this) } });
    }
    else if (item.status == '7') {
        this.props.navigation.navigate({ routeName: 'RequestDetailsScreen', key: 'RequestHome', params: { requestDetails: item, returnData: this.returnData.bind(this) } });
    }
    else if (item.status == '8') {
        this.props.navigation.navigate({ routeName: 'RequestDetailsScreen', key: 'RequestHome', params: { requestDetails: item, returnData: this.returnData.bind(this) } });
    }
    else if (item.status == '9') {
        this.props.navigation.navigate({ routeName: 'RequestDetailsScreen', key: 'RequestHome', params: { requestDetails: item, returnData: this.returnData.bind(this) } });
    }
    
  };

  onRefresh() {

    this.setState({
        pullToRefresh:true,
        requestList: [],
        offset: 0,
        loading: false,
        loadMoreCompleted: false,
      });

    if (this.state.connection_Status == "Online") {
      
      setTimeout(() => {

          this.onFetchRequestList();
          
      }, 2000);
      
    }
    else {
      this.setState({ pullToRefresh: false });
      Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
    }
  }

  handleLoadMore = () => {
    if (!this.state.loading && !this.state.loadMoreCompleted){
      
      this.onFetchRequestList();
      
    }
  };

  emptyComponent = () => {
    return (
        <View style={customStyles.emptyContainer}>
          <Text style={customStyles.emptyText}>No records found</Text>
        </View>
    );
    
  };

  renderFooter = () => {
    if (!this.state.loading) return null;
    return (
      <ActivityIndicator
        style={{ color: '#000' }}
      />
    );
  };


  render() {

    if (this.state.loading && this.page === 1) {
      return <View style={{
        width: '100%',
        height: '100%'
      }}><ActivityIndicator style={{ color: '#000' }} /></View>;
    }

    return (
      <View style={customStyles.container}>
          <FlatList
                data={this.state.requestList}
                onRefresh={() => this.onRefresh()}
                refreshing={this.state.pullToRefresh}
                ListFooterComponent={this.renderFooter.bind(this)}
                onEndReachedThreshold={0.4}
                onEndReached={this.handleLoadMore.bind(this)}
                renderItem={this.renderRequestItem}
                keyExtractor={(item, index) => index.toString()}
                ListEmptyComponent={this.emptyComponent.bind(this)}
          />   
      </View>
      
    );
  }
}


const customStyles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000',
    
  },
  flexColunm:{
      flexDirection: 'column',
  },
  flexRow:{
      flexDirection: 'row'
  },
  mediumFont: {
    fontSize: txtMediumSize,
    fontFamily: Constant.KThemeFontRegular,
 },
 smallFont: {
    fontSize: txtSmallSize,
    fontFamily: Constant.KThemeFontRegular,
 },
 headerFont: {
  fontSize: headerFontSize,
  fontFamily: Constant.KThemeFontSemiBold,
  },
 textStyle: {
    fontSize: 20,
    color: 'white',
    fontWeight: 'bold'
  },
  countryStyle: {
    flex: 1,
  },
  emptyContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    height: (Dimensions.get('window').height)-150,
  },
  emptyText: {
    fontSize: (((Dimensions.get('window').width) * 5.5) / 100),
    fontFamily: Constant.KThemeFontSemiBold,
    color: '#fff',
  },
});

class RequestListCell extends React.Component {
  render() {
    const { item, index, onSelectRequestCell, requestList} = this.props;
    return (
      //RequestDetailsScreen
      <View style={requestCellStyles.container}>
        
        <TouchableOpacity
                    onPress={this.props.onSelectRequestCell.bind(this, item, index)}
                    activeOpacity = {1.0}
              >
               
              
        <View style={requestCellStyles.topContainer}>
            <Image source={{uri: Constant.KCarImagePathURL + item.icon}} style={[requestCellStyles.photo, {flex: 0.2, alignSelf: 'center', margin: 5}]} />
            <View style={{flexDirection:"column", flex: 0.8, justifyContent: 'center'}}>
                <Text style={[customStyles.headerFont, requestCellStyles.blackFont]}>${item.final_amount_after_discount}</Text>
                <Text style={[customStyles.smallFont, requestCellStyles.blackFont]}>{item.pickup_date_time}</Text>
                <Text style={[customStyles.smallFont, requestCellStyles.blackFont]}>{item.car_type}</Text>
            </View>

            <View style={[customStyles.flexColunm, {justifyContent: 'center', flex: 0.2, alignItems: 'center', margin: 5}]}>
            <Image 
                source={ 
                  (item.user_detail != null) ? {uri: Constant.KUserImagePathURL + item.user_detail.image} : require('../../images/userPlaceholder.png')
                } 
                style={[requestCellStyles.photo, {borderRadius: 25, marginBottom: 3}]} 
            />
            </View>
        </View>

        <View style={{flexDirection:"row", justifyContent: 'space-between', marginLeft: 10, marginRight: 10, marginBottom: 20}}>
            <Text style={[customStyles.headerFont, requestCellStyles.whiteFont,{alignSelf:'flex-start'}]}>Booking Id : {item.id}</Text>
            <View style={{alignSelf:'flex-end', flexDirection:"row", justifyContent: 'center'}}>
                <Image source={item.statusImg} style={{height: 15, width: 15, marginRight: 3}} />
                <Text style={[customStyles.smallFont, requestCellStyles.whiteFont]}>{item.statusText}</Text>
            </View>
        </View>
        

        <View style={{flexDirection:"row", flex: 1, marginBottom: 20, marginRight: 10}}>

            <View style={requestCellStyles.pickUpImage}>
                <Image source={require('../../images/pickUp.png')} style={{height: 20, width: 20}} />
                <Image style={{height: "100%", width: 1, backgroundColor:'white',}} />
            </View> 

            <View style={{flexDirection:"column", flex: 0.9,}}>
                    <Text style={[requestCellStyles.garyFont, customStyles.smallFont]}>Pick Up Location</Text>
                    <Text style={[requestCellStyles.whiteFont, customStyles.mediumFont, requestCellStyles.margin5]}>{item.pickup_area}</Text>
            </View>
           
        </View>

        <View style={{flexDirection:"row", flex: 1, marginBottom: 20, marginRight: 10}}>

            <View style={{flexDirection:"column", flex: 0.1, alignItems: 'center'}}>
                <Image source={require('../../images/destination.png')} style={{height: 20, width: 20}} />
            </View>

            <View style={{flexDirection:"column", flex: 0.9,}}>
                <View style={{flexDirection:"column"}}>
                    <Text style={[requestCellStyles.garyFont, customStyles.smallFont]}>Destination Location</Text>
                    <Text style={[requestCellStyles.whiteFont, customStyles.mediumFont, requestCellStyles.margin5]}>{item.drop_area}</Text>
                </View>
            </View>
           
        </View>

        </TouchableOpacity>
    </View>
      
    );
  }
}

const requestCellStyles = StyleSheet.create({
    container: {
        flex: 1,
        margin:15,
        borderRadius: 5,
        backgroundColor: 'black',
        borderColor: 'gray',
        borderWidth: 0.5,
        marginBottom: 0,
    },
    topContainer: {
        flex: 1,
        backgroundColor: '#fff',
        height: 80,
        flexDirection:"row",
        marginBottom: 15,
        borderTopRightRadius: 5,
        borderTopLeftRadius: 5,
    },
    whiteFont: {
      color: '#fff',
    },
    blackFont: {
      color: '#000',
    },
    appColorFont: {
      color: Constant.KAppColor,
    },
    garyFont: {
      color: 'gray',
    },
    title: {
        fontSize: 16,
    },
    container_text: {
        flex: 1,
        flexDirection: 'column',
        marginLeft: 12,
        justifyContent: 'center',
    },
    description: {
        fontSize: 11,
        fontStyle: 'italic',
    },
    photo: {
        height: 50,
        width: 50,        
    },
    pickUpImage: {
        flexDirection:"column", 
        flex: 0.1, 
        alignItems: 'center',
    },
    margin5: {
      marginTop: 5,
    }
});