import React from 'react';
import { StyleSheet, Text, View, Image, YellowBox, Dimensions, TouchableOpacity, Alert, ScrollView, Button } from 'react-native';
import { NavigationBar } from 'navigationbar-react-native';
import NetInfo from "@react-native-community/netinfo";
import ProgressLoader from 'rn-progress-loader';
import Toast from 'react-native-simple-toast';

import CommonUtilsObj from '../CommonClasses/CommonUtils';
import Constant from '../CommonClasses/Constant';

import userDefaults from 'react-native-user-defaults'

const headerFontSize = (((Dimensions.get('window').width) * 4) / 100);
const txtMediumSize = (((Dimensions.get('window').width) * 3.8) / 100);
const txtSmallSize = (((Dimensions.get('window').width) * 3.6) / 100);
const lineWidth = (Dimensions.get('window').width) - 40
const imageSize = ((Dimensions.get('window').width) - 55) / 3

export default class MediaDetails extends React.Component {

  constructor(props) {
 
    super(props);
 
    YellowBox.ignoreWarnings([
      'Warning: componentWillMount is deprecated',
      'Warning: componentWillReceiveProps is deprecated',
    ]);
    
    const { navigation } = this.props;  
    const details = navigation.getParam('requestDetails', '');
    this.state = {
        requestDetails: details,
        dogSelectedIndex: 0,
        media1: '',
        media2: '',
        media3: '',
    };

    this.onSelectDogTab=this.onSelectDogTab.bind(this);
    

  }

  componentDidMount() {
      this.setMediaImageSource()
  }

  setMediaImageSource() {

      switch(this.state.requestDetails.media.length) {
 
      case 1:
        this.setState({
          media1: {uri: Constant.KMediaPathURL + this.state.requestDetails.media[0].image_name},
          media2: '',
          media3: '',
        });
        break;
      
      case 2:
        this.setState({
          media1: {uri: Constant.KMediaPathURL + this.state.requestDetails.media[0].image_name},
          media2: {uri: Constant.KMediaPathURL + this.state.requestDetails.media[1].image_name},
          media3: '',
        });
        break;

      case 3:
        this.setState({
          media1: {uri: Constant.KMediaPathURL + this.state.requestDetails.media[0].image_name},
          media2: {uri: Constant.KMediaPathURL + this.state.requestDetails.media[1].image_name},
          media3: {uri: Constant.KMediaPathURL + this.state.requestDetails.media[2].image_name},
        });
        break;

      default:
        console.log('');
    
    }
  }

  onSelectDogTab(index) {
      this.setState({dogSelectedIndex : index})
  }

  renderTabs() {
    return this.state.requestDetails.doginfo.map((item, index) => {
        return (
            <TouchableOpacity
                activeOpacity = {1.0}
                style={ (this.state.dogSelectedIndex == index) ? [customStyles.selectedTab, {width: ((Dimensions.get('window').width) - 45) / this.state.requestDetails.doginfo.length}] : [customStyles.unselectedTab, {width: ((Dimensions.get('window').width) - 45) / this.state.requestDetails.doginfo.length}]}
                onPress={this.onSelectDogTab.bind(this, index)}
            >
                <Text style={[customStyles.whiteFont, customStyles.mediumFont, {textAlign: 'center'}]}>Dog{index+1}</Text>
            </TouchableOpacity>
        );
    });
  }

  render() {

    return (
      <View style={customStyles.container}>
          <ScrollView>
              <View style={[customStyles.flexColunm, {margin: 20}]}>
                  <View style={[customStyles.flexRow, {justifyContent: 'center'}]}>
                      <Image source={this.state.media1} style={[{height: imageSize, width: imageSize, alignSelf: 'center', margin: 5, borderRadius: 5, backgroundColor: '#fff'}]} />
                      <Image source={this.state.media2} style={[{height: imageSize, width: imageSize, alignSelf: 'center', margin: 5, borderRadius: 5, backgroundColor: '#fff'}]} />
                      <Image source={this.state.media3} style={[{height: imageSize, width: imageSize, alignSelf: 'center', margin: 5, borderRadius: 5, backgroundColor: '#fff'}]} />
                  </View>
                  
                  <View style={[customStyles.flexColunm, customStyles.margin20]}>
                      <Text style={[customStyles.whiteFont, customStyles.headerFont]}>Detail description</Text>
                      <Text style={[customStyles.whiteFont, customStyles.mediumFont, customStyles.margin10,]}>{this.state.requestDetails.media_description}</Text>
                  </View>

                  <View style={[customStyles.flexColunm, customStyles.margin20]}>
                      <Text style={[customStyles.whiteFont, customStyles.headerFont]}>Dog details</Text>
                      <View style={[customStyles.flexRow, customStyles.margin10, {justifyContent: 'space-between', borderColor: 'gray', borderWidth: 0.5, backgroundColor: 'gray'}]}>
                          {
                              this.renderTabs()
                          }
                      </View>
                  </View>

                  <View style={[customStyles.flexColunm, customStyles.margin10, {backgroundColor: '#fff', padding: 10, borderRadius: 5, paddingBottom: 10}]}>
                      <View style={[customStyles.flexRow]}>
                          <Text style={[customStyles.blackFont, customStyles.smallFont]}>Dog's Name : </Text>
                          <Text style={[customStyles.garyFont, customStyles.smallFont]}>{this.state.requestDetails.doginfo[this.state.dogSelectedIndex].dog_name}</Text>
                      </View>

                      <View style={[customStyles.flexRow, customStyles.margin10]}>
                          <Text style={[customStyles.blackFont, customStyles.smallFont]}>Dog's Breed : </Text>
                          <Text style={[customStyles.garyFont, customStyles.smallFont]}>{this.state.requestDetails.doginfo[this.state.dogSelectedIndex].dog_breed}</Text>
                      </View>

                      <View style={[customStyles.flexRow, customStyles.margin10]}>
                          <Text style={[customStyles.blackFont, customStyles.smallFont]}>Dog's Weight : </Text>
                          <Text style={[customStyles.garyFont, customStyles.smallFont]}>{this.state.requestDetails.doginfo[this.state.dogSelectedIndex].dog_weight} kg</Text>
                      </View>

                      <Text style={[customStyles.blackFont, customStyles.smallFont, customStyles.margin15]}>Is your dog current on all vaccinations?</Text>
                      <View style={[customStyles.flexRow, customStyles.margin10]}>

                        <View style={{ flex: 0.5, marginRight: 10, flexDirection: 'row'}}>
                            
                            <Image
                                  style={{height:20, width: 20, marginRight: 5}}
                                  source={ this.state.requestDetails.doginfo[this.state.dogSelectedIndex].dog_vaccinations == 'yes' ?                  
                                  require('../../images/check.png') : 
                                  require('../../images/checkOff.png')}
                                />
                            <Text style={[customStyles.garyFont, customStyles.smallFont]}>Yes</Text>
                        </View>

                        <View style={{ flex: 0.5, marginLeft: 10, flexDirection: 'row'}}>
                            
                            <Image
                                  style={{height:20, width: 20, marginRight: 5}}
                                  source={ this.state.requestDetails.doginfo[this.state.dogSelectedIndex].dog_vaccinations != 'yes' ?                  
                                  require('../../images/check.png') : 
                                  require('../../images/checkOff.png')}
                                />
                            <Text style={[customStyles.garyFont, customStyles.smallFont]}>No</Text>
                        </View>
                      </View>

                      <Text style={[customStyles.blackFont, customStyles.smallFont, customStyles.margin15]}>Will your dog be in a kennel/carrier?</Text>
                      <View style={[customStyles.flexRow, customStyles.margin10]}>

                        <View style={{ flex: 0.5, marginRight: 10, flexDirection: 'row'}}>
                            
                            <Image
                                  style={{height:20, width: 20, marginRight: 5}}
                                  source={ this.state.requestDetails.doginfo[this.state.dogSelectedIndex].dog_carrier == 'yes' ?                  
                                  require('../../images/check.png') : 
                                  require('../../images/checkOff.png')}
                                />
                            <Text style={[customStyles.garyFont, customStyles.smallFont]}>Yes</Text>
                        </View>

                        <View style={{ flex: 0.5, marginLeft: 10, flexDirection: 'row'}}>
                            
                            <Image
                                  style={{height:20, width: 20, marginRight: 5}}
                                  source={ this.state.requestDetails.doginfo[this.state.dogSelectedIndex].dog_carrier != 'yes' ?                  
                                  require('../../images/check.png') : 
                                  require('../../images/checkOff.png')}
                                />
                            <Text style={[customStyles.garyFont, customStyles.smallFont]}>No</Text>
                        </View>
                      </View>

                      { this.state.requestDetails.doginfo[this.state.dogSelectedIndex].dog_carrier == 'yes' &&
                          <View style={[customStyles.flexRow, customStyles.margin10]}>
                              <Text style={[customStyles.blackFont, customStyles.smallFont]}>Kennel/carrier size : </Text>
                              <Text style={[customStyles.garyFont, customStyles.smallFont]}>{this.state.requestDetails.doginfo[this.state.dogSelectedIndex].carrier_size}</Text>
                          </View>
                      }

                      <Text style={[customStyles.blackFont, customStyles.smallFont, customStyles.margin15]}>Does your dog have specific needs?</Text>
                      <View style={[customStyles.flexRow, customStyles.margin10]}>

                        <View style={{ flex: 0.5, marginRight: 10, flexDirection: 'row'}}>
                            
                            <Image
                                  style={{height:20, width: 20, marginRight: 5}}
                                  source={ this.state.requestDetails.doginfo[this.state.dogSelectedIndex].pets_special_needs != '' ?                  
                                  require('../../images/check.png') : 
                                  require('../../images/checkOff.png')}
                                />
                            <Text style={[customStyles.garyFont, customStyles.smallFont]}>Yes</Text>
                        </View>

                        <View style={{ flex: 0.5, marginLeft: 10, flexDirection: 'row'}}>
                            
                            <Image
                                  style={{height:20, width: 20, marginRight: 5}}
                                  source={ this.state.requestDetails.doginfo[this.state.dogSelectedIndex].pets_special_needs == '' ?                  
                                  require('../../images/check.png') : 
                                  require('../../images/checkOff.png')}
                                />
                            <Text style={[customStyles.garyFont, customStyles.smallFont]}>No</Text>
                        </View>
                      </View>

                      { this.state.requestDetails.doginfo[this.state.dogSelectedIndex].pets_special_needs != '' &&
                          <View style={[customStyles.flexRow, customStyles.margin10]}>
                              <Text style={[customStyles.garyFont, customStyles.smallFont]}>{this.state.requestDetails.doginfo[this.state.dogSelectedIndex].pets_special_needs}</Text>
                          </View>
                      }
                      

                  </View>

              </View>
          </ScrollView>
      </View>
    );
  }
}

const customStyles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000',    
  },
  flexColunm:{
      flexDirection: 'column',
  },
  flexRow:{
      flexDirection: 'row'
  },
  mediumFont: {
    fontSize: txtMediumSize,
    fontFamily: Constant.KThemeFontRegular,
  },
  headerFont: {
    fontSize: headerFontSize,
    fontFamily: Constant.KThemeFontSemiBold,
  },
  smallFont: {
    fontSize: txtSmallSize,
    fontFamily: Constant.KThemeFontRegular,
  },
  whiteFont: {
    color: '#fff',
  },
  blackFont: {
    color: '#000',
  },
  garyFont: {
    color: 'gray',
  },
  margin10: {
    marginTop: 10,
  },
  margin15: {
    marginTop: 15,
  },
  margin20: {
    marginTop: 25,
  },
  selectedTab: {
    justifyContent: 'center', 
    alignItems: 'center', 
    backgroundColor: 'gray', 
    height: 40, 
  },
  unselectedTab: {
    justifyContent: 'center', 
    alignItems: 'center', 
    backgroundColor: '#000', 
    height: 40, 
  }
});







