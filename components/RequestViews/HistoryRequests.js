import React from 'react';
import { StyleSheet, Dimensions, YellowBox, Text, View, Button, TextInput, Image, ScrollView, TouchableOpacity, Alert, SafeAreaView, PixelRatio } from 'react-native';
import {KeyboardAvoidingView, Keyboard, TouchableWithoutFeedback, TouchableHighlight} from 'react-native';
import { NavigationBar } from 'navigationbar-react-native';
import userDefaults from 'react-native-user-defaults'
import { Modal, FlatList, SectionList, ActivityIndicator } from 'react-native';
import NetInfo from "@react-native-community/netinfo";
import ProgressLoader from 'rn-progress-loader';
import Toast from 'react-native-simple-toast';
import 'react-native-gesture-handler'

import CommonUtilsObj from '../CommonClasses/CommonUtils';
import Constant from '../CommonClasses/Constant';
import CustomStyles from '../CommonClasses/CustomStyles';
import Message from '../CommonClasses/Message';
import APIManagerObj from '../CommonClasses/APIManager';

export default class HistoryRequests extends React.Component {

  constructor(props) {
 
    super(props);
 
    YellowBox.ignoreWarnings([
      'Warning: componentWillMount is deprecated',
      'Warning: componentWillReceiveProps is deprecated',
    ]);
    this.state = {
      visible: false,
      loading: false,
      connection_Status : "",
      loadMoreCompleted: false,
      requestList: [],
      offset: 0,
      pullToRefresh: false,
    };
  }

  componentDidMount() {

    NetInfo.isConnected.addEventListener(
        'connectionChange',
        this._handleConnectivityChange

    );
   
    NetInfo.isConnected.fetch().done((isConnected) => {

      if(isConnected == true)
      {
        this.setState({connection_Status : "Online"})
        this.onFetchRequestList();
      }
      else
      {
        this.setState({connection_Status : "Offline"})
      }
    });
  }

  componentWillUnmount() {
    NetInfo.isConnected.removeEventListener(
        'connectionChange',
        this._handleConnectivityChange
 
    );
  }

  componentWillUnmount() {
    NetInfo.isConnected.removeEventListener(
        'connectionChange',
        this._handleConnectivityChange
    );
  }

  _handleConnectivityChange = (isConnected) => {
    if(isConnected == true)
      {
        this.setState({connection_Status : "Online"})
      }
      else
      {
        this.setState({connection_Status : "Offline"})
      }
  };

  async onFetchRequestList() {
    if (this.state.connection_Status == "Online") {

      this.setState({ 
        visible: true,
        loading: true,
        pullToRefresh: false,
      });

      var data = new FormData();
      data.append('driver_id', CommonUtilsObj.userDetails.id);
      data.append('off', this.state.offset);
      console.log(data)

      let responseData = await APIManagerObj.callApiWithData(Constant.KGetRequestHistoryListURL, data);
      console.log('json response ....' + JSON.stringify(responseData));

      if (responseData.status == "success") {
        var list1=this.state.requestList
        var list=responseData.all_trip;
        list1.push(...list)
   
        const items = list1.map(function(item){
            if (item.status == '1' || item.status == '2' || item.status == '5') {
                item.statusText = 'Pending';
                item.statusImg = require('../../images/pending.png')
            }
            else if (item.status == '3') {
                item.statusText = 'Accepted';
                item.statusImg = require('../../images/acceptRequest.png')
            }
            else if (item.status == '4' || item.status == '6') {
                item.statusText = 'Cancelled';
                item.statusImg = require('../../images/cancelRequest.png')
            }
            else if (item.status == '7') {
                item.statusText = 'Arrived';
                item.statusImg = require('../../images/ArrivedRequest.png')
            }
            else if (item.status == '8') {
                item.statusText = 'On Trip';
                item.statusImg = require('../../images/onTripRequest.png')
            }
            else if (item.status == '9') {
                item.statusText = 'Completed';
                item.statusImg = require('../../images/completed.png')
            }
            else {
                item.statusText = '';
                item.statusImg = require('../../images/cancelRequest.png')
            }
        });

        this.setState({
          visible: false,
          offset: responseData.offset,
          requestList: list1,
          loading: false,
          pullToRefresh: false,
        });        
      }
      else {
        this.setState({
          visible: false,
          loading: false,
          loadMoreCompleted: true,
          pullToRefresh: false,
        }); 
        setTimeout(() => {
          if (responseData.error_code != '16' && responseData.error_code != '15') {
            if(typeof(responseData.message) === 'string')
            {
                Toast.showWithGravity(responseData.message, Toast.LONG, Toast.TOP)
            }
            else if(typeof(responseData.error) === 'string')
            {
                Toast.showWithGravity(responseData.error, Toast.LONG, Toast.TOP)
            }
            else {
                if (responseData.error) {
                  Toast.showWithGravity(responseData.error.toString(), Toast.LONG, Toast.TOP)
                }
                else {
                  Toast.showWithGravity(Message.KAPIError, Toast.LONG, Toast.TOP)
                }
            }
          }
        }, 200); 
      }
    }
    else {
      this.setState({lastApiTag : 1})
      Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
    }
  }
  
  renderRequestItem = ({ item, index }) => (
    <RequestListCell
      item={item}
      index={index}
      onSelectRequestCell={this.onSelectRequestCell}
      requestList={this.state.requestList}
    />
  );

  returnData(isUpdated) {
    this.setState({isUpdated: isUpdated});
  }

  onSelectRequestCell = (item, index) => {

    if (item.status == '1' || item.status == '2' || item.status == '5') {
        this.props.navigation.navigate('SearchRequestScreen', {returnData: this.returnData.bind(this), requestDetails: item});
    }
    else if (item.status == '3') {
        this.props.navigation.navigate({ routeName: 'RequestDetailsScreen', key: 'RequestHome', params: { requestDetails: item, returnData: this.returnData.bind(this) } });
        // this.props.navigation.navigate('RequestDetailsScreen', {returnData: this.returnData.bind(this), requestDetails: item});
    }
    else if (item.status == '4' || item.status == '6') {
        this.props.navigation.navigate({ routeName: 'HistoryRequestDetailsScreen', key: 'RequestHome', params: { requestDetails: item, returnData: this.returnData.bind(this) } });
    }
    else if (item.status == '7') {
        this.props.navigation.navigate({ routeName: 'RequestDetailsScreen', key: 'RequestHome', params: { requestDetails: item, returnData: this.returnData.bind(this) } });
    }
    else if (item.status == '8') {
        this.props.navigation.navigate({ routeName: 'RequestDetailsScreen', key: 'RequestHome', params: { requestDetails: item, returnData: this.returnData.bind(this) } });
    }
    else if (item.status == '9') {
        this.props.navigation.navigate({ routeName: 'HistoryRequestDetailsScreen', key: 'RequestHome', params: { requestDetails: item, returnData: this.returnData.bind(this) } });
    }
    
  };

  onRefresh() {

    this.setState({
        pullToRefresh:true,
        requestList: [],
        offset: 0,
        loading: false,
        loadMoreCompleted: false,
      });

    if (this.state.connection_Status == "Online") {
      
      setTimeout(() => {

          this.onFetchRequestList();
          
      }, 2000);
      
    }
    else {
      this.setState({ pullToRefresh: false });
      Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
    }
  }

  handleLoadMore = () => {
    if (!this.state.loading && !this.state.loadMoreCompleted){
      
      this.onFetchRequestList();
      
    }
  };

  emptyComponent = () => {
    return (
        <View style={CustomStyles.emptyContainer}>
          <Text style={CustomStyles.emptyText}>No records found</Text>
        </View>
    );
    
  };

  renderFooter = () => {
    if (!this.state.loading) return null;
    return (
      <ActivityIndicator
        style={{ color: '#000' }}
      />
    );
  };


  render() {

    if (this.state.loading && this.page === 1) {
      return <View style={{
        width: '100%',
        height: '100%'
      }}><ActivityIndicator style={{ color: '#000' }} /></View>;
    }

    return (
      <View style={CustomStyles.container1}>
          <View style={{backgroundColor: "#fff", justifyContent: 'center', alignItems: 'center'}}>

          <ProgressLoader
            visible={this.state.visible}
            isModal={true} isHUD={true}
            hudColor={"#fff"}
            height={200}
            width={200}
            color={"#8edf01"} />
          </View>

          <FlatList
                data={this.state.requestList}
                onRefresh={() => this.onRefresh()}
                refreshing={this.state.pullToRefresh}
                // ListFooterComponent={this.renderFooter.bind(this)}
                onEndReachedThreshold={0.4}
                onEndReached={this.handleLoadMore.bind(this)}
                renderItem={this.renderRequestItem}
                keyExtractor={(item, index) => index.toString()}
                ListEmptyComponent={this.emptyComponent.bind(this)}
          />   
      </View>
      
    );
  }
}

class RequestListCell extends React.Component {
  render() {
    const { item, index, onSelectRequestCell, requestList} = this.props;
    return (
      //RequestDetailsScreen
      <View style={CustomStyles.requestCellContainer}>
        
        <TouchableOpacity
                    onPress={this.props.onSelectRequestCell.bind(this, item, index)}
                    activeOpacity = {1.0}
              >
               
              
        <View style={CustomStyles.requestCellTopContainer}>
            <Image source={{uri: Constant.KCarImagePathURL + item.icon}} style={[CustomStyles.photo, {flex: 0.2, alignSelf: 'center', margin: 5}]} />
            <View style={{flexDirection:"column", flex: 0.8, justifyContent: 'center', marginLeft: 7}}>
                <Text style={[CustomStyles.requestCellFont]}>${item.final_amount_after_discount}</Text>
                <Text style={[CustomStyles.requestSmallFont]}>{item.pickup_date_time}</Text>
                <Text style={[CustomStyles.requestSmallFont]}>{item.car_type}</Text>
            </View>

            <View style={[CustomStyles.flexColunm, {justifyContent: 'center', flex: 0.2, alignItems: 'center', margin: 5}]}>
            <Image 
                source={ 
                  (item.user_detail != null) ? {uri: Constant.KUserImagePathURL + item.user_detail.image} : require('../../images/userPlaceholder.png')
                } 
                style={[CustomStyles.photo, {borderRadius: 25, marginBottom: 3}]} 
            />
            </View>
        </View>

        <View style={{flexDirection:"row", justifyContent: 'space-between', marginLeft: 10, marginRight: 10, marginBottom: 20}}>
            <Text style={[CustomStyles.requestCellFont, CustomStyles.whiteFont]}>Booking Id : {item.id}</Text>
            <View style={{alignSelf:'flex-end', flexDirection:"row", justifyContent: 'center'}}>
                <Image source={item.statusImg} style={{height: 15, width: 15, marginRight: 3}} />
                <Text style={[CustomStyles.requestSmallFont, CustomStyles.whiteFont, {marginTop: -2}]}>{item.statusText}</Text>
            </View>
        </View>
        

        <View style={{flexDirection:"row", flex: 1, marginBottom: 20, marginRight: 10}}>

            <View style={CustomStyles.pickUpImage}>
                <Image source={require('../../images/pickUp.png')} style={{height: 20, width: 20}} />
                <Image style={{height: "100%", width: 1, backgroundColor:'white',}} />
            </View> 

            <View style={{flexDirection:"column", flex: 0.9,}}>
                    <Text style={[CustomStyles.garyFont, CustomStyles.smallFont, {alignSelf: 'flex-start'}]}>Pick Up Location</Text>
                    <Text style={[CustomStyles.requestMediumCellFont, CustomStyles.margin5]}>{item.pickup_area}</Text>
            </View>
           
        </View>

        <View style={{flexDirection:"row", flex: 1, marginBottom: 20, marginRight: 10}}>

            <View style={{flexDirection:"column", flex: 0.1, alignItems: 'center'}}>
                <Image source={require('../../images/destination.png')} style={{height: 20, width: 20}} />
            </View>

            <View style={{flexDirection:"column", flex: 0.9,}}>
                <View style={{flexDirection:"column"}}>
                    <Text style={[CustomStyles.garyFont, CustomStyles.smallFont, {alignSelf: 'flex-start'}]}>Destination Location</Text>
                    <Text style={[CustomStyles.requestMediumCellFont, CustomStyles.margin5]}>{item.drop_area}</Text>
                </View>
            </View>
           
        </View>

        </TouchableOpacity>
    </View>
      
    );
  }
}
