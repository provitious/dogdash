import React from 'react';
import { StyleSheet, Text, View, Image, YellowBox, Dimensions, TouchableOpacity, Alert, ScrollView } from 'react-native';
import { NavigationBar } from 'navigationbar-react-native';
import NetInfo from "@react-native-community/netinfo";
import ProgressLoader from 'rn-progress-loader';
import Toast from 'react-native-simple-toast';

import CommonUtilsObj from '../CommonClasses/CommonUtils';
import Constant from '../CommonClasses/Constant';

import userDefaults from 'react-native-user-defaults'

const headerFontSize = (((Dimensions.get('window').width) * 4) / 100);
const txtMediumSize = (((Dimensions.get('window').width) * 3.8) / 100);
const lineWidth = (Dimensions.get('window').width) - 40
const btnHeight = (((Dimensions.get('window').width) * 10) / 100);
const btnFontSize = (((Dimensions.get('window').width) * 4.1) / 100);

export default class RequestDetails extends React.Component {

  constructor(props) {
 
    super(props);
 
    YellowBox.ignoreWarnings([
      'Warning: componentWillMount is deprecated',
      'Warning: componentWillReceiveProps is deprecated',
    ]);
    
    const { navigation } = this.props;  
    const details = navigation.getParam('requestDetails', '');

    this.state = {
        requestDetails: details,
        fromConfirmRequest: navigation.getParam('fromConfirmRequest', false),
        fromPayment: navigation.getParam('fromPayment', ''),
    };

  }

  onClickPayNow() {

    this.props.navigation.navigate('CardSelectionScreen', {fromPayment: true, requestDetails: this.state.requestDetails});
  }

  render() {

    return (
      <View style={customStyles.container}>
          <ScrollView>
              <View style={[customStyles.flexColunm, {margin: 20}]}>

                  { this.state.fromConfirmRequest && 
                  <Text style={[customStyles.whiteFont, customStyles.mediumFont]}>Estimated fare details</Text>
                  }

                  { !this.state.fromConfirmRequest && 
                  <Text style={[customStyles.whiteFont, customStyles.headerFont, customStyles.margin5, {textAlign: 'center'}]}>Booking ID : {this.state.requestDetails.id}</Text>
                  }
                  <Text style={[customStyles.whiteFont, customStyles.headerFont, customStyles.margin20]}>Total Amount : ${this.state.requestDetails.amount_after_discount}</Text>
                  <Text style={[customStyles.whiteFont, customStyles.headerFont, customStyles.margin10]}>Discount : ${this.state.requestDetails.coupon_discount_amount}</Text>

                  <Text style={[customStyles.whiteFont, customStyles.mediumFont, customStyles.margin20]}>Ride Fare : ${this.state.requestDetails.base_fare}</Text>
                  <View style={[customStyles.flexRow, customStyles.line]}></View>

                  { !this.state.fromConfirmRequest && 
                  <Text style={[customStyles.whiteFont, customStyles.mediumFont]}>Loading Time({this.state.requestDetails.wait_time} minutes) : ${this.state.requestDetails.trip_waitTime_price}</Text>
                  }

                  { !this.state.fromConfirmRequest && 
                  <View style={[customStyles.flexRow, customStyles.line]}></View>
                  }

                  { !this.state.fromConfirmRequest && 

                  <Text style={[customStyles.whiteFont, customStyles.mediumFont]}>Distance({this.state.requestDetails.actual_km} km) : ${this.state.requestDetails.trip_distance_price}</Text>
                  }

                  { this.state.fromConfirmRequest && 
                  <Text style={[customStyles.whiteFont, customStyles.mediumFont]}>Distance({this.state.requestDetails.actual_km}) : {this.state.requestDetails.trip_distance_price}</Text>
                  }

                  <View style={[customStyles.flexRow, customStyles.line]}></View>

                  { !this.state.fromConfirmRequest && 
                  <Text style={[customStyles.whiteFont, customStyles.mediumFont]}>Deduct from Wallet : ${this.state.requestDetails.deduction_from_wallet}</Text>
                  }

                  { !this.state.fromConfirmRequest && 
                  <View style={[customStyles.flexRow, customStyles.line]}></View>
                  }
              </View>
          </ScrollView>

          { this.state.fromPayment &&
          <TouchableOpacity
            style={customStyles.themeButton}
            onPress={this.onClickPayNow.bind(this)}
           >
              <Text style={customStyles.themeBtnText}> Pay Now </Text>
          </TouchableOpacity>
          }
      </View>
    );
  }
}

const customStyles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000',    
  },
  flexColunm:{
      flexDirection: 'column',
  },
  flexRow:{
      flexDirection: 'row'
  },
  mediumFont: {
    fontSize: txtMediumSize,
    fontFamily: Constant.KThemeFontRegular,
  },
  headerFont: {
    fontSize: headerFontSize,
    fontFamily: Constant.KThemeFontSemiBold,
  },
  whiteFont: {
    color: '#fff',
  },
  margin10: {
    marginTop: 10,
  },
  margin20: {
    marginTop: 25,
  },
  line: {
    height: 1, 
    width: lineWidth, 
    margin: 16, 
    marginLeft: 0, 
    backgroundColor: 'gray'
  },
  themeButton:{
        marginTop:35,
        borderRadius:5,
        height: btnHeight,
        backgroundColor: '#8edf01',
        justifyContent: 'center',
        alignItems: 'center',
        bottom: 20,
        position: 'absolute',
        width: '90%',
        margin: 20,
    },
    themeBtnText:{
        color:'#222',
        textAlign: 'center',
        margin: 5,
        fontSize: btnFontSize,
        fontFamily: Constant.KThemeFontRegular,
    },
});







