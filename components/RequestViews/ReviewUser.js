import React from 'react';
import { StyleSheet, Dimensions, YellowBox, Text, View, Button, TextInput, Image, ScrollView, TouchableOpacity, Alert, SafeAreaView, PixelRatio } from 'react-native';
import {KeyboardAvoidingView, Keyboard, TouchableWithoutFeedback} from 'react-native';
import { NavigationBar } from 'navigationbar-react-native';
import userDefaults from 'react-native-user-defaults'
import { Modal, FlatList, SectionList, ActivityIndicator } from 'react-native';
import NetInfo from "@react-native-community/netinfo";
import ProgressLoader from 'rn-progress-loader';
import Toast from 'react-native-simple-toast';
import { Rating, AirbnbRating } from 'react-native-ratings';
import { StackActions, NavigationActions } from 'react-navigation';

import CommonUtilsObj from '../CommonClasses/CommonUtils';
import Constant from '../CommonClasses/Constant';
import CustomStyles from '../CommonClasses/CustomStyles';
import Message from '../CommonClasses/Message';
import APIManagerObj from '../CommonClasses/APIManager';

const imageSize = (((Dimensions.get('window').width) * 30) / 100);
const vWidth = (((Dimensions.get('window').width) * 50) / 100);

const headerFontSize = (((Dimensions.get('window').width) * 5) / 100);
const headerSubFontSize = (((Dimensions.get('window').width) * 3.7) / 100);
const txtMediumSize = (((Dimensions.get('window').width) * 4.5) / 100);
const txtSmallSize = (((Dimensions.get('window').width) * 3.6) / 100);
const btnHeight = (((Dimensions.get('window').width) * 10) / 100);
const btnFontSize = (((Dimensions.get('window').width) * 4.1) / 100);

export default class ReviewUser extends React.Component {

  constructor(props) {
 
    super(props);
 
    YellowBox.ignoreWarnings([
      'Warning: componentWillMount is deprecated',
      'Warning: componentWillReceiveProps is deprecated',
    ]);

    const { navigation } = this.props;  
    const bookingId = navigation.getParam('booking_id', '');
    let details = navigation.getParam('requestDetails', '');
    console.log('params details : ' + details);

    this.state = {
      visible: false,
      connection_Status : "",
      booking_id: bookingId,
      requestDetails: details,
      comment: '',
      rating: 0,
      btnBottom: 30,
    };

    this.onFocus = this.onFocus.bind(this);
    this.onChangeText = this.onChangeText.bind(this);
    this.onSubmitComment = this.onSubmitComment.bind(this);
    this.commentRef = this.updateRef.bind(this, 'comment');

    this.onSubmitReview=this.onSubmitReview.bind(this);

  }

  componentDidMount() {
    NetInfo.isConnected.addEventListener(
        'connectionChange',
        this._handleConnectivityChange
    );
   
    NetInfo.isConnected.fetch().done((isConnected) => {
      if(isConnected == true)
      {
        this.setState({connection_Status : "Online"})
        this.onFetchRequestDetails();
      }
      else
      {
        this.setState({connection_Status : "Offline"})
      }
    });

    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
  }

  _keyboardDidShow = () => {
      this.setState({btnBottom : -100})
  }

  _keyboardDidHide = () => {
      this.setState({btnBottom : 30})
  }

  componentWillUnmount() {
    NetInfo.isConnected.removeEventListener(
        'connectionChange',
        this._handleConnectivityChange
    );
  }

  _handleConnectivityChange = (isConnected) => {

    if(isConnected == true)
      {
        this.setState({connection_Status : "Online"})
      }
      else
      {
        this.setState({connection_Status : "Offline"})
      }
  };

  async onFetchRequestDetails() {
    if (this.state.connection_Status == "Online") {

      this.setState({ 
        visible: true,
        loaded: false,
       });

      var data = new FormData();
      data.append('driver_id', CommonUtilsObj.userDetails.id);
      data.append('off', 0);
      data.append('booking_id', this.state.booking_id);

      let responseData = await APIManagerObj.callApiWithData(Constant.KRequestDetailURL, data);
      console.log('json response ....' + JSON.stringify(responseData));

      if (responseData.status == "success") {
        this.setState({
          visible: false,
          requestDetails: responseData.all_trip[0],
        });         
      }
      else {
        this.setState({
          visible: false,
        }); 
        setTimeout(() => {
          if(typeof(responseData.message) === 'string')
            {
                Toast.showWithGravity(responseData.message, Toast.LONG, Toast.TOP)
            }
          else if(typeof(responseData.error) === 'string')
          {
              Toast.showWithGravity(responseData.error, Toast.LONG, Toast.TOP)
          }
          else {
              if (responseData.error) {
                Toast.showWithGravity(responseData.error.toString(), Toast.LONG, Toast.TOP)
              }
              else {
                Toast.showWithGravity(Message.KAPIError, Toast.LONG, Toast.TOP)
              }
          }
        }, 200); 
      }
    }
    else {
      this.setState({lastApiTag : 1})
      Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
    }
  }

  onFocus() {
      let { errors = {} } = this.state;

      for (let name in errors) {
        let ref = this[name];

        if (ref && ref.isFocused()) {
          delete errors[name];
        }
      }

      this.setState({ errors });
  }

  updateRef(name, ref) {
    this[name] = ref;
  }

  onChangeText(text) {
    ['comment']
      .map((name) => ({ name, ref: this[name] }))
      .forEach(({ name, ref }) => {
        if (ref.isFocused()) {
          this.setState({ [name]: text });
        }
      });
  }

  
  onSubmitComment(text) {
    
    let commentVal = text.nativeEvent.text;
    this.comment.blur();
  }

  ratingCompleted(rating) {
    this.setState({ rating: rating });
  }

  onSubmitReview() {
    if (this.state.rating == 0 || this.state.rating == '0') {
        Toast.showWithGravity('Rating should not be zero.', Toast.LONG, Toast.TOP);
    }
    else {
      this.onReviewUser();
    }
  }

  async onReviewUser() {
    if (this.state.connection_Status == "Online") {

      this.setState({ visible: true });
      var data = new FormData();
      data.append('driver_id', CommonUtilsObj.userDetails.id);
      data.append('user_id', this.state.requestDetails.user_id);
      data.append('driver_comment', this.state.comment);
      data.append('book_id', this.state.requestDetails.id);
      data.append('user_rate', this.state.rating);

      let responseData = await APIManagerObj.callApiWithData(Constant.KReviewUserURL, data);
      console.log('json response ....' + JSON.stringify(responseData));

      if (responseData.status == "success") {
        this.setState({
          visible: false,

        });
        
        setTimeout(() => {
          if (responseData.is_trip_running == true) {
            let requestDetails = {};
            requestDetails.id = responseData.booking_id;
            const navigateAction = StackActions.reset({
              index: 0,
              actions: [NavigationActions.navigate({ routeName: "NotificationRequestDetailsScreen" , params: { fromNotification: true, requestDetails: requestDetails }})],
            });

            this.props.navigation.dispatch(navigateAction);
          }
          else {
            const navigateAction = StackActions.reset({
              index: 0,
              actions: [NavigationActions.navigate({ routeName: "First"})],
            });
            this.props.navigation.dispatch(navigateAction);
          }
          
          Toast.showWithGravity(responseData.message, Toast.LONG, Toast.TOP)
        }, 200);         
      }
      else {
        this.setState({
          visible: false,
        }); 
        setTimeout(() => {
          if(typeof(responseData.message) === 'string')
          {
              Toast.showWithGravity(responseData.message, Toast.LONG, Toast.TOP)
          }
          else if(typeof(responseData.error) === 'string')
          {
              Toast.showWithGravity(responseData.error, Toast.LONG, Toast.TOP)
          }
          else {
              if (responseData.error) {
                Toast.showWithGravity(responseData.error.toString(), Toast.LONG, Toast.TOP)
              }
              else {
                Toast.showWithGravity(Message.KAPIError, Toast.LONG, Toast.TOP)
              }
          }
        }, 200); 
      }
    }
    else {
      this.setState({lastApiTag : 1})
      Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
    }
  }

  render() {

    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
      <View style={customStyles.container}>

          <View
                style={{backgroundColor: "#fff", justifyContent: 'center', alignItems: 'center'}}>
          <ProgressLoader
            visible={this.state.visible}
            isModal={true} isHUD={true}
            hudColor={"#fff"}
            height={200}
            width={200}
            color={"#8edf01"} />
          </View>

          <View style={customStyles.flexColunm}>
              <Image style={customStyles.avatar} source={{uri: Constant.KUserImagePathURL + this.state.requestDetails.user_detail.image}} />
              <Text style={[customStyles.mediumFont, customStyles.margin10, {alignSelf: 'center'}]}>{this.state.requestDetails.user_detail.name}</Text>

              <AirbnbRating
                count={5}
                reviews={[]}
                defaultRating={0}
                selectedColor={Constant.KAppColor}
                size={30}
                onFinishRating={this.ratingCompleted.bind(this)}
                starContainerStyle={{ marginBottom: 15, marginTop: -20}}
              />

          </View>  

          <KeyboardAvoidingView style={customStyles.flexColunm} behavior="padding" enabled>
          <View style={[{margin: 20}]}>
            <Text style={[customStyles.mediumFont]}>Leave a comment</Text>
            <TextInput
                style={[customStyles.textMultilineInput, customStyles.margin10]}
                ref={this.commentRef}
                placeholder = "Describe your experience..." 
                keyboardType='default'
                autoCapitalize='sentences'
                autoCorrect={true}
                enablesReturnKeyAutomatically={true}
                onFocus={this.onFocus}
                onChangeText={this.onChangeText}
                onSubmitEditing={this.onSubmitComment}
                returnKeyType='done'
                numberOfLines={4}
                multiline={true}
                textAlignVertical={'top'}
            />
              
          </View>  
          </KeyboardAvoidingView>

          <TouchableOpacity
            style={[customStyles.themeButton, {bottom: this.state.btnBottom}]}
            onPress={this.onSubmitReview.bind(this)}
           >
              <Text style={customStyles.themeBtnText}> Submit </Text>
          </TouchableOpacity> 
      </View>
      </TouchableWithoutFeedback>
      
    );
  }
}


const customStyles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000',
    
  },
  flexColunm:{
      flexDirection: 'column',
  },
  flexRow:{
      flexDirection: 'row'
  },
  avatar: {
    borderRadius: imageSize/2,
    width: imageSize,
    height: imageSize,
    marginTop: 25,
    alignSelf: 'center',
  },
  mediumFont: {
    fontSize: txtMediumSize,
    fontFamily: Constant.KThemeFontRegular,
    color: '#fff',
 },
 smallFont: {
    fontSize: txtSmallSize,
    fontFamily: Constant.KThemeFontRegular,
    color: 'gray',
    alignSelf: 'center',
 },
 margin10: {
  marginTop:10,
 },
 headerFont: {
  fontSize: headerFontSize,
  fontFamily: Constant.KThemeFontSemiBold,
  color: '#000',
},
headerSubFont: {
  fontSize: headerSubFontSize,
  fontFamily: Constant.KThemeFontRegular,
  color: '#000',
},
textMultilineInput: {
  height: 80,
  fontSize: txtSmallSize,
  fontFamily: Constant.KThemeFontRegular,
  backgroundColor: 'white',
  paddingLeft: 5,
},
themeButton:{
  marginTop:35,
  borderRadius:5,
  height: btnHeight,
  backgroundColor: '#8edf01',
  justifyContent: 'center',
  alignItems: 'center',
  bottom: 20,
  position: 'absolute',
  width: '90%',
  margin: 20,
},
themeBtnText:{
  color:'#222',
  textAlign: 'center',
  margin: 5,
  fontSize: btnFontSize,
  fontFamily: Constant.KThemeFontRegular,
},
});
