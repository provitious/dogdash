import React from 'react';
import { Window, StyleSheet, Dimensions, YellowBox, Text, View, Button, TextInput, Image, ScrollView, TouchableHighlight, TouchableOpacity, Alert, SafeAreaView, PixelRatio, Linking, PermissionsAndroid } from 'react-native';
import {KeyboardAvoidingView, Keyboard, TouchableWithoutFeedback} from 'react-native';
import { NavigationBar } from 'navigationbar-react-native';
import ProgressLoader from 'rn-progress-loader';
import NetInfo from "@react-native-community/netinfo";
import Toast from 'react-native-simple-toast';
import 'react-native-gesture-handler'
import MapView, {PROVIDER_GOOGLE} from 'react-native-maps';
import { Marker } from 'react-native-maps';
import Geolocation from 'react-native-geolocation-service'; //'@react-native-community/geolocation';
import { Modal, FlatList } from 'react-native';
import { StackActions, NavigationActions } from 'react-navigation';
import EventBus from 'react-native-event-bus'

import Constant from '../CommonClasses/Constant';
import CommonUtilsObj from '../CommonClasses/CommonUtils';
import SocketManagerObj from '../CommonClasses/SocketManager';
import CustomStyles from '../CommonClasses/CustomStyles';
import Message from '../CommonClasses/Message';
import APIManagerObj from '../CommonClasses/APIManager';

export default class Home extends React.Component {

  updateRef(name, ref) {
      this[name] = ref;
    }

  constructor(props) {
      super(props);

      var socketStatus = '';
      if (CommonUtilsObj.userDetails.socket_status == "Active") {
          socketStatus = "Go Offline";
      }
      else {
          socketStatus = "Go Online";
      }

      this.state = {
        loaded: false,
        visible: false,
        connection_Status: '',
        currentLatitude: 0,
        currentLongitude: 0,
        heading: 0,
        truckList: [],
        modalVisible: false,
        refresh: false,
        selectedIndex: 0,
        selectedTruckDetails: '',
        truckImage: { uri: Constant.KCarImagePathURL + CommonUtilsObj.cabDetails.active_side_icon },
        truckNumber: CommonUtilsObj.cabDetails.license_plate,
        truckType: CommonUtilsObj.cabDetails.cartype,
        truckModel: CommonUtilsObj.cabDetails.model,
        currentDriverStatus: socketStatus,
        latDelta: 0.003,
        longDelta: 0.003,
      }

      YellowBox.ignoreWarnings([
          'Warning: componentWillReceiveProps is deprecated',
          'Warning: componentWillUpdate is deprecated',
          'Warning: componentWillUpdate has been renamed, and is not recommended for use',
          'Warning: componentWillReceiveProps has been renamed, and is not recommended for use',
          'RCTRootView cancelTouches',
          'VirtualizedLists should never be nested inside plain ScrollViews with the same orientation - use another VirtualizedList-backed container instead.'
        ]);

      CommonUtilsObj.navigationProps = this.props;
  }

  componentDidMount() {

    

    NetInfo.isConnected.addEventListener(
        'connectionChange',
        this._handleConnectivityChange
    );
   
    NetInfo.isConnected.fetch().done((isConnected) => {

      if(isConnected == true)
      {
        this.setState({connection_Status : "Online"})
        this.onFetchTruckList();
        
      }
      else
      {
        this.setState({connection_Status : "Offline"})
      }
    });
    this.getLocationUser();

    EventBus.getInstance().addListener("HeadingChangeEvent", this.listener = data => {
        // handle the event
       // alert(CommonUtilsObj.driverLatitude)
       this.setState({
        heading : CommonUtilsObj.heading,
      })
    });
  }

  getLocationUser = async () => {

    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
          {
            'title': '',
            'message': 'Need access to your location '
          }
        )
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          console.log("You can use the location")

          Geolocation.getCurrentPosition(
            (position) => {
              this.setState({
                currentLatitude: position.coords.latitude,
                currentLongitude: position.coords.longitude,
              });

            },
            (error) => {
                if (error.code == 2) {
                  // alert('Please enable location service from setting')
                }
                else {
                  // alert(error.message);
                }
                // See error code charts below.
                
            },
            { enableHighAccuracy: true, timeout: 20000, maximumAge: 10000  }

            // { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
        );

        } else {
          console.log("location permission denied")
          // alert("Location permission denied, Please enable location service from setting");
        }
      } catch (err) {
        console.warn(err)
      }
    } 
    else {
      Geolocation.requestAuthorization();
      Geolocation.getCurrentPosition(
            (position) => {
              this.setState({
                currentLatitude: position.coords.latitude,
                currentLongitude: position.coords.longitude,
              });

            },
            (error) => {
                if (error.code == 2) {
                  // alert('Please enable location service from setting')
                }
                else {
                  // alert(error.message);
                }
                // See error code charts below.
                
            },
            { enableHighAccuracy: true, timeout: 20000, maximumAge: 10000  }
            );
    }
  };

  componentWillUnmount() {

    NetInfo.isConnected.removeEventListener(
        'connectionChange',
        this._handleConnectivityChange
    );
  }

  _handleConnectivityChange = (isConnected) => {
    if(isConnected == true)
      {
        this.setState({connection_Status : "Online"})
      }
      else
      {
        this.setState({connection_Status : "Offline"})
      }
  };

  async onFetchTruckList() {
    if (this.state.connection_Status == "Online") {

      this.setState({ visible: true });
      var data = new FormData();
      data.append('driver_id', CommonUtilsObj.userDetails.id);

      let responseData = await APIManagerObj.callApiWithData(Constant.KGetTruckListURL, data);
      console.log('json response ....' + JSON.stringify(responseData));

      if (responseData.status == "success") {
        this.setState({
          visible: false,
          truckList: responseData.cars,
        });

        const items = responseData.cars.map((item) => {  
          if (item.CurrentCarFlag == '1') {
              this.setState({
                selectedTruckDetails: item,
                selectedIndex: item.id,
              });
          }
        });         
      }
      else {
        this.setState({
          visible: false,
        }); 
        setTimeout(() => {
          if(typeof(responseData.error) === 'string')
          {
              Toast.showWithGravity(responseData.error, Toast.LONG, Toast.TOP)
          }
          else {
              if (responseData.error) {
                Toast.showWithGravity(responseData.error.toString(), Toast.LONG, Toast.TOP)
              }
              else {
                Toast.showWithGravity(Message.KAPIError, Toast.LONG, Toast.TOP)
              }
          }
        }, 200); 
      }
    }
    else {
      this.setState({lastApiTag : 1})
      Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
    }
  }

  async onCheckCurrentBooking() {
    if (this.state.connection_Status == "Online") {

      this.setState({ visible: true });
      var data = new FormData();
      data.append('driverId', CommonUtilsObj.userDetails.id);

      let responseData = await APIManagerObj.callApiWithData(Constant.KCheckCurrentBookingURL, data);
      console.log('json response ....' + JSON.stringify(responseData));

      if (responseData.status == "success") {
        this.setState({ currentDriverStatus: 'Go Online' });
        setTimeout(() => {
          this.onChangeDriverSocketStatus('Inactive');
        }, 200);       
      }
      else {
        this.setState({
          visible: false,
        }); 
        setTimeout(() => {
          if(typeof(responseData.message) === 'string')
          {
              Toast.showWithGravity(responseData.message, Toast.LONG, Toast.TOP)
          }
          else if(typeof(responseData.error) === 'string')
          {
              Toast.showWithGravity(responseData.error, Toast.LONG, Toast.TOP)
          }
          else {
              if (responseData.error) {
                Toast.showWithGravity(responseData.error.toString(), Toast.LONG, Toast.TOP)
              }
              else {
                Toast.showWithGravity(Message.KAPIError, Toast.LONG, Toast.TOP)
              }
          }
        }, 200); 
      }
    }
    else {
      this.setState({lastApiTag : 1})
      Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
    }
  }

  onClickChangeStatus() {
    if (CommonUtilsObj.userDetails.socket_status == "Active") {
        this.onCheckCurrentBooking();
        // this.setState({ currentDriverStatus: 'Go Online' });
        // setTimeout(() => {
        //   this.onChangeDriverSocketStatus('Inactive');
        // }, 200);
    }
    else {
        this.setState({ currentDriverStatus: 'Go Offline' });
        // this.onChangeDriverSocketStatus('Active');
        setTimeout(() => {
          this.onChangeDriverSocketStatus('Active');
        }, 200);
    }
  }

  async onChangeDriverSocketStatus (status) {
    if (this.state.connection_Status == "Online") {

      this.setState({ visible: true });
      var data = new FormData();
      data.append('driverId', CommonUtilsObj.userDetails.id);
      data.append('socket_status', status);
      data.append('flag', '2');
      data.append('lat', this.state.currentLatitude);
      data.append('long', this.state.currentLongitude);
      data.append('driverName', CommonUtilsObj.userDetails.name);
      data.append('seat_capacity', CommonUtilsObj.cabDetails.seat_capacity);
      data.append('is_pool', CommonUtilsObj.cabDetails.is_pool);
      data.append('cartype', CommonUtilsObj.cabDetails.cab_id);
      data.append('driverstatus', '1');
      console.log(data)

      let responseData = await APIManagerObj.callApiWithData(Constant.KManageDriverStatusURL, data);
      console.log('onChangeDriverSocketStatus json response ....' + JSON.stringify(responseData));

      if (responseData.status == "success" || responseData.success == true) {
        this.setState({
          visible: false,
        });         

        let obj = CommonUtilsObj.loggedUserData;
        obj.Driver_detail[0].socket_status = status;
        // CommonUtilsObj.setLoggedUserDetails(obj);
        if (Platform.OS==='android') {
            CommonUtilsObj.setLoggedUserDetails(JSON.stringify(obj));
        }
        else {
            CommonUtilsObj.setLoggedUserDetails(obj);
        }

        setTimeout(() => {
          if (status == 'Active') {
            Toast.showWithGravity('Online mode set', Toast.LONG, Toast.TOP)
          }
          else {
            Toast.showWithGravity('Offline mode set', Toast.LONG, Toast.TOP)
          }
        }, 200);         
      }
      else {
        this.setState({
          visible: false,
        }); 
        setTimeout(() => {
          if(typeof(responseData.error) === 'string')
          {
              Toast.showWithGravity(responseData.error, Toast.LONG, Toast.TOP)
          }
          else {
              if (responseData.error) {
                Toast.showWithGravity(responseData.error.toString(), Toast.LONG, Toast.TOP)
              }
              else {
                Toast.showWithGravity(Message.KAPIError, Toast.LONG, Toast.TOP)
              }
          }
        }, 200); 
      }
    }
    else {
      this.setState({lastApiTag : 1})
      Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
    }
  }

  showModal() {
    this.setState({ modalVisible: true });
    // CommonUtilsObj.renderNewRequestModal()
  }

  hideModal() {
    this.setState({ 
      modalVisible: false,
      selectedIndex: this.state.selectedTruckDetails.id,
    });
  }

  onClickChangeTruck() {

    const items = this.state.truckList.map((item) => {  
        if (item.id == this.state.selectedIndex) {
          this.setState({
            selectedTruckDetails: item,
          });
        }
    });

    this.setState({
      refresh:true,
      modalVisible: false,
    });

    setTimeout(() => {
          this.onChangeTruck();
    }, 100);
  }

  onClickManageTruck() {
      const navigateAction = StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: "TruckListScreen"})],
      });
      this.props.navigation.dispatch(navigateAction);
  } 

  async onChangeTruck() {
    if (this.state.connection_Status == "Online") {

      this.setState({ visible: true });
      var data = new FormData();
      data.append('driver_id', CommonUtilsObj.userDetails.id);
      data.append('car_id', this.state.selectedIndex);

      let responseData = await APIManagerObj.callApiWithData(Constant.KSetCurrentTruckURL, data);
      console.log('json response ....' + JSON.stringify(responseData));

      if (responseData.status == "success") {
        this.setState({
          visible: false,
        }); 
        this.onFetchTruckList();
        this.onUpdateDriverCarStatusOnSocket();

        let obj = CommonUtilsObj.loggedUserData;
        obj.current_car[0] = this.state.selectedTruckDetails;
        // CommonUtilsObj.setLoggedUserDetails(obj);

        if (Platform.OS==='android') {
            CommonUtilsObj.setLoggedUserDetails(JSON.stringify(obj));
        }
        else {
            CommonUtilsObj.setLoggedUserDetails(obj);
        }

        setTimeout(() => {
          this.setState({
            visible: false,
            truckImage: { uri: Constant.KCarImagePathURL + CommonUtilsObj.cabDetails.active_side_icon },
            truckNumber: CommonUtilsObj.cabDetails.license_plate,
            truckType: CommonUtilsObj.cabDetails.cartype,
            truckModel: CommonUtilsObj.cabDetails.model,
          }); 
        }, 100);

        setTimeout(() => {
            Toast.showWithGravity(responseData.message, Toast.LONG, Toast.TOP)
        }, 200);         
      }
      else {
        this.setState({
          visible: false,
        }); 
        setTimeout(() => {
          if(typeof(responseData.error) === 'string')
          {
              Toast.showWithGravity(responseData.error, Toast.LONG, Toast.TOP)
          }
          else {
              if (responseData.error) {
                Toast.showWithGravity(responseData.error.toString(), Toast.LONG, Toast.TOP)
              }
              else {
                Toast.showWithGravity(Message.KAPIError, Toast.LONG, Toast.TOP)
              }
          }
        }, 200); 
      }
    }
    else {
      this.setState({lastApiTag : 1})
      Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
    }
  }

  async onUpdateDriverCarStatusOnSocket() {
    if (this.state.connection_Status == "Online") {

      this.setState({ visible: true });
      var data = new FormData();
      data.append('driverId', CommonUtilsObj.userDetails.id);
      data.append('flag', '4');
      data.append('lat', this.state.currentLatitude);
      data.append('long', this.state.currentLongitude);
      data.append('driverName', CommonUtilsObj.userDetails.name);
      data.append('seat_capacity', CommonUtilsObj.cabDetails.seat_capacity);
      data.append('is_pool', CommonUtilsObj.cabDetails.is_pool);
      data.append('cartype', CommonUtilsObj.cabDetails.cab_id);
      data.append('cartypename', this.state.selectedTruckDetails.cartype);
      data.append('caricon', this.state.selectedTruckDetails.active_side_icon);
      data.append('license_plate', this.state.selectedTruckDetails.license_plate);
      data.append('rating', CommonUtilsObj.userDetails.rating);
      data.append('driverimage', CommonUtilsObj.userDetails.image);

      let responseData = await APIManagerObj.callApiWithData(Constant.KManageDriverStatusURL, data);
      console.log('json response ....' + JSON.stringify(responseData));

      if (responseData.status == "success") {
        this.setState({
          visible: false,
        });        
      }
      else {
        this.setState({
          visible: false,
        }); 
        setTimeout(() => {
          if(typeof(responseData.error) === 'string')
          {
              Toast.showWithGravity(responseData.error, Toast.LONG, Toast.TOP)
          }
          else {
              if (responseData.error) {
                Toast.showWithGravity(responseData.error.toString(), Toast.LONG, Toast.TOP)
              }
              else {
                Toast.showWithGravity(Message.KAPIError, Toast.LONG, Toast.TOP)
              }
          }
        }, 200); 
      }
    }
    else {
      this.setState({lastApiTag : 1})
      Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
    }
  }

  onSelectCell = (item, index) => {
      this.setState({
        refresh:true,
        selectedIndex: item.id,
      });
  };
  
  renderPodcastItem = ({ item, index }) => (
    <FlatListCell
      item={item}
      index={index}
      onSelectCell={this.onSelectCell}
      selectedIndex={this.state.selectedIndex}
      cabList={this.state.cabList}
    />
  );

  render() {

      return(

          <View style={CustomStyles.container}>
              <View
              style={{backgroundColor: "#000", justifyContent: 'center', alignItems: 'center'}}>

              <ProgressLoader
                visible={this.state.visible}
                isModal={true} isHUD={true}
                hudColor={"#fff"}
                height={200}
                width={200}
                color={"#8edf01"} />
              </View>

              { (this.state.currentLatitude != 0) &&
              <View style={[CustomStyles.centerContainer, {flex:1, marginTop: 0}]}>
                  
                  { (this.state.currentLatitude != 0) &&
                  <MapView 
                      ref="map"
                      provider = {PROVIDER_GOOGLE}
                      style={{flex: 1,}}        
                      initialRegion={{          
                        latitude: this.state.currentLatitude,          
                        longitude: this.state.currentLongitude, 
                        latitudeDelta: this.state.latDelta,
                        longitudeDelta: this.state.longDelta,
                      }}
                      showsUserLocation={false}   
                      onRegionChangeComplete={region => {
                          // console.log('onRegionChangeComplete..........' + region.latitudeDelta)

                        if (this.state.latDelta != region.latitudeDelta || this.state.longDelta != region.longitudeDelta) {
                          this.setState({
                          latDelta: region.latitudeDelta,
                          longDelta: region.longitudeDelta,
                        });
                        }
                        
                      }}    
                  >
                    <Marker
                      // key={index}
                      // name={marker.name}
                      coordinate={{
                        latitude: this.state.currentLatitude,
                        longitude: this.state.currentLongitude,
                      }}
                      rotation={this.state.heading}
                    >
                    <Image source={require('../../images/carIcon.png')} style={{height: 40, width: 40 }} 
                    resizeMode={'contain'}/>
                    </Marker>
                  </MapView>
                  }
                  <View style={{ flexDirection: 'column', justifyContent: 'center', alignItems:'center'}}>
                            
                      <View style={{ flexDirection: 'row', marginLeft: 10, marginRight: 10}}>

                          <View style={{flexDirection: 'row', justifyContent: 'flex-start', alignItems:'center'}}>
                            <Image
                              style={{height:70, width:70, borderRadius: 35}}
                              source={this.state.truckImage}
                            />
                          </View>
                      
                          <View style={{flexDirection: 'row', justifyContent: 'flex-start', alignItems:'center', height: 95, width: (Dimensions.get('window').width)-100, marginLeft:10 }}>
                              <View style={[CustomStyles.flexColunm, {justifyContent: 'flex-start', flex: 1}]}>
                                      <View style={[CustomStyles.flexRow, {justifyContent: 'space-between', alignItems: 'center'}]}>
                                          <Text style={[CustomStyles.titleText, {marginTop: 0}]}>{this.state.truckNumber}</Text>
                                          {
                                          // <TouchableOpacity
                                          //   style={[CustomStyles.themeButton, {width: 100, marginTop: 0}]}
                                          //   onPress={this.showModal.bind(this)}
                                          // >
                                          //     <Text style={[CustomStyles.themeButtonText]}>Change</Text>
                                          // </TouchableOpacity>

                                        }
                                      </View>
                                      <Text style={[CustomStyles.descGrayText, {textAlign: 'left'}]}>{this.state.truckType} - {this.state.truckModel}</Text>
                              </View>
                          </View>
                      </View>

                      <View style={{ flexDirection: 'row', marginBottom: 10}}>
                      <TouchableOpacity
                        style={[CustomStyles.themeButton, {width: '90%', marginTop: 15, backgroundColor: (this.state.currentDriverStatus == "Go Online") ? 'green' : 'red'}]}
                        onPress={this.onClickChangeStatus.bind(this)}
                      >
                          <Text style={[CustomStyles.themeButtonText, {color: '#fff'}]}>{this.state.currentDriverStatus} </Text>
                      </TouchableOpacity>
                      </View>                  
                  </View> 
              </View> 
              }
              <Modal
                animationType="slide" // fade
                transparent={true}
                visible={this.state.modalVisible}>
                <View style={{flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}>
                    <View style={{backgroundColor: 'black', height: 300, width: (Dimensions.get('window').width)-80}}>
                        <View style={{ backgroundColor: '#8edf01', flexDirection: 'row', justifyContent: 'center', height:40, marginBottom:10}}>
                            <Text style={[CustomStyles.themeButtonText, {marginTop: 10}]}>Select Truck</Text>
                            
                        </View>
                        <TouchableOpacity
                        onPress={() => this.hideModal()} 
                        style={{height: 25, width: 25, position: 'absolute', marginLeft: (Dimensions.get('window').width)-115, marginTop:8}}
                        >
                        <Image
                              ref="imgChecked"
                              style={{height: 25, width: 25, }}
                              source={require('../../images/cancel.png')}
                            />
                        </TouchableOpacity>
                        <View style={{ flex: 10}}>
                            <FlatList
                            showsVerticalScrollIndicator
                            data={this.state.truckList}
                            renderItem={this.renderPodcastItem}
                            keyExtractor={(item, index) => index.toString()}
                          />
                        </View>

                        <View style={[CustomStyles.flexRow, {justifyContent: 'space-around', alignItems: 'center', marginBottom: 30}]}>
                            <TouchableOpacity
                                style={[CustomStyles.themeButton, {marginTop:0, width: 100, backgroundColor: '#fff'}]}
                                onPress={() => this.onClickChangeTruck()} 
                                >
                                <Text style={[CustomStyles.themeButtonText]}>
                                  Change
                                </Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                                style={[CustomStyles.themeButton, {marginTop:0, width: 150}]}
                                onPress={() => this.onClickManageTruck()} 
                                >
                                <Text style={[CustomStyles.themeButtonText]}>
                                  Manage Truck
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
              </Modal>  
      </View>
      )
    }
}

class FlatListCell extends React.Component {
  render() {
    const { item, index, selectedIndex} = this.props;

    return (
      <TouchableWithoutFeedback 
        onPress={this.props.onSelectCell.bind(this, item, index)}>
        <View 
          style={[ CustomStyles.countryStyle, { flexDirection: 'row',marginTop : 10, marginBottom : 10, marginLeft: 20, marginRight: 20, borderBottomColor: 'gray', borderBottomWidth: 0.5 }]}>
          <Image
                ref="imgChecked"
                style={{height: 25, width: 25, marginRight: 8, marginBottom : 15}}
                source={ selectedIndex === item.id ?                  
                require('../../images/checkSelected.png') : 
                require('../../images/checkUnselected.png')}
              />
          <Text style={{fontSize: 15, color: 'white', marginTop: 3}}>
            {item.cartype}
          </Text>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

