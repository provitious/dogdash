import React, { Component, Fragment } from 'react';
import { StyleSheet, Dimensions, YellowBox, Text, View, Button, TextInput, Image, ScrollView, TouchableOpacity, Alert, SafeAreaView, PixelRatio } from 'react-native';
import {KeyboardAvoidingView, Keyboard, TouchableWithoutFeedback} from 'react-native';
import userDefaults from 'react-native-user-defaults'
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import ImagePicker from 'react-native-image-picker';
import { Modal, FlatList } from 'react-native';
import ProgressLoader from 'rn-progress-loader';
import DateTimePicker from "react-native-modal-datetime-picker";
import { Dropdown } from 'react-native-material-dropdown';
import { StackActions, NavigationActions } from 'react-navigation';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'
import EventBus from 'react-native-event-bus'
import NetInfo from "@react-native-community/netinfo";
import Toast from 'react-native-simple-toast';

import Constant from '../CommonClasses/Constant';
import CommonUtilsObj from '../CommonClasses/CommonUtils';
import CustomStyles from '../CommonClasses/CustomStyles';
import Message from '../CommonClasses/Message';
import APIManagerObj from '../CommonClasses/APIManager';

import cdata from './Countries'

var defaultFlag = cdata.filter(
  obj => obj.name === 'United States'
  )[0].flag

var defaultCode = cdata.filter(
  obj => obj.name === 'United States'
  )[0].dial_code

const txtInputHeight = (((Dimensions.get('window').width) * 9.8) / 100);
const txtInputFontSize = (((Dimensions.get('window').width) * 3.6) / 100);

class EditProfile extends Component {

    constructor(props) {
        super(props);

        this.onFocus = this.onFocus.bind(this);
        
        this.onChangePersonalDetailsText = this.onChangePersonalDetailsText.bind(this);
        this.onChangeLegalDetailsText = this.onChangeLegalDetailsText.bind(this);
        this.onChangeVehicleDetailsText = this.onChangeVehicleDetailsText.bind(this);

        this.nameRef = this.updateRef.bind(this, 'name');
        this.emailRef = this.updateRef.bind(this, 'email');
        this.mobileRef = this.updateRef.bind(this, 'mobile');
        this.addressLine1Ref = this.updateRef.bind(this, 'addressLine1');
        this.addressLine2Ref = this.updateRef.bind(this, 'addressLine2');
        this.postalCodeRef = this.updateRef.bind(this, 'postalCode');

        this.vehicleMakeRef = this.updateRef.bind(this, 'vehicleMake');
        this.vehicleModelRef = this.updateRef.bind(this, 'vehicleModel');
        this.vehicleNumberRef = this.updateRef.bind(this, 'vehicleNumber');
        this.dogCapacityRef = this.updateRef.bind(this, 'dogCapacity');
        this.licenseNumberRef = this.updateRef.bind(this, 'licenseNumber');
        this.licensePlateRef = this.updateRef.bind(this, 'licensePlate');
        this.insuranceNumberRef = this.updateRef.bind(this, 'insuranceNumber');
        this.ssnNumberRef = this.updateRef.bind(this, 'ssnNumber');

        this.onSubmitName = this.onSubmitName.bind(this);
        this.onSubmitEmail = this.onSubmitEmail.bind(this);
        this.onSubmitMobile = this.onSubmitMobile.bind(this);
        this.onSubmitAddressLine1 = this.onSubmitAddressLine1.bind(this);
        this.onSubmitAddressLine2 = this.onSubmitAddressLine2.bind(this);
        this.onSubmitPostalCode = this.onSubmitPostalCode.bind(this);

        this.onSubmitVehicleMake = this.onSubmitVehicleMake.bind(this);
        this.onSubmitVehicleModel = this.onSubmitVehicleModel.bind(this);
        this.onSubmitVehicleNumber = this.onSubmitVehicleNumber.bind(this);
        this.onSubmitDogCapacity = this.onSubmitDogCapacity.bind(this);
        this.onSubmitLicenseNumber = this.onSubmitLicenseNumber.bind(this);
        this.onSubmitLicensePlate = this.onSubmitLicensePlate.bind(this);
        this.onSubmitInsuranceNumber = this.onSubmitInsuranceNumber.bind(this);
        this.onSubmitSnnNumber = this.onSubmitSnnNumber.bind(this);

        this.selectPhotoTapped = this.selectPhotoTapped.bind(this);

         // defaultFlag = cdata.filter(
         //  obj => obj.dial_code === CommonUtilsObj.userDetails.phone_code
         //  )[0].flag

         // defaultCode = cdata.filter(
         //  obj => obj.dial_code === CommonUtilsObj.userDetails.phone_code
         //  )[0].dial_code

        console.log('CommonUtilsObj.cabDetails.car_no' + CommonUtilsObj.cabDetails.car_no);
        this.state = {
            connection_Status : "",
            visible: false,
            cabList: [],
            isVisiblePeronalDetails: true,
            isVisibleVehicleDetails: false,
            isVisibleLegalDetails: false,
            profileSource: { uri: Constant.KDriverImagePathURL + CommonUtilsObj.userDetails.image },
            name: CommonUtilsObj.userDetails.name,
            email: CommonUtilsObj.userDetails.email,
            flag: defaultFlag,
            phoneCode: defaultCode,//CommonUtilsObj.userDetails.phone_code,
            mobile: CommonUtilsObj.userDetails.phone,
            addressLine1: CommonUtilsObj.userDetails.address_line1,
            addressLine2: CommonUtilsObj.userDetails.address_line2,
            postalCode: CommonUtilsObj.userDetails.postal_code,
            didUpdate: false,
            timerSec: 3,
            city: CommonUtilsObj.userDetails.city,
            cityID: CommonUtilsObj.userDetails.state_code,
            cityList: [],
            stateName: CommonUtilsObj.userDetails.state,
            stateID: CommonUtilsObj.userDetails.state_id,
            stateList: [],
            modalVisible: false,
            birthDate: CommonUtilsObj.userDetails.dob,
            isDateTimePickerVisible: false,
            gender: CommonUtilsObj.userDetails.gender,
            vehicleMake: CommonUtilsObj.cabDetails.car_make,
            vehicleModel: CommonUtilsObj.cabDetails.model,
            vehicleType: CommonUtilsObj.cabDetails.cartype,
            vehicleTypeId: CommonUtilsObj.cabDetails.car_type,
            vehicleNumber: CommonUtilsObj.userDetails.car_no,
            dogCapacity: CommonUtilsObj.userDetails.Seating_Capacity,
            licenseNumber: CommonUtilsObj.userDetails.license_no,
            ssnNumber: CommonUtilsObj.userDetails.ssn_no,
            licenseExpiryDate: CommonUtilsObj.userDetails.license_expiry_date,
            isLicenseExpiryDatePickerVisible: false,
            licensePlate: CommonUtilsObj.cabDetails.license_plate,
            insuranceNumber: CommonUtilsObj.userDetails.Insurance,
            registrationImage: { uri: Constant.KDriverImagePathURL + CommonUtilsObj.userDetails.vehicle_registration_img },
            frontImage: { uri: Constant.KDriverImagePathURL + CommonUtilsObj.userDetails.driver_license_front },
            backImage: { uri: Constant.KDriverImagePathURL + CommonUtilsObj.userDetails.driver_license_back },
            adsImage: { uri: Constant.KDriverImagePathURL + CommonUtilsObj.userDetails.license_ads },
        };

        YellowBox.ignoreWarnings([
          'Warning: componentWillMount is deprecated',
          'Warning: componentWillReceiveProps is deprecated',
          'Warning: componentWillUpdate is deprecated',
          'Warning: componentWillUpdate has been renamed, and is not recommended for use',
          'Warning: componentWillReceiveProps has been renamed, and is not recommended for use',
        ]);
    }

    componentDidMount() {

      NetInfo.isConnected.addEventListener(
          'connectionChange',
          this._handleConnectivityChange

      );
     
      NetInfo.isConnected.fetch().done((isConnected) => {

        if(isConnected == true)
        {
          this.setState({connection_Status : "Online"})
          this.onFetchCabList();
          this.onFetchCityList(CommonUtilsObj.userDetails.state_id);
          this.onFetchStateList();
        }
        else
        {
          this.setState({connection_Status : "Offline"})
        }

      });

    }

    componentWillUnmount() {
 
      NetInfo.isConnected.removeEventListener(
          'connectionChange',
          this._handleConnectivityChange
   
      );
   
    }

    _handleConnectivityChange = (isConnected) => {

      if(isConnected == true)
        {
          this.setState({connection_Status : "Online"})
        }
        else
        {
          this.setState({connection_Status : "Offline"})
        }
    };

    async onFetchCabList() {
      if (this.state.connection_Status == "Online") {

        this.setState({ visible: true });

        let responseData = await APIManagerObj.callApi(Constant.KGetCabListURL);
        console.log('json response ....' + JSON.stringify(responseData));

        if (responseData.status == "success") {
          this.setState({
            visible: false,
            cabList: responseData.Car_Type.map(item => { return {value: item.cartype, cid: item.cab_id} }),
          });        
        }
        else {
          this.setState({
            visible: false,
          }); 
          setTimeout(() => {
            if(typeof(responseData.message) === 'string')
            {
                Toast.showWithGravity(responseData.message, Toast.LONG, Toast.TOP)
            }
            else if(typeof(responseData.error) === 'string')
            {
                Toast.showWithGravity(responseData.error, Toast.LONG, Toast.TOP)
            }
            else {
                if (responseData.error) {
                  Toast.showWithGravity(responseData.error.toString(), Toast.LONG, Toast.TOP)
                }
                else {
                  Toast.showWithGravity(Message.KAPIError, Toast.LONG, Toast.TOP)
                }
            }
          }, 200); 
        }
      }
      else {
        this.setState({lastApiTag : 1})
        Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
      }
    }

    async onFetchStateList() {
      if (this.state.connection_Status == "Online") {

        
        let responseData = await APIManagerObj.callApi(Constant.KGetStateListURL);
        // console.log('json response ....' + JSON.stringify(responseData));

        if (responseData.status == "success") {
          this.setState({
            visible: false,
            stateList: responseData.states.map(item => { return {value: item.state, sid: item.id} }),
          });         
        }
        else {
          setTimeout(() => {
            if(typeof(responseData.message) === 'string')
            {
                Toast.showWithGravity(responseData.message, Toast.LONG, Toast.TOP)
            }
            else if(typeof(responseData.error) === 'string')
            {
                Toast.showWithGravity(responseData.error, Toast.LONG, Toast.TOP)
            }
            else {
                if (responseData.error) {
                  Toast.showWithGravity(responseData.error.toString(), Toast.LONG, Toast.TOP)
                }
                else {
                  Toast.showWithGravity(Message.KAPIError, Toast.LONG, Toast.TOP)
                }
            }
          }, 200); 
        }
      }
      else {
        this.setState({lastApiTag : 1})
        Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
      }
    }

    async onFetchCityList(stateID) {
      if (this.state.connection_Status == "Online") {

        var data = new FormData();
        data.append('id', stateID);
        let responseData = await APIManagerObj.callApiWithData(Constant.KGetCityListURL, data);
        // console.log('json response ....' + JSON.stringify(responseData));

        if (responseData.status == "success") {
          this.setState({
            visible: false,
            cityList: responseData.cities.map(item => { return {value: item.city, cid: item.id} }),
          });         
        }
        else {
          setTimeout(() => {
            if(typeof(responseData.message) === 'string')
            {
                Toast.showWithGravity(responseData.message, Toast.LONG, Toast.TOP)
            }
            else if(typeof(responseData.error) === 'string')
            {
                Toast.showWithGravity(responseData.error, Toast.LONG, Toast.TOP)
            }
            else {
                if (responseData.error) {
                  Toast.showWithGravity(responseData.error.toString(), Toast.LONG, Toast.TOP)
                }
                else {
                  Toast.showWithGravity(Message.KAPIError, Toast.LONG, Toast.TOP)
                }
            }
          }, 200); 
        }
      }
      else {
        this.setState({lastApiTag : 1})
        Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
      }
    }

    updateRef(name, ref) {
      this[name] = ref;
    }

    onFocus() {
      let { errors = {} } = this.state;
      console.log(errors);
      for (let name in errors) {
        let ref = this[name];
        
        if (ref && ref.isFocused()) {
          delete errors[name];
        }
      }

      this.setState({ errors });
    }

    onChangePersonalDetailsText(text) {
      ['email', 'name', 'mobile', 'addressLine1', 'addressLine2', 'postalCode']
        .map((name) => ({ name, ref: this[name] }))
        .forEach(({ name, ref }) => {
          if (ref.isFocused()) {
            this.setState({ [name]: text });
            if (name == 'mobile') {
              this.setState({ [name]: text.replace(/[^0-9]/g, '') });
            }

            if (name == 'name') {
              if (!text.replace(/\s/g, '').length) {
                this.setState({ [name]: text.trim() });
              }
            }

            if (name == 'addressLine1') {
              if (!text.replace(/\s/g, '').length) {
                this.setState({ [name]: text.trim() });
              }
            }

            if (name == 'addressLine2') {
              if (!text.replace(/\s/g, '').length) {
                this.setState({ [name]: text.trim() });
              }
            }

            if (name == 'postalCode') {
              this.setState({ [name]: text.replace(/[^0-9]/g, '') });
            }
          }
        });
    }

    onChangeLegalDetailsText(text) {
      ['licenseNumber', 'licensePlate', 'insuranceNumber', 'ssnNumber']
        .map((name) => ({ name, ref: this[name] }))
        .forEach(({ name, ref }) => {
          if (ref.isFocused()) {
            this.setState({ [name]: text });

            if (name == 'licenseNumber') {
              if (!text.replace(/\s/g, '').length) {
                this.setState({ licenseNumber: text.trim() });
              }
            }

            if (name == 'licensePlate') {
              if (!text.replace(/\s/g, '').length) {
                this.setState({ [name]: text.trim() });
              }
            }

            if (name == 'insuranceNumber') {
              if (!text.replace(/\s/g, '').length) {
                this.setState({ [name]: text.trim() });
              }
            }

            if (name == 'ssnNumber') {
              this.setState({ [name]: text.replace(/[^0-9]/g, '') });
            }
          }

          
        });
    }

    onChangeVehicleDetailsText(text) {
      ['vehicleMake', 'vehicleModel', 'vehicleNumber', 'dogCapacity']
        .map((name) => ({ name, ref: this[name] }))
        .forEach(({ name, ref }) => {
          if (ref.isFocused()) {
            this.setState({ [name]: text });
            if (name == 'dogCapacity') {
              this.setState({ [name]: text.replace(/[^0-9]/, '') });
            }

            if (name == 'vehicleMake') {
              if (!text.replace(/\s/g, '').length) {
                this.setState({ vehicleMake: text.trim() });
              }
            }

            else if (name == 'vehicleModel') {
              if (!text.replace(/\s/g, '').length) {
                this.setState({ [name]: text.trim() });
              }
            }

            else if (name == 'vehicleNumber') {
              if (!text.replace(/\s/g, '').length) {
                this.setState({ [name]: text.trim() });
              }
            }
          }
        });
    }

    showModal() {
      this.setState({ modalVisible: true })
    }

    hideModal() {
      this.setState({ modalVisible: false })
    }

    async getCountry(country) {
      if (this.state.connection_Status == "Online") {
        const countryData = await cdata
      try {
        const countryCode = await countryData.filter(
          obj => obj.name === country
        )[0].dial_code
        const countryFlag = await countryData.filter(
          obj => obj.name === country
        )[0].flag
        // Set data from user choice of country
        this.setState({ phoneCode: countryCode, flag: countryFlag})
        await this.hideModal()
      }
      catch (err) {
        console.log(err)
      }
    }
    else {
      Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
    }
      
    }

    onClickPersonalDetails() {
      this.setState({ 
        isVisiblePeronalDetails: true,
        isVisibleVehicleDetails: false,
        isVisibleLegalDetails: false, 
      });
    }

    onClickVehicleDetails() {
      var flag = true;
      var errorMsg = [];

      // if (this.state.profileSource == require('../../images/userPlaceholder.png')) {
      //     errorMsg.push('Select profile image')
      //     flag = false;
      // }

      let nameVal = this.state.name
      if (!nameVal) {
          errorMsg.push('Name should not be empty')
          flag = false;
      }

      let emailVal = this.state.email
      let emailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ;

      if (!emailVal) {
          errorMsg.push('Email should not be empty')
          flag = false;
      } 
      else {
        if (emailRegex.test(emailVal) == false) {
            errorMsg.push('Invalid email address')
            flag = false;
        }
      }

      let mobileVal = this.state.mobile
      if (!mobileVal) {
          errorMsg.push('Mobile should not be empty')
          flag = false;
      } 
      else if (mobileVal.length < 10) {
        errorMsg.push('Invalid mobile number')
          flag = false;
      } 

      if (this.state.birthDate == 'Select birth date') {
          errorMsg.push('Please select birth date')
          flag = false;
      }

      if (flag) {
        this.setState({ 
          isVisiblePeronalDetails: false,
          isVisibleVehicleDetails: true,
          isVisibleLegalDetails: false, 
        });
      }
      else {
        Toast.showWithGravity('Please fill the required details', Toast.LONG, Toast.TOP)
      }

      
    }

    onClickLegalDetails() {

      var flag = true;
      var errorMsg = [];

      // if (this.state.profileSource == require('../../images/userPlaceholder.png')) {
      //     errorMsg.push('Select profile image')
      //     flag = false;
      // }

      let nameVal = this.state.name
      if (!nameVal) {
          errorMsg.push('Name should not be empty')
          flag = false;
      }

      let emailVal = this.state.email
      let emailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ;

      if (!emailVal) {
          errorMsg.push('Email should not be empty')
          flag = false;
      } 
      else {
        if (emailRegex.test(emailVal) == false) {
            errorMsg.push('Invalid email address')
            flag = false;
        }
      }

      let mobileVal = this.state.mobile
      if (!mobileVal) {
          errorMsg.push('Mobile should not be empty')
          flag = false;
      } 
      else if (mobileVal.length < 10) {
        errorMsg.push('Invalid mobile number')
          flag = false;
      } 

      if (this.state.birthDate == 'Select birth date') {
          errorMsg.push('Please select birth date')
          flag = false;
      }

      let vehicleMakeVal = this.state.vehicleMake
      if (!vehicleMakeVal) {
          errorMsg.push('Vehicle make should not be empty')
          flag = false;
      }

      let vehicleModelVal = this.state.vehicleModel
      if (!vehicleModelVal) {
          errorMsg.push('Vehicle model should not be empty')
          flag = false;
      }

      // let vehicleTypeVal = this.state.vehicleType
      // if (vehicleTypeVal == 'Select vehicle type') {
      //     errorMsg.push('Please select vehicle type')
      //     flag = false;
      // }

      let vehicleNumberVal = this.state.vehicleNumber
      if (!vehicleNumberVal) {
          errorMsg.push('Vehicle number should not be empty')
          flag = false;
      }

      let dogCapacityVal = this.state.dogCapacity
      if (!dogCapacityVal) {
          errorMsg.push('Dog capacity should not be empty')
          flag = false;
      }
      
      if (flag) {
        this.setState({ 
          isVisiblePeronalDetails: false,
          isVisibleVehicleDetails: false,
          isVisibleLegalDetails: true, 
        });
      }
      else {
        Toast.showWithGravity('Please fill the required details', Toast.LONG, Toast.TOP)
      }

      
    }

    showDateTimePicker = () => {
      this.setState({ isDateTimePickerVisible: true });
    };

    hideDateTimePicker = () => {
      this.setState({ isDateTimePickerVisible: false });
    };

    handleDatePicked = date => {
      console.log("A date has been picked: ", CommonUtilsObj.getFormatedDate(date));
      this.setState({ 
        birthDate: CommonUtilsObj.getFormatedDate(date),
        isDateTimePickerVisible: false,
         });
      this.hideDateTimePicker();
    };

    showExpiryDateTimePicker = () => {
      this.setState({ isLicenseExpiryDatePickerVisible: true });
    };

    hideExpiryDateTimePicker = () => {
      this.setState({ isLicenseExpiryDatePickerVisible: false });
    };

    handleExpiryDatePicked = date => {
      console.log("A date has been picked: ", date);
      this.setState({ 
        licenseExpiryDate: CommonUtilsObj.getFormatedDate(date),
        isLicenseExpiryDatePickerVisible: false,
         });
      this.hideExpiryDateTimePicker();
    };

    selectPhotoTapped(type) {
        const options = {
          quality: 1.0,
          maxWidth: 500,
          maxHeight: 500,
          storageOptions: {
            skipBackup: true,
          },
        };

        ImagePicker.showImagePicker(options, response => {
          console.log('Response = ', response);

          if (response.didCancel) {
            console.log('User cancelled photo picker');
          } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
          } else {
            let source = {uri: response.uri};

            if (type == 'profile') {
              setTimeout(() => {
                  this.setState({ profileSource: source })
                }, 200);
                
            }
            else if (type == 'registrationImage') {
                this.setState({ registrationImage: source })
            }
            else if (type == 'frontImage') {
                this.setState({ frontImage: source })
            }
            else if (type == 'backImage') {
                this.setState({ backImage: source })
            }
            else if (type == 'adsImage') {
                this.setState({ adsImage: source })
            }
          }
        });
    }

    onSubmitName(text) {
      this.email.focus();
    }

    onSubmitEmail(text) {
      let emailVal = text.nativeEvent.text
      let emailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ;

      if (!emailVal) {
        Toast.showWithGravity('Email should not be empty', Toast.LONG, Toast.TOP)
      } 
      else {
        
        if (emailRegex.test(emailVal) == false) {
          Toast.showWithGravity('Invalid email address', Toast.LONG, Toast.TOP)
        }
        else {
          this.mobile.focus();
        }
      }
    }

    onSubmitMobile(text) {
      this.addressLine1.focus();
    }

    onSubmitAddressLine1() {
      this.addressLine2.focus();
    }

    onSubmitAddressLine2() {
      this.postalCode.focus();
    }

    onSubmitPostalCode() {
      this.mobile.blur();
      this.setState({ isDateTimePickerVisible: true });
    }

    onSubmitVehicleMake(text) {
      this.vehicleModel.focus();
    }

    onSubmitVehicleModel(text) {
      this.vehicleModel.blur();
    }

    onSubmitVehicleNumber(text) {
      this.dogCapacity.focus();
    }

    onSubmitDogCapacity(text) {
      this.dogCapacity.blur();
    }

    onSubmitLicenseNumber(text) {
      this.licenseNumber.blur();
      this.setState({ isLicenseExpiryDatePickerVisible: true });
    }

    onSubmitLicensePlate(text) {
      this.insuranceNumber.focus();
    }

    onSubmitInsuranceNumber(text) {
      this.ssnNumber.focus();
    }

    onSubmitSnnNumber(Text) {
      this.ssnNumber.blur()
    }

    hidePersonalTextfiledKeyboard() {
      this.name.blur();
      this.email.blur();
      this.mobile.blur();
      this.addressLine1.blur();
      this.addressLine2.blur();
      this.postalCode.blur();
    }

    hideVehicleTextfiledKeyboard() {
      this.vehicleMake.blur();
      this.vehicleModel.blur();
      this.vehicleNumber.blur();
      this.dogCapacity.blur();
    }

    hideLegalTextfiledKeyboard() {
      this.licenseNumber.blur();
      this.licensePlate.blur();
      this.insuranceNumber.blur();
      this.ssnNumber.blur();
    }

    onValidatePersonalDetails() {

      this.hidePersonalTextfiledKeyboard();

      var flag = true;
      var errorMsg = [];

      // if (this.state.profileSource == require('../../images/userPlaceholder.png')) {
      //     errorMsg.push('Select profile image')
      //     flag = false;
      // }

      let nameVal = this.state.name
      if (!nameVal) {
          errorMsg.push(Message.KNameEmpty)
          flag = false;
      }

      let emailVal = this.state.email
      let emailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ;

      if (!emailVal) {
          errorMsg.push(Message.KEmailEmpty)
          flag = false;
      } 
      else {
        if (emailRegex.test(emailVal) == false) {
            errorMsg.push(Message.KEmailInvalid)
            flag = false;
        }
      }

      let mobileVal = this.state.mobile
      if (!mobileVal) {
          errorMsg.push(Message.KMobileEmpty)
          flag = false;
      } 
      else if (mobileVal.length < 10) {
        errorMsg.push('Invalid mobile number')
          flag = false;
      } 

      if (this.state.addressLine1 == '') {
          errorMsg.push(Message.KAddressLine1Empty)
          flag = false;
      }

      // if (this.state.addressLine2 == '') {
      //     errorMsg.push(Message.KAddressLine2Empty)
      //     flag = false;
      // }

      if (this.state.postalCode == '') {
          errorMsg.push(Message.KPostalCodeEmpty)
          flag = false;
      }

      if (this.state.city == 'Select city') {
          errorMsg.push(Message.KCityEmpty)
          flag = false;
      }

      if (this.state.stateName == 'Select state') {
          errorMsg.push(Message.KStateEmpty)
          flag = false;
      }

      if (this.state.birthDate == 'Select birth date') {
          errorMsg.push(Message.KBirthDateEmpty)
          flag = false;
      }

      if (flag) {
        if (this.state.connection_Status == "Online") {
            this.onClickVehicleDetails();
        }
        else {
          Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
        }
      }
      else {
        var errMsg = '';
        if (errorMsg.length > 2) {
            errMsg = Message.KRequiredFiledsEmpty;
        }
        else {
            if (errorMsg.length == 2) {
              errMsg = errorMsg[0] + ', ' + errorMsg[1]; 
            }
            else {
              errMsg = errorMsg[0]; 
            }
            
        }
        // Toast.showWithGravity(errorMsg.toString(), Toast.LONG, Toast.TOP)
        Toast.showWithGravity(errMsg, Toast.LONG, Toast.TOP)
      } 
    }

    onValidateVehicleDetails() {

      this.hideVehicleTextfiledKeyboard();

      var flag = true;
      var errorMsg = [];

      let vehicleMakeVal = this.state.vehicleMake
      if (!vehicleMakeVal) {
          errorMsg.push(Message.KVehicleMakeEmpty)
          flag = false;
      }

      let vehicleModelVal = this.state.vehicleModel
      if (!vehicleModelVal) {
          errorMsg.push(Message.KVehicleModelEmpty)
          flag = false;
      }

      // let vehicleTypeVal = this.state.vehicleType
      // if (vehicleTypeVal == 'Select vehicle type') {
      //     errorMsg.push('Please select vehicle type')
      //     flag = false;
      // }

      let vehicleNumberVal = this.state.vehicleNumber
      if (!vehicleNumberVal) {
          errorMsg.push(Message.KVehicleNumberEmpty)
          flag = false;
      }

      let dogCapacityVal = this.state.dogCapacity
      if (!dogCapacityVal) {
          errorMsg.push(Message.KDogCapacityEmpty)
          flag = false;
      }

      if (flag) {
        if (this.state.connection_Status == "Online") {
            this.onClickLegalDetails();
        }
        else {
          Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
        }
      }
      else {
        var errMsg = '';
        if (errorMsg.length > 2) {
            errMsg = Constant.KRequiredFiledsEmpty;
        }
        else {
            if (errorMsg.length == 2) {
              errMsg = errorMsg[0] + ', ' + errorMsg[1]; 
            }
            else {
              errMsg = errorMsg[0]; 
            }
            
        }
        // Toast.showWithGravity(errorMsg.toString(), Toast.LONG, Toast.TOP)
        Toast.showWithGravity(errMsg, Toast.LONG, Toast.TOP)
      }
    }

    onValidateLegalDetails() {

      this.hideLegalTextfiledKeyboard();

      var flag = true;
      var errorMsg = [];

      let licenseNumberVal = this.state.licenseNumber
      if (!licenseNumberVal) {
          errorMsg.push(Message.KLicenseNumberEmpty)
          flag = false;
      }

      let licenseExpiryDateVal = this.state.licenseExpiryDate
      if (licenseExpiryDateVal == 'Select license expiry date') {
          errorMsg.push(Message.KLicenseExpiryDateEmpty)
          flag = false;
      }

      let licensePlateVal = this.state.licensePlate
      if (!licensePlateVal) {
          errorMsg.push(Message.KlicensePlateEmpty)
          flag = false;
      }

      let insuranceNumberVal = this.state.insuranceNumber
      if (!insuranceNumberVal) {
          errorMsg.push(Message.KInsuranceNumberEmpty)
          flag = false;
      }

      let ssnNumberVal = this.state.ssnNumber
      if (!ssnNumberVal) {
          errorMsg.push(Message.KSSNNumberEmpty)
          flag = false;
      }

      if (this.state.registrationImage == require('../../images/vehicleRegistration.png')) {
          errorMsg.push(Message.KRegImageEmpty)
          flag = false;
      }

      if (this.state.frontImage == require('../../images/frontImage.png')) {
          errorMsg.push(Message.KLicFrontImageEmpty)
          flag = false;
      }

      if (this.state.backImage == require('../../images/backImage.png')) {
          errorMsg.push(Message.KLicBackImageEmpty)
          flag = false;
      }

      if (this.state.adsImage == require('../../images/backImage.png')) {
          errorMsg.push(Message.KLicADSImageEmpty)
          flag = false;
      }

      if (flag) {
        if (this.state.connection_Status == "Online") {
          this.onEditProfile();
        }
        else {
          Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
        }
      }
      else {
        var errMsg = '';
        if (errorMsg.length > 2) {
            errMsg = Constant.KRequiredFiledsEmpty;
        }
        else {
            if (errorMsg.length == 2) {
              errMsg = errorMsg[0] + ', ' + errorMsg[1]; 
            }
            else {
              errMsg = errorMsg[0]; 
            }
            
        }
        // Toast.showWithGravity(errorMsg.toString(), Toast.LONG, Toast.TOP)
        Toast.showWithGravity(errMsg, Toast.LONG, Toast.TOP)
      }
    }

    async onEditProfile() {
      if (this.state.connection_Status == "Online") {

        this.setState({ visible: true });
        var data = new FormData();
        if (this.state.profileSource == require('../../images/userPlaceholder.png')) {
          data.append('image', '');
        }
        else {
          data.append('image', {
              name: 'testProfile.jpg',
              uri: this.state.profileSource.uri, type: 'image/jpeg'
          });
        }
        data.append('uid', CommonUtilsObj.userDetails.id);
        data.append('name', this.state.name);
        data.append('email', this.state.email);
        data.append('phone', this.state.mobile);
        data.append('phone_code', this.state.phoneCode);
        data.append('dob', this.state.birthDate);
        data.append('gender', this.state.gender);
        data.append('device_token', CommonUtilsObj.deviceToken);
        data.append('device_type', 'ios');
        data.append('Car_Make', this.state.vehicleMake);
        data.append('Car_Model', this.state.vehicleModel);
        // data.append('car_type', this.state.vehicleTypeId);
        data.append('car_type', '13');
        data.append('car_no', this.state.vehicleNumber);
        data.append('Seating_Capacity', this.state.dogCapacity);
        data.append('license_no', this.state.licenseNumber);
        data.append('license_expiry_date', this.state.licenseExpiryDate);
        data.append('license_plate', this.state.licensePlate);
        data.append('Insurance', this.state.insuranceNumber);

        data.append('ssn_no', this.state.ssnNumber);
        data.append('address_line1', this.state.addressLine1);
        data.append('address_line2', this.state.addressLine2);
        data.append('city', this.state.city);
        data.append('state', this.state.stateName);
        data.append('postal_code', this.state.postalCode);
        data.append('state_code', this.state.cityID);

        data.append('vehicle_registration_img', {
            name: 'testRegistration.jpg',
            uri: this.state.registrationImage.uri, type: 'image/jpeg'
        });
        data.append('driver_license_front', {
            name: 'testLicenseFront.jpg',
            uri: this.state.frontImage.uri, type: 'image/jpeg'
        });
        data.append('driver_license_back', {
            name: 'testLicenseBack.jpg',
            uri: this.state.backImage.uri, type: 'image/jpeg'
        });
        data.append('license_ads', {
            name: 'testLicenseADS.jpg',
            uri: this.state.adsImage.uri, type: 'image/jpeg'
        });

        let responseData = await APIManagerObj.callApiWithData(Constant.KProfileEditURL, data);
        console.log('.current_car[0] json response ....' + JSON.stringify(responseData));
        if (responseData.status == "success") {
          CommonUtilsObj.cabDetails = responseData.current_car[0];

          let obj = CommonUtilsObj.loggedUserData;
          obj.Driver_detail[0] = responseData.Driver_detail[0];
          obj.current_car[0] = responseData.current_car[0];

          console.log(obj.userdetail);

          if (Platform.OS==='android') {
              CommonUtilsObj.setLoggedUserDetails(JSON.stringify(obj));
          }
          else {
              CommonUtilsObj.setLoggedUserDetails(obj);
          }

          this.setState({
              visible: false
          });
          
          this.props.navigation.pop()
                
          setTimeout(() => {
            Toast.showWithGravity(responseData.message, Toast.LONG, Toast.TOP)
            const navigateAction = StackActions.reset({
              index: 0,
              actions: [NavigationActions.navigate({ routeName: "SettingsScreen"})],
            });
            this.props.navigation.dispatch(navigateAction);
            EventBus.getInstance().fireEvent("ProfileChangeEvent", {
              
            });

          }, 200);        
        }
        else {
          this.setState({
            visible: false,
          }); 
          setTimeout(() => {
            if(typeof(responseData.message) === 'string')
            {
                Toast.showWithGravity(responseData.message, Toast.LONG, Toast.TOP)
            }
            else if(typeof(responseData.error) === 'string')
            {
                Toast.showWithGravity(responseData.error, Toast.LONG, Toast.TOP)
            }
            else {
                if (responseData.error) {
                  Toast.showWithGravity(responseData.error.toString(), Toast.LONG, Toast.TOP)
                }
                else {
                  Toast.showWithGravity(Message.KAPIError, Toast.LONG, Toast.TOP)
                }
            }
          }, 200); 
        }
      }
      else {
        this.setState({lastApiTag : 1})
        Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
      }
    }

    async onEditProfile1() {
      if (this.state.connection_Status == "Online") {
        this.setState({
            visible: true
        });

        var data = new FormData();
        data.append('image', {
            name: 'testProfile.jpg',
            uri: this.state.profileSource.uri, type: 'image/jpeg'
        });
        data.append('uid', CommonUtilsObj.userDetails.id);
        data.append('name', this.state.name);
        data.append('email', this.state.email);
        data.append('phone', this.state.mobile);
        data.append('phone_code', this.state.phoneCode);
        data.append('dob', this.state.birthDate);
        data.append('gender', this.state.gender);
        data.append('device_token', CommonUtilsObj.deviceToken);
        data.append('device_type', 'ios');
        data.append('Car_Make', this.state.vehicleMake);
        data.append('Car_Model', this.state.vehicleModel);
        // data.append('car_type', this.state.vehicleTypeId);
        data.append('car_type', '13');
        data.append('car_no', this.state.vehicleNumber);
        data.append('Seating_Capacity', this.state.dogCapacity);
        data.append('license_no', this.state.licenseNumber);
        data.append('license_expiry_date', this.state.licenseExpiryDate);
        data.append('license_plate', this.state.licensePlate);
        data.append('Insurance', this.state.insuranceNumber);
        data.append('vehicle_registration_img', {
            name: 'testRegistration.jpg',
            uri: this.state.registrationImage.uri, type: 'image/jpeg'
        });
        data.append('driver_license_front', {
            name: 'testLicenseFront.jpg',
            uri: this.state.frontImage.uri, type: 'image/jpeg'
        });
        data.append('driver_license_back', {
            name: 'testLicenseBack.jpg',
            uri: this.state.backImage.uri, type: 'image/jpeg'
        });
        data.append('license_ads', {
            name: 'testLicenseADS.jpg',
            uri: this.state.adsImage.uri, type: 'image/jpeg'
        });

        console.log(data);
        console.log(Constant.KProfileEditURL);

        try {
            let response = await fetch(
              Constant.KProfileEditURL,
              {
                method: "POST",
                headers: {
                 "Accept": "application/json",
                 // "Content-Type": "application/json"
                 'Content-Type': 'multipart/form-data',
                },
                body: data
             }
            )
          .then((response) => response.json())
          .then((responseData) => {
             console.log('edit Params json response ....' + JSON.stringify(responseData));
             if (responseData.status == "success") {
               
                // CommonUtilsObj.setLoggedUserDetails(responseData)

                let obj = CommonUtilsObj.loggedUserData;

                console.log(responseData.Driver_detail[0]);

                obj.Driver_detail[0] = responseData.Driver_detail[0];

                console.log(obj.userdetail);

                if (Platform.OS==='android') {
                    CommonUtilsObj.setLoggedUserDetails(JSON.stringify(obj));
                }
                else {
                    CommonUtilsObj.setLoggedUserDetails(obj);
                }

                this.setState({
                    visible: false
                });

                setTimeout(() => {
                  CommonUtilsObj.getLoggedUserDetails()
                }, 500);
                
                this.props.navigation.pop()
                                
                setTimeout(() => {
                  console.log('CommonUtilsObj.userDetails.phone_code = ' + CommonUtilsObj.userDetails.phone_code);

                  Toast.showWithGravity(responseData.message, Toast.LONG, Toast.TOP)
                  const navigateAction = StackActions.reset({
                    index: 0,
                    actions: [NavigationActions.navigate({ routeName: "SettingsScreen"})],
                  });
                  this.props.navigation.dispatch(navigateAction);
                  EventBus.getInstance().fireEvent("ProfileChangeEvent", {
                    
                  });

                }, 2000);
              }
              else {
                this.setState({
                    visible: false
                });
                setTimeout(() => {
                  Toast.showWithGravity(responseData.message, Toast.LONG, Toast.TOP);
                }, 200);
              }
          })
          .done();
     
        } catch (errors) {
          this.setState({
              visible: false
          });
          let err = "Error is" + errors
          Toast.showWithGravity(err, Toast.LONG, Toast.TOP)
        }
    }
    else {
      Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
    }

         
    }

    renderPhotoView() {
      return (
        <View>
          <TouchableOpacity onPress={this.selectPhotoTapped.bind(this, 'profile')}>
            <View
              style={[CustomStyles.avatar, CustomStyles.avatarContainer, {marginBottom: 0}]}>
              {this.state.profileSource === null ? (
                <Text>Select a Photo</Text>
              ) : (
                <Image style={CustomStyles.avatar} source={this.state.profileSource} />
              )}
            </View>
          </TouchableOpacity>
        </View>
      )
    }

    renderPersonalDetails() {
        return(
            <View style={[CustomStyles.flexColunm, {margin: 10, marginTop: 5}]}>
              { (this.state.name != '') &&
                <Text style={CustomStyles.labelText}>Full name</Text>
              }
              <TextInput
                style={CustomStyles.textInput}
                ref={this.nameRef}
                placeholder = "Enter full name" 
                keyboardType='default'
                autoCapitalize='sentences'
                autoCorrect={true}
                enablesReturnKeyAutomatically={true}
                onFocus={this.onFocus}
                onChangeText={this.onChangePersonalDetailsText}
                onSubmitEditing={this.onSubmitName}
                returnKeyType='next'
                value={this.state.name}
              />

              { (this.state.email != '') &&
                <Text style={CustomStyles.labelText}>Email</Text>
              }
              <TextInput
                style={CustomStyles.textInput}
                ref={this.emailRef}
                placeholder = "Enter email address" 
                keyboardType='email-address'
                autoCapitalize='none'
                autoCorrect={true}
                enablesReturnKeyAutomatically={true}
                onFocus={this.onFocus}
                onChangeText={this.onChangePersonalDetailsText}
                onSubmitEditing={this.onSubmitEmail}
                returnKeyType='next'
                value={this.state.email}
              />

              { (this.state.mobile != '') &&
                <Text style={CustomStyles.labelText}>Mobile</Text>
              }
              <View style={{backgroundColor: 'white', height: CommonUtilsObj.btnHeight, marginTop: 15, borderRadius: 5, flexDirection: 'row', justifyContent: 'flex-start' ,backgroundColor: 'white', alignItems: 'center'}}>
                
                <TouchableOpacity
                      style={{marginLeft:10, alignSelf: 'center'}}
                      // onPress={this.showModal.bind(this)}
                >
                <Text style={{fontSize: txtInputFontSize + 7}}>{this.state.flag}</Text>
                </TouchableOpacity>
                <Text style={{fontSize: txtInputFontSize + 3, alignSelf: 'center', marginLeft:5}}>{this.state.phoneCode}</Text>

                <TextInput
                    style={{height: txtInputHeight, fontSize: txtInputFontSize, fontFamily: Constant.KThemeFontRegular , flex: 1, marginLeft: 5}}
                    ref={this.mobileRef}
                    placeholder = "Enter mobile number" 
                    keyboardType='phone-pad'
                    autoCapitalize='none'
                    autoCorrect={true}
                    enablesReturnKeyAutomatically={true}
                    onFocus={this.onFocus}
                    onChangeText={this.onChangePersonalDetailsText}
                    onSubmitEditing={this.onSubmitEmail}
                    returnKeyType='next'
                    maxLength={10}
                    value={this.state.mobile}
                />
              </View>

              { (this.state.addressLine1 != '') &&
                <Text style={CustomStyles.labelText}>Address Line 1</Text>
              }
              <TextInput
                style={CustomStyles.textInput}
                ref={this.addressLine1Ref}
                placeholder = "Enter address line 1" 
                keyboardType='default'
                autoCapitalize='sentences'
                autoCorrect={true}
                enablesReturnKeyAutomatically={true}
                onFocus={this.onFocus}
                onChangeText={this.onChangePersonalDetailsText}
                onSubmitEditing={this.onSubmitAddressLine1}
                returnKeyType='next'
                value={this.state.addressLine1}
              />

              { (this.state.addressLine2 != '') &&
                <Text style={CustomStyles.labelText}>Address Line 2</Text>
              }
              <TextInput
                style={CustomStyles.textInput}
                ref={this.addressLine2Ref}
                placeholder = "Enter address line 2" 
                keyboardType='default'
                autoCapitalize='sentences'
                autoCorrect={true}
                enablesReturnKeyAutomatically={true}
                onFocus={this.onFocus}
                onChangeText={this.onChangePersonalDetailsText}
                onSubmitEditing={this.onSubmitAddressLine2}
                returnKeyType='next'
                value={this.state.addressLine2}
              />

              { (this.state.postalCode != '') &&
                <Text style={CustomStyles.labelText}>Postal Code</Text>
              }
              <TextInput
                style={CustomStyles.textInput}
                ref={this.postalCodeRef}
                placeholder = "Enter postal code" 
                keyboardType='number-pad'
                autoCapitalize='sentences'
                autoCorrect={true}
                enablesReturnKeyAutomatically={true}
                onFocus={this.onFocus}
                onChangeText={this.onChangePersonalDetailsText}
                onSubmitEditing={this.onSubmitPostalCode}
                returnKeyType='next'
                value={this.state.postalCode}
              />

              { (this.state.stateName != 'Select state') &&
                <Text style={CustomStyles.labelText}>State</Text>
              }
              <Dropdown
                data={this.state.stateList}
                containerStyle = {{borderBottomColor: 'transparent', backgroundColor: 'white', borderRadius: 5, borderWidth: 0.5, height: CommonUtilsObj.btnHeight, paddingLeft : 5, paddingRight : 5, marginTop: 17}}
                dropdownOffset = {{top: 7, left: 0}}
                value = {this.state.stateName}
                onFocus={()=>{console.log("onFocus")}}
                dropdownPosition={0}
                // onChangeText={(value, bid)=>{dogsDetails[index].breed = dogBreedList[bid].bid, console.log(dogBreedList[bid].bid)}}

                onChangeText={(value, index)=>{
                  this.setState({ 
                    stateName: value,
                    stateID: this.state.stateList[index].sid,
                    city: 'Select city',
                    cityID: '',
                    didUpdate: true,
                    timerSec: 3,
                  })

                  this._interval = setInterval(() => {
                    console.log(this.state.timerSec);
                    if (this.state.timerSec <= 1) {
                      // alert('updated')
                      console.log(this.state.timerSec);
                      this.setState({
                        didUpdate: false,
                      });

                      clearInterval(this._interval);

                    }
                    else {
                      this.setState({
                        timerSec: this.state.timerSec - 1,
                      });
                    }
                  }, 1000);

                  setTimeout(() => {
                    this.onFetchCityList(this.state.stateList[index].sid);
                  }, 200);
                }}
                labelTextStyle={{color: (this.state.stateName == 'Select state') ? 'gray' : 'black'}}
                affixTextStyle={{color: (this.state.stateName == 'Select state') ? 'gray' : 'black'}}
                itemTextStyle={{color: (this.state.stateName == 'Select state') ? 'gray' : 'black'}}
                labelTextStyle={{color: (this.state.stateName == 'Select state') ? 'gray' : 'black'}}
                style={{color: (this.state.stateName == 'Select state') ? 'gray' : 'black'}}
              />

              { (this.state.city != 'Select city') &&
                <Text style={CustomStyles.labelText}>City</Text>
              }
              { !this.state.didUpdate && 
              <Dropdown
                data={this.state.cityList}
                containerStyle = {{borderBottomColor: 'transparent', backgroundColor: 'white', borderRadius: 5, borderWidth: 0.5, height: CommonUtilsObj.btnHeight, paddingLeft : 5, paddingRight : 5, marginTop: 17}}
                dropdownOffset = {{top: 7, left: 0}}
                value = {this.state.city}
                onFocus={()=>{console.log("onFocus")}}
                dropdownPosition={0}
                onChangeText={(value, index)=>{
                  this.setState({ 
                    city: value,
                    cityID: this.state.cityList[index].cid,
                  })
                }}
                labelTextStyle={{color: (this.state.city == 'Select city') ? 'gray' : 'black'}}
                affixTextStyle={{color: (this.state.city == 'Select city') ? 'gray' : 'black'}}
                itemTextStyle={{color: (this.state.city == 'Select city') ? 'gray' : 'black'}}
                labelTextStyle={{color: (this.state.city == 'Select city') ? 'gray' : 'black'}}
                style={{color: (this.state.city == 'Select city') ? 'gray' : 'black'}}
                
              />
              }

              { (this.state.birthDate != 'Select birth date') &&
                <Text style={CustomStyles.labelText}>Birth date</Text>
              }
              <TouchableOpacity
                onPress={this.showDateTimePicker}
                style={CustomStyles.birhDateBtn}
              >
                  <Text style={{fontSize: txtInputFontSize, fontFamily: Constant.KThemeFontRegular}}>{this.state.birthDate}</Text>
              </TouchableOpacity>

              { (this.state.gender != 'Select gender') &&
                <Text style={CustomStyles.labelText}>Gender</Text>
              }
              <Dropdown
                data={[{value: 'Male'}, {value: 'Female'}]}
                containerStyle = {{borderBottomColor: 'transparent', backgroundColor: 'white', borderRadius: 5, borderWidth: 0.5, height: CommonUtilsObj.btnHeight, paddingLeft : 5, paddingRight : 5, marginTop: 17}}
                dropdownOffset = {{top: 7, left: 0}}
                value = {this.state.gender}
                onFocus={()=>{console.log("onFocus")}}
                dropdownPosition={0}
                onChangeText={(value)=>{this.setState({ gender: value})}}
                labelTextStyle={{color: (this.state.gender == 'Select gender') ? 'gray' : 'black'}}
                affixTextStyle={{color: (this.state.gender == 'Select gender') ? 'gray' : 'black'}}
                itemTextStyle={{color: (this.state.gender == 'Select gender') ? 'gray' : 'black'}}
                labelTextStyle={{color: (this.state.gender == 'Select gender') ? 'gray' : 'black'}}
                style={{color: (this.state.gender == 'Select gender') ? 'gray' : 'black'}}
              />

              <TouchableOpacity
                  style={[CustomStyles.themeButton, {marginTop: 25}]}
                  onPress={this.onValidatePersonalDetails.bind(this)}
               >
                  <Text style={CustomStyles.themeButtonText}>Next</Text>
              </TouchableOpacity>

              
            </View>
          )
    }

    renderVehicleDetails() {
        return(
            <View style={[CustomStyles.flexColunm, {margin: 10, marginTop: 5}]}>
              { (this.state.vehicleMake != '') &&
                <Text style={CustomStyles.labelText}>Make</Text>
              }
              <TextInput
                style={CustomStyles.textInput}
                ref={this.vehicleMakeRef}
                placeholder = "Enter vehicle make" 
                keyboardType='default'
                autoCapitalize='sentences'
                autoCorrect={true}
                enablesReturnKeyAutomatically={true}
                onFocus={this.onFocus}
                onChangeText={this.onChangeVehicleDetailsText}
                onSubmitEditing={this.onSubmitVehicleMake}
                returnKeyType='next'
                value={this.state.vehicleMake}
              />

              { (this.state.vehicleModel != '') &&
                <Text style={CustomStyles.labelText}>Model</Text>
              }
              <TextInput
                style={CustomStyles.textInput}
                ref={this.vehicleModelRef}
                placeholder = "Enter model" 
                keyboardType='default'
                autoCapitalize='sentences'
                autoCorrect={true}
                enablesReturnKeyAutomatically={true}
                onFocus={this.onFocus}
                onChangeText={this.onChangeVehicleDetailsText}
                onSubmitEditing={this.onSubmitVehicleModel}
                returnKeyType='next'
                value={this.state.vehicleModel}
              />

              {
              // <Dropdown
              //   data={this.state.cabList}
              //   containerStyle = {{borderBottomColor: 'transparent', backgroundColor: 'white', borderRadius: 5, borderWidth: 0.5, height: txtInputHeight+1, paddingLeft : 10, paddingRight : 10, marginTop: 17}}
              //   dropdownOffset = {{top: 7, left: 0}}
              //   value = {this.state.vehicleType}
              //   onFocus={()=>{console.log("onFocus")}}
              //   dropdownPosition={0}
              //   onChangeText={(value, cid)=>{
              //     this.setState({ 
              //       vehicleType: value,
              //       vehicleTypeId: this.state.cabList[cid].cid, 
              //     });
              //     console.log(this.state.cabList[cid].cid)}
              //   }
              // />
              }

              { (this.state.vehicleNumber != '') &&
                <Text style={CustomStyles.labelText}>Number</Text>
              }
              <TextInput
                style={CustomStyles.textInput}
                ref={this.vehicleNumberRef}
                placeholder = "Enter vehicle number" 
                keyboardType='default'
                autoCapitalize='sentences'
                autoCorrect={true}
                enablesReturnKeyAutomatically={true}
                onFocus={this.onFocus}
                onChangeText={this.onChangeVehicleDetailsText}
                onSubmitEditing={this.onSubmitVehicleNumber}
                returnKeyType='next'
                value={this.state.vehicleNumber}
              />

              { (this.state.dogCapacity != '') &&
                <Text style={CustomStyles.labelText}>Dog capacity</Text>
              }
              <TextInput
                style={CustomStyles.textInput}
                ref={this.dogCapacityRef}
                placeholder = "Enter dog capacity" 
                keyboardType='phone-pad'
                autoCapitalize='sentences'
                autoCorrect={true}
                enablesReturnKeyAutomatically={true}
                onFocus={this.onFocus}
                onChangeText={this.onChangeVehicleDetailsText}
                onSubmitEditing={this.onSubmitDogCapacity}
                returnKeyType='done'
                value={this.state.dogCapacity}
              />

              <TouchableOpacity
                  style={[CustomStyles.themeButton, {marginTop: 25}]}
                  onPress={this.onValidateVehicleDetails.bind(this)}
               >
                  <Text style={CustomStyles.themeButtonText}>Next</Text>
              </TouchableOpacity>

            </View>
        )
    }

    renderLegalDetails() {
        return(
            <View style={[CustomStyles.flexColunm, {margin: 10, marginTop: 5, marginBottom: 20}]}>
              { (this.state.licenseNumber != '') &&
                <Text style={CustomStyles.labelText}>License number</Text>
              }
              <TextInput
                style={CustomStyles.textInput}
                ref={this.licenseNumberRef}
                placeholder = "Enter driving license number" 
                keyboardType='default'
                autoCapitalize='sentences'
                autoCorrect={true}
                enablesReturnKeyAutomatically={true}
                onFocus={this.onFocus}
                onChangeText={this.onChangeLegalDetailsText}
                onSubmitEditing={this.onSubmitLicenseNumber}
                returnKeyType='next'
                value={this.state.licenseNumber}
              />

              { (this.state.licenseExpiryDate != 'Select license expiry date') &&
                <Text style={CustomStyles.labelText}>Expiry date</Text>
              }
              <TouchableOpacity
                onPress={this.showExpiryDateTimePicker}
                style={CustomStyles.birhDateBtn}
              >
                  <Text style={{fontSize: txtInputFontSize, fontFamily: Constant.KThemeFontRegular, color: (this.state.licenseExpiryDate == 'Select license expiry date') ? 'gray' : 'black'}}>{this.state.licenseExpiryDate}</Text>
              </TouchableOpacity>

              { (this.state.licensePlate != '') &&
                <Text style={CustomStyles.labelText}>Liecense plate</Text>
              }
              <TextInput
                style={CustomStyles.textInput}
                ref={this.licensePlateRef}
                placeholder = "Enter license plate" 
                keyboardType='default'
                autoCapitalize='sentences'
                autoCorrect={true}
                enablesReturnKeyAutomatically={true}
                onFocus={this.onFocus}
                onChangeText={this.onChangeLegalDetailsText}
                onSubmitEditing={this.onSubmitLicensePlate}
                returnKeyType='next'
                value={this.state.licensePlate}
              />

              { (this.state.insuranceNumber != '') &&
                <Text style={CustomStyles.labelText}>Insurance number</Text>
              }
              <TextInput
                style={CustomStyles.textInput}
                ref={this.insuranceNumberRef}
                placeholder = "Enter insurance number" 
                keyboardType='default'
                autoCapitalize='sentences'
                autoCorrect={true}
                enablesReturnKeyAutomatically={true}
                onFocus={this.onFocus}
                onChangeText={this.onChangeLegalDetailsText}
                onSubmitEditing={this.onSubmitInsuranceNumber}
                returnKeyType='done'
                value={this.state.insuranceNumber}
              />

              { (this.state.ssnNumber != '') &&
                <Text style={CustomStyles.labelText}>SSN number</Text>
              }
              <TextInput
                style={CustomStyles.textInput}
                ref={this.ssnNumberRef}
                placeholder = "Enter SSN number" 
                keyboardType='number-pad'
                autoCapitalize='sentences'
                autoCorrect={true}
                enablesReturnKeyAutomatically={true}
                onFocus={this.onFocus}
                onChangeText={this.onChangeLegalDetailsText}
                onSubmitEditing={this.onSubmitSnnNumber}
                returnKeyType='next'
                value={this.state.ssnNumber}
                maxLength={4}
              />
            </View>
        )
    }

    renderLegalDocumentView() {
      return (
        <View>
          <Text style={[CustomStyles.otherText, {marginHorizontal: 10, marginBottom: -20}]}>Vehicle Registration</Text>
          <TouchableOpacity onPress={this.selectPhotoTapped.bind(this, 'registrationImage')}>
            <View
              style={[CustomStyles.legalDoc, CustomStyles.legalDocContainer, {marginBottom: 0}]}>
              {this.state.profileSource === null ? (
                <Text>Select a Photo</Text>
              ) : (
                <Image style={CustomStyles.legalDoc} source={this.state.registrationImage} />
              )}
            </View>
          </TouchableOpacity>

          <Text style={[CustomStyles.otherText, {marginHorizontal: 20, marginBottom: -20, marginTop: 20}]}>License Front Image</Text>
          <TouchableOpacity onPress={this.selectPhotoTapped.bind(this, 'frontImage')}>
            <View
              style={[CustomStyles.legalDoc, CustomStyles.legalDocContainer, {marginBottom: 0}]}>
              {this.state.profileSource === null ? (
                <Text>Select a Photo</Text>
              ) : (
                <Image style={CustomStyles.legalDoc} source={this.state.frontImage} />
              )}
            </View>
          </TouchableOpacity>

          <Text style={[CustomStyles.otherText, {marginHorizontal: 20, marginBottom: -20, marginTop: 20}]}>License Back Image</Text>
          <TouchableOpacity onPress={this.selectPhotoTapped.bind(this, 'backImage')}>
            <View
              style={[CustomStyles.legalDoc, CustomStyles.legalDocContainer, {marginBottom: 0}]}>
              {this.state.profileSource === null ? (
                <Text>Select a Photo</Text>
              ) : (
                <Image style={CustomStyles.legalDoc} source={this.state.backImage} />
              )}
            </View>
          </TouchableOpacity>

          <Text style={[CustomStyles.otherText, {marginHorizontal: 20, marginBottom: -20, marginTop: 20}]}>License ADS Image</Text>
          <TouchableOpacity onPress={this.selectPhotoTapped.bind(this, 'adsImage')}>
            <View
              style={[CustomStyles.legalDoc, CustomStyles.legalDocContainer, {marginBottom: 0}]}>
              {this.state.profileSource === null ? (
                <Text>Select a Photo</Text>
              ) : (
                <Image style={CustomStyles.legalDoc} source={this.state.adsImage} />
              )}
            </View>
          </TouchableOpacity>

          

          <TouchableOpacity
              style={[CustomStyles.themeButton, {marginLeft:20, marginRight: 20, marginBottom: 50, marginTop: 30}]}
              onPress={this.onValidateLegalDetails.bind(this)}
           >
              <Text style={CustomStyles.themeButtonText}> Update </Text>
          </TouchableOpacity>

          
        </View>
      )
    }

    render() {
        let { errors = {}, secureTextEntry, ...data } = this.state;
        let { flag } = this.state
        let { phoneCode } = this.state
        const countryData = cdata

        return(

            <View style={[CustomStyles.container]}>
                <View
                style={{backgroundColor: "#fff", justifyContent: 'center', alignItems: 'center'}}>

                <ProgressLoader
                  visible={this.state.visible}
                  isModal={true} isHUD={true}
                  hudColor={"#fff"}
                  height={200}
                  width={200}
                  color={"#8edf01"} />
                </View>

                <View style={[CustomStyles.centerContainer, {flex:1, marginTop: 0}]}>
                  <View style={{flexDirection: 'row', height: 45, position: 'absolute', top: 0, left: 0, right: 0}}>
                        <View style={CustomStyles.flexColunm, {flex: 1, height: 45, backgroundColor: '#fff'}}>
                            <TouchableOpacity
                              style={{justifyContent:'center', alignItems: 'center', height: 45}}
                              onPress={this.onClickPersonalDetails.bind(this)}
                            >
                                <Text style={{color:'#000', textAlign: 'center', fontSize: txtInputFontSize, fontFamily: Constant.KThemeFontRegular}}>Personal Details</Text>
                                { this.state.isVisiblePeronalDetails && 
                                    <View style={{backgroundColor: Constant.KAppColor, height: 5, position: 'absolute', bottom:0, flex: 1, width:(Dimensions.get('window').width)/3}}>
                                    </View>
                                }
                            </TouchableOpacity>
                        </View>

                        <View style={CustomStyles.flexColunm, {flex: 1, height: 45, backgroundColor: '#fff'}}>
                            <TouchableOpacity
                              style={{justifyContent:'center', alignItems: 'center', height: 45}}
                              onPress={this.onClickVehicleDetails.bind(this)}
                            >
                                <Text style={{color:'#000', textAlign: 'center', fontSize: txtInputFontSize, fontFamily: Constant.KThemeFontRegular}}>Vehicle Details</Text>
                                { this.state.isVisibleVehicleDetails && 
                                    <View style={{backgroundColor: Constant.KAppColor, height: 5, position: 'absolute', bottom:0, flex: 1, width:(Dimensions.get('window').width)/3}}>
                                    </View>
                                }
                            </TouchableOpacity>
                        </View>

                        <View style={CustomStyles.flexColunm, {flex: 1, height: 45, backgroundColor: '#fff'}}>
                            <TouchableOpacity
                              style={{justifyContent:'center', alignItems: 'center', height: 45}}
                              onPress={this.onClickLegalDetails.bind(this)}
                            >
                                <Text style={{color:'#000', textAlign: 'center', fontSize: txtInputFontSize, fontFamily: Constant.KThemeFontRegular}}>Legal Details</Text>
                                { this.state.isVisibleLegalDetails && 
                                    <View style={{backgroundColor: Constant.KAppColor, height: 5, position: 'absolute', bottom:0, flex: 1, width:(Dimensions.get('window').width)/3}}>
                                    </View>
                                }
                            </TouchableOpacity>
                        </View>
                  </View>

                  <KeyboardAwareScrollView style = {[CustomStyles.flexColunm, {flex:0.8, marginTop: 45}]}>

                  <View >

                    { this.state.isVisiblePeronalDetails && 
                      <View style={{flexDirection: 'row'}}>
                        <ScrollView >
                        {this.renderPhotoView()}
                        {this.renderPersonalDetails()}
                        </ScrollView>
                      </View>
                    }

                    { this.state.isVisibleVehicleDetails && 
                      <View style={{flexDirection: 'row'}}>
                        <ScrollView>
                        {this.renderVehicleDetails()}
                        </ScrollView>
                      </View>
                    }

                    { this.state.isVisibleLegalDetails && 
                      <View style={{flexDirection: 'row'}}>
                        <ScrollView>
                        {this.renderLegalDetails()}
                        {this.renderLegalDocumentView()}
                        </ScrollView>
                      </View>
                    }

                    <DateTimePicker
                      isVisible={this.state.isDateTimePickerVisible}
                      onConfirm={this.handleDatePicked}
                      onCancel={this.hideDateTimePicker}
                      mode={'date'}
                      maximumDate={this.dateBefore18()}
                      // minimumDate={}
                    />

                    <DateTimePicker
                      isVisible={this.state.isLicenseExpiryDatePickerVisible}
                      onConfirm={this.handleExpiryDatePicked}
                      onCancel={this.hideExpiryDateTimePicker}
                      mode={'date'}
                      //maximumDate={this.dateBefore18()}
                      minimumDate={new Date()}
                    />

                    <Modal
                            animationType="slide" 
                            transparent={false}
                            visible={this.state.modalVisible}>
                            <View style={{ flex: 1, backgroundColor: Constant.KAppColor1 }}>
                              
                                <Text style={{fontSize: 18, color: 'white', fontWeight: 'bold', marginTop: 40, marginBottom: 20, alignSelf: 'center', textAlign: 'center'}}>
                                  Select country code
                                </Text>
                              <View style={{ flex: 10}}>
                                <FlatList
                                  data={countryData}
                                  keyExtractor={(item, index) => index.toString()}
                                  renderItem={
                                    ({ item }) =>
                                      <TouchableWithoutFeedback 
                                        onPress={() => this.getCountry(item.name)}>
                                        <View 
                                          style={
                                            [
                                              CustomStyles.countryStyle, 
                                              {
                                                flexDirection: 'row', 
                                                alignItems: 'center',
                                                justifyContent: 'space-between'
                                              }
                                            ]
                                          }>
                                          
                                          <Text style={{fontSize: 15, color: 'black'}}>
                                            {item.name} ({item.dial_code})
                                          </Text>

                                          <Text style={{fontSize: 32}}>
                                            {item.flag}
                                          </Text>
                                        </View>
                                      </TouchableWithoutFeedback>
                                  }
                                />
                              </View>
                              <TouchableOpacity
                                onPress={() => this.hideModal()} 
                                style={CustomStyles.closeButtonStyle}>
                                <Text style={{backgroundColor: Constant.KAppColor1, fontSize: 20, color: 'white', fontWeight: 'bold', paddingLeft: 40, paddingRight:40, paddingTop: 8, paddingBottom:8, borderRadius:5}}>
                                  Close
                                </Text>
                              </TouchableOpacity>
                            </View>
                    </Modal>

                  </View>
                  </KeyboardAwareScrollView>
                </View>
        </View>
        )
    }

    dateBefore18() {
        const date = new Date();
        const year = date.getFullYear();
        const month = date.getMonth();
        const day = date.getDate();
        return new Date(year - 18, month, day)
    }
}

export default EditProfile;
