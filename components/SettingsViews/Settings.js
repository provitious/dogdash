import React from 'react';
import { StyleSheet, Dimensions, YellowBox, Text, View, Button, TextInput, Image, ScrollView, TouchableOpacity, Alert, SafeAreaView, PixelRatio } from 'react-native';
import { NavigationBar } from 'navigationbar-react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

import Constant from '../CommonClasses/Constant';
import CommonUtilsObj from '../CommonClasses/CommonUtils';
import ChangePassword from './ChangePassword';
import CustomStyles from '../CommonClasses/CustomStyles';
import Message from '../CommonClasses/Message';
import APIManagerObj from '../CommonClasses/APIManager';

export default class Settings extends React.Component {

  constructor(props) {
      super(props);
      this.onClickEditProfile=this.onClickEditProfile.bind(this);
      this.onClickChangePassword=this.onClickChangePassword.bind(this);
      this.state = {
        sourceUrl: { uri: Constant.KDriverImagePathURL + CommonUtilsObj.userDetails.image },
        // sourceUrl: (CommonUtilsObj.userDetails.image =! '0' || CommonUtilsObj.userDetails.image != '') ? { uri: Constant.KDriverImagePathURL + CommonUtilsObj.userDetails.image } : require('../../images/userPlaceholder.png'),

        name: CommonUtilsObj.userDetails.name,
        email: CommonUtilsObj.userDetails.email,
        loaded: false,
      }

      console.log(JSON.stringify(CommonUtilsObj.userDetails))

      console.log(Constant.KDriverImagePathURL + CommonUtilsObj.userDetails.image);
      CommonUtilsObj.navigationProps = this.props;
  }

  componentDidMount() {
    
    const {navigation} = this.props;
    navigation.addListener ('willFocus', () =>{

      this.setState({
          sourceUrl: { uri: Constant.KDriverImagePathURL + CommonUtilsObj.userDetails.image },
        name: CommonUtilsObj.userDetails.name,
        email: CommonUtilsObj.userDetails.email,
      });
    });
  }

  onClickChangePassword() {
    this.props.navigation.navigate('ChangePasswordScreen');
  }

  onClickEditProfile() {
    this.props.navigation.navigate('EditProfileScreen');
  }

  render() {
        
        return(

          <View style={CustomStyles.container1}>
              <Image style={CustomStyles.avatarReview} source={this.state.sourceUrl} />
              <Text style={[CustomStyles.mediumFont, {marginTop: 10}]}>{this.state.name}</Text>
              <Text style={[CustomStyles.smallFont, {marginTop: 5}]}>{this.state.email}</Text>

              <View style={{height:0.5, backgroundColor:'gray', margin: 20,}}>
              </View>

              <TouchableOpacity
                onPress={this.onClickEditProfile.bind(this)}
              >
              <View style={CustomStyles.flexRow, {height: 50}}>
                  <View style={{ flex: 0.5, marginLeft: 30, flexDirection: 'row'}}>
                      <Icon name="user" size={25} color="#8edf01" />
                      <Text style={CustomStyles.btnFont}>Edit Profile</Text>
                  </View>
              </View>
              </TouchableOpacity>

              <View style={{height:1, backgroundColor:'gray', margin: 20, marginTop: -5}}>
              </View>

              <TouchableOpacity
                onPress={this.onClickChangePassword.bind(this)}
              >
              <View style={CustomStyles.flexRow, {height: 50}}>
                  <View style={{ flex: 0.5, marginLeft: 30, flexDirection: 'row'}}>
                      <Icon name="lock" size={25} color="#8edf01" />
                      <Text style={CustomStyles.btnFont}>Change Password</Text>
                  </View>
              </View>
              </TouchableOpacity>

              <View style={{height:1, backgroundColor:'gray', margin: 20, marginTop: -5}}>
              </View>
          </View>
        )
    }
}







