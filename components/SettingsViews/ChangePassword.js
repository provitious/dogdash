import React from 'react';
import { StyleSheet, Dimensions, YellowBox, Text, View, Button, TextInput, Image, ScrollView, TouchableOpacity, Alert, SafeAreaView, PixelRatio } from 'react-native';
import {KeyboardAvoidingView, Keyboard, TouchableWithoutFeedback} from 'react-native';import { NavigationBar } from 'navigationbar-react-native';
import ProgressLoader from 'rn-progress-loader';
import Toast from 'react-native-simple-toast';
import NetInfo from "@react-native-community/netinfo";

import Constant from '../CommonClasses/Constant';
import CommonUtilsObj from '../CommonClasses/CommonUtils';
import CustomStyles from '../CommonClasses/CustomStyles';
import Message from '../CommonClasses/Message';
import APIManagerObj from '../CommonClasses/APIManager';

export default class ChangePassword extends React.Component {

  constructor(props) {
      super(props);
      
      this.onChangeText = this.onChangeText.bind(this);
      this.onFocus = this.onFocus.bind(this);
      this.oldPasswordRef = this.updateRef.bind(this, 'oldPassword');
      this.newPasswordRef = this.updateRef.bind(this, 'newPassword');
      this.confirmPasswordRef = this.updateRef.bind(this, 'confirmPassword');
      this.onValidate = this.onValidate.bind(this);

      this.onSubmitOldPassword = this.onSubmitOldPassword.bind(this);
      this.onSubmitNewPassword = this.onSubmitNewPassword.bind(this);
      this.onSubmitConfirmPassword = this.onSubmitConfirmPassword.bind(this);

      this.state = {
        loaded: false,
        oldPassword: '',
        newPassword: '',
        confirmPassword: '',
        hideOldPassword: true,
        hideNewPassword: true,
        hideConfirmPassword: true,
        connection_Status : "",
        btnBottom: 20,
      }

      YellowBox.ignoreWarnings([
          'Warning: componentWillMount is deprecated',
          'Warning: componentWillReceiveProps is deprecated',
          'Warning: componentWillUpdate is deprecated',
          'Warning: componentWillUpdate has been renamed, and is not recommended for use',
          'Warning: componentWillReceiveProps has been renamed, and is not recommended for use',
        ]);
  }

  componentDidMount() {
      NetInfo.isConnected.addEventListener(
          'connectionChange',
          this._handleConnectivityChange

      );
     
      NetInfo.isConnected.fetch().done((isConnected) => {

        if(isConnected == true)
        {
          this.setState({connection_Status : "Online"})
        }
        else
        {
          this.setState({connection_Status : "Offline"})
        }

      });

      this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
      this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
 
  }

  _handleConnectivityChange = (isConnected) => {

      if(isConnected == true)
        {
          this.setState({connection_Status : "Online"})
        }
        else
        {
          this.setState({connection_Status : "Offline"})
        }
    };

  componentWillUnmount() {

    NetInfo.isConnected.removeEventListener(
        'connectionChange',
        this._handleConnectivityChange
 
    );
 
  }

  _keyboardDidShow = () => {
      this.setState({btnBottom : -100})
  }

  _keyboardDidHide = () => {
      this.setState({btnBottom : 30})
  }

  manageOldPasswordVisibility = () =>
  {
    this.setState({ hideOldPassword: !this.state.hideOldPassword });
  }

  manageNewPasswordVisibility = () =>
  {
    this.setState({ hideNewPassword: !this.state.hideNewPassword });
  }

  manageConfirmPasswordVisibility = () =>
  { 
    this.setState({ hideConfirmPassword: !this.state.hideConfirmPassword });
  }

  updateRef(name, ref) {
    this[name] = ref;
  }

  onFocus() {
    let { errors = {} } = this.state;

    for (let name in errors) {
      let ref = this[name];

      if (ref && ref.isFocused()) {
        delete errors[name];
      }
    }

    this.setState({ errors });
  }

  onChangeText(text) {
    ['oldPassword', 'newPassword', 'confirmPassword']
      .map((name) => ({ name, ref: this[name] }))
      .forEach(({ name, ref }) => {
        if (ref.isFocused()) {
          this.setState({ [name]: text });
        }
      });
  }

  onSubmitOldPassword(text) {
    this.newPassword.focus();
    let passwordRegex  = /^(?=.*\d)(?=.*[aA-zZ]).{6,20}$/;
    let passwordVal = text.nativeEvent.text

    if (!passwordVal) {
      Toast.showWithGravity(Message.KOldPasswordEmpty, Toast.LONG, Toast.TOP)
    } 
    // else {
    //   if (passwordVal.length < 6) {
    //     Toast.showWithGravity(Message.KOldPasswordInvalid, Toast.LONG, Toast.TOP)
    //   }
    //   else if (passwordRegex.test(passwordVal) == false) {
    //     Toast.showWithGravity(Message.KOldPasswordLengthInvalid, Toast.LONG, Toast.TOP)
    //   } 
    // }

  }

  onSubmitNewPassword(text) {
    this.confirmPassword.focus();
    let passwordRegex  = /^(?=.*\d)(?=.*[aA-zZ]).{6,20}$/;
    let passwordVal = text.nativeEvent.text

    if (!passwordVal) {
      Toast.showWithGravity(Message.KNewPasswordEmpty, Toast.LONG, Toast.TOP)
    } 
    else {
      if (passwordVal.length < 6) {
        Toast.showWithGravity(Message.KNewPasswordLengthInvalid, Toast.LONG, Toast.TOP)
      }
      else if (passwordRegex.test(passwordVal) == false) {
        Toast.showWithGravity(Message.KNewPasswordInvalid, Toast.LONG, Toast.TOP)
      } 
    }

  }

  onSubmitConfirmPassword(text) {
    this.confirmPassword.blur();
    let passwordRegex  = /^(?=.*\d)(?=.*[aA-zZ]).{6,20}$/;
    let passwordVal = text.nativeEvent.text

    if (!passwordVal) {
      Toast.showWithGravity(Message.KConfirmPasswordEmpty, Toast.LONG, Toast.TOP)
    } 
    else if (this.state.newPassword != this.state.confirmPassword){
        Toast.showWithGravity(Message.KNewConfirmPasswordNotMatched, Toast.LONG, Toast.TOP)
    }

  }

  onValidate() {
    this.oldPassword.blur();
    this.newPassword.blur();
    this.confirmPassword.blur();

    var flag = true;
    var errorMsg = [];

    let passwordRegex  = /^(?=.*\d)(?=.*[aA-zZ]).{6,20}$/;
    let oldPWVal = this.state.oldPassword

    if (!oldPWVal) {
      errorMsg.push(Message.KOldPasswordEmpty)
      flag = false;
    } 
    // else {
    //   if (oldPWVal.length < 6) {
    //     errorMsg.push('Old password should atleast 6 characters long')
    //     flag = false;
    //   }
    //   else if (passwordRegex.test(oldPWVal) == false) {
    //     errorMsg.push('Old password should contain atleast one number and one letter')
    //     flag = false;
    //   } 
    // }

    let newPWVal = this.state.newPassword

    if (!newPWVal) {
      errorMsg.push(Message.KNewPasswordEmpty)
      flag = false;
    } 
    else {
      if (newPWVal.length < 6) {
        errorMsg.push(Message.KNewPasswordLengthInvalid)
        flag = false;
      }
      else if (passwordRegex.test(newPWVal) == false) {
        errorMsg.push(Message.KNewPasswordInvalid)
        flag = false;
      } 
    }

    let confirmPWVal = this.state.confirmPassword

    if (!confirmPWVal) {
      errorMsg.push(Message.KConfirmPasswordEmpty)
      flag = false;
    } 
    else if (this.state.newPassword != this.state.confirmPassword){
        errorMsg.push(Message.KNewConfirmPasswordNotMatched)
        flag = false;
    }

    if (flag) {
      if (this.state.connection_Status == "Online") {
        this.onSubmitPassword();
      }
      else {
        Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
      }
    }
    else {
      Toast.showWithGravity(errorMsg.toString(), Toast.LONG, Toast.TOP)
    }
  }

  async onSubmitPassword() {
    this.oldPassword.blur();
    this.newPassword.blur();
    this.confirmPassword.blur();

    if (this.state.connection_Status == "Online") {

      this.setState({ visible: true });
      
      var data = new FormData();
      data.append('did', CommonUtilsObj.userDetails.id);
      data.append('current_password', this.state.oldPassword);
      data.append('password', this.state.newPassword);

      let responseData = await APIManagerObj.callApiWithData(Constant.KChangePasswordURL, data);
      console.log('json response ....' + JSON.stringify(responseData));

      if (responseData.status == "success") {
        this.setState({
          visible: false
        });
        CommonUtilsObj.setLoginState(Constant.KLogout);
        setTimeout(() => {
          Toast.showWithGravity(responseData.message, Toast.LONG, Toast.TOP)
        }, 200);         
      }
      else {
        this.setState({
          visible: false,
        }); 
        setTimeout(() => {
          if(typeof(responseData.message) === 'string')
          {
            Toast.showWithGravity(responseData.message, Toast.LONG, Toast.TOP)
          }
          else if(typeof(responseData.error) === 'string')
          {
              Toast.showWithGravity(responseData.error, Toast.LONG, Toast.TOP)
          }
          else {
              if (responseData.error) {
                Toast.showWithGravity(responseData.error.toString(), Toast.LONG, Toast.TOP)
              }
              else {
                Toast.showWithGravity(Message.KAPIError, Toast.LONG, Toast.TOP)
              }
          }
        }, 200); 
      }
    }
    else {
      this.setState({lastApiTag : 1})
      Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
    }
  }

  render() {
    
    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>

            <View style={CustomStyles.container}>
                <View
                style={{backgroundColor: "#fff", justifyContent: 'center', alignItems: 'center'}}>

                <ProgressLoader
                  visible={this.state.visible}
                  isModal={true} isHUD={true}
                  hudColor={"#fff"}
                  height={200}
                  width={200}
                  color={"#8edf01"} />
                </View>

                <View style={CustomStyles.centerContainer}>
                <KeyboardAvoidingView style={CustomStyles.flexColunm} behavior="padding" enabled>

                  <View style={CustomStyles.flexColunm}>
                    { (this.state.oldPassword != '') &&
                      <Text style={CustomStyles.labelText}>Old password</Text>
                    }
                    <View style = { CustomStyles.textBoxBtnHolder }>
                      <TextInput 
                        secureTextEntry = { this.state.hideOldPassword } 
                        style = { CustomStyles.textInput }
                        ref={this.oldPasswordRef}
                        placeholder = "Enter old password" 
                        autoCapitalize='none'
                        autoCorrect={false}
                        enablesReturnKeyAutomatically={true}
                        onFocus={this.onFocus}
                        onChangeText={this.onChangeText}
                        onSubmitEditing={this.onSubmitOldPassword}
                        returnKeyType='next'
                        maxLength={20}
                        characterRestriction={20}
                      />
                      <TouchableOpacity 
                        activeOpacity = { 0.8 } 
                        style = { CustomStyles.visibilityBtn } 
                        onPress = { this.manageOldPasswordVisibility }
                        >
                        <Image 
                        source = { ( this.state.hideOldPassword ) ? require('../../images/eyeHide.png') : require('../../images/eye.png') } 
                        style = { CustomStyles.btnImage } 
                        />
                      </TouchableOpacity>
                    </View>

                    { (this.state.newPassword != '') &&
                      <Text style={CustomStyles.labelText}>New password</Text>
                    }
                    <View style = { CustomStyles.textBoxBtnHolder }>
                      <TextInput 
                        secureTextEntry = { this.state.hideNewPassword } 
                        style = { CustomStyles.textInput }
                        ref={this.newPasswordRef}
                        placeholder = "Enter new password" 
                        autoCapitalize='none'
                        autoCorrect={false}
                        enablesReturnKeyAutomatically={true}
                        onFocus={this.onFocus}
                        onChangeText={this.onChangeText}
                        onSubmitEditing={this.onSubmitNewPassword}
                        returnKeyType='next'
                        maxLength={20}
                        characterRestriction={20}
                      />
                      <TouchableOpacity 
                        activeOpacity = { 0.8 } 
                        style = { CustomStyles.visibilityBtn } 
                        onPress = { this.manageNewPasswordVisibility }
                        >
                        <Image 
                        source = { ( this.state.hideNewPassword ) ? require('../../images/eyeHide.png') : require('../../images/eye.png') } 
                        style = { CustomStyles.btnImage } 
                        />
                      </TouchableOpacity>
                    </View>

                    { (this.state.confirmPassword != '') &&
                      <Text style={CustomStyles.labelText}>Confirm password</Text>
                    }
                    <View style = { CustomStyles.textBoxBtnHolder }>
                      <TextInput 
                        secureTextEntry = { this.state.hideConfirmPassword } 
                        style = { CustomStyles.textInput }
                        ref={this.confirmPasswordRef}
                        placeholder = "Enter confirm password" 
                        autoCapitalize='none'
                        autoCorrect={false}
                        enablesReturnKeyAutomatically={true}
                        onFocus={this.onFocus}
                        onChangeText={this.onChangeText}
                        onSubmitEditing={this.onSubmitConfirmPassword}
                        returnKeyType='done'
                        maxLength={20}
                        characterRestriction={20}
                      />
                      <TouchableOpacity 
                        activeOpacity = { 0.8 } 
                        style = { CustomStyles.visibilityBtn } 
                        onPress = { this.manageConfirmPasswordVisibility }
                        >
                        <Image 
                        source = { ( this.state.hideConfirmPassword ) ? require('../../images/eyeHide.png') : require('../../images/eye.png') } 
                        style = { CustomStyles.btnImage } 
                        />
                      </TouchableOpacity>
                    </View>

                    
                  </View>

                </KeyboardAvoidingView>
                </View>
                <TouchableOpacity
                  style={[CustomStyles.themeButtonAbsolute, {bottom: this.state.btnBottom}]}
                  onPress={this.onValidate.bind(this)}
                 >
                    <Text style={CustomStyles.themeButtonText}> Submit </Text>
                </TouchableOpacity>
            </View>
        </TouchableWithoutFeedback>
    );
  }
}








