import React from 'react';
import { StyleSheet, Dimensions, YellowBox, Text, View, Button, TextInput, Image, ScrollView, TouchableOpacity, Alert, SafeAreaView, PixelRatio } from 'react-native';
import {KeyboardAvoidingView, Keyboard, TouchableWithoutFeedback} from 'react-native';

import { NavigationBar } from 'navigationbar-react-native';
import { Dropdown } from 'react-native-material-dropdown';

import NetInfo from "@react-native-community/netinfo";
import ProgressLoader from 'rn-progress-loader';
import Toast from 'react-native-simple-toast';
import { Modal, FlatList, SectionList, ActivityIndicator } from 'react-native';
import DatePicker from 'react-native-datepicker'
import Moment from 'moment';

import Constant from '../CommonClasses/Constant';
import CommonUtilsObj from '../CommonClasses/CommonUtils';
import CustomStyles from '../CommonClasses/CustomStyles';
import Message from '../CommonClasses/Message';
import APIManagerObj from '../CommonClasses/APIManager';

const headerFontSize = (((Dimensions.get('window').width) * 11) / 100);
const headerSubFontSize = (((Dimensions.get('window').width) * 3.3) / 100);
const txtMediumSize = (((Dimensions.get('window').width) * 4.5) / 100);
const txtSmallSize = (((Dimensions.get('window').width) * 3.6) / 100);
const cellDateFontSize = (((Dimensions.get('window').width) * 4.2) / 100);
const cellHeaderFontSize = (((Dimensions.get('window').width) * 3.9) / 100);
const cellOtherFontSize = (((Dimensions.get('window').width) * 3.4) / 100);
const txtInputHeight = (((Dimensions.get('window').width) * 9.8) / 100);

const dateList = ["1", "2", "3"];

export default class Wallet extends React.Component {

  constructor(props) {
      super(props);

      this.onClickAllTransaction=this.onClickAllTransaction.bind(this);
      this.onClickDebitTransaction=this.onClickDebitTransaction.bind(this);
      this.onClickCreditTransaction=this.onClickCreditTransaction.bind(this);
      this.hideModal=this.hideModal.bind(this);
      this.showModal=this.showModal.bind(this);

      let newDate = new Date()
      let dateStr = newDate.getDate() + "-" + (newDate.getMonth() + 1) + "-" + newDate.getFullYear();

      this.state = {
        balance: 0.0,
        visible: false,
        connection_Status : "",
        allTransactionList: [],
        allOffset: 0,
        isSelectedAll: true,
        allLoading: false,
        allLoadMoreCompleted: false,
        allPullToRefresh: false,
        allHasScrolled: false,
        allStartDate: '',
        allEndDate: '',
        debitTransactionList: [],
        debitOffset: 0,
        isSelectedDebit: false,
        debitLoading: false,
        debitLoadMoreCompleted: false,
        debitPullToRefresh: false,
        debitHasScrolled: false,
        debitStartDate: '',
        debitEndDate: '',
        creditTransactionList: [],
        creditOffset: 0,
        isSelectedCredit: false,
        creditLoading: false,
        creditLoadMoreCompleted: false,
        creditPullToRefresh: false,
        creditHasScrolled: false,
        creditStartDate: '',
        creditEndDate: '',
        modalVisible: false,
        modalMarginPer: 45,
        refresh: false,
        selectedIndex: 0,
        filterList: ['Last 30 days', 'Last 60 days', 'Last 90 days', 'Custom'],
        startDate: dateStr,
        endDate: dateStr,
        isUpdated: false
      }

      CommonUtilsObj.navigationProps = this.props;
      
  }

  componentDidMount() {
    
    Moment.locale('en');
    const {navigation} = this.props;
    navigation.addListener ('willFocus', () =>{

      if (this.state.isUpdated) {
          if (this.state.connection_Status == "Online") {
            this.onFetchWalletBalance();
            this.onFetchAllTransactionList();
            this.onFetchDebitTransactionList();
            this.onFetchCreditTransactionList();
          }
          else {
            Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
          }
      }
      this.setState({
          isUpdated: false,
      });
    });

    NetInfo.isConnected.addEventListener(
        'connectionChange',
        this._handleConnectivityChange

    );
   
    NetInfo.isConnected.fetch().done((isConnected) => {

      if(isConnected == true)
      {
        this.setState({connection_Status : "Online"})
      }
      else
      {
        this.setState({connection_Status : "Offline"})
      }

    if (this.state.connection_Status == "Online") {
      this.onFetchWalletBalance();
      this.onFetchAllTransactionList();
      this.onFetchDebitTransactionList();
      this.onFetchCreditTransactionList();
    }
    else {
      Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
    }
    });
  }

  componentWillUnmount() {
    NetInfo.isConnected.removeEventListener(
        'connectionChange',
        this._handleConnectivityChange
 
    );
  }

  componentWillUnmount() {
    NetInfo.isConnected.removeEventListener(
        'connectionChange',
        this._handleConnectivityChange
 
    );
  }

  _handleConnectivityChange = (isConnected) => {
    if(isConnected == true)
      {
        this.setState({connection_Status : "Online"})
      }
      else
      {
        this.setState({connection_Status : "Offline"})
      }
  };

  async onFetchWalletBalance() {
    if (this.state.connection_Status == "Online") {

      this.setState({ visible: true });
      var data = new FormData();
      data.append('driver_id', CommonUtilsObj.userDetails.id);

      let responseData = await APIManagerObj.callApiWithData(Constant.KGetWalletBalanceURL, data);
      console.log('json response ....' + JSON.stringify(responseData));

      if (responseData.status == "success") {
        this.setState({
          visible: false,
          balance: responseData.current_balance
        });        
      }
      else {
        this.setState({
          visible: false,
        }); 
        setTimeout(() => {
          if(typeof(responseData.message) === 'string')
          {
              Toast.showWithGravity(responseData.message, Toast.LONG, Toast.TOP)
          }
          else if(typeof(responseData.error) === 'string')
          {
              Toast.showWithGravity(responseData.error, Toast.LONG, Toast.TOP)
          }
          else {
              if (responseData.error) {
                Toast.showWithGravity(responseData.error.toString(), Toast.LONG, Toast.TOP)
              }
              else {
                Toast.showWithGravity(Message.KAPIError, Toast.LONG, Toast.TOP)
              }
          }
        }, 200); 
      }
    }
    else {
      this.setState({lastApiTag : 1})
      Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
    }
  }

  renderAllTransactionListItem = ({ item, index }) => (
    <WalletListCell
      item={item}
      index={index}
      transactionList={this.state.allTransactionList}
    />
  );

  renderDebitTransactionListItem = ({ item, index }) => (
    <WalletListCell
      item={item}
      index={index}
      transactionList={this.state.debitTransactionList}
    />
  );

  renderCreditTransactionListItem = ({ item, index }) => (
    <WalletListCell
      item={item}
      index={index}
      transactionList={this.state.creditTransactionList}
    />
  );

  onClickAllTransaction() {
    this.setState({
      isSelectedAll: true,
      isSelectedDebit: false,
      isSelectedCredit: false,
    });
  }

  onClickDebitTransaction() {
    this.setState({
      isSelectedAll: false,
      isSelectedDebit: true,
      isSelectedCredit: false,
    });
  }

  onClickCreditTransaction() {
    this.setState({
      isSelectedAll: false,
      isSelectedDebit: false,
      isSelectedCredit: true,
    });
  }

  async onFetchAllTransactionList() {
    if (this.state.connection_Status == "Online") {

      this.setState({ visible: true });
      var data = new FormData();
      data.append('driver_id', CommonUtilsObj.userDetails.id);
      data.append('off', this.state.allOffset);
      data.append('start_date', this.state.allStartDate);
      data.append('end_date', this.state.allEndDate);

      let responseData = await APIManagerObj.callApiWithData(Constant.KGetAllTransactionListURL, data);
      console.log('json response ....' + JSON.stringify(responseData));

      if (responseData.status == "success") {
        var list1=this.state.allTransactionList
        var list=responseData.transactions;
        list1.push(...list)

        var dateArr = [];

        const items = list1.map((item) => {  
            item.displayDate = false
          
            // console.log(item.created_date)
            // console.log(Moment(item.created_date).format('DD-MM-YYYY hh:mm a'))
            let date = new Date(item.created_date.replace(" ", "T"))
                            

            let dateStr =  Moment(item.created_date).format('ddd, MMM DD YYYY'); //date.toDateString(); Thu Mar 12 2020
            item.time = Moment(item.created_date).format('hh:mm A');//this.getTransactionTime(date);
      
            if (item.description == null) {
                  item.description = 'Initial entry';
            }

            if (item.comment == null) {
                  item.comment = 'Initial entry';
            }

            if (item.payment_method == null) {
                  item.payment_method = '';
            }

            if (dateArr.indexOf(dateStr) > -1) {
            }
            else {
                dateArr.push(dateStr);
                item.displayDate = true
                item.date = dateStr
            }

            if (item.transaction_type === '0') {
              item.booingID = 'Order Id : ' + item.id;
              item.type = 'credit';
            }
            else if (item.transaction_type === '1' || item.transation_type === '9') {
              item.booingID = 'TXN Id : ' + item.transaction_id;
              item.type = 'debit';
            }
            else if (item.transaction_type === '2' || item.transation_type === '5' || item.transation_type == '6') {
              if (item.booking_id == null) {
                  item.booingID = 'Booking Id : ' + '0';
              }
              else {
                  item.booingID = 'Booking Id : ' + item.booking_id;
              }
              
              item.type = 'credit';
            }
        });
        dateArr = [];

        this.setState({
          visible: false,
          allOffset: responseData.offset,
          allTransactionList: list1,
          allLoading: false,
          allLoadMoreCompleted: true,
          allPullToRefresh: false,
        });         
      }
      else {
        this.setState({
            visible: false,
            allLoading: false,
            allLoadMoreCompleted: true,
            allPullToRefresh: false,
        });
        setTimeout(() => {
          if(typeof(responseData.message) === 'string')
          {
              Toast.showWithGravity(responseData.message, Toast.LONG, Toast.TOP)
          }
          else if(typeof(responseData.error) === 'string')
          {
              Toast.showWithGravity(responseData.error, Toast.LONG, Toast.TOP)
          }
          else {
              if (responseData.error) {
                Toast.showWithGravity(responseData.error.toString(), Toast.LONG, Toast.TOP)
              }
              else {
                Toast.showWithGravity(Message.KAPIError, Toast.LONG, Toast.TOP)
              }
          }
        }, 200); 
      }
    }
    else {
      this.setState({lastApiTag : 1})
      Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
    }
  }

  onAllTransactionRefresh() {

    if (this.state.connection_Status == "Online") {
      this.setState({
        allPullToRefresh:true,
        allTransactionList: [],
        allOffset: 0,
        allLoading: false,
        allLoadMoreCompleted: false,
      });
      setTimeout(() => {
          this.onFetchAllTransactionList();
      }, 0);
      
    }
    else {
      this.setState({ allPullToRefresh: false });
      Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
    }
  }

  handleAllTransactionLoadMore = () => {
    if (!this.state.allLoading && !this.state.allLoadMoreCompleted){
      
      this.onFetchAllTransactionList(); // method for API call 
    }
  };

  async onFetchDebitTransactionList() {
    if (this.state.connection_Status == "Online") {

      this.setState({ visible: true });
      var data = new FormData();
      data.append('driver_id', CommonUtilsObj.userDetails.id);
      data.append('off', this.state.debitOffset);
      data.append('start_date', this.state.debitStartDate);
      data.append('end_date', this.state.debitEndDate);

      let responseData = await APIManagerObj.callApiWithData(Constant.KGetDebitTransactionListURL, data);
      console.log('json response ....' + JSON.stringify(responseData));

      if (responseData.status == "success") {
        var list1=this.state.debitTransactionList
        var list=responseData.transactions;
        list1.push(...list)

        var dateArr = [];

        const items = list1.map((item) => {  
            item.displayDate = false
            
            let date = new Date(item.created_date.replace(" ", "T"))
            let dateStr = Moment(item.created_date).format('ddd, MMM DD YYYY'); //date.toDateString();
            item.time = Moment(item.created_date).format('hh:mm A');
            
            if (item.description == null) {
                  item.description = 'Initial entry';
            }
Moment(item.created_date).format('hh:mm A');
            if (item.comment == null) {
                  item.comment = 'Initial entry';
            }

            if (item.payment_method == null) {
                  item.payment_method = '';
            }

            if (dateArr.indexOf(dateStr) > -1) {
            }
            else {
                dateArr.push(dateStr);
                item.displayDate = true
                item.date = dateStr
            }

            if (item.transaction_type === '0') {
              item.booingID = 'Order Id : ' + item.id;
              item.type = 'credit';
            }
            else if (item.transaction_type === '1' || item.transation_type === '9') {
              item.booingID = 'TXN Id : ' + item.transaction_id;
              item.type = 'debit';
            }
            else if (item.transaction_type === '2' || item.transation_type === '5' || item.transation_type == '6') {
              if (item.booking_id == null) {
                  item.booingID = 'Booking Id : ' + '0';
              }
              else {
                  item.booingID = 'Booking Id : ' + item.booking_id;
              }
              item.type = 'credit';
            } 
        });
        dateArr = [];

        this.setState({
          visible: false,
          debitOffset: responseData.offset,
          debitTransactionList: list1,
          debitLoading: false,
          debitLoadMoreCompleted: true,
          debitPullToRefresh: false,
        });         
      }
      else {
        this.setState({
            visible: false,
            debitLoading: false,
            debitLoadMoreCompleted: true,
            debitPullToRefresh: false,
        });
        setTimeout(() => {
          if(typeof(responseData.message) === 'string')
          {
              Toast.showWithGravity(responseData.message, Toast.LONG, Toast.TOP)
          }
          else if(typeof(responseData.error) === 'string')
          {
              Toast.showWithGravity(responseData.error, Toast.LONG, Toast.TOP)
          }
          else {
              if (responseData.error) {
                Toast.showWithGravity(responseData.error.toString(), Toast.LONG, Toast.TOP)
              }
              else {
                Toast.showWithGravity(Message.KAPIError, Toast.LONG, Toast.TOP)
              }
          }
        }, 200); 
      }
    }
    else {
      this.setState({lastApiTag : 1})
      Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
    }
  }

  onDebitTransactionRefresh() {

    if (this.state.connection_Status == "Online") {
      this.setState({
        debitPullToRefresh:true,
        debitTransactionList: [],
        debitOffset: 0,
        debitLoading: false,
        debitLoadMoreCompleted: false,
      });
      setTimeout(() => {
          this.onFetchDebitTransactionList();
      }, 0);
      
    }
    else {
      this.setState({ debitPullToRefresh: false });
      Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
    }
  }

  handleDebitTransactionLoadMore = () => {
    if (!this.state.debitLoading && !this.state.debitLoadMoreCompleted){
      
      this.onFetchDebitTransactionList(); 
    }
  };

  async onFetchCreditTransactionList() {
    if (this.state.connection_Status == "Online") {

      this.setState({ visible: true });
      var data = new FormData();
      data.append('driver_id', CommonUtilsObj.userDetails.id);
      data.append('off', this.state.creditOffset);
      data.append('start_date', this.state.creditStartDate);
      data.append('end_date', this.state.creditEndDate);

      let responseData = await APIManagerObj.callApiWithData(Constant.KGetCreditTransactionListURL, data);
      console.log('json response ....' + JSON.stringify(responseData));

      if (responseData.status == "success") {
        var list1=this.state.creditTransactionList
        var list=responseData.transactions;
        list1.push(...list)

        var dateArr = [];

        const items = list1.map((item) => {  
            item.displayDate = false
            
            let date = new Date(item.created_date.replace(" ", "T"))
            let dateStr = Moment(item.created_date).format('ddd, MMM DD YYYY'); //date.toDateString();
            item.time = Moment(item.created_date).format('hh:mm A');
    
            if (item.description == null) {
                  item.description = 'Initial entry';
            }

            if (item.comment == null) {
                  item.comment = 'Initial entry';
            }

            if (item.payment_method == null) {
                  item.payment_method = '';
            }
            
            if (dateArr.indexOf(dateStr) > -1) {
            }
            else {
                dateArr.push(dateStr);
                item.displayDate = true
                item.date = dateStr
            }

            if (item.transaction_type === '0') {
              item.booingID = 'Order Id : ' + item.id;
              item.type = 'credit';
            }
            else if (item.transaction_type === '1' || item.transation_type === '9') {
              item.booingID = 'TXN Id : ' + item.transaction_id;
              item.type = 'debit';
            }
            else if (item.transaction_type === '2' || item.transation_type === '5' || item.transation_type == '6') {
              if (item.booking_id == null) {
                  item.booingID = 'Booking Id : ' + '0';
              }
              else {
                  item.booingID = 'Booking Id : ' + item.booking_id;
              }
              item.type = 'credit';
            } 
        });
        dateArr = [];

        this.setState({
          visible: false,
          creditOffset: responseData.offset,
          creditTransactionList: list1,
          creditLoading: false,
          creditLoadMoreCompleted: true,
          creditPullToRefresh: false,
        });         
      }
      else {
        this.setState({
            visible: false,
            creditLoading: false,
            creditLoadMoreCompleted: true,
            creditPullToRefresh: false,
        });
        setTimeout(() => {
          if(typeof(responseData.message) === 'string')
          {
              Toast.showWithGravity(responseData.message, Toast.LONG, Toast.TOP)
          }
          else if(typeof(responseData.error) === 'string')
          {
              Toast.showWithGravity(responseData.error, Toast.LONG, Toast.TOP)
          }
          else {
              if (responseData.error) {
                Toast.showWithGravity(responseData.error.toString(), Toast.LONG, Toast.TOP)
              }
              else {
                Toast.showWithGravity(Message.KAPIError, Toast.LONG, Toast.TOP)
              }
          }
        }, 200); 
      }
    }
    else {
      this.setState({lastApiTag : 1})
      Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
    }
  }

  onCreditTransactionRefresh() {

    if (this.state.connection_Status == "Online") {
      this.setState({
        creditPullToRefresh:true,
        creditTransactionList: [],
        creditOffset: 0,
        creditLoading: false,
        creditLoadMoreCompleted: false,
      });
      setTimeout(() => {
          this.onFetchCreditTransactionList();
      }, 0);
      
    }
    else {
      this.setState({ creditPullToRefresh: false });
      Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
    }
  }

  handleCreditTransactionLoadMore = () => {
    if (!this.state.creditLoading && !this.state.creditLoadMoreCompleted){
      
      this.onFetchCreditTransactionList(); 
    }
  };

  getTransactionTime(date){
    var TimeType, hour, minutes, seconds, fullTime;

    hour = date.getHours(); 
    if(hour <= 11)
    {
      TimeType = 'AM';
    }
    else{
      TimeType = 'PM';
    }

    if( hour > 12 )
    {
      hour = hour - 12;
    }
 
    if( hour == 0 )
    {
        hour = 12;
    } 

    minutes = date.getMinutes();
    if(minutes < 10)
    {
      minutes = '0' + minutes.toString();
    }

    seconds = date.getSeconds();
    if(seconds < 10)
    {
      seconds = '0' + seconds.toString();
    }

    return hour.toString() + ':' + minutes.toString() + ' ' + TimeType.toString();
  }

  returnData(isUpdated) {
    this.setState({isUpdated: isUpdated});
  }

  onRequestPayout = (item, index) => {
    
      this.props.navigation.navigate('RequestPayoutScreen', {returnData: this.returnData.bind(this), balance: this.state.balance});
  };

  showModal() {
    this.setState({
      selectedIndex: 0, 
      modalVisible: true, 
    })
  }

  hideModal() {
    this.setState({ modalVisible: false })
  }

  onDone() {

    var today = new Date();

    if (this.state.selectedIndex == 0) {
        var priorDate = new Date().setDate(today.getDate()-30);
        let newDate = new Date(priorDate);
        let dateStr = newDate.getDate() + "-" + (newDate.getMonth()+1) + "-" + newDate.getFullYear();
        let newEndDate = new Date()
        let dateEndStr = newEndDate.getDate() + "-" + (newEndDate.getMonth() + 1) + "-" + newEndDate.getFullYear();

        if (this.state.isSelectedAll) {
          this.setState({
              allStartDate: dateStr,
              allEndDate: dateEndStr,
              allTransactionList: [],
              allOffset: 0,
              modalVisible: false,
              refresh: true,     
          });

          setTimeout(() => {
            this.onFetchAllTransactionList();
          }, 100);
        }
        else if (this.state.isSelectedDebit) {
            this.setState({
                debitStartDate: dateStr,
                debitEndDate: dateEndStr,
                debitTransactionList: [],
                debitOffset: 0,
                modalVisible: false,
                refresh: true,     
            });

            setTimeout(() => {
              this.onFetchDebitTransactionList();
            }, 100);
        }
        else if (this.state.isSelectedCredit) {
            this.setState({
                creditStartDate: dateStr,
                creditEndDate: dateEndStr,
                creditTransactionList: [],
                creditOffset: 0,
                modalVisible: false,
                refresh: true,     
            });

            setTimeout(() => {
              this.onFetchCreditTransactionList();
            }, 100);
        }
    }
    else if (this.state.selectedIndex == 1) {
        var priorDate = new Date().setDate(today.getDate()-60);
        let newDate = new Date(priorDate);
        let dateStr = newDate.getDate() + "-" + (newDate.getMonth() + 1) + "-" + newDate.getFullYear();
        let newEndDate = new Date()
        let dateEndStr = newEndDate.getDate() + "-" + (newEndDate.getMonth() + 1) + "-" + newEndDate.getFullYear();

        if (this.state.isSelectedAll) {
          this.setState({
              allStartDate: dateStr,
              allEndDate: dateEndStr,
              allTransactionList: [],
              allOffset: 0,
              modalVisible: false,
              refresh: true,     
          });

          setTimeout(() => {
            this.onFetchAllTransactionList();
          }, 100);
        }
        else if (this.state.isSelectedDebit) {
            this.setState({
                debitStartDate: dateStr,
                debitEndDate: dateEndStr,
                debitTransactionList: [],
                debitOffset: 0,
                modalVisible: false,
                refresh: true,     
            });

            setTimeout(() => {
              this.onFetchDebitTransactionList();
            }, 100);
        }
        else if (this.state.isSelectedCredit) {
            this.setState({
                creditStartDate: dateStr,
                creditEndDate: dateEndStr,
                creditTransactionList: [],
                creditOffset: 0,
                modalVisible: false,
                refresh: true,     
            });

            setTimeout(() => {
              this.onFetchCreditTransactionList();
            }, 100);
        }
    }
    else if (this.state.selectedIndex == 2) {
        var priorDate = new Date().setDate(today.getDate()-60);
        let newDate = new Date(priorDate);
        let dateStr = newDate.getDate() + "-" + (newDate.getMonth() + 1) + "-" + newDate.getFullYear();
        let newEndDate = new Date()
        let dateEndStr = newEndDate.getDate() + "-" + (newEndDate.getMonth() + 1) + "-" + newEndDate.getFullYear();

        if (this.state.isSelectedAll) {
          this.setState({
              allStartDate: dateStr,
              allEndDate: dateEndStr,
              allTransactionList: [],
              allOffset: 0,
              modalVisible: false,
              refresh: true,     
          });

          setTimeout(() => {
            this.onFetchAllTransactionList();
          }, 100);
        }
        else if (this.state.isSelectedDebit) {
            this.setState({
                debitStartDate: dateStr,
                debitEndDate: dateEndStr,
                debitTransactionList: [],
                debitOffset: 0,
                modalVisible: false,
                refresh: true,     
            });

            setTimeout(() => {
              this.onFetchDebitTransactionList();
            }, 100);
        }
        else if (this.state.isSelectedCredit) {
            this.setState({
                creditStartDate: dateStr,
                creditEndDate: dateEndStr,
                creditTransactionList: [],
                creditOffset: 0,
                modalVisible: false,
                refresh: true,     
            });

            setTimeout(() => {
              this.onFetchCreditTransactionList();
            }, 100);
        }
    }
    else if (this.state.selectedIndex == 3) {
        if (this.state.isSelectedAll) {
            this.setState({
                allStartDate: this.state.startDate,
                allEndDate: this.state.endDate,
                allTransactionList: [],
                allOffset: 0,
                modalVisible: false,
                refresh: true,     
            });

            setTimeout(() => {
              this.onFetchAllTransactionList();
            }, 100);
        }
        else if (this.state.isSelectedDebit) {
            this.setState({
                debitStartDate: this.state.startDate,
                debitEndDate: this.state.endDate,
                debitTransactionList: [],
                debitOffset: 0,
                modalVisible: false,
                refresh: true,     
            });

            setTimeout(() => {
              this.onFetchDebitTransactionList();
            }, 100);
        }
        else if (this.state.isSelectedCredit) {
            this.setState({
                creditStartDate: this.state.startDate,
                creditEndDate: this.state.endDate,
                creditTransactionList: [],
                creditOffset: 0,
                modalVisible: false,
                refresh: true,     
            });

            setTimeout(() => {
              this.onFetchCreditTransactionList();
            }, 100);
        }
    } 
  }

  onReset() {
    if (this.state.isSelectedAll) {
        this.setState({
            allStartDate: "",
            allEndDate: "",
            allTransactionList: [],
            allOffset: 0,
            modalVisible: false,
            refresh: true,     
        });

        setTimeout(() => {
          this.onFetchAllTransactionList();
        }, 100);
    }
    else if (this.state.isSelectedDebit) {
        this.setState({
            debitStartDate: "",
            debitEndDate: "",
            debitTransactionList: [],
            debitOffset: 0,
            modalVisible: false,
            refresh: true,     
        });

        setTimeout(() => {
          this.onFetchDebitTransactionList();
        }, 100);
    }
    else if (this.state.isSelectedCredit) {
        this.setState({
            creditStartDate: "",
            creditEndDate: "",
            creditTransactionList: [],
            creditOffset: 0,
            modalVisible: false,
            refresh: true,     
        });

        setTimeout(() => {
          this.onFetchCreditTransactionList();
        }, 100);
    }
  }

  onSelectCell = (item, index) => {
      
      if (index == 3) {
          this.setState({
            modalMarginPer: 35,
          });
      }
      else {
          this.setState({
            modalMarginPer: 45,
          });
      }

      setTimeout(() => {
          this.setState({
        refresh:true,
        selectedIndex: index,
      });
      }, 100);

  };

  emptyComponent = () => {
    return (
        <View style={CustomStyles.walletEmptyContainer}>
          <Text style={CustomStyles.walletEmptyText}>No records found</Text>
        </View>
    );
  };

  render() {

    return (
      <View style={CustomStyles.container1}>

          <View
              style={{backgroundColor: "#fff", justifyContent: 'center', alignItems: 'center'}}>
              <ProgressLoader
              visible={this.state.visible}
              isModal={true} isHUD={true}
              hudColor={"#fff"}
              height={200}
              width={200}
              color={"#8edf01"} />
          </View>

          <View style={{ marginLeft: 5, flexDirection: 'row', justifyContent: 'center', alignItems:'center', backgroundColor: Constant.KAppColor1, height: (((Dimensions.get('window').height) * 25) / 100)}}>

              <View style={CustomStyles.flexColunm, {flex: 1}}>
                  <Text style={{fontSize: headerFontSize, fontFamily: Constant.KThemeFontSemiBold, color: '#fff', textAlign:'center'}}>${this.state.balance}</Text>
                  <Text style={{fontSize: headerSubFontSize, fontFamily: Constant.KThemeFontLight, color: '#fff', textAlign:'center'}}>Wallet Balance</Text>
                  {
                    // <TouchableOpacity
                    //   style={{justifyContent:'center', alignItems: 'center', padding: 5, marginTop: 10}}
                    //   onPress={this.onRequestPayout.bind(this)}
                    // >
                    //     <Text style={{color:'#000', textAlign: 'center', fontSize: headerSubFontSize+2, fontFamily: Constant.KThemeFontRegular, backgroundColor: Constant.KAppColor, padding: 5}}>Request Payout</Text>
                    // </TouchableOpacity>
                  }

                   

              </View>
          </View>

          <View>
            <View style={{ flex: 1, flexDirection: 'row', height: 50}}>
                <View style={CustomStyles.flexColunm, {flex: 1, height: 50, backgroundColor: '#fff'}}>
                    <TouchableOpacity
                      style={{justifyContent:'center', alignItems: 'center', padding: 5, height: 50}}
                      onPress={this.onClickAllTransaction.bind(this)}
                    >
                        <Text style={{color:'#000', textAlign: 'center', fontSize: headerSubFontSize+4, fontFamily: Constant.KThemeFontRegular}}>All</Text>
                        { this.state.isSelectedAll && 
                            <View style={{backgroundColor: Constant.KAppColor, height: 5, position: 'absolute', bottom:0, flex: 1, width:(Dimensions.get('window').width)/3}}>
                            </View>
                        }
                    </TouchableOpacity>
                </View>

                <View style={CustomStyles.flexColunm, {flex: 1, height: 50, backgroundColor: '#fff'}}>
                    <TouchableOpacity
                      style={{justifyContent:'center', alignItems: 'center', padding: 5, marginTop: 10}}
                      onPress={this.onClickDebitTransaction.bind(this)}
                    >
                        <Text style={{color:'#000', textAlign: 'center', fontSize: headerSubFontSize+4, fontFamily: Constant.KThemeFontRegular}}>Debit</Text>
                        { this.state.isSelectedDebit && 
                            <View style={{backgroundColor: Constant.KAppColor, height: 5, position: 'absolute', bottom:-10, flex: 1, width:(Dimensions.get('window').width)/3}}>
                            </View>
                        }
                    </TouchableOpacity>
                </View>

                <View style={CustomStyles.flexColunm, {flex: 1, height: 50, backgroundColor: '#fff'}}>
                    <TouchableOpacity
                      style={{justifyContent:'center', alignItems: 'center', padding: 5, marginTop: 10}}
                      onPress={this.onClickCreditTransaction.bind(this)}
                    >
                        <Text style={{color:'#000', textAlign: 'center', fontSize: headerSubFontSize+4, fontFamily: Constant.KThemeFontRegular}}>Credit</Text>
                        { this.state.isSelectedCredit && 
                            <View style={{backgroundColor: Constant.KAppColor, height: 5, position: 'absolute', bottom:-10, flex: 1, width:(Dimensions.get('window').width)/3}}>
                            </View>
                        }
                    </TouchableOpacity>
                </View>
            </View> 
          </View>

          <View style={{marginTop: 50, height: (((Dimensions.get('window').height) * 75 ) / 100) - 150}}>
            { this.state.isSelectedAll && 
                <FlatList
                  showsVerticalScrollIndicator
                  data={this.state.allTransactionList}
                  renderItem={this.renderAllTransactionListItem}
                  keyExtractor={(item, index) => index.toString()}
                  onRefresh={() => this.onAllTransactionRefresh()}
                  refreshing={this.state.allPullToRefresh}
                  onEndReachedThreshold={0.4}
                  onEndReached={this.handleAllTransactionLoadMore.bind(this)}
                  ListEmptyComponent={this.emptyComponent.bind(this)}
                /> 
            }

            { this.state.isSelectedDebit && 
                <FlatList
                  showsVerticalScrollIndicator
                  data={this.state.debitTransactionList}
                  renderItem={this.renderDebitTransactionListItem}
                  keyExtractor={(item, index) => index.toString()}
                  onRefresh={() => this.onDebitTransactionRefresh()}
                  refreshing={this.state.debitPullToRefresh}
                  onEndReachedThreshold={0.4}
                  onEndReached={this.handleDebitTransactionLoadMore.bind(this)}
                  ListEmptyComponent={this.emptyComponent.bind(this)}
                /> 
            }

            { this.state.isSelectedCredit && 
                <FlatList
                  showsVerticalScrollIndicator
                  data={this.state.creditTransactionList}
                  renderItem={this.renderCreditTransactionListItem}
                  keyExtractor={(item, index) => index.toString()}
                  onRefresh={() => this.onCreditTransactionRefresh()}
                  refreshing={this.state.creditPullToRefresh}
                  onEndReachedThreshold={0.4}
                  onEndReached={this.handleCreditTransactionLoadMore.bind(this)}
                  ListEmptyComponent={this.emptyComponent.bind(this)}
                /> 
            }
            
          </View>

          <View style={{position:'absolute', bottom: 20, marginLeft: ((Dimensions.get('window').width) - 70), height: 60,}}>
              <TouchableOpacity
                    onPress={this.showModal.bind(this)}
              >
                <Image
                    style={{height:60, width: 60,}}
                    source={require('../../images/filter.png')}
                />
              </TouchableOpacity>   
          </View>

          { this.state.modalVisible &&
          <Modal
            animationType="slide" 
            transparent={true}
            onBackdropPress={this.hideModal.bind(this)}
            visible={true}>
            <View style={{ flex: 1, backgroundColor: Constant.KAppColor1 ,margin: 30, marginTop: (((Dimensions.get('window').width) * this.state.modalMarginPer) / 100), marginBottom:(((Dimensions.get('window').width) * this.state.modalMarginPer) / 100)}}>
                <View style={{ backgroundColor: '#8edf01', flexDirection: 'row', justifyContent: 'center', height:45, marginBottom:10}}>
                    <Text style={{color: 'black', fontWeight: 'bold',alignSelf: 'center', textAlign: 'center', padding: 15}}>Wallet Filter</Text>
                    
                </View>
                <View style={{ position: 'absolute', marginLeft: (Dimensions.get('window').width)-90, marginTop:10, height: 40, width: 40}}>
                        <TouchableOpacity
                        onPress={this.hideModal.bind(this)}
                        style={{height: 25, width: 25, position: 'absolute'}}
                    >
                        <Image
                          ref="imgChecked"
                          style={{height: 25, width: 25,}}
                          source={require('../../images/cancel.png')}
                        />
                    </TouchableOpacity>
                    </View>
                <View style={{ flex: 10}}>
                    <FlatList
                    showsVerticalScrollIndicator
                    data={this.state.filterList}
                    renderItem={({ item, index }) => (
                      <TouchableWithoutFeedback 
                            onPress={this.onSelectCell.bind(this, item, index)}>
                        <View style={CustomStyles.flexColunm}>
                            <View 
                              style={
                                [
                                  CustomStyles.countryStyle, 
                                  {
                                    flexDirection: 'row', 
                                    marginTop : 4,
                                    marginBottom : 4,
                                    marginLeft: 20,
                                    marginRight: 20,
                                    
                                  }
                                ]
                              }>
                              <Image
                                    ref="imgChecked"
                                    style={{height: 25, width: 25, marginRight: 8, marginBottom : 5}}
                                    source={ this.state.selectedIndex === index ?                  
                                    require('../../images/checkSelected.png') : 
                                    require('../../images/checkUnselected.png')}
                                  />
                              <Text style={{fontSize: 15, color: 'white', marginTop: 3}}>
                                {item}
                              </Text>
                            </View>

                            <View style={CustomStyles.flexRow}>
                                      { this.state.selectedIndex === index &&  this.state.selectedIndex == 3 &&
                                        <DatePicker
                                            style={{marginLeft: 20, width: (((Dimensions.get('window').width) / 2) - 55), backgroundColor: 'white', borderRadius: 5, height: txtInputHeight, justifyContent: 'flex-start'}}
                                            date={this.state.startDate}
                                            mode="date"
                                            placeholder="select date"
                                            format="DD-MM-YYYY"
                                            minDate="1950-01-01"
                                            maxDate={new Date()}
                                            confirmBtnText="Confirm"
                                            cancelBtnText="Cancel"
                                            showIcon={false}
                                            CustomStyles={{
                                              dateInput: { borderWidth: 0 }
                                              
                                            }}
                                            onDateChange={(date) => {
                                              this.setState({
                                                  startDate: date,
                                              });

                                            }}
                                        />
                                      }
                                      { this.state.selectedIndex === index &&  this.state.selectedIndex == 3 && 
                                        <DatePicker
                                            style={{marginLeft: 10, marginRight: 20, width: (((Dimensions.get('window').width) / 2) - 55), backgroundColor: 'white', borderRadius: 5, height: txtInputHeight, justifyContent: 'flex-start'}}
                                            date={this.state.endDate}
                                            mode="date"
                                            placeholder="select date"
                                            format="DD-MM-YYYY"
                                            minDate="1950-01-01"
                                            maxDate={new Date()}
                                            confirmBtnText="Confirm"
                                            cancelBtnText="Cancel"
                                            showIcon={false}
                                            CustomStyles={{
                                              dateInput: { borderWidth: 0 }
                                              
                                            }}
                                            onDateChange={(date) => {
                                              this.setState({
                                                  endDate: date,
                                              });
                                            }}
                                        />
                                      }
                                  </View>
                        </View>
                        </TouchableWithoutFeedback>
                    )}
                    
                    keyExtractor={(item, index) => index.toString()}
                  />
                </View>

                <View style={[CustomStyles.flexRow, {marginBottom: 30, marginTop: 10, alignItems: 'center', justifyContent: 'center'}]}>
                  <TouchableOpacity
                    onPress={() => this.onReset()} 
                    style={{height: 35, width: 110, borderRadius:5, backgroundColor: '#8edf01', marginRight: 20, alignItems: 'center', justifyContent: 'center'}}
                    >
                    <Text style={{fontSize: 15, width: 100, alignSelf: 'center', textAlign: 'center', color: 'black', fontWeight: 'bold'}}>
                      Reset
                    </Text>
                  </TouchableOpacity>

                  <TouchableOpacity
                    onPress={() => this.onDone()} 
                    style={{height: 35, width: 110, borderRadius:5, backgroundColor: '#8edf01', alignItems: 'center', justifyContent: 'center'}}
                    >
                    <Text style={{backgroundColor: '#8edf01', fontSize: 15, width: 100, alignSelf: 'center', textAlign: 'center', color: 'black', fontWeight: 'bold'}}>
                      Done
                    </Text>
                  </TouchableOpacity>

              </View>
            </View>
          </Modal> 
        }
      </View>
      
    );
  }
}

class WalletListCell extends React.Component {
  render() {
    const { item, index} = this.props;

    return (

          <View style={[CustomStyles.flexColunm, {flex:1, marginTop: 10, marginBottom: 10}]}>

              <Text style={ (item.displayDate) ? CustomStyles.cellDateFont : CustomStyles.height0}>{item.date}</Text>

              <View style={[CustomStyles.flexRow ,{alignItems: 'center', justifyContent: 'center', marginHorizontal: 10, }]}>

                  <Image
                      style={{height:60, width: 60, marginRight: 10}}
                      source={ ( item.type == 'credit' ) ? require('../../images/credit.png') : require('../../images/debit.png')}
                  />

                  <View style={[CustomStyles.flexColunm, {flex: 1}]}>
                      <View style={[CustomStyles.flexRow,{ marginBottom: 3}]}>
                          <View style={[CustomStyles.flexRow, {justifyContent: 'space-between', flex: 1}]}>
                              <Text numberOfLines={1} ellipsizeMode='tail' style={[CustomStyles.cellHeaderFont, {marginRight: 10}]}>{CommonUtilsObj.Capitalize(item.description)}</Text>
                              </View>
                          <View style={{flexDirection: 'row', justifyContent:'flex-end'}}>
                              <Text style={ (item.type == 'credit') ? [CustomStyles.cellHeaderFont, CustomStyles.creditFontColor] : [CustomStyles.cellHeaderFont, CustomStyles.debitFontColor]}>{item.amount}</Text>
                              </View>
                      </View>
                      <View style={{flex:1 , flexDirection: 'column'}}>
                          <View style={{flex:1, flexDirection: 'row'}}>
                          <View style={{flex:1, flexDirection: 'row'}}>
                              <Text numberOfLines={1} ellipsizeMode='tail' style={[CustomStyles.cellOtherFont, {marginRight: 10} ]}>{item.booingID}</Text>
                          </View>
                          <View style={{flexDirection: 'row', justifyContent:'flex-end'}}>
                              <Text style={CustomStyles.cellOtherFont}>{item.time}</Text>
                          </View>
                      </View>

                          <Text style={CustomStyles.cellOtherFont}>Closing Balance : {item.current_balance}</Text>
                      </View>
                  </View>
              </View>
          </View>
    );
  }
}
