import React from 'react';
import { StyleSheet, Dimensions, YellowBox, Text, View, Button, TextInput, Image, ScrollView, TouchableOpacity, Alert, SafeAreaView, PixelRatio } from 'react-native';
import {KeyboardAvoidingView, Keyboard, TouchableWithoutFeedback} from 'react-native';
import { NavigationBar } from 'navigationbar-react-native';
import userDefaults from 'react-native-user-defaults'
import NetInfo from "@react-native-community/netinfo";
import ProgressLoader from 'rn-progress-loader';
import Toast from 'react-native-simple-toast';
import { StackActions, NavigationActions } from 'react-navigation';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'

import CommonUtilsObj from '../CommonClasses/CommonUtils';
import Constant from '../CommonClasses/Constant';
import CustomStyles from '../CommonClasses/CustomStyles';
import Message from '../CommonClasses/Message';
import APIManagerObj from '../CommonClasses/APIManager';

const headerFontSize = (((Dimensions.get('window').width) * 11) / 100);

export default class RequestPayout extends React.Component {

  constructor(props) {
 
    super(props);
 
    YellowBox.ignoreWarnings([
      'Warning: componentWillMount is deprecated',
      'Warning: componentWillReceiveProps is deprecated',
    ]);

    const { navigation } = this.props;  
    const balance = navigation.getParam('balance', 0.0);

    this.state = {
        visible: false,
        connection_Status : "",
        balance: balance,
        amount: 0,
    };
    

    this.onFocus = this.onFocus.bind(this);
    this.onValidate = this.onValidate.bind(this);
    this.onChangeText = this.onChangeText.bind(this);
    this.onSubmitamount = this.onSubmitamount.bind(this);

    this.amountRef = this.updateRef.bind(this, 'amount');
  }

  componentDidMount() {

    NetInfo.isConnected.addEventListener(
        'connectionChange',
        this._handleConnectivityChange

    );
   
    NetInfo.isConnected.fetch().done((isConnected) => {

      if(isConnected == true)
      {
        this.setState({connection_Status : "Online"})
      }
      else
      {
        this.setState({connection_Status : "Offline"})
      }
    });

  }

  componentWillUnmount() {

    NetInfo.isConnected.removeEventListener(
        'connectionChange',
        this._handleConnectivityChange
 
    );
 
  }

  componentWillUnmount() {

    NetInfo.isConnected.removeEventListener(
        'connectionChange',
        this._handleConnectivityChange
 
    );
 
  }

  _handleConnectivityChange = (isConnected) => {

    if(isConnected == true)
      {
        this.setState({connection_Status : "Online"})
      }
      else
      {
        this.setState({connection_Status : "Offline"})
      }
  };

  updateRef(name, ref) {
    this[name] = ref;
  }

  onFocus() {
    let { errors = {} } = this.state;

    for (let name in errors) {
      let ref = this[name];

      if (ref && ref.isFocused()) {
        delete errors[name];
      }
    }

    this.setState({ errors });
  }

  onChangeText(text) {
    ['amount']
      .map((name) => ({ name, ref: this[name] }))
      .forEach(({ name, ref }) => {
        if (ref.isFocused()) {
          this.setState({ [name]: text });
          if (name == 'amount') {
              this.setState({ [name]: text.replace(/[^0-9]/, '') });
            }
        }
      });
  }

  onSubmitamount(text) {
      
    this.amount.blur();
  }

  onValidate() {

    this.amount.blur();

    var flag = true;
    var errorMsg = [];

    let amtVal = this.state.amount
    if (!amtVal) {
        errorMsg.push('Amount should not be empty')
        flag = false;
    }
    else if (amtVal < 20) {
        errorMsg.push('Amount should be greater than $20.')
        flag = false;
    }

    if (flag) {
      if (this.state.connection_Status == "Online") {
          this.onRequestPayout();
      }
      else {
        Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
      }
    }
    else {
      Toast.showWithGravity(errorMsg.toString(), Toast.LONG, Toast.TOP)
    }
  }

  async onRequestPayout() {
    if (this.state.connection_Status == "Online") {

      this.setState({ visible: true });
      var data = new FormData();
      data.append('driver_id', CommonUtilsObj.userDetails.id);
      data.append('payout_amt', this.state.amount);

      let responseData = await APIManagerObj.callApiWithData(Constant.KRequestPayoutURL, data);
      console.log('json response ....' + JSON.stringify(responseData));

      if (responseData.status == "success") {
        this.setState({
            visible: false,
        });
        setTimeout(() => {
          Toast.showWithGravity(responseData.message, Toast.LONG, Toast.TOP);
          // this.props.navigation.pop();
          const navigateAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: "WalletScreen"})],
          });
          this.props.navigation.dispatch(navigateAction);
        }, 200);         
      }
      else {
        this.setState({
          visible: false,
        }); 
        setTimeout(() => {
          if(typeof(responseData.message) === 'string')
          {
              Toast.showWithGravity(responseData.message, Toast.LONG, Toast.TOP)
          }
          else if(typeof(responseData.error) === 'string')
          {
              Toast.showWithGravity(responseData.error, Toast.LONG, Toast.TOP)
          }
          else {
              if (responseData.error) {
                Toast.showWithGravity(responseData.error.toString(), Toast.LONG, Toast.TOP)
              }
              else {
                Toast.showWithGravity(Message.KAPIError, Toast.LONG, Toast.TOP)
              }
          }
        }, 200); 
      }
    }
    else {
      this.setState({lastApiTag : 1})
      Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
    }
  }

  render() {

    return (

            <View style={CustomStyles.container1}>
            <KeyboardAwareScrollView style = {[CustomStyles.flexColunm, {flex:0.8}]}>
                <View
                style={{backgroundColor: "#fff", justifyContent: 'center', alignItems: 'center'}}>

                <ProgressLoader
                  visible={this.state.visible}
                  isModal={true} isHUD={true}
                  hudColor={"#fff"}
                  height={200}
                  width={200}
                  color={"#8edf01"} />
                </View>

                <View>

                    <View style={{ marginLeft: 5, flexDirection: 'row', justifyContent: 'center', alignItems:'center', backgroundColor: Constant.KAppColor1, height: (((Dimensions.get('window').height) * 25) / 100)}}>

                        <Text style={{fontSize: headerFontSize, fontFamily: Constant.KThemeFontSemiBold, color: '#fff', textAlign:'center'}}>${this.state.balance}</Text>
                        
                    </View>

                    <View style={{ marginRight: 0, flexDirection: 'row', justifyContent: 'flex-start', alignItems:'center', backgroundColor: Constant.KAppColor1, height: 100, marginTop: 10, marginLeft: 0}}>

                        <View style={[CustomStyles.flexColunm, {margin: 20, flex: 1, paddingBottom: 5}]}>

                            <Text style={[CustomStyles.cellDateFont, {marginBottom: 0}]}>How much would you like to payout?</Text>

                            <View style={[CustomStyles.flexRow, {marginTop: 15, alignItems: 'center'}]}>
                              <Image
                                ref="imgChecked"
                                style={{height: 35, width: 35, marginRight: 0}}
                                source={ require('../../images/walletGreen.png')}
                              />
                              <TextInput
                                style={[CustomStyles.textInput, {flex:1, paddingLeft: 5, marginLeft: 10, marginTop: 0}]}
                                ref={this.amountRef}
                                placeholder = "$0" 
                                placeholderTextColor = "gray"
                                keyboardType='number-pad'
                                autoCapitalize='sentences'
                                autoCorrect={true}
                                enablesReturnKeyAutomatically={true}
                                onFocus={this.onFocus}
                                onChangeText={this.onChangeText}
                                onSubmitEditing={this.onSubmitamount}
                                returnKeyType='next'
                                value= {this.state.amount}
                              />
                            </View>
                        </View>
                    </View>

                
                
            </View>

                    <TouchableOpacity
                  style={[CustomStyles.themeButton, {marginTop:250,}]}
                  onPress={this.onValidate.bind(this)}
                 >
                    <Text style={CustomStyles.themeButtonText}> Payout </Text>
                </TouchableOpacity>
                </KeyboardAwareScrollView>
      </View>
      
    );
  }
}
