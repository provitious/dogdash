import React from 'react';
import { StyleSheet, Dimensions, YellowBox, Text, View, Button, TextInput, Image, ScrollView, TouchableOpacity, Alert, SafeAreaView, PixelRatio } from 'react-native';
import {KeyboardAvoidingView, Keyboard, TouchableWithoutFeedback} from 'react-native';
import { NavigationBar } from 'navigationbar-react-native';
import userDefaults from 'react-native-user-defaults'
import { Modal, FlatList, SectionList, ActivityIndicator } from 'react-native';
import NetInfo from "@react-native-community/netinfo";
import ProgressLoader from 'rn-progress-loader';
import Toast from 'react-native-simple-toast';
import { Rating, AirbnbRating } from 'react-native-ratings';

import CommonUtilsObj from '../CommonClasses/CommonUtils';
import Constant from '../CommonClasses/Constant';
import CustomStyles from '../CommonClasses/CustomStyles';
import Message from '../CommonClasses/Message';
import APIManagerObj from '../CommonClasses/APIManager';

const imageSize = (((Dimensions.get('window').width) * 30) / 100);
const vWidth = (((Dimensions.get('window').width) * 50) / 100);

const headerFontSize = (((Dimensions.get('window').width) * 5) / 100);
const headerSubFontSize = (((Dimensions.get('window').width) * 3.7) / 100);
const txtMediumSize = (((Dimensions.get('window').width) * 4.5) / 100);
const txtSmallSize = (((Dimensions.get('window').width) * 3.6) / 100);

export default class Review extends React.Component {

  constructor(props) {
 
    super(props);
 
    YellowBox.ignoreWarnings([
      'Warning: componentWillMount is deprecated',
      'Warning: componentWillReceiveProps is deprecated',
    ]);

    console.log(JSON.stringify(CommonUtilsObj.userDetails));

    this.state = {
      visible: false,
      loading: false,
      loadMoreCompleted: false,
      connection_Status : "",
      sourceUrl: { uri: Constant.KDriverImagePathURL + CommonUtilsObj.userDetails.image },
      name: CommonUtilsObj.userDetails.name,
      mobile: CommonUtilsObj.userDetails.phone,
      rating: CommonUtilsObj.userDetails.avg_rate,
      ratingList: [],
      offset: 0,
      cancellCnt: 0,
      successfullCnt: 0,
      pullToRefresh: false,
      hasScrolled: false
    };
    CommonUtilsObj.navigationProps = this.props;
  }

  componentDidMount() {

    NetInfo.isConnected.addEventListener(
        'connectionChange',
        this._handleConnectivityChange

    );
   
    NetInfo.isConnected.fetch().done((isConnected) => {

      if(isConnected == true)
      {
        this.setState({connection_Status : "Online"})
      }
      else
      {
        this.setState({connection_Status : "Offline"})
      }


    if (this.state.connection_Status == "Online") {
      this.obFetchRatingList();
    }
    else {
      Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
    }

    });

    if (this.state.selectedCabDetails == '') {
        console.log('empty');
    }
    else {
      console.log(this.state.selectedCabDetails);
    }
  }

  componentWillUnmount() {

    NetInfo.isConnected.removeEventListener(
        'connectionChange',
        this._handleConnectivityChange
 
    );
 
  }

  componentWillUnmount() {

    NetInfo.isConnected.removeEventListener(
        'connectionChange',
        this._handleConnectivityChange
 
    );
 
  }

  _handleConnectivityChange = (isConnected) => {

    if(isConnected == true)
      {
        this.setState({connection_Status : "Online"})
      }
      else
      {
        this.setState({connection_Status : "Offline"})
      }
  };

  async obFetchRatingList() {
    if (this.state.connection_Status == "Online") {
      this.setState({ loading: true })
    this.setState({
        visible: true
    });

    // var data = {
    //     driver_id: CommonUtilsObj.userDetails.id,
    //     off: this.state.offset,
    // };

    var data = new FormData();
    data.append('driver_id', CommonUtilsObj.userDetails.id);
    data.append('off', this.state.offset);

    console.log(data);
    try {
        let response = await fetch(
          Constant.KGetReviewRatingURL,
          {
            method: "POST",
            headers: {
             "Accept": "application/json",
             // "Content-Type": "application/json"
             'Content-Type': 'multipart/form-data',
            },
            body: data //JSON.stringify(data)
         }
        )
    .then((response) => response.json())
    .then((responseData) => {
       console.log('json response ....' + JSON.stringify(responseData));
       this.setState({ pullToRefresh: false });
       if (responseData.status == "success") {

            var list1=this.state.ratingList
            var list=responseData.driver_rate_detail;
            list1.push(...list)

            this.setState({
              visible: false,
              offset: responseData.offset,
              ratingList: list1,
              cancellCnt: responseData.driver_cancelled_trips,
              successfullCnt: responseData.driver_successful_trips,
          });

            this.setState({ loading: false })
        }
        else {
          this.setState({
              visible: false
          });

           this.setState({ loading: false })
           this.setState({ loadMoreCompleted: true })
        }
    })
    .done();
 
    } catch (errors) {
      this.setState({
          visible: false
      });
      let err = "Error is" + errors
      Toast.showWithGravity(err, Toast.SHORT, Toast.TOP)

       this.setState({ loading: false })
    }
    }
    else {
      Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
    }
     
  }

  showModal() {
    this.setState({ modalVisible: true })
  }

  hideModal() {
    this.setState({ modalVisible: false })
  }

  onDone() {
    this.setState({
        refresh:true,
        selectedCabDetails: this.state.cabList[this.state.selectedIndex],
        modalVisible: false,
      });
  }

  onSelectCell = (item, index) => {
      this.setState({
        refresh:true,
        selectedIndex: index,
      });
      console.log('selected cell ...' + index);
  };

  renderPodcastItem = ({ item, index }) => (
    <FlatListCell
      item={item}
      index={index}
      onSelectCell={this.onSelectCell}
      selectedIndex={this.state.selectedIndex}
      ratingList={this.state.ratingList}
    />
  );

  onRefresh() {

    this.setState({
        pullToRefresh:true,
        ratingList: [],
        offset: 0,
        loading: false,
        loadMoreCompleted: false,
      });

    if (this.state.connection_Status == "Online") {
      
      setTimeout(() => {
          this.obFetchRatingList();
      }, 2000);
      
    }
    else {
      this.setState({ pullToRefresh: false });
      Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
    }
  }

  handleLoadMore = () => {
    if (!this.state.loading && !this.state.loadMoreCompleted){
      
      this.obFetchRatingList(); // method for API call 
    }
  };

  renderFooter = () => {
   //it will show indicator at the bottom of the list when data is loading otherwise it returns null
    if (!this.state.loading) return null;
    return (
      <ActivityIndicator
        style={{ color: '#000' }}
      />
    );
  };

  render() {

    if (this.state.loading && this.page === 1) {
      return <View style={{
        width: '100%',
        height: '100%'
      }}><ActivityIndicator style={{ color: '#000' }} /></View>;
    }

    return (
      <View style={[CustomStyles.container1]}>
          <SectionList
                onRefresh={() => this.onRefresh()}
                refreshing={this.state.pullToRefresh}
                ListFooterComponent={this.renderFooter.bind(this)}
                onEndReachedThreshold={0.4}
                onEndReached={this.handleLoadMore.bind(this)}
                renderItem={({item, index, section}) =>

                  <View style={{ flex: 0.5, flexDirection: 'row', justifyContent: 'center', alignItems:'center'}}>
                            
                    { (section.title=='1') ? 
                         <View style={CustomStyles.container}>
                            <Image style={CustomStyles.avatarReview} source={this.state.sourceUrl} />
                            <Text style={[CustomStyles.mediumFont, CustomStyles.margin10]}>{this.state.name}</Text>
                            <Text style={CustomStyles.smallFont}>{this.state.mobile}</Text>
                            <Rating
                              type='custom'
                              ratingColor={Constant.KAppColor}
                              ratingBackgroundColor='white'
                              ratingCount={5}
                              imageSize={30}
                              startingValue={this.state.rating}
                              onFinishRating={this.ratingCompleted}
                              style={{ marginBottom: 15, marginTop: 5}}
                              readonly={true}
                            />

                        </View>
                    : 
                        <View style={CustomStyles.flexColunm, {width: (Dimensions.get('window').width)}}>
                            <View style={{ flex: 1, flexDirection: 'row', padding:15}}>
                                <View style={{flexDirection: 'row', justifyContent: 'center', alignItems:'center', marginRight:3}}>
                                    <Image
                                      style={{height:70, width:70, borderRadius: 35}}
                                      source={{uri: Constant.KUserImagePathURL + item.image}}
                                    />
                                </View>

                                <View style={{ marginLeft: 5, flexDirection: 'row', justifyContent: 'flex-start', alignItems:'center', marginRight: 20}}>

                                    <View style={CustomStyles.flexColunm, {marginRight: 40}}>
                                        <View style={{flexDirection: 'row', justifyContent: 'flex-start'}}>
                                        <Text style={CustomStyles.mediumFont}>{item.name}</Text>
                                        </View>
                                        <View style={{flexDirection: 'row', justifyContent: 'flex-start'}}>
                                        <Text style={ (item.driver_comment=='') ? CustomStyles.height0 : CustomStyles.smallFont}>{item.user_comment}</Text>
                                        </View>
                                        <View style={{flexDirection: 'row', justifyContent: 'flex-start'}}>
                                        <Rating
                                          type='custom'
                                          ratingColor={Constant.KAppColor}
                                          ratingBackgroundColor='white'
                                          ratingCount={5}
                                          imageSize={20}
                                          startingValue={item.driver_rate}
                                          onFinishRating={this.ratingCompleted}
                                          style={{ marginTop: 5 }}
                                          readonly={true}
                                        />
                                        </View>
                                    </View>

                                </View>

                            </View>
                        </View>
                    }

                  </View>    
                }

                renderSectionHeader={({section: {title}}) => (
                    <View style={ (title=='1') ? CustomStyles.flexRowStyle1 : CustomStyles.flexRowStyle2}>
                      
                      <View style={{ flex: 0.5, flexDirection: 'row', justifyContent: 'center', alignItems:'center',backgroundColor:Constant.KAppColor}}>
                            <Text style={CustomStyles.headerFont}>{this.state.successfullCnt}</Text>
                            <Text style={CustomStyles.headerSubFont}> Successful</Text>
                      </View>

                      <View style={{width:1, backgroundColor:'gray', alignSelf:'center'}}>
                      </View>

                      <View style={{ flex: 0.5, flexDirection: 'row', justifyContent: 'center', alignItems:'center',backgroundColor:Constant.KAppColor}}>
                            <Text style={CustomStyles.headerFont}>{this.state.cancellCnt}</Text>
                            <Text style={CustomStyles.headerSubFont}> Cancelled</Text>
                      </View>
                  </View>
                )}
                sections={[
                  {title: '1', data: ['item1']},
                  {title: '2', data: this.state.ratingList},
                ]}
                keyExtractor={(item, index) => item + index}
          />      
      </View>
      
    );
  }
}


const CustomStyles11 = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000',
    
  },
  flexColunm:{
      flexDirection: 'column',
  },
  flexRow:{
      flexDirection: 'row'
  },
  flexRowStyle1:{
      flexDirection: 'row',
      height: 0,
  },
  flexRowStyle2:{
      flexDirection: 'row',
      height: 45,
      backgroundColor: '#000'
  },
  sectionStyle1: {
    fontWeight: 'bold', 
    height: 0, 
    backgroundColor: 'gray'
  },
  sectionStyle2: {
    fontWeight: 'bold', 
    height: 50, 
    backgroundColor: 'gray'
  },
  avatar: {
    borderRadius: imageSize/2,
    width: imageSize,
    height: imageSize,
    marginTop: 25,
    alignSelf: 'center',
  },
  mediumFont: {
    fontSize: txtMediumSize,
    fontFamily: Constant.KThemeFontRegular,
    color: '#fff',
    alignSelf: 'center',
 },
 smallFont: {
    fontSize: txtSmallSize,
    fontFamily: Constant.KThemeFontRegular,
    color: 'gray',
    alignSelf: 'center',
 },
 margin10: {
  marginTop:10,
 },
 headerFont: {
  fontSize: headerFontSize,
  fontFamily: Constant.KThemeFontSemiBold,
  color: '#000',
},
headerSubFont: {
  fontSize: headerSubFontSize,
  fontFamily: Constant.KThemeFontRegular,
  color: '#000',
},
height0: {
  height:0,
 },
});
