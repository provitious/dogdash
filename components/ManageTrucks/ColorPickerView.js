import React from 'react';
import { StyleSheet, Dimensions, YellowBox, Text, View, Button, TextInput, Image, ScrollView, TouchableOpacity, Alert, SafeAreaView, PixelRatio } from 'react-native';
import {KeyboardAvoidingView, Keyboard, TouchableWithoutFeedback} from 'react-native';
import { NavigationBar } from 'navigationbar-react-native';

import CommonUtilsObj from '../CommonClasses/CommonUtils';
import Constant from '../CommonClasses/Constant';
import CustomStyles from '../CommonClasses/CustomStyles';

import { TriangleColorPicker, ColorPicker, toHsv, fromHsv } from 'react-native-color-picker'

export default class ColorPickerView extends React.Component {

  constructor(props) {
 
    super(props);
 
    YellowBox.ignoreWarnings([
      'Warning: componentWillMount is deprecated',
      'Warning: componentWillReceiveProps is deprecated',
    ]);

    const { navigation } = this.props;  
    const isEdit = navigation.getParam('isEdit', false);

    this.state = {
      visible: false,
      isEdit: false,
      selectedColor: '',
      numberPlateColor: navigation.getParam('defaultColor', '#ffd200')
    };
  }

  onClickDone() {
      this.props.navigation.state.params.returnData(true, this.state.selectedColor);
      this.props.navigation.goBack();
  }

  render() {

    return (

      <View style={CustomStyles.container1}>
            <ColorPicker
              defaultColor={'#00ff81'}
              onColorSelected={color => console.log(`Color selected: ${color}`)}
              onColorChange={color => {
                // let color = 
                //SampleText.replace("Developer", "App Builder");
                this.setState({
                    selectedColor: fromHsv(color),
                });
                console.log('Color selected:' + (fromHsv(color)))
              }}
              style={{flex: 1, margin:20}}
            />

            <TouchableOpacity
                style={[CustomStyles.themeButton, {marginBottom: 30}]}
                onPress={this.onClickDone.bind(this)}
             >
                <Text style={CustomStyles.themeButtonText}> Done </Text>
            </TouchableOpacity>
      </View>
      
    );
  }
}
