import React from 'react';
import { StyleSheet, Dimensions, YellowBox, Text, View, Button, TextInput, Image, ScrollView, TouchableOpacity, Alert, SafeAreaView, PixelRatio } from 'react-native';
import {KeyboardAvoidingView, Keyboard, TouchableWithoutFeedback} from 'react-native';
import { NavigationBar } from 'navigationbar-react-native';
import userDefaults from 'react-native-user-defaults'
import NetInfo from "@react-native-community/netinfo";
import ProgressLoader from 'rn-progress-loader';
import Toast from 'react-native-simple-toast';
import { Dropdown } from 'react-native-material-dropdown';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'
import Geolocation from 'react-native-geolocation-service'; //'@react-native-community/geolocation';

import CommonUtilsObj from '../CommonClasses/CommonUtils';
import Constant from '../CommonClasses/Constant';
import CustomStyles from '../CommonClasses/CustomStyles';
import Message from '../CommonClasses/Message';
import APIManagerObj from '../CommonClasses/APIManager';

const txtInputHeight = (((Dimensions.get('window').width) * 9.8) / 100);
const txtInputFontSize = (((Dimensions.get('window').width) * 3.6) / 100);
var noOfMonths = [{value: '1'}, {value: '2'}, {value: '3'}, {value: '4'}, {value: '5'}, {value: '6'}, {value: '7'}, {value: '8'}, {value: '9'}, {value: '10'}, {value: '11'}, {value: '12'}];
var noOfYears = [];

export default class AddEditTruck extends React.Component {

  constructor(props) {
 
    super(props);
 
    YellowBox.ignoreWarnings([
      'Warning: componentWillMount is deprecated',
      'Warning: componentWillReceiveProps is deprecated',
    ]);

    const { navigation } = this.props;  
    const isEdit = navigation.getParam('isEdit', false);

    if (isEdit) {
        const selectedTruckDetails = navigation.getParam('selectedTruckDetails', 'NO-Value');  
        
        this.state = {
          visible: false,
          connection_Status : "",
          truckMake: selectedTruckDetails.car_make,
          truckMonth: (!selectedTruckDetails.car_month) ? '' : selectedTruckDetails.car_month ,
          truckYear: (!selectedTruckDetails.car_year) ? '' : selectedTruckDetails.car_year,
          licensePlate: selectedTruckDetails.license_plate,
          licensePlateColor: selectedTruckDetails.color,
          truckBrand: selectedTruckDetails.brand,
          truckType: selectedTruckDetails.cartype,
          truckTypeId: selectedTruckDetails.car_type,
          dogCapacity: selectedTruckDetails.seat_capacity,
          truckModel: selectedTruckDetails.model,
          isEdit: true,
          updatedColor: '',
          isUpdated: false,
          selectedTruckDetails: selectedTruckDetails,
          cabList: [],
          currentLat: 0,
          currentLong: 0,
        };
    }
    else {
        this.state = {
          visible: false,
          connection_Status : "",
          truckMake: '',
          truckMonth: '',
          truckYear: '',
          licensePlate: '',
          licensePlateColor: '#ffd200',
          truckBrand: '',
          truckType: 'Type',
          truckTypeId: '',
          dogCapacity: '',
          truckModel: '',
          isEdit: false,
          updatedColor: '',
          isUpdated: false,
          cabList: [],
          currentLat: 0,
          currentLong: 0,
        };
    }

    this.onFocus = this.onFocus.bind(this);
    this.onValidate = this.onValidate.bind(this);
    this.onChangeText = this.onChangeText.bind(this);
    
    this.onSubmitTruckMake = this.onSubmitTruckMake.bind(this);
    this.onSubmitLicensePlate = this.onSubmitLicensePlate.bind(this);
    this.onSubmitTruckBrand = this.onSubmitTruckBrand.bind(this);
    this.onSubmitDogCapacity = this.onSubmitDogCapacity.bind(this);
    this.onSubmitTruckModel = this.onSubmitTruckModel.bind(this);

    this.truckMakeRef = this.updateRef.bind(this, 'truckMake');
    this.licensePlateRef = this.updateRef.bind(this, 'licensePlate');
    this.truckBrandRef = this.updateRef.bind(this, 'truckBrand');
    this.dogCapacityRef = this.updateRef.bind(this, 'dogCapacity');
    this.truckModelRef = this.updateRef.bind(this, 'truckModel');

    let currentYear = new Date().getFullYear();
    
    for(let i = 0; i < 50; i++){
      noOfYears.push({value: currentYear - i});
    } 
  }

  returnData(isUpdated, updatedColor) {
    console.log('updatedColor : ' + updatedColor);
    this.setState({
      updatedColor: updatedColor,
      isUpdated: isUpdated,
    });
  }

  componentDidMount() {
    NetInfo.isConnected.addEventListener(
        'connectionChange',
        this._handleConnectivityChange
    );
   
    NetInfo.isConnected.fetch().done((isConnected) => {
      if(isConnected == true)
      {
        this.setState({connection_Status : "Online"})
        this.onFetchCabList();
      }
      else
      {
        this.setState({connection_Status : "Offline"})
      }
    });

    Geolocation.getCurrentPosition(
              (position) => {
                  console.log('getCurrentPosition ' + position);
                  this.setState({
                    currentLat: position.coords.latitude,
                    currentLong:  position.coords.longitude,
                  });
              },
              (error) => {
                  // See error code charts below.
                  console.log(error.code, error.message);
              },
              { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
        );

    const {navigation} = this.props;
    navigation.addListener ('willFocus', () =>{

      console.log('before color: ' + this.state.updatedColor);
      if (this.state.isUpdated) {
          this.setState({
            isUpdated: false,
            licensePlateColor: this.state.updatedColor,
          });

          console.log('after color : ' + this.state.updatedColor);
      }
    });

  }

  componentWillUnmount() {
    NetInfo.isConnected.removeEventListener(
        'connectionChange',
        this._handleConnectivityChange
    );
  }

  _handleConnectivityChange = (isConnected) => {
    if(isConnected == true)
      {
        this.setState({connection_Status : "Online"})
      }
      else
      {
        this.setState({connection_Status : "Offline"})
      }
  };

  async onFetchCabList() {
    if (this.state.connection_Status == "Online") {

      this.setState({ visible: true });

      let responseData = await APIManagerObj.callApi(Constant.KGetCabListURL);
      console.log('json response ....' + JSON.stringify(responseData));

      if (responseData.status == "success") {
        this.setState({
          visible: false,
          cabList: responseData.Car_Type.map(item => { return {value: item.cartype, cid: item.cab_id} }),
        });         
      }
      else {
        this.setState({
          visible: false,
        }); 
        setTimeout(() => {
          if(typeof(responseData.message) === 'string')
            {
                Toast.showWithGravity(responseData.message, Toast.LONG, Toast.TOP)
            }
          else if(typeof(responseData.error) === 'string')
          {
              Toast.showWithGravity(responseData.error, Toast.LONG, Toast.TOP)
          }
          else {
              if (responseData.error) {
                Toast.showWithGravity(responseData.error.toString(), Toast.LONG, Toast.TOP)
              }
              else {
                Toast.showWithGravity(Message.KAPIError, Toast.LONG, Toast.TOP)
              }
          }
        }, 200); 
      }
    }
    else {
      this.setState({lastApiTag : 1})
      Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
    }
  }

  updateRef(name, ref) {
    this[name] = ref;
  }

  onFocus() {
    let { errors = {} } = this.state;

    for (let name in errors) {
      let ref = this[name];

      if (ref && ref.isFocused()) {
        delete errors[name];
      }
    }

    this.setState({ errors });
  }

  onChangeText(text) {
    ['truckMake', 'licensePlate', 'truckBrand', 'dogCapacity', 'truckModel']
      .map((name) => ({ name, ref: this[name] }))
      .forEach(({ name, ref }) => {
        if (ref.isFocused()) {
          this.setState({ [name]: text });
          if (name == 'dogCapacity') {
              this.setState({ [name]: text.replace(/[^0-9]/, '') });
          }

          if (name == 'truckMake') {
            if (!text.replace(/\s/g, '').length) {
              this.setState({ [name]: text.trim() });
            }
          }

          if (name == 'licensePlate') {
            if (!text.replace(/\s/g, '').length) {
              this.setState({ [name]: text.trim() });
            }
          }

          if (name == 'truckBrand') {
            if (!text.replace(/\s/g, '').length) {
              this.setState({ [name]: text.trim() });
            }
          }

          if (name == 'truckModel') {
            if (!text.replace(/\s/g, '').length) {
              this.setState({ [name]: text.trim() });
            }
          }
        }
      });
  }

  onSubmitTruckMake(text) {
      
    this.licensePlate.focus();
  }

  onSubmitLicensePlate(text) {
    this.truckBrand.focus();
  }

  onSubmitTruckBrand(text) {
    this.dogCapacity.focus();
  }

  onSubmitDogCapacity(text) {
    this.truckModel.focus();
  }

  onSubmitTruckModel(text) {
    this.truckModel.blur();
  }

  dismissKeyboard() {
    this.truckMake.blur();
    this.licensePlate.blur();
    this.truckBrand.blur();
    this.dogCapacity.blur();
    this.truckModel.blur();
  }

  onValidate() {
    this.dismissKeyboard();

    var flag = true;
    var errorMsg = [];

    let truckMakeVal = this.state.truckMake
    if (!truckMakeVal) {
        errorMsg.push('Make should not be empty')
        flag = false;
    }

    if (this.state.truckMonth == '') {
        errorMsg.push('Select month')
        flag = false;
    }

    if (this.state.truckYear == '') {
        errorMsg.push('Select year')
        flag = false;
    }

    let licensePlatVal = this.state.licensePlate
    if (!licensePlatVal) {
        errorMsg.push('License plate should not be empty')
        flag = false;
    }

    let truckBrandVal = this.state.truckBrand
    if (!truckBrandVal) {
        errorMsg.push('Brand should not be empty')
        flag = false;
    }

    // if (this.state.truckType == 'Type') {
    //     errorMsg.push('Select type')
    //     flag = false;
    // }

    let dogCapacityVal = this.state.dogCapacity
    if (!dogCapacityVal) {
        errorMsg.push('Dog loading capacity should not be empty')
        flag = false;
    }

    let truckModelVal = this.state.truckModel
    if (!truckModelVal) {
        errorMsg.push('Model should not be empty')
        flag = false;
    }

    if (flag) {
      if (this.state.connection_Status == "Online") {
        if (this.state.isEdit) {
            this.onEditCard();
        }
        else {
            this.onAddTruck();
        } 
      }
      else {
        Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
      }
    }
    else {
      var errMsg = '';
        if (errorMsg.length > 2) {
            errMsg = 'Please fill all the require details.';
        }
        else {
            if (errorMsg.length == 2) {
              errMsg = errorMsg[0] + ', ' + errorMsg[1]; 
            }
            else {
              errMsg = errorMsg[0]; 
            }
            
        }
        // Toast.showWithGravity(errorMsg.toString(), Toast.LONG, Toast.TOP)
        Toast.showWithGravity(errMsg, Toast.LONG, Toast.TOP)
    }
  }

  async onAddTruck() {
    if (this.state.connection_Status == "Online") {

      this.setState({ visible: true });
      var data = new FormData();
      data.append('driver_id', CommonUtilsObj.userDetails.id);
      data.append('license_plate', this.state.licensePlate);
      data.append('brand', this.state.truckBrand);
      data.append('model', this.state.truckModel);
      data.append('color', this.state.licensePlateColor);
      data.append('car_make', this.state.truckMake);
      data.append('car_year', this.state.truckYear);
      data.append('car_month', this.state.truckMonth);
      // data.append('car_type', this.state.truckTypeId);
      data.append('car_type', '13');
      data.append('transmission_type', 'manual');
      data.append('seat_capacity', this.state.dogCapacity);

      let responseData = await APIManagerObj.callApiWithData(Constant.KAddTruckDetailsURL, data);
      console.log('json response ....' + JSON.stringify(responseData));

      if (responseData.status == "success") {
        this.setState({
              visible: false
          });

          setTimeout(() => {
            Toast.showWithGravity(responseData.message, Toast.LONG, Toast.TOP)
            this.props.navigation.state.params.returnData(true);
            this.props.navigation.goBack();
            this.onChangeDriverSocketStatus();
          }, 200);         
      }
      else {
        this.setState({
          visible: false,
        }); 
        setTimeout(() => {
          if(typeof(responseData.message) === 'string')
          {
              Toast.showWithGravity(responseData.message, Toast.LONG, Toast.TOP)
          }
          else if(typeof(responseData.error) === 'string')
          {
              Toast.showWithGravity(responseData.error, Toast.LONG, Toast.TOP)
          }
          else {
              if (responseData.error) {
                Toast.showWithGravity(responseData.error.toString(), Toast.LONG, Toast.TOP)
              }
              else {
                Toast.showWithGravity(Message.KAPIError, Toast.LONG, Toast.TOP)
              }
          }
        }, 200); 
      }
    }
    else {
      this.setState({lastApiTag : 1})
      Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
    }
  }

  async onEditCard() {
    if (this.state.connection_Status == "Online") {

      this.setState({ visible: true });
      var data = new FormData();
      data.append('driver_id', CommonUtilsObj.userDetails.id);
      data.append('license_plate', this.state.licensePlate);
      data.append('brand', this.state.truckBrand);
      data.append('model', this.state.truckModel);
      data.append('color', this.state.licensePlateColor);
      data.append('car_make', this.state.truckMake);
      data.append('car_year', this.state.truckYear);
      data.append('car_month', this.state.truckMonth);
      // data.append('car_type', this.state.truckTypeId);
      data.append('car_type', '13');
      data.append('transmission_type', 'manual');
      data.append('seat_capacity', this.state.dogCapacity);
      data.append('car_id', this.state.selectedTruckDetails.id);

      let responseData = await APIManagerObj.callApiWithData(Constant.KEditTruckDetailsURL, data);
      console.log('json response ....' + JSON.stringify(responseData));

      if (responseData.status == "success") {
        this.setState({
              visible: false
          });

          setTimeout(() => {
            Toast.showWithGravity(responseData.message, Toast.LONG, Toast.TOP)
            this.props.navigation.state.params.returnData(true);
            this.props.navigation.goBack();
            this.onChangeDriverSocketStatus();
          }, 200);         
      }
      else {
        this.setState({
          visible: false,
        }); 
        setTimeout(() => {
          if(typeof(responseData.message) === 'string')
          {
              Toast.showWithGravity(responseData.message, Toast.LONG, Toast.TOP)
          }
          else if(typeof(responseData.error) === 'string')
          {
              Toast.showWithGravity(responseData.error, Toast.LONG, Toast.TOP)
          }
          else {
              if (responseData.error) {
                Toast.showWithGravity(responseData.error.toString(), Toast.LONG, Toast.TOP)
              }
              else {
                Toast.showWithGravity(Message.KAPIError, Toast.LONG, Toast.TOP)
              }
          }
        }, 200); 
      }
    }
    else {
      this.setState({lastApiTag : 1})
      Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
    }
  }

  async onChangeDriverSocketStatus() {
    if (this.state.connection_Status == "Online") {

      this.setState({ visible: true });
      var data = new FormData();
      data.append('driverId', CommonUtilsObj.userDetails.id);
      data.append('socket_status', 'Active');
      data.append('flag', '2');
      data.append('lat', this.state.currentLat);
      data.append('long', this.state.currentLong);
      data.append('driverName', CommonUtilsObj.userDetails.name);
      data.append('seat_capacity', CommonUtilsObj.cabDetails.seat_capacity);
      data.append('is_pool', CommonUtilsObj.cabDetails.is_pool);
      data.append('cartype', CommonUtilsObj.cabDetails.cab_id);
      data.append('driverstatus', '1');

      let responseData = await APIManagerObj.callApiWithData(Constant.KManageDriverStatusURL, data);
      console.log('json response ....' + JSON.stringify(responseData));

      if (responseData.status == "success") {
        this.setState({
              visible: false
          });        
      }
      else {
        this.setState({
          visible: false,
        }); 
         
      }
    }
    else {
      this.setState({lastApiTag : 1})
    }
  }


  onClickColorPicker() {
      this.props.navigation.navigate('ColorPickerViewScreen', {returnData: this.returnData.bind(this), isEdit: this.state.isEdit, defaultColor: this.state.licensePlateColor});
  }

  render() {

    return (

            <View style={CustomStyles.container}>
                <View
                style={{backgroundColor: "#fff", justifyContent: 'center', alignItems: 'center'}}>

                <ProgressLoader
                  visible={this.state.visible}
                  isModal={true} isHUD={true}
                  hudColor={"#fff"}
                  height={200}
                  width={200}
                  color={"#8edf01"} />
                </View>

                <KeyboardAwareScrollView style = {[CustomStyles.flexColunm, {flex:1}]}>

                    <Image resizeMode='contain' style={{alignSelf: 'center', height: 100, width: 150, marginTop: 20}} source={require('../../images/deliveryVanGray.png')} />

                    <View style={{backgroundColor: this.state.licensePlateColor, width: (Dimensions.get('window').width) - 60, alignSelf: 'center', marginTop: 20, borderRadius:8}}>
                        <Image resizeMode='stretch' style={{height: 70, width: (Dimensions.get('window').width) - 60}} source={require('../../images/numberPlate.png')} />
                        <Text style={{position: 'absolute', marginTop: 30, marginLeft: 30, fontSize: txtInputFontSize, fontFamily: Constant.KThemeFontRegular}}> {this.state.licensePlate} </Text>
                    </View>

                    { (this.state.truckMake != '') &&
                      <Text style={[CustomStyles.labelText, {marginLeft: 20}]}>Make</Text>
                    }
                    <TextInput
                      style={[CustomStyles.textInput,{marginBottom: 20, margin: 20}]}
                      ref={this.truckMakeRef}
                      placeholder = "Make" 
                      keyboardType='default'
                      autoCapitalize='sentences'
                      autoCorrect={true}
                      enablesReturnKeyAutomatically={true}
                      onFocus={this.onFocus}
                      onChangeText={this.onChangeText}
                      onSubmitEditing={this.onSubmitTruckMake}
                      returnKeyType='next'
                      value= {this.state.truckMake}
                    />


                    <View style={{flexDirection: 'row', marginLeft: 20, marginRight: 20, justifyContent:'space-between'}}>
                      <View style={{flexDirection: 'column'}}>
                        <Text style={[CustomStyles.labelText, {marginBottom: 10, marginTop: 0}]}>Month</Text>
                        <Dropdown
                          data={noOfMonths}
                          containerStyle = {{width: ((Dimensions.get('window').width)/2)-30, borderBottomColor: 'transparent', backgroundColor: 'white', borderRadius: 5, borderWidth: 0.5, height: txtInputHeight, paddingLeft : 10, paddingRight : 10}}
                          dropdownOffset = {{top: 7, left: 0}}
                          value = {this.state.truckMonth}
                          // onFocus={()=>{this.cardHolderName.blur(); this.cardNumber.blur();}}
                          dropdownPosition={0}
                          onChangeText={(value)=>{this.setState({ truckMonth: value })}}
                        />
                      </View>
                      <View style={{flexDirection: 'column'}}>
                        <Text style={[CustomStyles.labelText, {marginBottom: 10, marginTop: 0}]}>Year</Text>
                        <Dropdown
                          data={noOfYears}
                          containerStyle = {{width: ((Dimensions.get('window').width)/2)-30, borderBottomColor: 'transparent', backgroundColor: 'white', borderRadius: 5, borderWidth: 0.5, height: txtInputHeight, paddingLeft : 10, paddingRight : 10}}
                          dropdownOffset = {{top: 7, left: 0}}
                          value = {this.state.truckYear}
                          // onFocus={()=>{this.cardHolderName.blur(); this.cardNumber.blur();}}
                          dropdownPosition={0}
                          onChangeText={(value)=>{this.setState({ truckYear: value })}}
                        />
                      </View>
                    </View>

                    { (this.state.licensePlate != '') &&
                      <Text style={[CustomStyles.labelText, {marginLeft: 20, marginBottom: -10}]}>License plate</Text>
                    }
                    <View style={{flexDirection: 'row', margin: 20, justifyContent:'space-between'}}>
                        <TextInput
                          style={[CustomStyles.textInput, {marginLeft:0, width: (Dimensions.get('window').width)-110, marginTop: 2}]}
                          ref={this.licensePlateRef}
                          placeholder = "License Plate" 
                          keyboardType='default'
                          autoCapitalize='sentences'
                          autoCorrect={true}
                          enablesReturnKeyAutomatically={true}
                          onFocus={this.onFocus}
                          onChangeText={this.onChangeText}
                          onSubmitEditing={this.onSubmitLicensePlate}
                          returnKeyType='next'
                          value= {this.state.licensePlate}
                        />
                        <View style={{backgroundColor: this.state.licensePlateColor, height: txtInputHeight, width: 40}}>
                           

                            <TouchableOpacity
                              onPress={this.onClickColorPicker.bind(this)}
                              style={{backgroundColor: 'transparent', height: txtInputHeight, width: 40}}
                            >
                            </TouchableOpacity>

                        </View>
                    </View>

                    { (this.state.truckBrand != '') &&
                      <Text style={[CustomStyles.labelText, {marginLeft: 20, marginBottom: 10, marginTop: 0}]}>Brand</Text>
                    }
                    <TextInput
                      style={[CustomStyles.textInput,{marginHorizontal: 20, marginTop: 0, marginBottom: 20}]}
                      ref={this.truckBrandRef}
                      placeholder = "Brand" 
                      keyboardType='default'
                      autoCapitalize='sentences'
                      autoCorrect={true}
                      enablesReturnKeyAutomatically={true}
                      onFocus={this.onFocus}
                      onChangeText={this.onChangeText}
                      onSubmitEditing={this.onSubmitTruckBrand}
                      returnKeyType='next'
                      value= {this.state.truckBrand}
                    />
                    {
                    // <Dropdown
                    //   data={this.state.cabList}
                    //   containerStyle = {{width: ((Dimensions.get('window').width))-40, borderBottomColor: 'transparent', backgroundColor: 'white', borderRadius: 5, borderWidth: 0.5, height: txtInputHeight, paddingLeft : 10, paddingRight : 10, marginLeft:20}}
                    //   dropdownOffset = {{top: 7, left: 0}}
                    //   value = {this.state.truckType}
                    //   // onFocus={()=>{this.cardHolderName.blur(); this.cardNumber.blur();}}
                    //   dropdownPosition={0}
                    //   // onChangeText={(value)=>{this.setState({ truckType: value })}}
                    //   onChangeText={(value, cid)=>{
                    //     this.setState({ 
                    //       truckType: value,
                    //       truckTypeId: this.state.cabList[cid].cid, 
                    //     });
                    //     console.log(this.state.cabList[cid].cid)}
                    //   }
                    // />
                  }

                    { (this.state.dogCapacity != '') &&
                      <Text style={[CustomStyles.labelText, {marginLeft: 20, marginBottom: 10, marginTop: 0}]}>Dog capacity</Text>
                    }
                    <TextInput
                      style={[CustomStyles.textInput,{marginBottom: 20, marginTop: 0, marginHorizontal: 20}]}
                      ref={this.dogCapacityRef}
                      placeholder = "Dog Capacity" 
                      keyboardType='number-pad'
                      autoCapitalize='sentences'
                      autoCorrect={true}
                      enablesReturnKeyAutomatically={true}
                      onFocus={this.onFocus}
                      onChangeText={this.onChangeText}
                      onSubmitEditing={this.onSubmitDogCapacity}
                      returnKeyType='next'
                      value= {this.state.dogCapacity}
                    />

                    { (this.state.truckModel != '') &&
                      <Text style={[CustomStyles.labelText, {marginLeft: 20, marginBottom: 10, marginTop: 0}]}>Model</Text>
                    }
                    <TextInput
                      style={[CustomStyles.textInput,{marginBottom: 20, marginHorizontal: 20, marginTop:0}]}
                      ref={this.truckModelRef}
                      placeholder = "Model" 
                      keyboardType='default'
                      autoCapitalize='sentences'
                      autoCorrect={true}
                      enablesReturnKeyAutomatically={true}
                      onFocus={this.onFocus}
                      onChangeText={this.onChangeText}
                      onSubmitEditing={this.onSubmitTruckModel}
                      returnKeyType='next'
                      value= {this.state.truckModel}
                    />

                <TouchableOpacity
                  style={[CustomStyles.themeButton, {width: '90%'}]}
                  onPress={this.onValidate.bind(this)}
                >
                  <Text style={CustomStyles.themeButtonText}> Save </Text>
                </TouchableOpacity>
                
                </KeyboardAwareScrollView>
            

                
      </View>
      
    );
  }
}

