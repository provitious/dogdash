import React, { Component } from 'react';
import { StyleSheet, Platform, View, Text, Image, ScrollView, Dimensions, Alert, Share, TouchableOpacity} from 'react-native';
// import Icon from 'react-native-vector-icons/FontAwesome';
import 'react-native-gesture-handler';
import { StackActions, NavigationActions } from 'react-navigation';
import EventBus from 'react-native-event-bus'
import NetInfo from "@react-native-community/netinfo";

import Constant from '../CommonClasses/Constant';
import CommonUtilsObj from '../CommonClasses/CommonUtils';
import Toast from 'react-native-simple-toast';

const profileFontSize = (((Dimensions.get('window').width) * 5) / 100);
const menuFontSize = (((Dimensions.get('window').width) * 3.7) / 100);

class Custom_Side_Menu extends Component {

  constructor(props) {
      super(props);
      this.state = {
        sourceUrl: { uri: Constant.KDriverImagePathURL + CommonUtilsObj.userDetails.image },
        name: CommonUtilsObj.userDetails.name,
        connection_Status : "",
      }

      console.log('......' + Constant.KDriverImagePathURL + CommonUtilsObj.userDetails.image)
  }

  componentDidMount() {
    
    NetInfo.isConnected.addEventListener(
        'connectionChange',
        this._handleConnectivityChange

    );
   
    NetInfo.isConnected.fetch().done((isConnected) => {

      if(isConnected == true)
      {
        this.setState({connection_Status : "Online"})
      }
      else
      {
        this.setState({connection_Status : "Offline"})
      }

    });

    const {navigation} = this.props;
    navigation.addListener ('willFocus', () =>{

      this.setState({
          sourceUrl: { uri: Constant.KDriverImagePathURL + CommonUtilsObj.userDetails.image },
        name: CommonUtilsObj.userDetails.name,
      });
    });

    EventBus.getInstance().addListener("ProfileChangeEvent", this.listener = data => {
        // handle the event
        this.setState({
          sourceUrl: { uri: Constant.KDriverImagePathURL + CommonUtilsObj.userDetails.image },
          name: CommonUtilsObj.userDetails.name,
      });
    })
  }

  componentWillUnmount() {
 
    NetInfo.isConnected.removeEventListener(
        'connectionChange',
        this._handleConnectivityChange
    );
 
  }

  _handleConnectivityChange = (isConnected) => {

    if(isConnected == true)
      {
        this.setState({connection_Status : "Online"})
      }
      else
      {
        this.setState({connection_Status : "Offline"})
      }
  };

  onShare = async () => {
    console.log("share called...")
    try {
      const result = await Share.share({
        message:
          'You have been invited to join the DogDash community where delivering dogs and tracking driver instantly at your finger tips. Download our free app for your favourite OS. \n iTunes : https://dogdash.com/ios \n Playstore : https://dogdash.com/android',
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };

  async onChangeDriverSocketStatus() {

    if (this.state.connection_Status == "Online") {
    //   this.setState({
    //     visible: true,
    // });

    var data = new FormData();
    data.append('driverId', CommonUtilsObj.userDetails.id);
    data.append('socket_status', 'Inactive');
    data.append('flag', '2');
    data.append('lat', CommonUtilsObj.driverLatitude);
    data.append('long', CommonUtilsObj.driverLongitude);
    data.append('driverName', CommonUtilsObj.userDetails.name);
    data.append('seat_capacity', CommonUtilsObj.cabDetails.seat_capacity);
    data.append('is_pool', CommonUtilsObj.cabDetails.is_pool);
    data.append('cartype', CommonUtilsObj.cabDetails.cab_id);
    data.append('driverstatus', '1');
    // data.append('geometry', {"type": "point", "coordinates": [this.state.currentLatitude, this.state.currentLongitude]});

    console.log(data)
    console.log(Constant.KManageDriverStatusURL)
    try {
        let response = await fetch(
          Constant.KManageDriverStatusURL,
          {
            method: "POST",
            headers: {
             "Accept": "application/json",
             // "Content-Type": "application/json"
             'Content-Type': 'multipart/form-data',
            },
            body: data //JSON.stringify(data)
         }
        )
    .then((response) => response.json())
    .then((responseData) => {
       console.log('json response ....' + JSON.stringify(responseData));
       if (responseData.status == "success") {
          this.setState({
            visible: false,
          });         

          // let obj = CommonUtilsObj.loggedUserData;
          // obj.Driver_detail[0].socket_status = status;
          // CommonUtilsObj.setLoggedUserDetails(obj);
          // if (Platform.OS==='android') {
          //     CommonUtilsObj.setLoggedUserDetails(JSON.stringify(obj));
          // }
          // else {
          //     CommonUtilsObj.setLoggedUserDetails(obj);
          // }

          // setTimeout(() => {
          //   if (status == 'Active') {
          //     Toast.showWithGravity('Online mode set', Toast.LONG, Toast.TOP)
          //   }
          //   else {
          //     Toast.showWithGravity('Offline mode set', Toast.LONG, Toast.TOP)
          //   }
          // }, 200);
        }
        else {
          this.setState({
              visible: false,
          });

          setTimeout(() => {
              Toast.showWithGravity(responseData.error, Toast.LONG, Toast.TOP)
          }, 200);
        }
    })
    .done();
 
    } catch (errors) {
      this.setState({
          visible: false,
      });
      let err = "Error is" + errors
      setTimeout(() => {
          Toast.showWithGravity(err, Toast.LONG, Toast.TOP)
      }, 100);
    } 
    }
    else {
      Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
    }
    
  }

  async onLogout() {
        if (this.state.connection_Status == "Online") {
          var data = new FormData();
        data.append('driver_id', CommonUtilsObj.userDetails.id);

        console.log(data);
        try {
            let response = await fetch(
              Constant.KLogoutURL,
              {
                method: "POST",
                headers: {
                 "Accept": "application/json",
                 // "Content-Type": "application/json"
                 'Content-Type': 'multipart/form-data',
                },
                body: data//JSON.stringify(data)
             }
            )
        .then((response) => response.json())
        .then((responseData) => {
           console.log('json response ....' + JSON.stringify(responseData));

           if (responseData.status == "success") {
                CommonUtilsObj.setLoginState(Constant.KLogout);

                Toast.showWithGravity('Logout successfully!', Toast.LONG, Toast.TOP)
            }
            else {
              setTimeout(() => {
                if(typeof(responseData.message) === 'string')
                {
                    Toast.showWithGravity(responseData.message, Toast.LONG, Toast.TOP)
                }
                else if(typeof(responseData.error) === 'string')
                {
                    Toast.showWithGravity(responseData.error, Toast.LONG, Toast.TOP)
                }
                else {
                    if (responseData.error) {
                      Toast.showWithGravity(responseData.error.toString(), Toast.LONG, Toast.TOP)
                    }
                    else {
                      Toast.showWithGravity(Message.KAPIError, Toast.LONG, Toast.TOP)
                    }
                }
              }, 200);
            }
        })
        .done();
     
        } catch (errors) {
          
          let err = "Error is" + errors
          Toast.showWithGravity(err, Toast.LONG, Toast.TOP)
        }
        }
        else {
          Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
        }
         
  }

  render() {

    return (

      <View style={styles.sideMenuContainer}>
        <ScrollView style = {{width: Dimensions.get('window').width - 130}}>
       
            <TouchableOpacity
                onPress={() => {  
                const navigateAction = StackActions.reset({
                  index: 0,
                  actions: [NavigationActions.navigate({ routeName: "SettingsScreen"})],
                });
                this.props.navigation.dispatch(navigateAction);
            }}
             >
                <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 10}}>
                    <Image source={this.state.sourceUrl}
                      style={styles.sideMenuProfileIcon} />
                    <Text style={styles.profileText} numberOfLines={4} ellipsizeMode='tail' >{this.state.name}</Text>
                </View>
            </TouchableOpacity>

            

        <View style={{ width: '100%', height: 1, backgroundColor: '#000', marginTop: 15}} />

        <View style={{width: '100%'}}>

            
              <TouchableOpacity
                  onPress={() => {  
                  const navigateAction = StackActions.reset({
                    index: 0,
                    actions: [NavigationActions.navigate({ routeName: "First"})],
                  });
                  this.props.navigation.dispatch(navigateAction);}}
               >
                  <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 10}}>
                      <Image source={require('../../images/home.png')}
                        style={styles.sideMenuIcon} />
                  
                      <Text style={styles.menuText}  >Home</Text>
                  </View>
              </TouchableOpacity>

            <TouchableOpacity
                onPress={() => {  
                const navigateAction = StackActions.reset({
                  index: 0,
                  actions: [NavigationActions.navigate({ routeName: "RequestScreen"})],
                });
                this.props.navigation.dispatch(navigateAction);
            }}
             >
                <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 10}}>
                    <Image source={require('../../images/requests.png')}
                      style={styles.sideMenuIcon} />
                
                    <Text style={styles.menuText}>All Requests</Text>
                </View>
            </TouchableOpacity>


            <TouchableOpacity
                onPress={() => {  
                const navigateAction = StackActions.reset({
                  index: 0,
                  actions: [NavigationActions.navigate({ routeName: "ReviewScreen"})],
                });
                this.props.navigation.dispatch(navigateAction);
            }}
             >
                <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 10}}>
                    <Image source={require('../../images/review.png')}
                      style={styles.sideMenuIcon} />
                
                    <Text style={styles.menuText}>My Review</Text>
                </View>
            </TouchableOpacity>

            <TouchableOpacity
                onPress={() => {  
                const navigateAction = StackActions.reset({
                  index: 0,
                  actions: [NavigationActions.navigate({ routeName: "WalletScreen"})],
                });
                this.props.navigation.dispatch(navigateAction);
            }}
             >
                <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 10}}>
                    <Image source={require('../../images/wallet.png')}
                      style={styles.sideMenuIcon} />
                
                    <Text style={styles.menuText}>Wallet</Text>
                </View>
            </TouchableOpacity>

            <TouchableOpacity
                onPress={() => {  
                const navigateAction = StackActions.reset({
                  index: 0,
                  actions: [NavigationActions.navigate({ routeName: "TruckListScreen"})],
                });
                this.props.navigation.dispatch(navigateAction);
            }}
             >
                <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 10}}>
                    <Image source={require('../../images/manageTruck.png')}
                      style={styles.sideMenuIcon} />
                
                    <Text style={styles.menuText}  >Manage Truck</Text>
                </View>
            </TouchableOpacity>

            <TouchableOpacity
                onPress={() => {  
                const navigateAction = StackActions.reset({
                  index: 0,
                  actions: [NavigationActions.navigate({ routeName: "SettingsScreen"})],
                });
                this.props.navigation.dispatch(navigateAction);
            }}
             >
                <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 10}}>
                    <Image source={require('../../images/settings.png')}
                      style={styles.sideMenuIcon} />
                
                    <Text style={styles.menuText}>Setting</Text>
                </View>
            </TouchableOpacity>

            <TouchableOpacity
                onPress={() => {  
                const navigateAction = StackActions.reset({
                  index: 0,
                  actions: [NavigationActions.navigate({ routeName: "CardListScreen"})],
                });
                this.props.navigation.dispatch(navigateAction);
            }}
             >
                <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 10}}>
                    <Image source={require('../../images/payment.png')}
                      style={styles.sideMenuIcon} />
                
                    <Text style={styles.menuText}  >Manage Payment</Text>
                </View>
            </TouchableOpacity>

            <TouchableOpacity
                onPress={() => {  
                const navigateAction = StackActions.reset({
                  index: 0,
                  actions: [NavigationActions.navigate({ routeName: "AccountListScreen"})],
                });
                this.props.navigation.dispatch(navigateAction);
            }}
             >
                <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 10}}>
                    <Image source={require('../../images/payment.png')}
                      style={styles.sideMenuIcon} />
                
                    <Text style={styles.menuText}  >Manage Account</Text>
                </View>
            </TouchableOpacity>

            <TouchableOpacity
                onPress={() => { this.onShare() }}
             >
                <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 10}}>
                    <Image source={require('../../images/invite.png')}
                      style={styles.sideMenuIcon} />
                
                    <Text style={styles.menuText}>Invite</Text>
                </View>
            </TouchableOpacity>

            

            <TouchableOpacity
                onPress={() => {  
                const navigateAction = StackActions.reset({
                  index: 0,
                  actions: [NavigationActions.navigate({ routeName: "PrivacyPolicyScreen"})],
                });
                this.props.navigation.dispatch(navigateAction);
            }}
             >
                <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 10}}>
                    <Image source={require('../../images/privacyPolicy.png')}
                      style={styles.sideMenuIcon} />
                
                    <Text style={styles.menuText}>Privacy Policy</Text>
                </View>
            </TouchableOpacity>

            <TouchableOpacity
                onPress={() => {  
                const navigateAction = StackActions.reset({
                  index: 0,
                  actions: [NavigationActions.navigate({ routeName: "AboutUsScreen"})],
                });
                this.props.navigation.dispatch(navigateAction);
            }}
             >
                <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 10}}>
                    <Image source={require('../../images/aboutUs.png')}
                      style={styles.sideMenuIcon} />
                
                    <Text style={styles.menuText}>About Us</Text>
                </View>
            </TouchableOpacity>

            <TouchableOpacity
                onPress={() => {  
                const navigateAction = StackActions.reset({
                  index: 0,
                  actions: [NavigationActions.navigate({ routeName: "SupportScreen"})],
                });
                this.props.navigation.dispatch(navigateAction);
            }}
             >
                <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 10}}>
                    <Image source={require('../../images/support.png')}
                      style={styles.sideMenuIcon} />
                
                    <Text style={styles.menuText}>Support</Text>
                </View>
            </TouchableOpacity>

            <TouchableOpacity
                onPress={() => {  
                const navigateAction = StackActions.reset({
                  index: 0,
                  actions: [NavigationActions.navigate({ routeName: "TermsConditionsScreen"})],
                });
                this.props.navigation.dispatch(navigateAction);
            }}
             >
                <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 10}}>
                    <Image source={require('../../images/privacyPolicy.png')}
                      style={styles.sideMenuIcon} />
                
                    <Text style={styles.menuText}>Terms & Conditions</Text>
                </View>
            </TouchableOpacity>

            <TouchableOpacity
                 onPress={() => 
                { Alert.alert(
                  Constant.KAppTitle,
                  'Are you sure, you want to logout?',
                  [
                    {text: 'Yes', onPress: () => {
                      if (this.state.connection_Status == "Online") {
                        this.onChangeDriverSocketStatus()
                        this.onLogout();
                         
                      }
                      else {
                        Toast.showWithGravity(Constant.KCheckInternetConnection, Toast.LONG, Toast.TOP)
                      }
                      
                    }},
                    {
                      text: 'No',
                      onPress: () => console.log('Cancel Pressed'),
                      style: 'cancel',
                    },
                  ],
                  {cancelable: false},
                );
                }} 
             >
                <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 10}}>
                    <Image source={require('../../images/logout.png')}
                      style={styles.sideMenuIcon} />
                
                    <Text style={styles.menuText}>Logout</Text>
                </View>
            </TouchableOpacity>

       </View>

       <View style={{ width: '100%', height: 1, backgroundColor: '#000', marginTop: 15}} />

       </ScrollView>
      </View>
    );
  }
}

export default Custom_Side_Menu;

const styles = StyleSheet.create({

  sideMenuContainer: {

    width: '100%',
    height: '100%',
    backgroundColor: '#000',
    alignItems: 'center',
    paddingTop: 20
  },
  profileText:{
    color: '#fff',
    marginTop: 15,
    marginLeft : 10,
    marginRight : 10,
    fontSize: profileFontSize,
    fontFamily: Constant.KThemeFontSemiBold,
    flex: 1,
  },

  sideMenuProfileIcon:
  {
    // resizeMode: 'center',
    width: 80, 
    height: 80, 
    borderRadius: 80/2,
    marginTop: 15,
    marginLeft: 15
  },

  sideMenuIcon:
  {
    resizeMode: 'stretch',
    width: 22, 
    height: 20, 
    marginRight: 10,
    marginLeft: 20,
    
  },

  menuText:{
    marginTop: 10,
    marginBottom: 10,
    color: '#fff',
    fontSize: menuFontSize,
    fontFamily: Constant.KThemeFontRegular,
  },

  
});