import React, { Component } from 'react';
import { Dimensions } from 'react-native';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import FirstActivity_StackNavigator from '../Navigations/FirstActivity_StackNavigator';
import Custom_Side_Menu from './Custom_Side_Menu';
import { DrawerNavigator, createDrawerNavigator } from 'react-navigation-drawer';
import 'react-native-gesture-handler';

const MyDrawer_Navigator = createDrawerNavigator({
  MainStack: {
    screen: FirstActivity_StackNavigator
  },
  SecondStack: {
    screen: FirstActivity_StackNavigator
  },
  ThirdStack: {
    screen: FirstActivity_StackNavigator
  }
  },
  {
    contentComponent: Custom_Side_Menu,
    drawerWidth: Dimensions.get('window').width - 130,
  });

const MyDrawerNavigator = createAppContainer(MyDrawer_Navigator);
export default MyDrawerNavigator