import React from 'react';
import { View, TouchableOpacity, TextInput, StyleSheet } from 'react-native';
import SignInStack from './SignInStack';
import Icon from 'react-native-vector-icons/FontAwesome';
import DrawerContent from './DrawerContent';
import HomeScreen from '../screens/HomeScreen';
import RankingScreen from '../screens/RankingScreen';
// import { createAppContainer } from 'react-navigation';
import { createStackNavigator, StackNavigator } from 'react-navigation-stack';
import { createDrawerNavigator } from 'react-navigation-drawer';
import { createAppContainer } from 'react-navigation';
import { DrawerNavigator, DrawerActions } from 'react-navigation-drawer';
import { withNavigation } from 'react-navigation'
import SignInScreen from '../screens/SignInScreen';


const DrawerNavigation = createDrawerNavigator(
  {
    HomeScreen: { screen: HomeScreen },
    RankingScreen: { screen: RankingScreen },
  },
  {
    contentComponent: DrawerContent,
    drawerPosition: 'left',
    drawerWidth: 200,
  }
);

const MainStack = createStackNavigator(
  {
    DrawerNavigation: DrawerNavigation,
  },
  {

    navigationOptions: ({ navigation }) => ({
      headerLeft: (
        <TouchableOpacity
          onPress={() => {
            navigation.dispatch(DrawerActions.toggleDrawer())
          }}
        >
          <Icon
            name="bars"
            size={20}
            color="#000"
            style={styles.headerLeftIconStyle}
          />
        </TouchableOpacity>
      ),
      headerTitle: (
        <View style={styles.searchInputContainer}>
          <Icon
            name="search"
            size={20}
            color="#aaa"
            style={styles.searchInputIconStyle}
          />
          <TextInput
            style={styles.searchInputStyle}
            underlineColorAndroid="transparent"
            placeholder="search"
          />
        </View>
      ),
      drawerLockMode: 'locked-open',
      
    }),
    
    headerMode: 'float',
    transitionConfig: TransitionConfig,
  
  }
);

let TransitionConfig = () => {
  return {
    screenInterpolator: ({ position, scene }) => {
      const opacity = position.interpolate({
        inputRange: [scene.index - 1, scene.index],
        outputRange: [0, 1],
      });
      return {
        opacity: opacity,
      };
    },
  };
};

const RootStackNavigator = createStackNavigator(
  {
    SignInStack: { screen: SignInStack },
    MainStack: { screen: MainStack },
  },
  {
    headerMode: 'screen',
    transitionConfig: TransitionConfig,
  }
);

const styles = StyleSheet.create({
  headerLeftIconStyle: {
    marginLeft: 15,
  },
  searchInputContainer: {
    flex: 1,
    borderWidth: 1,
    borderRadius: 20,
    borderColor: '#999',
    flexDirection: 'row',
    backgroundColor: 'white',
  },
  searchInputIconStyle: {
    padding: 5,
  },
  searchInputStyle: {
    flex: 1,
    paddingRight: 10,
    textAlign: 'left',
  },
});

const MyDrawerNavigator = createAppContainer(RootStackNavigator);
export default MyDrawerNavigator

// const NavigationStack = createStackNavigator({
//   SignInScreen: { 
//     screen: SignInScreen,
//     navigationOptions: {
//       header: null,
//       headerStyle: {
//         backgroundColor: '#000'
//       },
//     },
//   },
  
// });


// const FirstActivityStackNavigator = createStackNavigator({
//   HomeScreen: {
//     screen: HomeScreen,
//     navigationOptions: ({ navigation }) => ({
//       title: 'Home',
//       headerLeft: <HamburgerIcon navigationProps={navigation} />,

//       headerStyle: {
//         backgroundColor: '#000'
//       },
//       headerTintColor: '#fff',
//     })
//   },

// });


// const AppNavigation = createAppContainer(RootStackNavigator);
// export default AppNavigation;
