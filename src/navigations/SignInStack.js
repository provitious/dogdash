import { createStackNavigator, StackNavigator } from 'react-navigation-stack';
import SignInScreen from '../screens/SignInScreen';

export default createStackNavigator(
  {
    SignInScreen: { screen: SignInScreen },
  },
  {
    headerMode: 'none',
  }
);
