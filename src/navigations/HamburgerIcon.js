import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View, TouchableOpacity, Image
} from 'react-native';
// import Icon from 'react-native-vector-icons/FontAwesome';
import { withNavigation, DrawerActions } from 'react-navigation'

import 'react-native-gesture-handler';

class HamburgerIcon extends Component {

  toggleDrawer = () => {
    this.props.navigation.dispatch(DrawerActions.toggleDrawer())
  };

  render() {

    return (

      <View style={{ flexDirection: 'row' }}>
        <TouchableOpacity onPress={this.toggleDrawer.bind(this)}>
          {/*Donute Button Image */}
          <Image
            source={require('../../images/menu.png')}
            style={{ width: 20, height: 20, marginLeft: 20 }}
          />
        </TouchableOpacity>
      </View>

    );

  }
}

export default HamburgerIcon;

