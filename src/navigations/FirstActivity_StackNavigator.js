import React, { Component } from 'react';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { StackActions, NavigationActions } from 'react-navigation';

import HamburgerIcon from '../SideMenu/HamburgerIcon';
import Icon from 'react-native-vector-icons/FontAwesome';

import Home from '../Home/Home';
import BookRequest from '../Home/BookRequest';
import LocationPicker1 from '../Home/LocationPicker';
import AddMedia from '../Home/AddMedia';
import RequestConfirmation from '../Home/RequestConfirmation';

import PrivacyPolicy from '../PrivacyPolicyViews/PrivacyPolicy';
import Support from '../SupportViews/Support';
import AboutUs from '../AboutUs/AboutUs';
import TermsConditions from '../TermsConditions/TermsConditions';
import Settings from '../SettingsViews/Settings';
import ChangePassword from '../SettingsViews/ChangePassword';
import EditProfile from '../SettingsViews/EditProfile';
import RateCard from '../RateCardView/RateCard';
import Review from '../ReviewView/Review';
import CardList from '../ManagePaymentsViews/CardList';
import AddEditCard from '../ManagePaymentsViews/AddEditCard';
import Wallet from '../WalletViews/Wallet';
import AddMoney from '../WalletViews/AddMoney';
import CardSelection from '../WalletViews/CardSelection';
import Request from '../RequestViews/Request';
import RequestDetails from '../RequestViews/RequestDetails';
import SearchRequest from '../RequestViews/SearchRequest';
import PaymentReceipt from '../RequestViews/PaymentReceipt';
import MediaDetails from '../RequestViews/MediaDetails';
import ReviewDriver from '../RequestViews/ReviewDriver';

const FirstActivityStackNavigator = createStackNavigator({
  First: {
    screen: Home,
    navigationOptions: ({ navigation }) => ({
      title: 'Home',
      headerLeft: <HamburgerIcon navigationProps={navigation} />,

      headerStyle: {
        backgroundColor: '#000'
      },
      headerTintColor: '#fff',
    })
  },
  BookRequestScreen: {
    screen: BookRequest,
    navigationOptions: ({ navigation }) => ({
      title: 'Book Request',
      headerLeft: <Icon style={{ marginLeft: 20 }} name="arrow-left" size={25} color="#fff" onPress={() => navigation.pop()}/>,

      headerStyle: {
        backgroundColor: '#000'
      },
      headerTintColor: '#fff',
    })
  },
  LocationPickerScreen: {
    screen: LocationPicker1,
    navigationOptions: {
      header: null,
      headerStyle: {
        backgroundColor: '#000'
      },
    },
  },
  AddMediaScreen: {
    screen: AddMedia,
    navigationOptions: ({ navigation }) => ({
      title: 'Add Media',
      headerLeft: <Icon style={{ marginLeft: 20 }} name="arrow-left" size={25} color="#fff" onPress={() => navigation.pop()}/>,

      headerStyle: {
        backgroundColor: '#000'
      },
      headerTintColor: '#fff',
    })
  },
  RequestConfirmationScreen: {
    screen: RequestConfirmation,
    navigationOptions: ({ navigation }) => ({
      title: 'Confirm Request',
      headerLeft: <Icon style={{ marginLeft: 20 }} name="arrow-left" size={25} color="#fff" onPress={() => navigation.pop()}/>,

      headerStyle: {
        backgroundColor: '#000'
      },
      headerTintColor: '#fff',
    })
  },
  RequestScreen: {
      screen: Request,
      key: 'RequestHome',
      navigationOptions: ({ navigation }) => ({
        title: 'Requests',
        headerLeft: <HamburgerIcon navigationProps={navigation} />,

        headerStyle: {
          backgroundColor: '#000'
        },
        headerTintColor: '#fff',
      })
  },
  SearchRequestScreen: {
      screen: SearchRequest,
      navigationOptions: ({ navigation }) => ({
        title: 'Searching For Drivers',
        headerLeft: <Icon style={{ marginLeft: 20 }} name="arrow-left" size={25} color="#fff" onPress={() => navigation.pop()}/>,

        headerStyle: {
          backgroundColor: '#000'
        },
        headerTintColor: '#fff',
      })
  },
  RequestDetailsScreen: {
      screen: RequestDetails,
      navigationOptions: ({ navigation }) => ({
        title: 'Trip Details',
        headerLeft: <Icon style={{ marginLeft: 20 }} name="arrow-left" size={25} color="#fff" onPress={() => navigation.pop()}/>,

        headerStyle: {
          backgroundColor: '#000'
        },
        headerTintColor: '#fff',
      })
  },
  NotificationRequestDetailsScreen: {
      screen: RequestDetails,
      navigationOptions: ({ navigation }) => ({
        title: 'Trip Details',
        headerLeft: <Icon style={{ marginLeft: 20 }} name="arrow-left" size={25} color="#fff" onPress={() => { 
            const navigateAction = StackActions.reset({
              index: 0,
              actions: [NavigationActions.navigate({ routeName: "RequestScreen"})],
            });

            navigation.dispatch(navigateAction);
        }}/>,

        headerStyle: {
          backgroundColor: '#000'
        },
        headerTintColor: '#fff',
      })
  },
  ReviewDriverScreen: {
      screen: ReviewDriver,
      navigationOptions: ({ navigation }) => ({
        title: 'Review Driver',
        headerLeft: <Icon style={{ marginLeft: 20 }} name="arrow-left" size={25} color="#fff" onPress={() => navigation.pop()}/>,

        headerStyle: {
          backgroundColor: '#000'
        },
        headerTintColor: '#fff',
      })
  },
  PaymentReceiptScreen: {
      screen: PaymentReceipt,
      navigationOptions: ({ navigation }) => ({
        title: 'Payment Details',
        headerLeft: <Icon style={{ marginLeft: 20 }} name="arrow-left" size={25} color="#fff" onPress={() => navigation.pop()}/>,

        headerStyle: {
          backgroundColor: '#000'
        },
        headerTintColor: '#fff',
      })
  },
  MediaDetailsScreen: {
      screen: MediaDetails,
      navigationOptions: ({ navigation }) => ({
        title: 'Media Details',
        headerLeft: <Icon style={{ marginLeft: 20 }} name="arrow-left" size={25} color="#fff" onPress={() => navigation.pop()}/>,

        headerStyle: {
          backgroundColor: '#000'
        },
        headerTintColor: '#fff',
      })
  },
  ReviewScreen: {
      screen: Review,
      navigationOptions: ({ navigation }) => ({
        title: 'Review',
        headerLeft: <HamburgerIcon navigationProps={navigation} />,

        headerStyle: {
          backgroundColor: '#000'
        },
        headerTintColor: '#fff',
      })
  },
  WalletScreen: {
      screen: Wallet,
      key: 'WalletHome',
      navigationOptions: ({ navigation }) => ({
        title: 'Wallet',
        headerLeft: <HamburgerIcon navigationProps={navigation} />,

        headerStyle: {
          backgroundColor: '#000'
        },
        headerTintColor: '#fff',
      })
  },
  AddMoneyScreen: {
      screen: AddMoney,
      navigationOptions: ({ navigation }) => ({
        title: 'Add Money',
        headerLeft: <Icon style={{ marginLeft: 20 }} name="arrow-left" size={25} color="#fff" onPress={() => navigation.pop()}/>,

        headerStyle: {
          backgroundColor: '#000'
        },
        headerTintColor: '#fff',
      })
  },
  CardSelectionScreen: {
      screen: CardSelection,
      navigationOptions: ({ navigation }) => ({
        title: 'Select Card',
        headerLeft: <Icon style={{ marginLeft: 20 }} name="arrow-left" size={25} color="#fff" onPress={() => navigation.pop()}/>,

        headerStyle: {
          backgroundColor: '#000'
        },
        headerTintColor: '#fff',
      })
  },
  CardListScreen: {
      screen: CardList,
      navigationOptions: ({ navigation }) => ({
        title: 'Card List',
        headerLeft: <HamburgerIcon navigationProps={navigation} />,

        headerStyle: {
          backgroundColor: '#000'
        },
        headerTintColor: '#fff',
      })
  },
  AddEditCardScreen: {
      screen: AddEditCard,
      navigationOptions: ({ navigation }) => ({
        title: 'Add Card',
        headerLeft: <Icon style={{ marginLeft: 20 }} name="arrow-left" size={25} color="#fff" onPress={() => navigation.pop()}/>,

        headerStyle: {
          backgroundColor: '#000'
        },
        headerTintColor: '#fff',
      })
  },
  RateCardScreen: {
      screen: RateCard,
      navigationOptions: ({ navigation }) => ({
        title: 'Rate Card',
        headerLeft: <HamburgerIcon navigationProps={navigation} />,

        headerStyle: {
          backgroundColor: '#000'
        },
        headerTintColor: '#fff',
      })
  },
  SettingsScreen: {
      screen: Settings,
      navigationOptions: ({ navigation }) => ({
        title: 'Account Settings',
        headerLeft: <HamburgerIcon navigationProps={navigation} />,

        headerStyle: {
          backgroundColor: '#000'
        },
        headerTintColor: '#fff',
      })
  },
  ChangePasswordScreen: {
      screen: ChangePassword,
      navigationOptions: ({ navigation }) => ({
        title: 'Change Password',
        headerLeft: <Icon style={{ marginLeft: 20 }} name="arrow-left" size={25} color="#fff" onPress={() => navigation.pop()}/>,

        headerStyle: {
          backgroundColor: '#000'
        },
        headerTintColor: '#fff',
      })
  },
  EditProfileScreen: {
      screen: EditProfile,
      navigationOptions: ({ navigation }) => ({
        title: 'Edit Profile',
        headerLeft: <Icon style={{ marginLeft: 20 }} name="arrow-left" size={25} color="#fff" onPress={() => navigation.pop()}/>,

        headerStyle: {
          backgroundColor: '#000'
        },
        headerTintColor: '#fff',
      })
  },
  PrivacyPolicyScreen: {
    screen: PrivacyPolicy,
    navigationOptions: ({ navigation }) => ({
      title: 'Privacy Policy',
      headerLeft: <HamburgerIcon navigationProps={navigation} />,

      headerStyle: {
        backgroundColor: '#000'
      },
      headerTintColor: '#fff',
    })
  },
  SupportScreen: {
    screen: Support,
    navigationOptions: ({ navigation }) => ({
      title: 'Support',
      headerLeft: <HamburgerIcon navigationProps={navigation} />,

      headerStyle: {
        backgroundColor: '#000'
      },
      headerTintColor: '#fff',
    })
  },
  AboutUsScreen: {
    screen: AboutUs,
    navigationOptions: ({ navigation }) => ({
      title: 'About Us',
      headerLeft: <HamburgerIcon navigationProps={navigation} />,

      headerStyle: {
        backgroundColor: '#000'
      },
      headerTintColor: '#fff',
    })
  },
  TermsConditionsScreen: {
    screen: TermsConditions,
    navigationOptions: ({ navigation }) => ({
      title: 'Terms & Conditions',
      headerLeft: <HamburgerIcon navigationProps={navigation} />,

      headerStyle: {
        backgroundColor: '#000'
      },
      headerTintColor: '#fff',
    })
  },
});
const FirstActivity_StackNavigator = createAppContainer(FirstActivityStackNavigator);
export default FirstActivity_StackNavigator;

