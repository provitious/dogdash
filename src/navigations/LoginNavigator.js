import React, { Component } from 'react';
import { StyleSheet, Alert } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import Icon from 'react-native-vector-icons/FontAwesome';

import Constant from '../CommonClasses/Constant';
import Login from '../Login/Login';
import Home from '../Dashboard/Home';
import TodaysTask from '../TodayTask/TodaysTask';
import TodaysTaskDetails from '../TodayTask/TodaysTaskDetails';
import ChangeTaskStatus from '../TodayTask/ChangeTaskStatus';
import DirectionMap from '../TodayTask/DirectionMap';

const NavigationStack = createStackNavigator({
  
  TodaysTask: { 
    screen: TodaysTask,
    navigationOptions: ({ navigation }) => ({
      title: 'Today\'s Task',
      headerLeft: null,
      headerStyle: {
        backgroundColor: '#d66c60'
      },
      headerTintColor: '#fff',
    }) 
  },
  TodaysTaskDetails: { 
    screen: TodaysTaskDetails,
    navigationOptions: ({ navigation }) => ({
      title: 'Today\'s Task Details',
      headerLeft: <Icon style={{ marginLeft: 10 }} name="arrow-left" size={25} color="#fff" onPress={() => navigation.pop()}/>,
      headerStyle: {
        backgroundColor: '#d66c60'
      },
      headerTintColor: '#fff',
    }) 
  },
  ChangeTaskStatus: { 
    screen: ChangeTaskStatus,
    navigationOptions: ({ navigation }) => ({
      title: 'Change Task Status',
      headerLeft: <Icon style={{ marginLeft: 10 }} name="arrow-left" size={25} color="#fff" onPress={() => navigation.pop()}/>,
      headerStyle: {
        backgroundColor: '#d66c60'
      },
      headerTintColor: '#fff',
    }) 
  },
  DirectionMap: { 
    screen: DirectionMap,
    navigationOptions: ({ navigation }) => ({
      title: 'Map',
      headerLeft: <Icon style={{ marginLeft: 10 }} name="arrow-left" size={25} color="#fff" onPress={() => navigation.pop()}/>,
      headerStyle: {
        backgroundColor: '#d66c60'
      },
      headerTintColor: '#fff',
    }) 
  },
  Home: { 
    screen: Home,
    navigationOptions: ({ navigation }) => ({
      title: 'Home',
      headerLeft: null,
      headerStyle: {
        backgroundColor: '#d66c60'
      },
      headerTintColor: '#fff',
    }) 
  },
});

// const NavigationHomeStack = createStackNavigator({
//   Home: { screen: Home },
// });

// CommonUtilsObj.getLoginStatus();
// setInterval( () => {
//         console.log('app navigation ' + CommonUtilsObj.loginStatus);

//       },1000)

// console.log('app navigation ' + CommonUtilsObj.loginStatus);

const HomeContainer = createAppContainer(NavigationStack);
// const HomeContainer = createAppContainer(NavigationHomeStack);
// CommonUtilsObj.loginStatus === "Login" ? createAppContainer(NavigationHomeStack) createAppContainer(NavigationStack) //
export default HomeContainer;