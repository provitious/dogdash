/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component, Fragment, useEffect } from 'react';
import firebase from 'react-native-firebase';

import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Share, 
  PermissionsAndroid,
  Platform,
} from 'react-native';

import CommonUtilsObj from './components/CommonClasses/CommonUtils';
import Constant from './components/CommonClasses/Constant';
import Container from './components/Navigations/AppNavigator';

import Custom_Side_Menu from './components/SideMenu/Custom_Side_Menu';
import HamburgerIcon from './components/SideMenu/HamburgerIcon';
import FirstActivity_StackNavigator from './components/Navigations/FirstActivity_StackNavigator';
import MyDrawerNavigator from './components/SideMenu/MyDrawer_Navigator';
import { StackActions, NavigationActions } from 'react-navigation';
import { NavigationBar } from 'navigationbar-react-native';

// import Login from './components/Login/Login'
import SocketManagerObj from './components/CommonClasses/SocketManager';
import Geolocation from 'react-native-geolocation-service'; //'@react-native-community/geolocation';
import SplashScreen from 'react-native-splash-screen';
import DefaultPreference from 'react-native-default-preference';
import userDefaults from 'react-native-user-defaults'

export default class App extends Component { 

  constructor(props) {
      super(props);
      this.state = {
        loaded: false,
        loginStatus: CommonUtilsObj.userLoginStatus
      }

      this.checkPermission();
      this.messageListener();

  }

  componentDidMount() {

    SocketManagerObj.initializeSocket();
    SocketManagerObj.establishConnection();

    this.getLocationUser();
    this.getRememberMeData();
    this.fetchProfileData();

    setInterval( () => { 
      console.log('componentDidMount');
     SocketManagerObj.sendCoordinates();
     // SocketManagerObj.updateDriverLocationToUser();
    },1000)

    setInterval( () => {
      this.setState({
        loaded : true,
        loginStatus: CommonUtilsObj.userLoginStatus
      })
    },5000)

    setTimeout(() => {
      SplashScreen.hide();
    }, 6000);

    // setTimeout(() => {
    //   CommonUtilsObj.navigationProps.navigation.navigate('MyModal');
    // }, 12000);

  }

   getLocationUser = async () => {
    if(Platform.OS === 'ios'){
      this.getCurrentLocation();
    }
    else {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
          {
            'title': 'Example App',
            'message': 'Example App access to your location '
          }
        )
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          console.log("You can use the location")
          // alert("You can use the location");

          Geolocation.getCurrentPosition(
            (position) => {
                console.log(position);
            },
            (error) => {
                // See error code charts below.
                console.log(error.code, error.message);
            },
            { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
        );

        } 
      } catch (err) {
        console.warn(err)
      }
    }
  };

  getCurrentLocation(){
    Geolocation.requestAuthorization();
    Geolocation.getCurrentPosition(
       (position) => {
           console.log(position);
       },
       (error) => {
         console.log("map error: ",error);
           console.log(error.code, error.message);
       },
       { enableHighAccuracy: false, timeout: 15000, maximumAge: 10000 }
   );
  }

  getRememberMeData() {
    if (Platform.OS === 'android')
    {
        console.log('app js............')
        DefaultPreference.get(Constant.KRememberMeKey)
        .then(function(data) {
          console.log('getRememberMeData : ' + data)
          CommonUtilsObj.rememberMeList = JSON.parse(data);
          CommonUtilsObj.rememberMeList.map(item => { console.log('item -> ' + item.email) })
        })
        .catch(function (err) {
          console.log(err);
        });
    }
    else {
       userDefaults.get(Constant.KRememberMeKey)
      .then(data => {
         console.log('getRememberMeData : ' + data)
          CommonUtilsObj.rememberMeList = JSON.parse(data);
          CommonUtilsObj.rememberMeList.map(item => { console.log('item -> ' + item.email) })
      })
      .catch(function (err) {
        return err;
      });
      
    }
    
  }

  fetchProfileData() {
      CommonUtilsObj.getLoginStatus();
      CommonUtilsObj.getLoggedUserDetails();
  }

  checkPermission = async () => {

    const enabled = await firebase.messaging().hasPermission();

    if (enabled) {
      
        this.getFcmToken();
    } else {
        this.requestPermission();
    }
  }

  getFcmToken = async () => {
    
    const fcmToken = await firebase.messaging().getToken();
    if (fcmToken) {
      console.log('getfcmToken : ' + fcmToken);
      CommonUtilsObj.deviceToken = fcmToken;
      // alert('Your Firebase Token is:'+ fcmToken);
    } else {
      // alert('Failed', 'No token received');
    }
  }

  requestPermission = async () => {
    try {
      await firebase.messaging().requestPermission();
      // User has authorised
      this.getFcmToken();

    } catch (error) {
        // User has rejected permissions
    }
  }

  messageListener = async () => { // called on tap
    this.notificationListener = firebase.notifications().onNotification((notification) => {
        const { title, data } = notification;
        console.log("notificationListener" + JSON.stringify(data));

        //new_request->  new request pop up
        //schedule_request ->  new request pop up
        //later_start_notification -> nothing
        //reject_request_user -> history details

        // CommonUtilsObj.navigationProps.navigation.navigate('NewRequestScreen');
        // console.log("notificationListener" + data.notification_type);
        CommonUtilsObj.notificationRequestDetails = data;

        if (data.notification_type == 'new_request') {
            CommonUtilsObj.requestStatus = Constant.KRequestAccepted;

            let requestDetails = {};
            requestDetails.id = data.booking_id;
            requestDetails.drop_area = data.dropadd;
            requestDetails.pickup_area = data.pickupadd;

            const navigateAction = StackActions.reset({
              index: 0,
              actions: [NavigationActions.navigate({ routeName: "NotificationRequestDetailsScreen" , params: { fromNotification: true, requestDetails: requestDetails }})],
            });

            CommonUtilsObj.navigationProps.navigation.navigate('MyModal', {requestDetails: requestDetails});
            // CommonUtilsObj.navigationProps.navigation.dispatch(navigateAction);
        }
        else if (data.notification_type == 'schedule_request') {
            CommonUtilsObj.requestStatus = Constant.KRequestAccepted;

            let requestDetails = {};
            requestDetails.id = data.booking_id;
            const navigateAction = StackActions.reset({
              index: 0,
              actions: [NavigationActions.navigate({ routeName: "NotificationRequestDetailsScreen" , params: { fromNotification: true, requestDetails: requestDetails }})],
            });

            CommonUtilsObj.navigationProps.navigation.dispatch(navigateAction);
        }
        else if (data.notification_type == 'reject_request_user') {
            CommonUtilsObj.requestStatus = Constant.KRequestAccepted;

            if (data.is_trip_running == "true") {
              // let requestDetails = {};
              // requestDetails.id = responseData.booking_id;
              let requestDetails = {};
              requestDetails.id = data.running_booking_id;
              const navigateAction = StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: "NotificationRequestDetailsScreen" , params: { fromNotification: true, requestDetails: requestDetails }})],
              });

              CommonUtilsObj.navigationProps.navigation.dispatch(navigateAction);
            }
            else {
              let requestDetails = {};
              requestDetails.id = data.booking_id;
              const navigateAction = StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: "First" , params: { fromNotification: true, requestDetails: requestDetails }})],
              });

              CommonUtilsObj.navigationProps.navigation.dispatch(navigateAction);
            }
            
        }
        // else if (data.notification_type == 'arrive_request') {
        //     CommonUtilsObj.requestStatus = Constant.KRequestArrived;
        //     let requestDetails = {};
        //     requestDetails.id = data.booking_id;
        //     const navigateAction = StackActions.reset({
        //       index: 0,
        //       actions: [NavigationActions.navigate({ routeName: "NotificationRequestDetailsScreen" , params: { fromNotification: true, requestDetails: requestDetails }})],
        //     });

        //     CommonUtilsObj.navigationProps.navigation.dispatch(navigateAction);
        // }
        // else if (data.notification_type == 'ontrip_request') {
        //     CommonUtilsObj.requestStatus = Constant.KRequestOnWay;
        //     let requestDetails = {};
        //     requestDetails.id = data.booking_id;
        //     const navigateAction = StackActions.reset({
        //       index: 0,
        //       actions: [NavigationActions.navigate({ routeName: "NotificationRequestDetailsScreen" , params: { fromNotification: true, requestDetails: requestDetails }})],
        //     });

        //     CommonUtilsObj.navigationProps.navigation.dispatch(navigateAction);
        // }
        // else if (data.notification_type == 'end_request') {
        //     CommonUtilsObj.requestStatus = Constant.KRequestEnd;
        //     let requestDetails = {};
        //     requestDetails.id = data.booking_id;
        //     const navigateAction = StackActions.reset({
        //       index: 0,
        //       actions: [NavigationActions.navigate({ routeName: "NotificationRequestDetailsScreen" , params: { fromNotification: true, requestDetails: requestDetails }})],
        //     });

        //     CommonUtilsObj.navigationProps.navigation.dispatch(navigateAction);
        // }
        // else if (data.notification_type == 'reject_request_driver') {
        //     CommonUtilsObj.requestStatus = Constant.KRequestRejected;
        //     let requestDetails = {};
        //     requestDetails.id = data.booking_id;
        //     const navigateAction = StackActions.reset({
        //       index: 0,
        //       actions: [NavigationActions.navigate({ routeName: "NotificationRequestDetailsScreen" , params: { fromNotification: true, requestDetails: requestDetails }})],
        //     });

        //     CommonUtilsObj.navigationProps.navigation.dispatch(navigateAction);
        // } 

    });

    this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => { // called on tap
        const { title, data } = notificationOpen.notification;
        console.log("notificationOpenedListener" + JSON.stringify(data));
        CommonUtilsObj.notificationRequestDetails = data;

        if (data.notification_type == 'new_request') {
            CommonUtilsObj.requestStatus = Constant.KRequestAccepted;

            let requestDetails = {};
            requestDetails.id = data.booking_id;
            requestDetails.id = data.booking_id;
            requestDetails.drop_area = data.dropadd;
            requestDetails.pickup_area = data.pickupadd;

            const navigateAction = StackActions.reset({
              index: 0,
              actions: [NavigationActions.navigate({ routeName: "NotificationRequestDetailsScreen" , params: { fromNotification: true, requestDetails: requestDetails }})],
            });

            // CommonUtilsObj.navigationProps.navigation.dispatch(navigateAction);
            CommonUtilsObj.navigationProps.navigation.navigate('MyModal', {requestDetails: requestDetails});
        }
        else if (data.notification_type == 'schedule_request') {
            CommonUtilsObj.requestStatus = Constant.KRequestAccepted;

            let requestDetails = {};
            requestDetails.id = data.booking_id;
            const navigateAction = StackActions.reset({
              index: 0,
              actions: [NavigationActions.navigate({ routeName: "NotificationRequestDetailsScreen" , params: { fromNotification: true, requestDetails: requestDetails }})],
            });

            CommonUtilsObj.navigationProps.navigation.dispatch(navigateAction);
        }
        else if (data.notification_type == 'reject_request_user') {
            CommonUtilsObj.requestStatus = Constant.KRequestAccepted;

            if (data.is_trip_running == "true") {
              // let requestDetails = {};
              // requestDetails.id = responseData.booking_id;
              let requestDetails = {};
              requestDetails.id = data.running_booking_id;
              const navigateAction = StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: "NotificationRequestDetailsScreen" , params: { fromNotification: true, requestDetails: requestDetails }})],
              });

              CommonUtilsObj.navigationProps.navigation.dispatch(navigateAction);
            }
            else {
              let requestDetails = {};
              requestDetails.id = data.booking_id;
              const navigateAction = StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: "First" , params: { fromNotification: true, requestDetails: requestDetails }})],
              });

              CommonUtilsObj.navigationProps.navigation.dispatch(navigateAction);
            }
        }
        // CommonUtilsObj.navigationProps.navigation.navigate('MyModal', {requestDetails: data});
    });

    const notificationOpen = await firebase.notifications().getInitialNotification();
    if (notificationOpen) {
        const { title } = notificationOpen.notification;
        console.log('notificationOpen');
    }

    this.messageListener = firebase.messaging().onMessage((message) => {
      // console.log(JSON.stringify(message));
      console.log('messageListener');
    });
  }

  renderLoadingView() {
      return (
        <View style = {styles.textcontainer}>
          <Text style = {{color: 'red', justifyContent: 'center'}}>Loading data......</Text>
        </View>
      );
   }

  render() {
    
      if (CommonUtilsObj.userLoginStatus == Constant.KLogin) {
        return (
         <MyDrawerNavigator/>
        );
      }
      else {
        return (
         <Container/>
        );
      }
    
  }

  render111() {
        return (
         <Container/>
        );
  }
}


const styles = StyleSheet.create({
     container: {
      flex: 1,
      backgroundColor: "white"
     },
     textcontainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
      },
});
